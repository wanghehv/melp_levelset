//////////////////////////////////////////////////////////////////////////
// IO functions
// Copyright (c) (2022-), Hui Wang
// This file is part of MESO, whose distribution is governed by the LICENSE file.
//////////////////////////////////////////////////////////////////////////
#pragma once

#include "EulerianParticles.h"
#include "LagrangianParticles.h"

//#include <vtkXMLStructuredGridWriter.h>
#include <vtkXMLPolyDataWriter.h>
//#include <vtkStructuredGrid.h>
#include <vtkPolyData.h>
#include <vtkDataSet.h>
#include <vtkPointData.h>
#include <vtkCellData.h>
#include <vtkCellArray.h>
#include <vtkDoubleArray.h>
#include <vtkIntArray.h>
#include <vtkVertexGlyphFilter.h>


namespace Meso {
    template<int d>
    class ParticlesIOUtil{
        Typedef_VectorD(d); Typedef_MatrixD(d);
    public:
        static void Write_Full_Lagrangian_Particles(const LagrangianParticles<d>& particles, std::string file_name){
            
            const Array<VectorD>& pos = particles.xRef();
            const Array<VectorD>& vel = particles.vRef();
            const Array<VectorD>& normal = particles.nRef();
            // const Array<real>& nden = particles.ndenRef();
            // const Array<real>& rho = particles.rhoRef();
            // const Array<VectorD>& pos_b = b_particles.xRef();

            //// setup VTK
            vtkNew<vtkXMLPolyDataWriter> writer;
            vtkNew<vtkPolyData> polyData;

            vtkNew<vtkPoints> nodes;
            nodes->Allocate(pos.size());
            vtkNew<vtkDoubleArray> posArray;
            posArray->SetName("Position");
            posArray->SetNumberOfComponents(3);
            vtkNew<vtkDoubleArray> velArray;
            velArray->SetName("Velocity");
            velArray->SetNumberOfComponents(3);
            vtkNew<vtkDoubleArray> normalArray;
            normalArray->SetName("Normal");
            normalArray->SetNumberOfComponents(3);

            for (int i = 0; i < pos.size(); i++) {
                Vector3 pos3 = MathFunc::V<3>(pos[i]);
                nodes->InsertNextPoint(pos3[0], pos3[1], pos3[2]);

                Vector3 vel3 = MathFunc::V<3>(vel[i]);
                velArray->InsertNextTuple3(vel3[0], vel3[1], vel3[2]);

                Vector3 normal3 = MathFunc::V<3>(normal[i]);
                normalArray->InsertNextTuple3(normal3[0], normal3[1], normal3[2]);

            }

            polyData->SetPoints(nodes);
            polyData->SetVerts(polyData->GetVerts());
            vtkNew<vtkVertexGlyphFilter> vertexGlyphFilter;

            polyData->GetPointData()->AddArray(velArray);
            polyData->GetPointData()->SetActiveVectors("velocity");

            polyData->GetPointData()->AddArray(normalArray);
            polyData->GetPointData()->SetActiveVectors("normal");

            writer->SetFileName(file_name.c_str());
            writer->SetInputData(polyData);
            writer->Write();
        }

        static void Write_Full_Eulerian_Particles(const EulerianParticles<d>& particles, std::string file_name){
            
            const Array<VectorD>& pos = particles.xRef();
            const Array<VectorD>& vel = particles.vRef();
            const Array<VectorD>& normal = particles.nRef();
            const Array<VectorD>& veltang = particles.vtRef();
            const Array<real>& vol = particles.VRef();
            const Array<real>& area = particles.aRef();
            const Array<real>& eta = particles.etaRef();
            const Array<real>& c = particles.cRef();
            // const Array<real>& rho = particles.rhoRef();
            // const Array<VectorD>& pos_b = b_particles.xRef();

            //// setup VTK
            vtkNew<vtkXMLPolyDataWriter> writer;
            vtkNew<vtkPolyData> polyData;

            vtkNew<vtkPoints> nodes;
            nodes->Allocate(pos.size());
            vtkNew<vtkDoubleArray> posArray;
            posArray->SetName("Position");
            posArray->SetNumberOfComponents(3);
            vtkNew<vtkDoubleArray> velArray;
            velArray->SetName("Velocity");
            velArray->SetNumberOfComponents(3);
            vtkNew<vtkDoubleArray> normalArray;
            normalArray->SetName("Normal");
            normalArray->SetNumberOfComponents(3);
            vtkNew<vtkDoubleArray> veltangArray;
            veltangArray->SetName("TangentVelocity");
            veltangArray->SetNumberOfComponents(3);
            vtkNew<vtkDoubleArray> volArray;
            volArray->SetName("Volume");
            volArray->SetNumberOfComponents(1);
            vtkNew<vtkDoubleArray> areaArray;
            areaArray->SetName("Area");
            areaArray->SetNumberOfComponents(1);
            vtkNew<vtkDoubleArray> etaArray;
            etaArray->SetName("Eta");
            etaArray->SetNumberOfComponents(1);
            vtkNew<vtkDoubleArray> cArray;
            cArray->SetName("Concentration");
            cArray->SetNumberOfComponents(1);

            for (int i = 0; i < pos.size(); i++) {
                Vector3 pos3 = MathFunc::V<3>(pos[i]);
                nodes->InsertNextPoint(pos3[0], pos3[1], pos3[2]);

                Vector3 vel3 = MathFunc::V<3>(vel[i]);
                velArray->InsertNextTuple3(vel3[0], vel3[1], vel3[2]);

                Vector3 normal3 = MathFunc::V<3>(normal[i]);
                normalArray->InsertNextTuple3(normal3[0], normal3[1], normal3[2]);
                
                Vector3 veltang3 = MathFunc::V<3>(veltang[i]);
                veltangArray->InsertNextTuple3(veltang3[0], veltang3[1], veltang3[2]);

                volArray->InsertNextTuple1(vol[i]);
                areaArray->InsertNextTuple1(area[i]);
                etaArray->InsertNextTuple1(eta[i]);
                cArray->InsertNextTuple1(c[i]);
            }

            polyData->SetPoints(nodes);
            polyData->SetVerts(polyData->GetVerts());
            vtkNew<vtkVertexGlyphFilter> vertexGlyphFilter;

            polyData->GetPointData()->AddArray(velArray);
            polyData->GetPointData()->SetActiveVectors("velocity");
            
            polyData->GetPointData()->AddArray(normalArray);
            polyData->GetPointData()->SetActiveVectors("normal");

            polyData->GetPointData()->AddArray(veltangArray);
            polyData->GetPointData()->SetActiveVectors("tangentvelocity");

            polyData->GetPointData()->AddArray(volArray);
            polyData->GetPointData()->SetActiveScalars("vol");
            
            polyData->GetPointData()->AddArray(areaArray);
            polyData->GetPointData()->SetActiveScalars("area");
            
            polyData->GetPointData()->AddArray(etaArray);
            polyData->GetPointData()->SetActiveScalars("eta");

            polyData->GetPointData()->AddArray(cArray);
            polyData->GetPointData()->SetActiveScalars("concentration");

            writer->SetFileName(file_name.c_str());
            writer->SetInputData(polyData);
            writer->Write();
        }

        static void Write_Binary_Eulerian_Particles(const EulerianParticles<d>& particles, std::string file_name){
            const Array<VectorD>& pos = particles.xRef();
            // const Array<VectorD>& vel = particles.vRef();
            const Array<VectorD>& normal = particles.nRef();
            // const Array<VectorD>& veltang = particles.vtRef();
            // const Array<real>& vol = particles.VRef();
            // const Array<real>& area = particles.aRef();
            const Array<real>& eta = particles.etaRef();
            // const Array<real>& c = particles.cRef();

            std::ofstream ofs;
            ofs.open(file_name, std::ios::out |std::ios::binary);
            char v = 'v';
            char empty = ' ';

            int size = particles.Size();
            ofs.write((char*)(&size), sizeof(int)); //// write int
            
            for(int i=0;i<size;i++){

            }

            for(VectorD const& x : pos) //// write all position
            {
                float x0 = x(0); ofs.write((char*)(&x0), sizeof(float));
                float x1 = x(1); ofs.write((char*)(&x1), sizeof(float));
                if constexpr(d==3){
                    float x2 = x(2); ofs.write((char*)(&x2), sizeof(float));}
            }

            for(VectorD const& x : normal) //// write all normal
            {
                float x0 = x(0); ofs.write((char*)(&x0), sizeof(float));
                float x1 = x(1); ofs.write((char*)(&x1), sizeof(float));
                if constexpr(d==3){
                    float x2 = x(2); ofs.write((char*)(&x2), sizeof(float));}
            }

            for(real const& x : eta) //// write all thickness
            {
                float xx = x;
                ofs.write((char*)(&xx), sizeof(float));
            }
            ofs.close();
        }
    };
}