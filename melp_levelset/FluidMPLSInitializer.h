//////////////////////////////////////////////////////////////////////////
// Initializer of FluidMPLS
// Copyright (c) (2022-), Hui Wang
// This file is part of MESO, whose distribution is governed by the LICENSE file.
//////////////////////////////////////////////////////////////////////////
#pragma once
#include "FluidMPLS.h"
#include "ImplicitManifold.h"
#include "Json.h"
#include "GridEulerFunc.h"

namespace Meso {
    template<int d>
    class FluidMPLSInitializer {
    public:
        Typedef_VectorD(d);
        Typedef_MatrixD(d);

        void Apply(json& j, FluidMPLS<d>& fluid) {

            //// init each regions
            std::string test = Json::Value(j, "test", std::string("cube_oscillation"));
            if (test == "cube_oscillation") Init_Cube_Oscillation(j, fluid);
            else if (test == "two_squares") Init_Two_Squares(j, fluid);
            else if (test == "three_squares") Init_Three_Squares(j, fluid);
            else if (test == "four_bubbles") Init_Four_Bubbles(j, fluid);
            else if (test == "single_bubble_on_tank") Init_Single_Bubble_On_Tank(j, fluid);
            else if (test == "drip_on_bubble") Init_Drip_On_Bubble(j, fluid);
            else if (test == "drop_on_tank") Init_Drop_On_Tank(j, fluid);
            else if (test == "bubble_stack") Init_Bubble_Stack(j, fluid);
            else if (test == "bubble_arise") Init_Bubble_Arise(j, fluid);

            else if (test == "tangential_flow") Init_Tangential_Flow(j, fluid);

            else Assert(false, "test {} not exist", test);
           
            //// init fluid
            fluid.Init(j);

            Info("FluidMPLSInitializer::initialization finished");
        }

        void Init_Cube_Oscillation(json& j, FluidMPLS<d>& fluid) {
            int scale = Json::Value<int>(j, "scale", 32);
            real length = Json::Value<real>(j, "length", 0.75); //// x
            real height = Json::Value<real>(j, "height", 0.25); //// yz
            real side_len = 1.0;
            real dx = side_len / scale;
            VectorDi grid_size = scale * MathFunc::Vi<d>(1, 1, 1);
            Grid<d> grid(grid_size, dx, VectorD::Zero(), CENTER);

            //// init levelset
            VectorD min_corner=VectorD::Ones()*(side_len*(1-height)*0.5);
            min_corner.x()=side_len*(1-length)*0.5;
            VectorD max_corner=VectorD::Ones()*(side_len*(1+height)*0.5);
            max_corner.x()=side_len*(1+length)*0.5;

            Box<d> box(min_corner, max_corner);
            LevelSet<d> levelset; levelset.Init(grid, box);

            fluid.mpls.Add_Region(levelset, Region<d>::RegionType::AIR);

            //// init particles
            //// init velocity field
            fluid.velocity.Init(grid,0);
            
            fluid.cell_type.Init(fluid.mpls.levelset.phi.grid, CellType::FLUID);
            Eigen::Matrix<int, 3, 2> bc_width;
            bc_width << 0, 0, 0, 0, 0, 0;
            GridEulerFunc::Set_Boundary_Cells(fluid.cell_type, bc_width, CellType::SOLID);
            Info("init cell type & boundary");
        }

        void Init_Two_Squares(json& j, FluidMPLS<d>& fluid) {
            
            int scale = Json::Value<int>(j, "scale", 32);
            real side_len = Json::Value<real>(j, "side_len", 1.0);
            real dx = side_len / scale;
            VectorDi grid_size = scale * MathFunc::Vi<d>(1, 1, 1);
            Grid<d> grid(grid_size, dx, VectorD::Zero(), CENTER);

            //// init levelset
            fluid.mpls.Init(grid);

            Array<LevelSet<d>> levelsets;
            
            //// init two squares
            {
                VectorD min_corner, max_corner;
                if constexpr(d==2){
                    min_corner=VectorD(0.5-0.15,0.5-0.3)*side_len;
                    max_corner=VectorD(0.5+0.15,0.5)*side_len;
                }
                if constexpr(d==3){
                    min_corner=VectorD(0.5-0.15,0.5-0.3,0.5-0.15)*side_len;
                    max_corner=VectorD(0.5+0.15,0.5,0.5+0.15)*side_len;
                }
                Box<d> box(min_corner,max_corner);
                levelsets.push_back(LevelSet<d>());
                LevelSet<d>& levelset=levelsets[levelsets.size()-1]; levelset.Init(grid,box);
                int id=fluid.mpls.Add_Region(levelset, Region<d>::RegionType::AIR);
                Info("init levelset for {}", id);
            }
            {
                VectorD min_corner, max_corner;
                if constexpr(d==2){
                    min_corner=VectorD(0.5-0.15,0.5)*side_len;
                    max_corner=VectorD(0.5+0.15,0.5+0.3)*side_len;
                }
                if constexpr(d==3){
                    min_corner=VectorD(0.5-0.15,0.5,0.5-0.15)*side_len;
                    max_corner=VectorD(0.5+0.15,0.5+0.3,0.5+0.15)*side_len;
                }
                Box<d> box(min_corner,max_corner);
                levelsets.push_back(LevelSet<d>());
                LevelSet<d>& levelset=levelsets[levelsets.size()-1]; levelset.Init(grid,box);
                int id=fluid.mpls.Add_Region(levelset, Region<d>::RegionType::AIR);
                Info("init levelset for {}", id);
            }
            //// require a external region, otherwise the Move_Particles_To_Voronoi_Levelset would report error
            {
                LevelSet<d> levelset(grid);
                levelset.phi.Exec_Nodes(
                    [&](const VectorDi cell) {
                        real phi=1e8;
                        for (int lid=0;lid<levelsets.size();lid++) {
                            phi=min(levelsets[lid].phi(cell),phi);
                        }
                        levelset.phi(cell)=-phi;
                    }
                );
                levelset.Fast_Marching(-1);
                int id=fluid.mpls.Add_Region(levelset, Region<d>::RegionType::AIR);
                fluid.mpls.region_map.find(id)->second->use_volume_control=false;
                Info("init levelset for {}", id);
            }

            fluid.velocity.Init(grid,0);
            Info("init velocity");

            fluid.cell_type.Init(fluid.mpls.levelset.phi.grid, CellType::FLUID);
            Eigen::Matrix<int, 3, 2> bc_width;
            bc_width << 0, 0, 0, 0, 0, 0;
            GridEulerFunc::Set_Boundary_Cells(fluid.cell_type, bc_width, CellType::SOLID);
            Info("init cell type & boundary");
        }

        void Init_Three_Squares(json& j, FluidMPLS<d>& fluid) {
            
            int scale = Json::Value<int>(j, "scale", 32);
            real side_len = Json::Value<real>(j, "side_len", 1.0);
            real dx = side_len / scale;
            VectorDi grid_size = scale * MathFunc::Vi<d>(1, 1, 1);
            Grid<d> grid(grid_size, dx, VectorD::Zero(), CENTER);

            //// init levelset
            fluid.mpls.Init(grid);

            Array<LevelSet<d>> levelsets;
            
            //// init two squares
            Array<VectorD> min_corners,max_corners;
            if constexpr(d==2){
                min_corners.resize(3);
                max_corners.resize(3);
                min_corners[0]=VectorD(0.5-0.3 ,0.5)*side_len;////left
                max_corners[0]=VectorD(0.5     ,0.5+0.3)*side_len;////left
                min_corners[1]=VectorD(0.5     ,0.5)*side_len;////right
                max_corners[1]=VectorD(0.5+0.3 ,0.5+0.3)*side_len;////right
                min_corners[2]=VectorD(0.5-0.15,0.5-0.3)*side_len;
                max_corners[2]=VectorD(0.5+0.15,0.5)*side_len;
            }
            if constexpr(d==3){
                min_corners.resize(4);
                max_corners.resize(4);
                min_corners[0]=VectorD(0.5-0.3 ,0.5     ,0.5)*side_len;////left
                max_corners[0]=VectorD(0.5     ,0.5+0.3 ,0.5+0.3)*side_len;////left
                min_corners[1]=VectorD(0.5     ,0.5     ,0.5)*side_len;////right
                max_corners[1]=VectorD(0.5+0.3 ,0.5+0.3 ,0.5+0.3)*side_len;////right
                min_corners[2]=VectorD(0.5-0.15,0.5-0.3 ,0.5)*side_len;
                max_corners[2]=VectorD(0.5+0.15,0.5     ,0.5+0.3)*side_len;

                min_corners[3]=VectorD(0.5-0.15,0.5-0.15 ,0.5-0.3)*side_len;
                max_corners[3]=VectorD(0.5+0.15,0.5+0.14 ,0.5)*side_len;
            }

            for(int i=0;i<min_corners.size();i++){
                Box<d> box(min_corners[i],max_corners[i]);

                levelsets.push_back(LevelSet<d>());
                LevelSet<d>& levelset=levelsets[levelsets.size()-1]; levelset.Init(grid,box);
                int id=fluid.mpls.Add_Region(levelset, Region<d>::RegionType::AIR);
                Info("init levelset for {}", id);
            }
            //// require a external region, otherwise the Move_Particles_To_Voronoi_Levelset would report error
            {
                LevelSet<d> levelset(grid);
                levelset.phi.Exec_Nodes(
                    [&](const VectorDi cell) {
                        real phi=1e8;
                        for (int lid=0;lid<levelsets.size();lid++) {
                            phi=min(levelsets[lid].phi(cell),phi);
                        }
                        levelset.phi(cell)=-phi;
                    }
                );
                levelset.Fast_Marching(-1);
                int id=fluid.mpls.Add_Region(levelset, Region<d>::RegionType::AIR);
                fluid.mpls.region_map.find(id)->second->use_volume_control=false;
                Info("init levelset for {}", id);
            }

            fluid.velocity.Init(grid,0);
            Info("init velocity");
            
            fluid.cell_type.Init(fluid.mpls.levelset.phi.grid, CellType::FLUID);
            Eigen::Matrix<int, 3, 2> bc_width;
            bc_width << 0, 0, 0, 0, 0, 0;
            GridEulerFunc::Set_Boundary_Cells(fluid.cell_type, bc_width, CellType::SOLID);
            Info("init cell type & boundary");
        }

        void Init_Four_Bubbles(json& j, FluidMPLS<d>& fluid) {
            
            // "driver": {
            //     "cfl": 1.0,
            //     "first_frame": 0,
            //     "fps": 500,
            //     "last_frame": 500,
            //     "min_step_frame_fraction": 0,
            //     "output_base_dir": "mpls_four_bubbles"
            // },
            // "scene": {
            //     "use_particle_correction": 1,
            //     "use_normal_dynamics": 1,
            //     "use_tangent_dynamics": 0,
            //     "side_len": 1.0,
            //     "scale": 128,
            //     "sigma": 0.157275,
            //     "test": "four_bubbles"
            // }
            int scale = Json::Value<int>(j, "scale", 32);
            real side_len = Json::Value<real>(j, "side_len", 1.0);
            real dx = side_len / scale;
            VectorDi grid_size = scale * MathFunc::Vi<d>(1, 1, 1);
            Grid<d> grid(grid_size, dx, VectorD::Zero(), CENTER);

            //// init levelset
            fluid.mpls.Init(grid);

            Array<LevelSet<d>> levelsets;
            
            //// init two squares
            Array<VectorD> centers(4, VectorD::Zero());
            if constexpr(d==2){
                centers[0]=VectorD(0.25,0.25)*side_len;
                centers[1]=VectorD(0.6,0.4)*side_len;
                centers[2]=VectorD(0.4,0.6)*side_len;
                centers[3]=VectorD(0.75,0.75)*side_len;
            }
            if constexpr(d==3){
                centers[0]=VectorD(0.35,0.35,0.5)*side_len;
                centers[1]=VectorD(0.65,0.35,0.5)*side_len;
                centers[2]=VectorD(0.5,0.65,0.35)*side_len;
                centers[3]=VectorD(0.5,0.65,0.65)*side_len;
            }
            for(int i=0;i<centers.size();i++)
            {
                VectorD center=centers[i];
                real radius=side_len*0.15;
                Sphere<d> sphere(center, radius);
                levelsets.push_back(LevelSet<d>());
                LevelSet<d>& levelset=levelsets[levelsets.size()-1]; levelset.Init(grid,sphere);

                int id=fluid.mpls.Add_Region(levelset, Region<d>::RegionType::AIR);
                fluid.mpls.region_map.find(id)->second->rho=DefaultDynamicsParameter::air_density;
                Info("init levelset for {}", id);
            }
            //// require a external region, otherwise the Move_Particles_To_Voronoi_Levelset would report error
            {
                LevelSet<d> levelset(grid);
                levelset.phi.Exec_Nodes(
                    [&](const VectorDi cell) {
                        real phi=1e8;
                        for (int lid=0;lid<levelsets.size();lid++) {
                            phi=min(levelsets[lid].phi(cell),phi);
                        }
                        levelset.phi(cell)=-phi;
                    }
                );
                levelset.Fast_Marching(-1);
                int id=fluid.mpls.Add_Region(levelset, Region<d>::RegionType::AIR);
                fluid.mpls.region_map.find(id)->second->use_volume_control=false;
                fluid.mpls.region_map.find(id)->second->rho=DefaultDynamicsParameter::air_density/20;
                Info("init levelset for {}", id);
            }

            fluid.velocity.Init(grid,0);
            Info("init velocity");
            grid.Exec_Faces([&](const int axis, const VectorDi face) {
                auto [cell0, cell1] = Face_Neighbor_Cells<d>(grid, axis, face);
                if(!grid.Valid(cell0) || !grid.Valid(cell1)) return;
                int rid=fluid.mpls.indicator(cell0);
                if(rid==centers.size()) rid=fluid.mpls.indicator(cell1);
                if(rid!=centers.size()){
                    VectorD v=(grid.Center()-centers[rid]).normalized()*0.5f;
                    fluid.velocity(axis, face)=v(axis);
                }
            });
            
            fluid.cell_type.Init(fluid.mpls.levelset.phi.grid, CellType::FLUID);
            Eigen::Matrix<int, 3, 2> bc_width;
            bc_width << 0, 0, 0, 0, 0, 0;
            GridEulerFunc::Set_Boundary_Cells(fluid.cell_type, bc_width, CellType::SOLID);
            Info("init cell type & boundary");
        }
        
        
        void Init_Single_Bubble_On_Tank(json& j, FluidMPLS<d>& fluid) {
            
            int scale = Json::Value<int>(j, "scale", 32);
            real side_len = Json::Value<real>(j, "side_len", 1.0);
            real dx = side_len / scale;
            VectorDi grid_size = scale * MathFunc::Vi<d>(1, 1, 1);
            Grid<d> grid(grid_size, dx, VectorD::Zero(), CENTER);

            //// init levelset
            fluid.mpls.Init(grid);

            Array<LevelSet<d>> levelsets;

            std::cout<<"init bubble on tank"<<std::endl;
            fluid.use_film_density=false;
            
            {
                VectorD center;
                if constexpr(d==2) center=VectorD(0.5,0.6)*side_len;
                if constexpr(d==3) center=VectorD(0.5,0.6,0.5)*side_len;
                real radius=side_len*0.1;
                Sphere<d> sphere(center, radius);
                levelsets.push_back(LevelSet<d>());
                LevelSet<d>& levelset=levelsets[levelsets.size()-1]; levelset.Init(grid,sphere);
                int id=fluid.mpls.Add_Region(levelset, Region<d>::RegionType::AIR);
                if(!fluid.use_film_density)
                    fluid.mpls.region_map.find(id)->second->rho*=50;
                Info("init levelset for {}", id);
            }

            {
                VectorD plane_center;
                if constexpr(d==2) plane_center=VectorD(0.5,0.3)*side_len;
                if constexpr(d==3) plane_center=VectorD(0.5,0.3,0.5)*side_len;
                Plane<d> plane(plane_center, VectorD::Unit(1));
                
                levelsets.push_back(LevelSet<d>());
                LevelSet<d>& levelset=levelsets[levelsets.size()-1]; levelset.Init(grid,plane);
                int id=fluid.mpls.Add_Region(levelset, Region<d>::RegionType::WATER);
                Info("init levelset for {}", id);
            }

            //// require a external region, otherwise the Move_Particles_To_Voronoi_Levelset would report error
            {
                LevelSet<d> levelset(grid);
                levelset.phi.Exec_Nodes(
                    [&](const VectorDi cell) {
                        real phi=1e8;
                        for (int lid=0;lid<levelsets.size();lid++) {
                            phi=min(levelsets[lid].phi(cell),phi);
                        }
                        levelset.phi(cell)=-phi;
                    }
                );
                levelset.Fast_Marching(-1);
                int id=fluid.mpls.Add_Region(levelset, Region<d>::RegionType::AIR);
                fluid.mpls.region_map.find(id)->second->use_volume_control=false;
                Info("init levelset for {}", id);
            }

            fluid.velocity.Init(grid,0);
            Info("init velocity");
            
            fluid.cell_type.Init(fluid.mpls.levelset.phi.grid, CellType::FLUID);
            Eigen::Matrix<int, 3, 2> bc_width;
            bc_width << 1, 1, 1, 0, 1, 1;
            GridEulerFunc::Set_Boundary_Cells(fluid.cell_type, bc_width, CellType::SOLID);
            Info("init cell type & boundary");

        }

        void Init_Drip_On_Bubble(json& j, FluidMPLS<d>& fluid) {
            
            int scale = Json::Value<int>(j, "scale", 32);
            real side_len = Json::Value<real>(j, "side_len", 1.0);
            real dx = side_len / scale;
            VectorDi grid_size = scale * MathFunc::Vi<d>(1, 1, 1);
            Grid<d> grid(grid_size, dx, VectorD::Zero(), CENTER);
            
            //// init levelset
            fluid.mpls.Init(grid);

            Array<LevelSet<d>> levelsets;

            std::cout<<"init bubble on tank"<<std::endl;

            Array<VectorD> min_corners,max_corners;
            min_corners.resize(2);
            max_corners.resize(2);
            if constexpr(d==2){
                min_corners[0]=VectorD(-1.,0.1)*side_len;
                max_corners[0]=VectorD(0.5,0.6)*side_len;
                min_corners[1]=VectorD(0.5,0.1)*side_len;
                max_corners[1]=VectorD(2.0,0.6)*side_len;

                // min_corners[0]=VectorD(-1.,0.1)*side_len;
                // max_corners[0]=VectorD(0.333,0.6)*side_len;
                // min_corners[1]=VectorD(0.333,0.1)*side_len;
                // max_corners[1]=VectorD(0.666,0.6)*side_len;
                // min_corners[2]=VectorD(0.666,0.1)*side_len;
                // max_corners[2]=VectorD(2.0,0.6)*side_len;

                // min_corners[3]=VectorD(-1.,0.35)*side_len;
                // max_corners[3]=VectorD(0.5,0.6)*side_len;
                // min_corners[4]=VectorD(0.5,0.35)*side_len;
                // max_corners[4]=VectorD(2.0,0.6)*side_len;
                
            }
            if constexpr(d==3){
                min_corners[0]=VectorD(-1.,0.1,-1)*side_len;
                max_corners[0]=VectorD(0.5,0.6,+2)*side_len;
                min_corners[1]=VectorD(0.5,0.1,-1)*side_len;
                max_corners[1]=VectorD(2.0,0.6,+2)*side_len;
            }
            
            ///// bubbles
            for(int i=0;i<min_corners.size();i++){
                Box<d> box(min_corners[i],max_corners[i]);
                levelsets.push_back(LevelSet<d>());
                LevelSet<d>& levelset=levelsets[levelsets.size()-1]; levelset.Init(grid,box);

                int id=fluid.mpls.Add_Region(levelset, Region<d>::RegionType::AIR);
                fluid.mpls.region_map.find(id)->second->rho*=50;
                Info("init levelset for {}", id);
            }

            //// top liquid
            {
                VectorD min_corner, max_corner;
                if constexpr(d==2){
                    min_corner=VectorD(-1.,0.6)*side_len;
                    max_corner=VectorD(2.0,0.7)*side_len;
                    // min_corner=VectorD(0.2,0.6)*side_len;
                    // max_corner=VectorD(0.8,0.7)*side_len;
                }
                if constexpr(d==3){
                    // min_corner=VectorD(-1.,0.6,-1)*side_len;
                    // max_corner=VectorD(2.0,0.8,+2)*side_len;
                    min_corner=VectorD(0.2,0.6,-1)*side_len;
                    max_corner=VectorD(0.8,0.7,+2)*side_len;
                }
                Box<d> box(min_corner,max_corner);

                levelsets.push_back(LevelSet<d>());
                LevelSet<d>& levelset=levelsets[levelsets.size()-1]; levelset.Init(grid,box);

                int id=fluid.mpls.Add_Region(levelset, Region<d>::RegionType::WATER);
                Info("init levelset for {}", id);
            }

            //// liquid tank
            {
                VectorD plane_center;
                if constexpr(d==2) plane_center=VectorD(0.5,0.1)*side_len;
                if constexpr(d==3) plane_center=VectorD(0.5,0.1,0.5)*side_len;
                Plane<d> plane(plane_center, VectorD::Unit(1));
                
                levelsets.push_back(LevelSet<d>());
                LevelSet<d>& levelset=levelsets[levelsets.size()-1]; levelset.Init(grid,plane);
                int id=fluid.mpls.Add_Region(levelset, Region<d>::RegionType::WATER);
                Info("init levelset for {}", id);
            }

            //// require a external region, otherwise the Move_Particles_To_Voronoi_Levelset would report error
            {
                LevelSet<d> levelset(grid);
                levelset.phi.Exec_Nodes(
                    [&](const VectorDi cell) {
                        real phi=1e8;
                        for (int lid=0;lid<levelsets.size();lid++) {
                            phi=min(levelsets[lid].phi(cell),phi);
                        }
                        levelset.phi(cell)=-phi;
                    }
                );
                levelset.Fast_Marching(-1);
                int id=fluid.mpls.Add_Region(levelset, Region<d>::RegionType::AIR);

                Info("init levelset for {}", id);
            }

            fluid.velocity.Init(grid,0);
            Info("init velocity");
            
            fluid.cell_type.Init(fluid.mpls.levelset.phi.grid, CellType::FLUID);
            Eigen::Matrix<int, 3, 2> bc_width;
            bc_width << 1, 1, 1, 0, 1, 1;
            GridEulerFunc::Set_Boundary_Cells(fluid.cell_type, bc_width, CellType::SOLID);
            Info("init cell type & boundary");
        }
        
        void Init_Drop_On_Tank(json& j, FluidMPLS<d>& fluid) {
            
            int scale = Json::Value<int>(j, "scale", 32);
            real side_len = Json::Value<real>(j, "side_len", 1.0);
            real dx = side_len / scale;
            VectorDi grid_size = scale * MathFunc::Vi<d>(1, 1, 1);
            Grid<d> grid(grid_size, dx, VectorD::Zero(), CENTER);
            
            //// init levelset
            fluid.mpls.Init(grid);

            Array<LevelSet<d>> levelsets;

            std::cout<<"init bubble on tank"<<std::endl;

            //// top liquid
            Array<VectorD> centers(4, VectorD::Zero());
            if constexpr(d==2){
                centers[0]=VectorD(0.25,0.35)*side_len;
                centers[1]=VectorD(0.75,0.35)*side_len;
                centers[2]=VectorD(0.35,0.6)*side_len;
                centers[3]=VectorD(0.65,0.6)*side_len;
            }
            if constexpr(d==3){
                centers[0]=VectorD(0.25,0.35,0.5)*side_len;
                centers[1]=VectorD(0.75,0.35,0.5)*side_len;
                centers[2]=VectorD(0.35,0.6,0.5)*side_len;
                centers[3]=VectorD(0.65,0.6,0.5)*side_len;
            }
            for(int i=0;i<centers.size();i++)
            {
                VectorD center=centers[i];
                real radius=side_len*0.1;
                Sphere<d> sphere(center, radius);
                levelsets.push_back(LevelSet<d>());
                LevelSet<d>& levelset=levelsets[levelsets.size()-1]; levelset.Init(grid,sphere);

                int id=fluid.mpls.Add_Region(levelset, Region<d>::RegionType::WATER);
                Info("init levelset for {}", id);
            }

            //// liquid tank
            {
                VectorD plane_center;
                if constexpr(d==2) plane_center=VectorD(0.5,0.2)*side_len;
                if constexpr(d==3) plane_center=VectorD(0.5,0.2,0.5)*side_len;
                Plane<d> plane(plane_center, VectorD::Unit(1));
                
                levelsets.push_back(LevelSet<d>());
                LevelSet<d>& levelset=levelsets[levelsets.size()-1]; levelset.Init(grid,plane);
                int id=fluid.mpls.Add_Region(levelset, Region<d>::RegionType::WATER);
                Info("init levelset for {}", id);
            }

            //// require a external region, otherwise the Move_Particles_To_Voronoi_Levelset would report error
            {
                LevelSet<d> levelset(grid);
                levelset.phi.Exec_Nodes(
                    [&](const VectorDi cell) {
                        real phi=1e8;
                        for (int lid=0;lid<levelsets.size();lid++) {
                            phi=min(levelsets[lid].phi(cell),phi);
                        }
                        levelset.phi(cell)=-phi;
                    }
                );
                levelset.Fast_Marching(-1);
                int id=fluid.mpls.Add_Region(levelset, Region<d>::RegionType::AIR);

                Info("init levelset for {}", id);
            }

            fluid.velocity.Init(grid,0);
            Info("init velocity");
            
            fluid.cell_type.Init(fluid.mpls.levelset.phi.grid, CellType::FLUID);
            Eigen::Matrix<int, 3, 2> bc_width;
            bc_width << 1, 1, 1, 0, 1, 1;
            GridEulerFunc::Set_Boundary_Cells(fluid.cell_type, bc_width, CellType::SOLID);
            Info("init cell type & boundary");
        }

        // void Init_Drip_On_Bubble(json& j, FluidMPLS<d>& fluid) {
            
        //     int scale = Json::Value<int>(j, "scale", 32);
        //     real side_len = Json::Value<real>(j, "side_len", 1.0);
        //     real dx = side_len / scale;
        //     VectorDi grid_size = scale * MathFunc::Vi<d>(1, 1, 1);
        //     Grid<d> grid(grid_size, dx, VectorD::Zero(), CENTER);

        //     //// init levelset
        //     fluid.mpls.Init(grid);

        //     Array<LevelSet<d>> levelsets;

        //     std::cout<<"init bubble on tank"<<std::endl;

        //     VectorD plane_center;
        //     if constexpr(d==2) plane_center=VectorD(0.5,0.3)*side_len;
        //     if constexpr(d==3) plane_center=VectorD(0.5,0.3,0.5)*side_len;
            
        //     ///// hemisphere bubble
        //     {
        //         VectorD center;
        //         if constexpr(d==2) center=VectorD(0.5,0.3)*side_len;
        //         if constexpr(d==3) center=VectorD(0.5,0.3,0.5)*side_len;
        //         real radius=side_len*0.25;
        //         Sphere<d> sphere(center, radius);
        //         Plane<d> plane(plane_center, -VectorD::Unit(1));

        //         levelsets.push_back(LevelSet<d>());
        //         LevelSet<d>& levelset=levelsets[levelsets.size()-1]; levelset.Init(grid);
        //         levelset.phi.Exec_Nodes([&](const VectorDi cell) {
        //             VectorD pos=levelset.phi.grid.Position(cell);
        //             levelset.phi(cell)=std::max(sphere.Phi(pos),plane.Phi(pos));
        //         });

        //         int id=fluid.mpls.Add_Region(levelset, Region<d>::RegionType::AIR);
        //         fluid.mpls.region_map.find(id)->second->rho*=50;
        //         Info("init levelset for {}", id);
        //     }
        //     //// thin liquid layer on the bubble
        //     // {
        //     //     VectorD min_corner, max_corner;
        //     //     if constexpr(d==2){
        //     //         min_corner=VectorD(0.5-0.2,0.55-0.025)*side_len;
        //     //         // max_corner=VectorD(0.5+0.2,0.55+0.025)*side_len;
        //     //         max_corner=VectorD(0.5,0.55+0.025)*side_len;
        //     //     }
        //     //     if constexpr(d==3){
        //     //         min_corner=VectorD(0.5-0.2,0.55-0.025,0.5-0.2)*side_len;
        //     //         // max_corner=VectorD(0.5+0.2,0.55+0.025,0.5+0.2)*side_len;
        //     //         max_corner=VectorD(0.5,0.55+0.025,0.5)*side_len;
        //     //     }
        //     //     Box<d> box(min_corner,max_corner);
        //     //     levelsets.push_back(LevelSet<d>());
        //     //     LevelSet<d>& levelset=levelsets[levelsets.size()-1]; levelset.Init(grid,box);
        //     //     int id=fluid.mpls.Add_Region(levelset, Region<d>::RegionType::WATER);
        //     //     Info("init levelset for {}", id);
        //     // }
        //     {
        //         VectorD center;
        //         if constexpr(d==2) center=VectorD(0.5-0.1,0.8)*side_len;
        //         if constexpr(d==3) center=VectorD(0.5-0.1,0.8,0.5)*side_len;
        //         real radius=side_len*0.025;
        //         Sphere<d> sphere(center, radius);
            
        //         // VectorD min_corner, max_corner;
        //         // if constexpr(d==2){
        //         //     min_corner=center-radius*VectorD::Unit(0)-0.2*side_len*VectorD::Unit(1);
        //         //     max_corner=center+radius*VectorD::Unit(0);
        //         // }
        //         // if constexpr(d==3){
        //         //     min_corner=center-radius*(VectorD::Unit(0)+VectorD::Unit(2))-0.2*side_len*VectorD::Unit(1);
        //         //     max_corner=center+radius*(VectorD::Unit(0)+VectorD::Unit(2));
        //         // }
        //         // Box<d> box(min_corner,max_corner);

        //         VectorD center2;
        //         if constexpr(d==2) center2=VectorD(0.5-0.1,0.8-0.08)*side_len;
        //         if constexpr(d==3) center2=VectorD(0.5-0.1,0.8-0.08,0.5)*side_len;
        //         real radius2=side_len*0.08;
        //         Sphere<d> sphere2(center2, radius2);
            

        //         levelsets.push_back(LevelSet<d>());
        //         LevelSet<d>& levelset=levelsets[levelsets.size()-1]; levelset.Init(grid);
        //         levelset.phi.Exec_Nodes([&](const VectorDi cell) {
        //             VectorD pos=levelset.phi.grid.Position(cell);
        //             levelset.phi(cell)=std::min(sphere.Phi(pos),sphere2.Phi(pos));
        //         });

        //         int id=fluid.mpls.Add_Region(levelset, Region<d>::RegionType::WATER);
        //         Info("init levelset for {}", id);

        //         // auto& itr=fluid.mpls.region_map.find(id);
        //         // for(int i=0;i<itr->second->particles->Size();i++){
        //         //     VectorD& x_i=itr->second->particles->x(i);
        //         //     if(x_i(1)>center(1)) itr->second->particles->t(i)=1;
        //         // }
        //         // Info("set fixed particles for {}", id);

        //         fluid.mpls.region_map.find(id)->second->use_volume_control=false;

        //     }

        //     //// liquid tank
        //     {
        //         Plane<d> plane(plane_center, VectorD::Unit(1));
                
        //         levelsets.push_back(LevelSet<d>());
        //         LevelSet<d>& levelset=levelsets[levelsets.size()-1]; levelset.Init(grid,plane);
        //         int id=fluid.mpls.Add_Region(levelset, Region<d>::RegionType::WATER);
        //         Info("init levelset for {}", id);
        //     }

        //     //// require a external region, otherwise the Move_Particles_To_Voronoi_Levelset would report error
        //     {
        //         LevelSet<d> levelset(grid);
        //         levelset.phi.Exec_Nodes(
        //             [&](const VectorDi cell) {
        //                 real phi=1e8;
        //                 for (int lid=0;lid<levelsets.size();lid++) {
        //                     phi=min(levelsets[lid].phi(cell),phi);
        //                 }
        //                 levelset.phi(cell)=-phi;
        //             }
        //         );
        //         levelset.Fast_Marching(-1);
        //         int id=fluid.mpls.Add_Region(levelset, Region<d>::RegionType::AIR);

        //         // VectorD center;
        //         // if constexpr(d==2) center=VectorD(0.5-0.1,0.8)*side_len;
        //         // if constexpr(d==3) center=VectorD(0.5-0.1,0.8,0.5)*side_len;
        //         // fluid.mpls.region_map.find(id)->second->use_volume_control=false;
                
        //         // auto& itr=fluid.mpls.region_map.find(id);
        //         // for(int i=0;i<itr->second->particles->Size();i++){
        //         //     VectorD& x_i=itr->second->particles->x(i);
        //         //     if(x_i(1)>center(1)) itr->second->particles->t(i)=1;
        //         // }
        //         // Info("set fixed particles for {}", id);


        //         Info("init levelset for {}", id);
        //     }

        //     fluid.velocity.Init(grid,0);
        //     Info("init velocity");

        //     {
        //         VectorD center;
        //         if constexpr(d==2) center=VectorD(0.5-0.1,0.8)*side_len;
        //         if constexpr(d==3) center=VectorD(0.5-0.1,0.8,0.5)*side_len;
        //         real radius=side_len*(0.025+0.005);
                
        //         VectorD min_corner, max_corner;
        //         if constexpr(d==2){
        //             min_corner=center-radius*VectorD::Unit(0);
        //             max_corner=center+radius*VectorD::Unit(0)+radius*VectorD::Unit(1);
        //         }
        //         if constexpr(d==3){
        //             min_corner=center-radius*(VectorD::Unit(0)+VectorD::Unit(2));
        //             max_corner=center+radius*(VectorD::Unit(0)+VectorD::Unit(2))+radius*VectorD::Unit(1);
        //         }

        //         VectorD v;
        //         if constexpr(d==2) v=VectorD(0,-3);
        //         if constexpr(d==3) v=VectorD(0,-3,0);
                
        //         std::shared_ptr<Emitter<d>> emitter=std::make_shared<BoxEmitter<d>>(min_corner,max_corner,v);
        //         fluid.emitters.push_back(emitter);
        //     }
        //     Info("init emitter");
            
        //     fluid.cell_type.Init(fluid.mpls.levelset.phi.grid, CellType::FLUID);
        //     Eigen::Matrix<int, 3, 2> bc_width;
        //     bc_width << 1, 1, 1, 0, 1, 1;
        //     GridEulerFunc::Set_Boundary_Cells(fluid.cell_type, bc_width, CellType::SOLID);
        //     Info("init cell type & boundary");

        // }

        void Init_Single_Bubble_Arise_In_Tank(json& j, FluidMPLS<d>& fluid) {
            
            int scale = Json::Value<int>(j, "scale", 32);
            real side_len = Json::Value<real>(j, "side_len", 1.0);
            real dx = side_len / scale;
            VectorDi grid_size = scale * MathFunc::Vi<d>(1, 1, 1);
            Grid<d> grid(grid_size, dx, VectorD::Zero(), CENTER);

            //// init levelset
            fluid.mpls.Init(grid);

            Array<LevelSet<d>> levelsets;

            //// init bubble [1/2-0.15, 1/3-0.15]-[1/2+0.15, 1/3+0.15]
            {
                VectorD min_corner, max_corner;
                if constexpr(d==2){
                    min_corner=VectorD(0.5-0.1,0.3-0.1)*side_len;
                    max_corner=VectorD(0.5+0.1,0.3+0.1)*side_len;
                }
                if constexpr(d==3){
                    min_corner=VectorD(0.5-0.1,0.3+0.1,0.5-0.1)*side_len;
                    max_corner=VectorD(0.5+0.1,0.3+0.1,0.5+0.1)*side_len;
                }
                Box<d> box(min_corner,max_corner);
                levelsets.push_back(LevelSet<d>());
                LevelSet<d>& levelset=levelsets[levelsets.size()-1]; levelset.Init(grid,box);
                int id=fluid.mpls.Add_Region(levelset, Region<d>::RegionType::AIR);
                fluid.mpls.region_map.find(id)->second->rho*=50;
                Info("init levelset for {}", id);
            }

            {
                VectorD min_corner, max_corner;
                if constexpr(d==2){
                    min_corner=VectorD(0.5-0.1,0.3-0.1)*side_len;
                    max_corner=VectorD(0.5+0.1,0.3+0.1)*side_len;
                }
                if constexpr(d==3){
                    min_corner=VectorD(0.5-0.1,0.3+0.1,0.5-0.1)*side_len;
                    max_corner=VectorD(0.5+0.1,0.3+0.1,0.5+0.1)*side_len;
                }
                Box<d> box(min_corner,max_corner);

                VectorD plane_center;
                if constexpr(d==2) plane_center=VectorD(0.5,0.45)*side_len;
                if constexpr(d==3) plane_center=VectorD(0.5,0.45,0.5)*side_len;
                Plane<d> plane(plane_center, VectorD::Unit(1));
                
                levelsets.push_back(LevelSet<d>());
                LevelSet<d>& levelset=levelsets[levelsets.size()-1]; levelset.Init(grid);

                levelset.phi.Exec_Nodes([&](const VectorDi cell) {
                    VectorD pos=levelset.phi.grid.Position(cell);
                    levelset.phi(cell)=std::max(-box.Phi(pos),plane.Phi(pos));
                });

                int id=fluid.mpls.Add_Region(levelset, Region<d>::RegionType::WATER);
                Info("init levelset for {}", id);
            }

            //// require a external region, otherwise the Move_Particles_To_Voronoi_Levelset would report error
            {
                LevelSet<d> levelset(grid);
                levelset.phi.Exec_Nodes(
                    [&](const VectorDi cell) {
                        real phi=1e8;
                        for (int lid=0;lid<levelsets.size();lid++) {
                            phi=min(levelsets[lid].phi(cell),phi);
                        }
                        levelset.phi(cell)=-phi;
                    }
                );
                levelset.Fast_Marching(-1);
                int id=fluid.mpls.Add_Region(levelset, Region<d>::RegionType::AIR);
                fluid.mpls.region_map.find(id)->second->use_volume_control=false;
                Info("init levelset for {}", id);
            }

            fluid.velocity.Init(grid,0);
            Info("init velocity");
            
            
            fluid.cell_type.Init(fluid.mpls.levelset.phi.grid, CellType::FLUID);
            Eigen::Matrix<int, 3, 2> bc_width;
            bc_width << 1, 1, 1, 0, 1, 1;
            GridEulerFunc::Set_Boundary_Cells(fluid.cell_type, bc_width, CellType::SOLID);
            Info("init cell type & boundary");

        }

        void Init_Bubble_Stack(json& j, FluidMPLS<d>& fluid) {
            
            int scale = Json::Value<int>(j, "scale", 32);
            real side_len = Json::Value<real>(j, "side_len", 1.0);
            real dx = side_len / scale;
            VectorDi grid_size = scale * MathFunc::Vi<d>(1, 1, 1);
            Grid<d> grid(grid_size, dx, VectorD::Zero(), CENTER);

            //// init levelset
            fluid.mpls.Init(grid);

            Array<LevelSet<d>> levelsets;

            std::cout<<"init bubble on tank"<<std::endl;
            // |   O   |
            // |  O O  |
            // |-------|
            Array<Sphere<d>> spheres;
            // {
            //     VectorD offset;
            //     if constexpr(d==2) offset=VectorD(0.2,0.3)*side_len;
            //     if constexpr(d==3) offset=VectorD(0.2,0.3,0.5)*side_len;
                
            //     for(int i=0;i<3;i++){
            //         VectorD coord=VectorD::Unit(0)*i;
            //         VectorD center=coord*0.3*side_len+offset;
            //         spheres.push_back(Sphere<d>(center, side_len * 0.1));
            //     }
            // }
            // {
            //     VectorD offset;
            //     if constexpr(d==2) offset=VectorD(0.25,0.7)*side_len;
            //     if constexpr(d==3) offset=VectorD(0.25,0.7,0.5)*side_len;
                
            //     for(int i=0;i<2;i++){
            //         VectorD coord=VectorD::Unit(0)*i;
            //         VectorD center=coord*0.5*side_len+offset;
            //         spheres.push_back(Sphere<d>(center, side_len * 0.15));
            //     }
            // }

            {
                VectorD offset;
                if constexpr(d==2) offset=VectorD(0.3,0.275)*side_len;
                if constexpr(d==3) offset=VectorD(0.3,0.275,0.5)*side_len;
                
                for(int i=0;i<2;i++){
                    VectorD coord=VectorD::Unit(0)*i;
                    VectorD center=coord*0.4*side_len+offset;
                    spheres.push_back(Sphere<d>(center, side_len * 0.15));
                }
            }
            {
                spheres.push_back(Sphere<d>(grid.Center()-VectorD::Unit(1)*0.025*side_len, side_len * 0.15));
            }

            for(int i=0;i<spheres.size();i++){
                levelsets.push_back(LevelSet<d>());
                LevelSet<d>& levelset=levelsets[levelsets.size()-1]; levelset.Init(grid,spheres[i]);
                
                int id=fluid.mpls.Add_Region(levelset, Region<d>::RegionType::AIR);
                fluid.mpls.region_map.find(id)->second->rho*=50;
                Info("init levelset for {}", id);

            }
            // {
            //     ImplicitUnion<d> water_shape(spheres[0],spheres[1]);
            //     levelsets.push_back(LevelSet<d>());
            //     LevelSet<d>& levelset=levelsets[levelsets.size()-1]; levelset.Init(grid,water_shape);
                
            //     int id=fluid.mpls.Add_Region(levelset, Region<d>::RegionType::AIR);
            //     fluid.mpls.region_map.find(id)->second->rho*=50;
            // }
            std::cout<<"init bubble on tank"<<std::endl;
            {
                VectorD plane_center;
                if constexpr(d==2) plane_center=VectorD(0.5,0.1)*side_len;
                if constexpr(d==3) plane_center=VectorD(0.5,0.1,0.5)*side_len;
                Plane<d> plane(plane_center, VectorD::Unit(1));
                
                levelsets.push_back(LevelSet<d>());
                LevelSet<d>& levelset=levelsets[levelsets.size()-1]; levelset.Init(grid, plane);

                int id=fluid.mpls.Add_Region(levelset, Region<d>::RegionType::WATER);
                Info("init levelset for {}", id);
            }
            std::cout<<"init bubble on tank"<<std::endl;
            //// require a external region, otherwise the Move_Particles_To_Voronoi_Levelset would report error
            {
                LevelSet<d> levelset(grid);
                levelset.phi.Exec_Nodes(
                    [&](const VectorDi cell) {
                        real phi=1e8;
                        for (int lid=0;lid<levelsets.size();lid++) {
                            phi=min(levelsets[lid].phi(cell),phi);
                        }
                        levelset.phi(cell)=-phi;
                    }
                );
                levelset.Fast_Marching(-1);
                int id=fluid.mpls.Add_Region(levelset, Region<d>::RegionType::AIR);
                fluid.mpls.region_map.find(id)->second->use_volume_control=false;
                Info("init levelset for {}", id);
            }

            std::cout<<"init bubble on tank"<<std::endl;
            fluid.velocity.Init(grid,0);
            // fluid.velocity.Calc_Faces(
            //     [&](const int axis, const VectorDi face) {
            //     	VectorD pos = grid.Face_Center(axis, face);
            //         //// u in x-axis
            //         if(axis==0)
            //             return  2*cos(CommonConstants::pi*pos(1))*sin(CommonConstants::pi*pos(1)) * pow(sin(CommonConstants::pi*pos(0)),2);
            //         //// v in y-axis
            //         if(axis==1)
            //             return  -2*cos(CommonConstants::pi*pos(0))*sin(CommonConstants::pi*pos(0)) * pow(sin(CommonConstants::pi*pos(1)),2);
            //         return 0.0;
            //     }
            // );
            std::cout<<"init bubble on tank"<<std::endl;
            Info("init velocity");

            fluid.cell_type.Init(fluid.mpls.levelset.phi.grid, CellType::FLUID);
            Eigen::Matrix<int, 3, 2> bc_width;
            bc_width << 1, 1, 1, 0, 1, 1;
            GridEulerFunc::Set_Boundary_Cells(fluid.cell_type, bc_width, CellType::SOLID);
            Info("init cell type & boundary");

        }

        void Init_Bubble_Arise(json& j, FluidMPLS<d>& fluid) {
            
            int scale = Json::Value<int>(j, "scale", 32);
            real side_len = Json::Value<real>(j, "side_len", 1.0);
            real dx = side_len / scale;
            VectorDi grid_size = scale * MathFunc::Vi<d>(1, 1, 1);
            Grid<d> grid(grid_size, dx, VectorD::Zero(), CENTER);

            //// init levelset
            fluid.mpls.Init(grid);

            Array<LevelSet<d>> levelsets;
            
            std::cout<<"init tank"<<std::endl;
            {
                VectorD plane_center;
                if constexpr(d==2) plane_center=VectorD(0.5,0.65)*side_len;
                if constexpr(d==3) plane_center=VectorD(0.5,0.65,0.5)*side_len;
                Plane<d> plane(plane_center, VectorD::Unit(1));
                
                levelsets.push_back(LevelSet<d>());
                LevelSet<d>& levelset=levelsets[levelsets.size()-1]; levelset.Init(grid, plane);

                int id=fluid.mpls.Add_Region(levelset, Region<d>::RegionType::WATER);
                Info("init levelset for {}", id);
            }

            std::cout<<"init ambient air"<<std::endl;
            //// require a external region, otherwise the Move_Particles_To_Voronoi_Levelset would report error
            {
                LevelSet<d> levelset(grid);
                levelset.phi.Exec_Nodes(
                    [&](const VectorDi cell) {
                        real phi=1e8;
                        for (int lid=0;lid<levelsets.size();lid++) {
                            phi=min(levelsets[lid].phi(cell),phi);
                        }
                        levelset.phi(cell)=-phi;
                    }
                );
                levelset.Fast_Marching(-1);
                int id=fluid.mpls.Add_Region(levelset, Region<d>::RegionType::AIR);
                fluid.mpls.region_map.find(id)->second->use_volume_control=false;
                Info("init levelset for {}", id);
            }

            fluid.velocity.Init(grid,0);

            fluid.cell_type.Init(fluid.mpls.levelset.phi.grid, CellType::FLUID);
            Eigen::Matrix<int, 3, 2> bc_width;
            bc_width << 1, 1, 1, 0, 1, 1;
            GridEulerFunc::Set_Boundary_Cells(fluid.cell_type, bc_width, CellType::SOLID);
            Info("init cell type & boundary");

        }

        
        void Init_Tangential_Flow(json& j, FluidMPLS<d>& fluid) {
            
            int scale = Json::Value<int>(j, "scale", 32);
            real side_len = Json::Value<real>(j, "side_len", 1.0);
            real dx = side_len / scale;
            VectorDi grid_size = scale * MathFunc::Vi<d>(1, 1, 1);
            Grid<d> grid(grid_size, dx, VectorD::Zero(), CENTER);

            //// init levelset
            fluid.mpls.Init(grid);

            Array<LevelSet<d>> levelsets;
            
            {
                // real length=0.2f;
                // VectorD min_corner=VectorD::Ones()*(side_len*0.5);
                // min_corner.x()=side_len*(1-length)*0.5;
                // min_corner.y()=side_len*(1-length)*0.5;
                // VectorD max_corner=VectorD::Ones()*(side_len*0.5);
                // max_corner.x()=side_len*(1+length)*0.5;
                // max_corner.y()=side_len*(1+length)*0.5;

                // Box<d> box(min_corner, max_corner);
                // std::cout<<"box:"<<min_corner.transpose()<<","<<max_corner.transpose()<<std::endl;
                
                VectorD center;
                if constexpr(d==2) center=VectorD(0.5,0.5)*side_len;
                if constexpr(d==3) center=VectorD(0.5,0.5,0.5)*side_len;
                real radius=side_len*0.2;
                Sphere<d> sphere(center, radius);

                levelsets.push_back(LevelSet<d>());
                LevelSet<d>& levelset=levelsets[levelsets.size()-1]; levelset.Init(grid,sphere);
                // LevelSet<d>& levelset=levelsets[levelsets.size()-1]; levelset.Init(grid,box);
                int id=fluid.mpls.Add_Region(levelset, Region<d>::RegionType::AIR);
                fluid.mpls.Add_Lagrangian_Particles(levelset);
                {
                    VectorD pos;
                    if constexpr(d==2) pos=VectorD(0.5,0.7)*side_len;
                    if constexpr(d==3) pos=VectorD(0.5,0.7,0.5)*side_len;
                    pos=LevelSetUtil<d>::Closest_Point_With_Iterations(levelset, pos, 5);
                    std::cout<<"pos="<<pos.transpose()<<std::endl;
                    std::cout<<"lsize="<<fluid.mpls.l_particles->Size()<<std::endl;
                    fluid.mpls.Add_Lagrangian_Particles(levelset, pos, 100);
                    std::cout<<"lsize="<<fluid.mpls.l_particles->Size()<<std::endl;
                }
                // //// removoe half of the particles
                // std::set<int> remove_set;
                // for(int i=fluid.mpls.l_particles->Size()/2; i<fluid.mpls.l_particles->Size();i++){
                //     remove_set.insert(i);
                // }
                // ParticleLevelSetHelper<d>::Remove_Particles(*fluid.mpls.l_particles, remove_set);
                // fluid.mpls.Add_Lagrangian_Particles(levelset);

                // if(!fluid.use_film_density)
                //     fluid.mpls.region_map.find(id)->second->rho*=50;
                Info("init levelset for {}", id);
            }

            std::cout<<"init ambient air"<<std::endl;
            //// require a external region, otherwise the Move_Particles_To_Voronoi_Levelset would report error
            {
                LevelSet<d> levelset(grid);
                levelset.phi.Exec_Nodes(
                    [&](const VectorDi cell) {
                        real phi=1e8;
                        for (int lid=0;lid<levelsets.size();lid++) {
                            phi=min(levelsets[lid].phi(cell),phi);
                        }
                        levelset.phi(cell)=-phi;
                    }
                );
                levelset.Fast_Marching(-1);
                int id=fluid.mpls.Add_Region(levelset, Region<d>::RegionType::AIR);
                fluid.mpls.region_map.find(id)->second->use_volume_control=false;
                Info("init levelset for {}", id);
            }

            fluid.velocity.Init(grid,0);

            {
                VectorD center;
                if constexpr(d==2) center=VectorD(0.5,0.5)*side_len;
                if constexpr(d==3) center=VectorD(0.5,0.5,0.5)*side_len;

                VectorD min_corner, max_corner;
                if constexpr(d==2){
                    min_corner=center-side_len*0.3*VectorD::Unit(0)-side_len*0.05*VectorD::Unit(1);
                    max_corner=center+side_len*0.3*VectorD::Unit(0)+side_len*0.05*VectorD::Unit(1);
                }
                if constexpr(d==3){
                    min_corner=center-side_len*0.3*(VectorD::Unit(0)+VectorD::Unit(2))-side_len*0.05*VectorD::Unit(1);
                    // max_corner=center+side_len*0.3*(VectorD::Unit(0)+VectorD::Unit(2))+side_len*0.05*VectorD::Unit(1);
                    max_corner=center+side_len*0.3*(VectorD::Unit(0))+side_len*0.05*VectorD::Unit(1);
                }

                VectorD v;
                if constexpr(d==2) v=VectorD(0,0.5);
                if constexpr(d==3) v=VectorD(0,0.5,0);
                
                std::shared_ptr<Emitter<d>> emitter=std::make_shared<BoxForcer<d>>(min_corner,max_corner,v);
                fluid.emitters.push_back(emitter);
            }
            Info("init emitter");

            fluid.cell_type.Init(fluid.mpls.levelset.phi.grid, CellType::FLUID);
            Eigen::Matrix<int, 3, 2> bc_width;
            bc_width << 0, 0, 0, 0, 0, 0;
            GridEulerFunc::Set_Boundary_Cells(fluid.cell_type, bc_width, CellType::SOLID);
            Info("init cell type & boundary");

        }

    };
}