//////////////////////////////////////////////////////////////////////////
// Fluid Euler with Particle Level Set
// Copyright (c) (2022-), Hui Wang
// This file is part of MESO, whose distribution is governed by the LICENSE file.
//////////////////////////////////////////////////////////////////////////
#pragma once
#include "Json.h"
#include "Simulator.h"
#include "ParticleLevelSet.h"
#include "Advection.h"
#include "MarchingCubes.h"
#include "GridEulerFunc.h"
#include "Interpolator.h"

// geometry
#include "GridUtil.h"
#include "FieldUtil.h"
#include "PointUtil.h"
#include "LevelSetUtil.h"
#include "PointLocalGeometry.h"
#include "GeometryDebugBuffer.h"


// dynamics
#include "DefaultDynamicsParameter.h"
#include "Multigrid.h"
#include "ConjugateGradient.h"
#include "SPH_Utils.h"
#include "SurfaceTension.h"


#include "IOFunc.h"
#include "NeighborSearcher.h"
#include <vtkTriangle.h>
#include <vtkLine.h>
#include <vtkCellData.h>
#include <vtkCellArray.h>

namespace Meso {

    enum CellType : int {
        INVALID = -1,
        FLUID,
        EMPTY,
        SOLID
    };

    template<class T, int d>
    std::tuple<Vector<int, d>, Vector<int, d>, T, T> Face_Neighbor_Cells_And_Values(const Field<T, d>& F, const int axis, const Vector<int, d> face, const T outside_val) {
        Typedef_VectorD(d);
        VectorDi cell0 = face - VectorDi::Unit(axis), cell1 = face;
        T val0, val1;
        if (F.grid.Valid(cell0)) val0 = F(cell0);
        else val0 = outside_val;
        if (F.grid.Valid(cell1)) val1 = F(cell1);
        else val1 = outside_val;
        return std::make_tuple(cell0, cell1, val0, val1);
    }

    template<int d>
    std::tuple<Vector<int, d>, Vector<int, d>> Face_Neighbor_Cells(const Grid<d>& grid, const int axis, const Vector<int, d> face){
        Typedef_VectorD(d);
        VectorDi cell0 = face - VectorDi::Unit(axis), cell1 = face;
        return std::make_tuple(cell0, cell1);
    }
    
    template<class T, int d>
    std::tuple<Vector<int, d>, Vector<int, d>, T, T> Cell_Neighbor_Faces_And_Values(const FaceField<T, d>& F, const int axis, const Vector<int, d> cell, const T outside_val) {
        Typedef_VectorD(d);
        VectorDi face0 = cell, face1 = cell + VectorDi::Unit(axis);
        T val0, val1;
        if (F.grid.Valid_Face(axis,face0)) val0 = F(axis,face0);
        else val0 = outside_val;
        if (F.grid.Valid_Face(axis,face1)) val1 = F(axis,face1);
        else val1 = outside_val;
        return std::make_tuple(face0, face1, val0, val1);
    }

    template<int d>
    class FluidPLS : public Simulator {
        Typedef_VectorD(d);
        Typedef_MatrixD(d);
        using VectorT=Vector<real,d-1>;								////tangential vector type
        using VectorTi=Vector<int,d-1>;								////tangential vector int
    public:
        enum CorrectionType { NONE = 0, CLOSEST_PARTICLE, MLS_PROJECTION, AUTO_STRATEGY };
        
        //// geometry, velocity initialized outside
        ParticleLevelSet<d> pls;
        std::shared_ptr<PointLocalGeometry<d>> local_geom;
        FaceField<real, d> velocity;

        //// levelset for comparison
        LevelSet<d> ls;

        //// options
        bool compare_with_levelset=true;
        CorrectionType correction_type=CorrectionType::MLS_PROJECTION;
        bool use_normal_dynamics=false;
        bool use_tangent_dynamics=false;
        bool use_particle_correction=true;
        
        //// for tangential dynamics
        SPH_Utils<d> sph;

        //// for normal dynamics
        // Needs to be initialized
        // physical quantities
        real air_density;
        real liquid_density;
        VectorD gravity_acc;
        real sigma;

        //// from FluidFreeSurface
        // define the system behavior
        Field<CellType,d> cell_type;
        BoundaryConditionDirect<FaceFieldDv<real, d>> velocity_bc;

        //utilities
        MaskedPoissonMapping<real, d> poisson;
        VCycleMultigridIntp<real, d> MG_precond;
        ConjugateGradient<real> MGPCG;

        //Temporary variables, don't need to be initialized
        FaceFieldDv<real, d> velocity_dev;
        FaceFieldDv<real, d> temp_velocity_dev;
        FieldDv<real, d> temp_phi_dev;
        FieldDv<real, d> temp_field_dev;
        FieldDv<real, d> pressure_dev;
        Field<bool, d> fixed_host;
        FaceField<real, d> vol_host;
        Field<real, d> div_host;
        
        //// for debug
        GeometryDebugBuffer<d> geo_debug_buffer;
        
        void Init(json& j){
            
            //// init levelset
            if(compare_with_levelset){
                ls.Init(pls.levelset.phi.grid);
                ls.phi.Deep_Copy(pls.levelset.phi);
            }

            //// set options, in the default setting, pls correction with levelset comparison
            use_normal_dynamics=Json::Value<int>(j, "use_normal_dynamics", 0);
            use_tangent_dynamics=Json::Value<int>(j, "use_tangent_dynamics", 0);
            use_particle_correction=Json::Value<int>(j, "use_particle_correction", 1);
            compare_with_levelset=Json::Value<int>(j, "compare_with_levelset", 1);

            //// 
            if(use_normal_dynamics){
                air_density = Json::Value<real>(j, "air_density", DefaultDynamicsParameter::air_density);
                liquid_density = Json::Value<real>(j, "liquid_density", DefaultDynamicsParameter::liquid_density);
                gravity_acc = MathFunc::V<d>(Json::Value<Vector3>(j, "gravity_acc", Vector3::Unit(1) * DefaultDynamicsParameter::gravity));
                sigma = Json::Value<real>(j, "sigma", DefaultDynamicsParameter::sigma);

                FaceField<bool, d> face_fixed(cell_type.grid);
                face_fixed.Calc_Faces(
                    [&](const int axis, const VectorDi face) {
                        auto [cell0, cell1, val0, val1] = Face_Neighbor_Cells_And_Values(cell_type, axis, face, INVALID);
                        if (val0 == CellType::SOLID || val1 == CellType::SOLID) return true;
                        return false;
                    }
                );
                velocity_bc.Init(face_fixed, velocity);

                poisson.Init(velocity.grid);
                MG_precond.Allocate_Poisson(velocity.grid);
                MGPCG.Init(&poisson, &MG_precond, false, -1, 1e-6);
            }
        }

        void Output_Mesh_As_VTU(const VertexMatrix<real, d> &verts, const ElementMatrix<d> &elements, const std::string file_name) {
            Typedef_VectorD(d);

            // setup VTK
            vtkNew<vtkXMLUnstructuredGridWriter> writer;
            vtkNew<vtkUnstructuredGrid> unstructured_grid;

            vtkNew<vtkPoints> nodes;
            nodes->Allocate(verts.rows());
            vtkNew<vtkCellArray> cellArray;

            for (int i = 0; i < verts.rows(); i++) {
                Vector<real, d> pos = verts.row(i).template cast<real>();
                Vector3 pos3 = MathFunc::V<3>(pos);
                nodes->InsertNextPoint(pos3[0], pos3[1], pos3[2]);
            }
            unstructured_grid->SetPoints(nodes);

            if constexpr (d == 2) {
                for (int i = 0; i < elements.rows(); i++) {
                    vtkNew<vtkLine> line;
                    line->GetPointIds()->SetId(0, elements(i, 0));
                    line->GetPointIds()->SetId(1, elements(i, 1));
                    cellArray->InsertNextCell(line);
                }
                unstructured_grid->SetCells(VTK_LINE, cellArray);
            }
            else if constexpr (d == 3) {
                for (int i = 0; i < elements.rows(); i++) {
                    vtkNew<vtkTriangle> triangle;
                    triangle->GetPointIds()->SetId(0, elements(i, 0));
                    triangle->GetPointIds()->SetId(1, elements(i, 1));
                    triangle->GetPointIds()->SetId(2, elements(i, 2));
                    cellArray->InsertNextCell(triangle);
                }
                unstructured_grid->SetCells(VTK_TRIANGLE, cellArray);
            }

            writer->SetFileName(file_name.c_str());
            writer->SetInputData(unstructured_grid);
            writer->Write();
        }

        virtual void Output(DriverMetaData& metadata){
            //// velocity
            Info("write velocity...");
            std::string vts_name = fmt::format("vts{:04d}.vts", metadata.current_frame);
            bf::path vtk_path = metadata.base_path / bf::path(vts_name);
            VTKFunc::Write_VTS(velocity, vtk_path.string());
            // VTKFunc::Write_VTS<real, d, DataHolder side>(pls.vel, vtk_path.string());
            
            {
                Info("write surface...");
                VertexMatrix<real, d> verts; ElementMatrix<d> elements;
                Marching_Cubes<real, d, HOST>(verts, elements, pls.levelset.phi);
                std::string surface_name = fmt::format("surface{:04d}.vtu", metadata.current_frame);
                bf::path surface_path = metadata.base_path / bf::path(surface_name);
                Output_Mesh_As_VTU(verts, elements, surface_path.string());
            }

            if(compare_with_levelset){
                Info("write surface levelset only...");
                VertexMatrix<real, d> verts; ElementMatrix<d> elements;
                Marching_Cubes<real, d, HOST>(verts, elements, ls.phi);
                std::string surface_name = fmt::format("surface_ls{:04d}.vtu", metadata.current_frame);
                bf::path surface_path = metadata.base_path / bf::path(surface_name);
                Output_Mesh_As_VTU(verts, elements, surface_path.string());
            }
            
            ///// particles
            {
                Info("write particles...");
                std::string particles_name = fmt::format("particles{:04d}.vtu", metadata.current_frame);
                bf::path particles_path = metadata.base_path / bf::path(particles_name);
                VTKFunc::Write_VTU_Particles<d>(pls.particles.xRef(), pls.particles.nRef(), particles_path.string());
            }

            {
                Info("write particles vel...");
                std::string particles_name = fmt::format("particles_velocity{:04d}.vtu", metadata.current_frame);
                bf::path particles_path = metadata.base_path / bf::path(particles_name);
                VTKFunc::Write_VTU_Particles<d>(pls.particles.xRef(), pls.particles.vRef(), particles_path.string());
            }
            
            // {
            //     Info("write debug helper");
            //     VertexMatrix<real, d> verts; ElementMatrix<d> elements;
            //     geo_debug_buffer.Convert_Mesh(verts, elements);
            //     std::string surface_name = fmt::format("debug_mesh{:04d}.vtu", metadata.current_frame);
            //     bf::path surface_path = metadata.base_path / bf::path(surface_name);
            //     Output_Mesh_As_VTU(verts, elements, surface_path.string());
            // }

            // {
            //     Info("write debug particles...");
            //     std::string particles_name = fmt::format("debug_particles{:04d}.vtu", metadata.current_frame);
            //     bf::path particles_path = metadata.base_path / bf::path(particles_name);
            //     VTKFunc::Write_VTU_Particles<d>(debug_particles.xRef(), debug_particles.nRef(), particles_path.string());
            // }

        };

        //can return inf
        virtual real CFL_Time(const real cfl){
            real dx = velocity.grid.dx;
            real max_vel = GridEulerFunc::Linf_Norm(velocity);
            return dx * cfl / max_vel;
        };

        virtual void Advance(DriverMetaData& metadata){
            real dt = metadata.dt;
            if(use_normal_dynamics){
                Info("Advect Velocity");
                velocity_dev=velocity;
                Advection<IntpLinearPadding0>::Advect(dt, temp_velocity_dev, velocity_dev, velocity_dev);
                velocity = temp_velocity_dev;
                velocity_dev = temp_velocity_dev;
            }

            Info("Advect PLS...");
            Advect_PLS(dt);
            
            Info("Fix Particle Geometry...");
            Fix_Particle_Geometry();

            if(use_particle_correction){
                Info("Correct PLS...");
                Correct_PLS();
            }

            if(use_normal_dynamics) Compute_Normal_Dynamics(dt);
            if(use_tangent_dynamics) Compute_Tangent_Dynamics(dt);
            // if(metadata.current_frame%10==1)
            //     Regenerate_PLS();
        };


        void Advect_PLS(real dt){
            

            //// advect levelset
            {
                Field<real, d> temp_phi = pls.levelset.phi;
                Advection<IntpLinearClamp>::Advect(dt, temp_phi, pls.levelset.phi, velocity);
                pls.levelset.phi = temp_phi;
            }

            //// advect levelset for comparison
            if(compare_with_levelset){
                Field<real, d> temp_phi = ls.phi;
                Advection<IntpLinearClamp>::Advect(dt, temp_phi, ls.phi, velocity);
                ls.phi = temp_phi;
                ls.Fast_Marching(-1);//will calculate whole field
            }

            //// advect particle & normal
            #pragma omp parallel for
            for (int i = 0; i < pls.particles.Size(); i++) {
                VectorD x=pls.particles.x(i);
                VectorD v=IntpLinear::Face_Vector(velocity,x);
                x+=dt * v;
                pls.particles.x(i) = x;
                pls.particles.v(i) = v;
                VectorD n=pls.particles.n(i);
                
                VectorD acc_n=VectorD::Zero();
                MatrixD grad_v=FieldSampler<IntpLinearClamp>::Gradient(velocity,x);

                for(int di=0;di<d;di++)
                for(int dj=0;dj<d;dj++)
                    acc_n(di)-=grad_v(dj,di)*n(dj);

                for(int dj=0;dj<d;dj++)
                for(int dk=0;dk<d;dk++)
                    acc_n+=n(dj)*n(dk)*grad_v(dj,dk)*n;
                pls.particles.n(i)=(n+acc_n*dt).normalized();
            }
        }

        void Correct_PLS(){
            if(correction_type==CorrectionType::NONE){
            }else if(correction_type==CorrectionType::CLOSEST_PARTICLE){
                pls.Correct_Levelset_With_Closest_Particles(local_geom);
            }else if(correction_type==CorrectionType::MLS_PROJECTION){
                // Correct_Levelset_With_Normal_MLS_Projection(pls.levelset, pls.particles);
                pls.Correct_Levelset_With_Normal_MLS_Projection(local_geom);
            }
            
            pls.levelset.Fast_Marching(-1);
            //// correct twice for better accuracy at interface
            if(correction_type==CorrectionType::NONE){
            }else if(correction_type==CorrectionType::CLOSEST_PARTICLE){
                pls.Correct_Levelset_With_Closest_Particles(local_geom, true);
            }else if(correction_type==CorrectionType::MLS_PROJECTION){
                // Correct_Levelset_With_Normal_MLS_Projection(pls.levelset, pls.particles);
                pls.Correct_Levelset_With_Normal_MLS_Projection(local_geom);
            }
        }

        void Fix_Particle_Geometry(){
            pls.Fix_Particle_Normal(local_geom);
            pls.Reseed_Insert_Particles(local_geom);
            pls.Reseed_Remove_Particles(local_geom);

            Update_Particle_Area(pls.particles);
        }

        void Regenerate_PLS(){
            Regenerate_Particles(pls.particles, pls.levelset);
            Info("reseed particles");
        }

        void Compute_Normal_Dynamics(real dt){
            // SurfaceTension::Explicit_Mean_Curvature(dt, velocity, pls.levelset, 1.0, pls.levelset.phi.grid.dx*5, pls.levelset.phi.grid.dx*3);
            
            //projection
            //vel_div=div(velocity)
            temp_field_dev=pls.levelset.phi;
            ExteriorDerivativePadding0::Apply(temp_field_dev, velocity_dev);

            div_host = temp_field_dev;
            Update_Poisson_System(fixed_host, vol_host, div_host);
            Apply_Jump_Condition_To_b(div_host, cell_type, pls.levelset, vol_host, sigma, dt);

            poisson.Init(fixed_host, vol_host);
            MG_precond.Update_Poisson(poisson, 2, 2);
            temp_field_dev = div_host;

            pressure_dev.Init(temp_field_dev.grid);
            auto [iter, res] = MGPCG.Solve(pressure_dev.Data(), temp_field_dev.Data());
            Info("Solve poisson with {} iters and residual {}", iter, res);

            //Info("solved pressure: \n{}", pressure_dev);

            //velocity+=grad(p)
            ExteriorDerivativePadding0::Apply(temp_velocity_dev, pressure_dev);
            temp_velocity_dev *= poisson.vol;

            velocity_dev += temp_velocity_dev;

            velocity=velocity_dev;
            Apply_Jump_Condition_To_Velocity(velocity, cell_type, pls.levelset, vol_host, sigma, dt);
            velocity_dev=velocity;

            velocity_bc.Apply(velocity_dev);

            Info("After projection max velocity {}", GridEulerFunc::Linf_Norm(velocity_dev));

            Extrapolation(velocity_dev);
            velocity_bc.Apply(velocity_dev);

            velocity=velocity_dev;
        }

        void Compute_Tangent_Dynamics(real dt){
            ;
        }

    //// normal dynamics
    public:
    void Update_Poisson_System(Field<bool, d>& fixed, FaceField<real, d>& vol, Field<real, d>& div) {
        //Step 1: decide cell types
        cell_type.Calc_Nodes(
            [&](const VectorDi cell) {
                if (cell_type(cell) == SOLID) {
                    return SOLID;
                }
                else if (pls.levelset.phi(cell) >= 0) return EMPTY;
                else return FLUID;
            }
        );

        //Step 2: decide fixed from cell types
        fixed.Init(cell_type.grid);
        fixed.Calc_Nodes(
            [&](const VectorDi cell) {
                if (cell_type(cell) == FLUID) return false;
                else return true;
            }
        );

        //Step 3: set div to 0 for air and solid cells
        div.Init(cell_type.grid);
        div.Exec_Nodes(
            [&](const VectorDi cell) {
                if (cell_type(cell) != FLUID) div(cell) = 0;
            }
        );

        //Step 4: set vol, and modify div additionally on the interface
        vol.Init(cell_type.grid);
        vol.Calc_Faces(
            [&](const int axis, const VectorDi face)->real {
                auto [cell0, cell1, type0, type1] = Face_Neighbor_Cells_And_Values(cell_type, axis, face, INVALID);
                
                //order: invalid, fluid,air,solid
                if (type0 > type1) {
                    std::swap(cell0, cell1);
                    std::swap(type0, type1);
                }
                
                if (type0 == INVALID && type1 == INVALID) return 0;
                if (type0 == INVALID && type1 == FLUID) return 1.0 / liquid_density;
                if (type0 == INVALID && type1 == EMPTY) return 1.0 / air_density;
                if (type0 == INVALID && type1 == SOLID) return 0;
                if (type0 == FLUID && type1 == FLUID) return 1.0 / liquid_density;
                if (type0 == FLUID && type1 == EMPTY) {//interface!
                    //todo: modify div
                    real phi0 = pls.levelset.phi(cell0), phi1 = pls.levelset.phi(cell1);
                    real theta = phi0 / (phi0 - phi1);
                    real den0 = liquid_density, den1 = air_density;
                    real density = theta * den0 + (1.0 - theta) * den1;
                    return 1.0 / density;
                }
                if (type0 == FLUID && type1 == SOLID) return 0;
                //type0,type1 in {EMPTY,SOLID}, the result is 0
                return 0;
            }
        );
    }
    void Extrapolation(FaceFieldDv<real, d>& velocity) {
        const auto grid = velocity.grid;
        FaceField<bool, d> face_valid_mask(grid);
        face_valid_mask.Calc_Faces(
            [&](const int axis, const VectorDi face) {
                VectorD pos = grid.Face_Center(axis, face);
                return IntpLinearClamp::Value(pls.levelset.phi, pos) < 0;
            }
        );

        std::array<InterpolatorLinearWithMask<real, d, HOST>, d> intp;
        for (int i = 0; i < d; i++) intp[i].Init_Shallow(face_valid_mask.Face_Reference(i));
        FaceField<real, d> velocity_host = velocity;
        velocity_host.Calc_Faces(
            [&](const int axis, const VectorDi face) {
                VectorD pos = grid.Face_Center(axis, face);
                real face_phi = IntpLinearClamp::Value(pls.levelset.phi, pos);
                if (face_phi > 0) {
                    VectorD interface_pos = pls.levelset.Closest_Point(pos);
                    return intp[axis].Value1(velocity_host.Face_Reference(axis), interface_pos);
                }
                else return velocity_host(axis, face);
            }
        );

        velocity = velocity_host;
    }

    //// pressure jump on levelset
    inline real Pressure_Jump(const LevelSet<d>& levelset, const VectorD& pos, real sigma, real dt) const {
        real curvature = LevelSetUtil<d>::Curvature(levelset,pos); return dt * sigma * curvature;
    }

    //// pressure jump on mls particle
    inline real Pressure_Jump(const LevelSet<d>& levelset, const EulerianParticles<d>& particles, std::shared_ptr<PointLocalGeometry<d>> local_geom, const VectorD& pos, real sigma, real dt){
        
        int ci=local_geom->Closest_Point(pos);
        VectorD intf_x=particles.x(ci);
        VectorD normal=particles.n(ci);
        MatrixD local_frame;
        LeastSquares::MLSPointSet<d-1> mls;
        
        {
            local_frame=local_geom->Spawn_Local_Frame(normal);
            // LeastSquares::MLSPointSet<d-1> mls;
            Array<int> nbs;
            int n=local_geom->Fit_Local_Geometry_MLS(intf_x,local_frame, nbs, mls);

            //// find closest point on MLS surface
            VectorD u=pos-intf_x;
            VectorD local_pos=local_geom->Local_Coords(u,local_frame);
            VectorD new_local_pos=local_geom->Closest_Point_To_Local_MLS(local_pos, mls, 5);   

            //// get normal vector
            VectorD local_normal=local_geom->Normal_On_Local_MLS(new_local_pos.head(d-1), mls);
            VectorD new_normal=local_geom->Unproject_To_World(local_normal,local_frame).normalized();
            normal=(new_normal.dot(normal)>0)?new_normal:-new_normal;

            //// limit local pos on tangent plane
            Array<VectorT> local_nbs_t_pos;
            for(int i=0;i<nbs.size();i++){
                VectorD u=particles.x(nbs[i])-intf_x;
                local_nbs_t_pos.push_back(local_geom->Local_Coords(u,local_frame).head(d-1));
            }
            VectorT limit_local_pos=local_geom->Limit_Local_Tangent(new_local_pos.head(d-1), local_nbs_t_pos);
            intf_x=local_geom->Unproject_To_World(new_local_pos, local_frame)+intf_x;
        }

        std::function<real(const int)> Point_Position[d];
        VectorD curvature;
        for(int di=0;di<d;di++){
            Point_Position[di] = [&](const int idx)->real
                {return particles.x(idx)[di]; };
            curvature[di]=local_geom->Laplacian_MLS(intf_x, local_frame, mls, Point_Position[di]);
        }

        VectorD ls_normal=LevelSetUtil<d>::Normal(levelset,pos);
        if(curvature.dot(ls_normal)>0) return curvature.norm()*sigma*dt;
        else return -curvature.norm()*sigma*dt;
    }
    

    void Apply_Jump_Condition_To_b(Field<real, d>& div, const Field<CellType, d>& cell_type, const LevelSet<d>& levelset, const FaceField<real, d>& vol, real sigma, real dt) {
        
        real one_over_dx = (real)1 / div.grid.dx;
        Grid<d>& grid=div.grid;
        div.Exec_Nodes([&](const VectorDi cell) {
            if (cell_type(cell) != FLUID) return;
            for(int ni=0;ni<grid.Neighbor_Node_Number();ni++){
                const VectorDi nb_cell=grid.Neighbor_Node(cell,ni);
                if(levelset.Is_Interface(cell, nb_cell)){
                    real theta=levelset.Theta(levelset.phi(cell), levelset.phi(nb_cell));
                    auto [axis,face]=GridUtil<d>::Neighbor_Face(grid,cell,ni);
                    

                    VectorD intf_pos = ((real)1 - theta) * grid.Position(cell) + theta * grid.Position(nb_cell);
                    real p_jump=Pressure_Jump(pls.levelset, intf_pos, sigma, dt);
                    // real p_jump=Pressure_Jump(pls.levelset, pls.particles, local_geom, intf_pos, sigma, dt);
                    
                    real coef=1.0;
                    real intf_coef=vol(axis,face);
                    div(cell) -= coef*intf_coef*p_jump;
                }
            }
        });
    }

    void Apply_Jump_Condition_To_Velocity(FaceField<real,d>& velocity, const Field<CellType, d>& cell_type, const LevelSet<d>& levelset, const FaceField<real,d>&vol, real sigma, real dt){
        
        Grid<d>& grid=velocity.grid;
        velocity.Exec_Faces([&](const int axis, const VectorDi face) {
            auto [cell0, cell1] = Face_Neighbor_Cells<d>(grid, axis, face);
            if(!grid.Valid(cell0) || !grid.Valid(cell1)) return;
            if(!levelset.Is_Interface(cell0, cell1)) return;
            real theta=levelset.Theta(levelset.phi(cell0), levelset.phi(cell1));
            VectorD intf_pos = ((real)1 - theta) * grid.Position(cell0) + theta * grid.Position(cell1);
            real p_jump=Pressure_Jump(pls.levelset, intf_pos, sigma, dt);
            // real p_jump=Pressure_Jump(pls.levelset, pls.particles, local_geom, intf_pos, sigma, dt);
            if(cell_type(cell0) == FLUID)
                velocity(axis,face)-=p_jump;
            else
                velocity(axis,face)+=p_jump;
        });

    }

    //// particle geometry
    public:
        void Update_Particle_Area(EulerianParticles<d>& particles){}

    //// regenerate pls
    public:
        void Regenerate_Particles(EulerianParticles<d>& particles, LevelSet<d>& levelset){
            Array<VectorD> temp_x;
            LevelSetUtil<d>::Sample_On_Levelset_Surface(temp_x, levelset, 40, 1, 10);
            particles.Resize(temp_x.size());
            for(int i=0;i<temp_x.size();i++){
                particles.x(i)=temp_x[i];
                particles.v(i)=VectorD::Zero();
            }
        }
    };
}