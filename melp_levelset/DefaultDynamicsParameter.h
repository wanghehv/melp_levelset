//////////////////////////////////////////////////////////////////////////
// default dynamics parameter
// Copyright (c) (2022-), Hui Wang
// This file is part of MELPLEVELSET, whose distribution is governed by the LICENSE file.
//////////////////////////////////////////////////////////////////////////
#pragma once
#include "Common.h"

namespace Meso{
    namespace DefaultDynamicsParameter{
    
        const real thickness=5e-7; //// 500 nm for avg
        const real concentration=1e-7; //// 10e-7 mol/m2 for avg

        // const real sigma=30*1e-3; //// 30 mN/m, in "Surface tension of flowing soap films"'
        
        ////"Chemomechanical Simulation of Soap Film Flow on Spherical Bubbles"
        const real sigma=7.275*1e-2; //// 7.275*10e−2 N/m
        const real gas_constant=8.3144598; //// 8.3144598 J/(mol K)
        const real temperature=298.15; //// 298.15K
        
        const real liquid_density=997; //// 997 kg/(m3)
        const real air_density=1.184; //// 1.184 kg/(m3)

        const real water_dynamic_viscosity=8.9*1e-4; //// Pa s
        const real air_kinetic_viscosity=1.562*1e-5; //// m2/s
        
        const real gravity=-9.8; //// m/s2

        const real ideal_gas_const=8.314; ////J/(K mol)
    };
}