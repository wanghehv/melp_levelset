#include "Advection.h"
#include "AuxFunc.h"
#include "Json.h"
#include "FluidPLS.h"
#include "FluidPLSInitializer.h"
// #include "FluidVLS.h"
// #include "FluidVLSInitializer.h"
#include "FluidMPLS.h"
#include "FluidMPLSInitializer.h"
#include "MLSTest.h"
#include "MLSTestInitializer.h"
#include "Driver.h"
using namespace Meso;

template<int d>
void Run_FluidPLS(json &j) {
	FluidPLS<d> fluid;
	FluidPLSInitializer<d> scene;
	Driver driver;
	driver.Initialize_And_Run(j, scene, fluid);
}

// template<int d>
// void Run_FluidVLS(json &j) {
// 	FluidVLS<d> fluid;
// 	FluidVLSInitializer<d> scene;
// 	Driver driver;
// 	driver.Initialize_And_Run(j, scene, fluid);
// }

template<int d>
void Run_FluidMPLS(json &j) {
	FluidMPLS<d> fluid;
	FluidMPLSInitializer<d> scene;
	Driver driver;
	driver.Initialize_And_Run(j, scene, fluid);
}

template<int d>
void Run_MLSTest(json& j){
	MLSTest<d> test;
	MLSTestInitializer<d> scene;
	Driver driver;
	driver.Initialize_And_Run(j,scene,test);
}

int main(int argc, char **argv) {
	try {
		json j = {
			{
				"driver",
				{
					{"last_frame",10}
				}
			},
			{"scene",json::object()}
		};
		if (argc > 1) {
			std::ifstream json_input(argv[1]);
			json_input >> j;
			json_input.close();
		}

		int dim = Json::Value(j, "dimension", 2);
		std::string simulator=Json::Value(j, "simulator", std::string("fluidpls"));
		
        if (simulator == "fluidpls") {
			if (dim == 2) { Run_FluidPLS<2>(j); }
			else if (dim == 3) { Run_FluidPLS<3>(j); }
		}

		// if (simulator == "fluidvls") {
		// 	if (dim == 2) { Run_FluidVLS<2>(j); }
		// 	else if (dim == 3) { Run_FluidVLS<3>(j); }
		// }

		if (simulator == "fluidmpls") {
			if (dim == 2) { Run_FluidMPLS<2>(j); }
			else if (dim == 3) { Run_FluidMPLS<3>(j); }
		}
		
		if (simulator == "mlstest") {
			if (dim == 2) { Run_MLSTest<2>(j); }
			else if (dim == 3) { Run_MLSTest<3>(j); }
		}
		
	}
	catch (nlohmann::json::exception& e)
	{
		Info("json exception {}", e.what());
	}
	return 0;
}