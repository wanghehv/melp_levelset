#include "ParticleLevelSetHelper.h"

namespace Meso{
    template ParticleLevelSetHelper<2>;
    template ParticleLevelSetHelper<3>;

    //// deprecated: correct levelset as its distance to the closest particle without changing the sign
    template<int d> void ParticleLevelSetHelper<d>::Correct_Levelset_With_Particles_Naive(LevelSet<d>& levelset, EulerianParticles<d>& particles, std::shared_ptr<PointLocalGeometry<d>> local_geom){
        Typedef_VectorD(d);
        Typedef_MatrixD(d);

        Grid<d> grid=levelset.phi.grid;
        local_geom->nbs_searcher->Update_Points(particles.xRef());
        levelset.phi.Exec_Nodes(
            [&](const VectorDi cell) {
                int sign=levelset.phi(cell)>=0?1:-1;
                real min_dist=abs(levelset.phi(cell));

                VectorD cx=grid.Position(cell);
                Array<int> nbs; 
                local_geom->nbs_searcher->Find_Nbs(cx,2*grid.dx,nbs);
                for (int i = 0; i < nbs.size(); i++) {
                    VectorD px=particles.x(nbs[i]);
                    min_dist=std::min(min_dist,(cx-px).norm());
                }
                levelset.phi(cell)=sign*min_dist;
            }
        );
    }

    //// deprecated: correct levelset as its distance to the closest particle with changing the sign
    template<int d> void ParticleLevelSetHelper<d>::Correct_Levelset_With_Closest_Particles(LevelSet<d>& levelset,  EulerianParticles<d>& particles, std::shared_ptr<PointLocalGeometry<d>> local_geom, bool use_default/*=false*/){
        Typedef_VectorD(d);
        Typedef_MatrixD(d);

        Grid<d> grid=levelset.phi.grid;
        local_geom->nbs_searcher->Update_Points(particles.xRef());
        real one_over_dx = (real)1 / grid.dx; real one_over_two_dx = (real).5 * one_over_dx;
        real nbs_radius=2*grid.dx;

        real curvature_threshold=one_over_two_dx*0.5f; //// one_over_two_dx is better than one_over_dx
        // real bias_threshold=grid.dx;
        
        real bias_threshold=2*grid.dx;

        VectorDi evaluated_cell=VectorDi::Zero();
        evaluated_cell(0)=52; evaluated_cell(1)=21;

        levelset.phi.Exec_Nodes(
            [&](const VectorDi cell) {
                int min_sign=levelset.phi(cell)>=0?1:-1;
                real phi=levelset.phi(cell);
                real min_dist=1e8;

                if constexpr(d==2)
                if(cell==evaluated_cell){
                    std::cout<<"cell="<<cell.transpose()<<", x="<<grid.Position(cell).transpose()<<", phi="<<phi<<std::endl;
                }

                // if(use_default)
                //     min_dist=abs(levelset.phi(cell));
                
                VectorD cx=grid.Position(cell);
                real curvature=LevelSetUtil<d>::Curvature(levelset,cx);
                bool change_sign=true;
                // if(abs(curvature)>curvature_threshold)
                //     change_sign=false;

                Array<int> nbs; 
                local_geom->nbs_searcher->Find_Nbs(cx,nbs_radius,nbs);
                for (int i = 0; i < nbs.size(); i++) {
                    VectorD px=particles.x(nbs[i]);
                    VectorD rel=cx-px;

                    real dist=rel.norm();
                    real sign=(phi>=0?1:-1);
                    // if(change_sign) sign=rel.dot(particles.n(nbs[i]))<0?-1:1;
                    if(change_sign) sign=rel.dot(LevelSetUtil<d>::Normal(levelset,px))<0?-1:1;
                    if(dist<min_dist && abs(phi-sign*dist)<bias_threshold){
                    // if(d<min_dist){
                        min_dist=dist;
                        min_sign=sign;
                    }
                }
                if(min_dist!=1e8)
                    levelset.phi(cell)=min_sign*min_dist;
                if constexpr(d==2)
                if(cell==evaluated_cell){
                    std::cout<<"cell="<<cell.transpose()<<", x="<<grid.Position(cell).transpose()<<", phi="<<levelset.phi(cell)<<", min(d*s)="<<min_dist*min_sign<<", change_sign="<<change_sign<<",curv="<<curvature<<":"<<curvature_threshold<<std::endl;
                }
            }
        );

        levelset.phi.Exec_Nodes([&](const VectorDi cell) {
            real phi=levelset.phi(cell);
            real sign=(phi>=0?1:-1);
            real avg_phi=0;
            for(int i=0;i<d;i++){
                real phi_nb=levelset.phi(cell+VectorDi::Unit(i));
                avg_phi+=phi_nb;
                real sign_nb=(phi>=0?1:-1);
                if(sign_nb==sign) return;
            }
            levelset.phi(cell)=avg_phi/(d*2);
        });
    }
    
    //// deprecated: correct the levelset based on mls projection
    template<int d> void ParticleLevelSetHelper<d>::Correct_Levelset_With_MLS_Projection(LevelSet<d>& levelset,  EulerianParticles<d>& particles, std::shared_ptr<PointLocalGeometry<d>> local_geom, int max_iter/*=2*/){
        Typedef_VectorD(d);
        Typedef_MatrixD(d);
        using VectorT=Vector<real,d-1>;                             ////tangential vector type
        using VectorTi=Vector<int,d-1>;                             ////tangential vector int
        
        Grid<d> grid=levelset.phi.grid;
        local_geom->nbs_searcher->Update_Points(particles.xRef());
        real one_over_dx = (real)1 / grid.dx; real one_over_two_dx = (real).5 * one_over_dx;
        // real nbs_radius=2*grid.dx;

        real curvature_threshold=one_over_two_dx*0.5f; //// one_over_two_dx is better than one_over_dx

        real bias_threshold=2*grid.dx;

        levelset.phi.Exec_Nodes([&](const VectorDi cell) {

            real phi=levelset.phi(cell);
            // if(abs(phi)>3*levelset.phi.grid.dx) return;

            real sign=(phi>=0?1:-1);
            VectorD cell_x=grid.Position(cell);
            real curvature=LevelSetUtil<d>::Curvature(levelset,cell_x);

            bool change_sign=true;
            if(abs(curvature)>curvature_threshold)
                change_sign=false;

            VectorD intf_x=LevelSetUtil<d>::Closest_Point_With_Iterations(levelset, cell_x, 5);
            // if(abs(levelset.Phi(intf_x))>3*levelset.phi.grid.dx) return;
            
            // if(nbs.size()<d*3) return; //// skip no neighbor particles
            for(int iter=0;iter<max_iter;iter++){
                MatrixD local_frame=local_geom->Local_Frame(intf_x);
                LeastSquares::MLSPointSet<d-1> mls;
                Array<int> nbs;
                int n=local_geom->Fit_Local_Geometry_MLS(intf_x,local_frame,nbs, mls);
                if(n<d*2) break;
                // VectorX mls_c=local_geom->Fit_Local_Geometry_MLS(intf_x,local_frame);

                VectorD u=cell_x-intf_x;
                VectorD local_pos=local_geom->Local_Coords(u,local_frame);
                VectorD new_local_pos=local_geom->Closest_Point_To_Local_MLS(local_pos, mls, 5);
                intf_x=local_geom->Unproject_To_World(new_local_pos, local_frame)+intf_x;
            }

            VectorD rel=cell_x-intf_x;
            real dist=rel.norm();
            if(change_sign) {
                real new_sign=rel.dot(LevelSetUtil<d>::Normal(levelset,intf_x))<0?-1:1;
                if(abs(phi-new_sign*dist)<bias_threshold)
                    sign=new_sign;
            }

            levelset.phi(cell)=sign*rel.norm();
        });
    }
}