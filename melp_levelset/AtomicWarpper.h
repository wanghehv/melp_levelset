#include <atomic>
#include <vector>

template <typename T>
struct AtomicWarpper
{
  std::atomic<T> val;

  AtomicWarpper()
    :val()
  {}

  AtomicWarpper(const std::atomic<T> &_val)
    :val(_val.load())
  {}

  AtomicWarpper(const AtomicWarpper &other)
    :val(other.val.load())
  {}

  AtomicWarpper &operator=(const AtomicWarpper &other)
  {
    val.store(other.val.load());
    return *this;
  }
};
