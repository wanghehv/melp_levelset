//////////////////////////////////////////////////////////////////////////
// Particle levelset helper, provide geometric functions used for surface particles in the levelset
// Copyright (c) (2022-), Hui Wang
// This file is part of MELPLEVELSET, whose distribution is governed by the LICENSE file.
//////////////////////////////////////////////////////////////////////////
#pragma once

#include "EulerianParticles.h"
#include "LagrangianParticles.h"
#include "LevelSet.h"
#include "AtomicWarpper.h"

#include "LevelSetUtil.h"
#include "PointUtil.h"
#include "PointLocalGeometry.h"
#include "Timer.h"

namespace Meso {
    template<class T>
    inline constexpr T constexpr_pow(const T base, unsigned const exponent)
    {
        // (parentheses not required in next line)
        return (exponent == 0)     ? 1 :
            (exponent % 2 == 0) ? constexpr_pow(base, exponent/2)*constexpr_pow(base, exponent/2) :
            base * constexpr_pow(base, (exponent-1)/2) * constexpr_pow(base, (exponent-1)/2);
    }

    template<int d> 
    class ParticleLevelSetHelper{
        Typedef_VectorD(d);
        Typedef_MatrixD(d);
        using VectorT=Vector<real,d-1>;    ////tangential vector type
        using VectorTi=Vector<int,d-1>;    ////tangential vector int
        using MatrixT=Matrix<real,d-1,d-1>;

public:

        static void Correct_Levelset_With_Particles_Naive(LevelSet<d>& levelset, EulerianParticles<d>& particles, std::shared_ptr<PointLocalGeometry<d>> local_geom);
        
        static void Correct_Levelset_With_Closest_Particles(LevelSet<d>& levelset, EulerianParticles<d>& particles, std::shared_ptr<PointLocalGeometry<d>> local_geom, bool use_default=false);
        
        static void Correct_Levelset_With_MLS_Projection(LevelSet<d>& levelset, EulerianParticles<d>& particles, std::shared_ptr<PointLocalGeometry<d>> local_geom, int max_iter=2);
        

        static void Correct_Levelset_With_Normal_MLS_Projection(LevelSet<d>& levelset,  EulerianParticles<d>& particles, std::shared_ptr<PointLocalGeometry<d>> local_geom, int max_iter=2){
            
            Grid<d> grid=levelset.phi.grid;
            local_geom->nbs_searcher->Update_Points(particles.xRef());
            local_geom->normal_ptr=std::make_shared<Array<VectorD> >(particles.nRef());
            real one_over_dx = (real)1 / grid.dx; real one_over_two_dx = (real).5 * one_over_dx;
            // real nbs_radius=2*grid.dx;

            real bias_threshold=grid.dx*2; //// hyper param, 2dx to avoid sub-grid details
            // VectorDi ref_cell; ref_cell<<7,21;


            levelset.phi.Exec_Nodes([&](const VectorDi cell) {

                real phi=levelset.phi(cell);
                if(abs(phi)>2*levelset.phi.grid.dx) return;

                real sign=(phi>=0?1:-1);
                VectorD cell_x=grid.Position(cell);
                
                int ci=local_geom->Closest_Point(cell_x);
                VectorD intf_x=particles.x(ci);
                VectorD normal=particles.n(ci);
                MatrixD local_frame;
                if(abs(levelset.Phi(intf_x))>2*levelset.phi.grid.dx) return;
                
                int iter=0;
                for(;iter<max_iter;iter++){
                    local_frame=local_geom->Spawn_Local_Frame(normal);
                    LeastSquares::MLSPointSet<d-1> mls;
                    Array<int> nbs;
                    int n=local_geom->Fit_Local_Geometry_MLS(intf_x,local_frame, nbs, mls);

                    if(n<d*2) break;

                    //// find closest point on MLS surface
                    VectorD u=cell_x-intf_x;
                    VectorD local_pos=local_geom->Local_Coords(u,local_frame);
                    VectorD new_local_pos=local_geom->Closest_Point_To_Local_MLS(local_pos, mls, 5);

                    //// limit local pos on tangent plane
                    Array<VectorT> local_nbs_t_pos;
                    for(int i=0;i<nbs.size();i++){
                        VectorD u=particles.x(nbs[i])-intf_x;
                        local_nbs_t_pos.push_back(local_geom->Local_Coords(u,local_frame).head(d-1));
                    }
                    VectorT limit_local_pos=local_geom->Limit_Local_Tangent(new_local_pos.head(d-1), local_nbs_t_pos);
                    // if(cell==ref_cell) {
                    //     std::cout<<"local pos:"<<new_local_pos.transpose()<<" -> "<<limit_local_pos.transpose()<<std::endl;
                    // }
                    new_local_pos<<limit_local_pos, mls(limit_local_pos);

                    //// get normal vector
                    VectorD local_normal=local_geom->Normal_On_Local_MLS(new_local_pos.head(d-1), mls);
                    VectorD new_normal=local_geom->Unproject_To_World(local_normal,local_frame).normalized();
                    normal=(new_normal.dot(normal)>0)?new_normal:-new_normal;
                    

                    // if(cell==ref_cell) {
                    //     debug_pos.push_back(intf_x);
                    //     debug_normal.push_back(normal);

                    //     //// print curve here to visualize the surface!!
                    //     {
                    //         VectorT st; st<<-grid.dx; VectorD sx; sx<<st, mls(st); VectorD wx=local_geom->Unproject_To_World(sx, local_frame)+intf_x;
                    //         std::cout<<wx(0)<<","<<wx(1)<<",0,"<<std::endl;
                    //     }
                    //     {
                    //         VectorT st; st<<-grid.dx/2; VectorD sx; sx<<st, mls(st); VectorD wx=local_geom->Unproject_To_World(sx, local_frame)+intf_x;
                    //         std::cout<<wx(0)<<","<<wx(1)<<",0,"<<std::endl;
                    //     }
                    //     {
                    //         VectorT st; st<<0; VectorD sx; sx<<st, mls(st); VectorD wx=local_geom->Unproject_To_World(sx, local_frame)+intf_x;
                    //         std::cout<<wx(0)<<","<<wx(1)<<",0,"<<std::endl;
                    //     }
                    //     {
                    //         VectorT st; st<<grid.dx/2; VectorD sx; sx<<st, mls(st); VectorD wx=local_geom->Unproject_To_World(sx, local_frame)+intf_x;
                    //         std::cout<<wx(0)<<","<<wx(1)<<",0,"<<std::endl;
                    //     }
                    //     {
                    //         VectorT st; st<<grid.dx; VectorD sx; sx<<st, mls(st); VectorD wx=local_geom->Unproject_To_World(sx, local_frame)+intf_x;
                    //         std::cout<<wx(0)<<","<<wx(1)<<",0,"<<std::endl;
                    //     }
                    // }
                    intf_x=local_geom->Unproject_To_World(new_local_pos, local_frame)+intf_x;
                    
                    // if(cell==ref_cell) {
                    //     std::cout<<"iter="<<iter<<std::endl;
                    //     std::cout<<"new_local_pos="<<new_local_pos.transpose()<<std::endl;
                    //     std::cout<<"new_pos="<<intf_x.transpose()<<std::endl;
                    //     std::cout<<"normal="<<normal.transpose()<<std::endl;
                    //     std::cout<<"n="<<n<<", <mls="<<mls.c.transpose()<<std::endl;
                    // }

                }

                //// update levelset
                VectorD rel=cell_x-intf_x;
                real dist=rel.norm();

                real new_sign=rel.dot(normal)<0?-1:1;
                sign=new_sign;
                
                // if(cell==ref_cell) {
                //     std::cout<<"after iter"<<std::endl;
                //     std::cout<<"cell="<<cell.transpose()<<", phi="<<levelset.phi(cell)<<"->"<<sign*rel.norm()<<std::endl;
                //     std::cout<<"n="<<normal.transpose()<<", rel=" << rel.transpose()<<",nr="<<rel.dot(normal)<<std::endl;
                //     std::cout<<"intf_x="<<intf_x.transpose()<<std::endl;
                // }
                if(iter>0 && abs(levelset.phi(cell)-sign*dist)<bias_threshold)
                    levelset.phi(cell)=sign*dist;
            });
        }

        static void Update_Normal_And_Radius(LevelSet<d>& levelset,  EulerianParticles<d>& particles, std::shared_ptr<PointLocalGeometry<d>> local_geom){
            Typedef_VectorD(d);
            Typedef_MatrixD(d);
            using VectorT=Vector<real,d-1>;    ////tangential vector type
            using VectorTi=Vector<int,d-1>;    ////tangential vector int
            
            Grid<d> grid=levelset.phi.grid;

            // geo_debug_buffer.Clear();
            local_geom->nbs_searcher->Update_Points(particles.xRef());
            local_geom->normal_ptr=std::make_shared<Array<VectorD> >(particles.nRef());
            Array<VectorD> normal_buffer=particles.nRef();
            
            #pragma omp parallel for
            for (int i = 0; i < particles.Size(); i++) {
                VectorD normal=particles.n(i);
                VectorD pos=particles.x(i);
                    
                //// fit mls
                MatrixD local_frame=local_geom->Spawn_Local_Frame(normal);
                LeastSquares::MLSPointSet<d-1> mls;
                Array<int> nbs;
                int n=local_geom->Fit_Local_Geometry_MLS(particles.x(i),local_frame, nbs, mls);
                if(n<0) continue;

                //// fit normal
                VectorD local_normal=local_geom->Normal_On_Local_MLS(VectorT::Zero(), mls);
                VectorD new_normal=local_geom->Unproject_To_World(local_normal,local_frame).normalized();
                new_normal=(new_normal.dot(normal)>0)?new_normal:-new_normal;
                normal_buffer[i]=new_normal;

                // real curv=mls.Curvature(VectorT::Zero());
                
                //// curvature->radius
                // std::function<real(const int)> Point_Position[d];
                // VectorD curvature;
                // for(int di=0;di<d;di++){
                //     Point_Position[di] = [&](const int idx)->real
                //         {return particles.x(idx)[di]; };
                //     curvature[di]=local_geom->Laplacian_MLS(i, local_frame, mls, Point_Position[di]);
                // }
                // if(curvature.norm()<1e-8){
                //     particles.r(i)=(real)1e8;
                // }else
                //     particles.r(i)=(real)1.0/curvature.norm();
                // particles.r(i)=(real)local_geom->v_r;//*(0.99+Random::Uniform(-0.01,0.01));

                // {
                //     LeastSquares::ASMLSPointSet<d> asmls;
                //     Array<int> nbs;
                //     real search_r=std::min(particles.r(i), local_geom->v_r);
                //     int n=local_geom->Fit_Local_Geometry_ASMLS(particles.x(i), search_r, nbs, asmls);
                //     if(n<0) {
                //         particles.r(i)=(real)1e8;
                //     }
                //     else{
                //         particles.r(i)=asmls.Sphere_Radius();
                //     }
                // }
            }
            
            #pragma omp parallel for
            for (int i = 0; i < particles.Size(); i++) {
                particles.n(i)=normal_buffer[i];
            }
        }
    ////
        static void Fix_Particle_Normal(LevelSet<d>& levelset,  EulerianParticles<d>& particles, std::shared_ptr<PointLocalGeometry<d>> local_geom){
            Typedef_VectorD(d);
            Typedef_MatrixD(d);
            using VectorT=Vector<real,d-1>;    ////tangential vector type
            using VectorTi=Vector<int,d-1>;    ////tangential vector int
            
            Grid<d> grid=levelset.phi.grid;

            // geo_debug_buffer.Clear();
            local_geom->nbs_searcher->Update_Points(particles.xRef());
            local_geom->normal_ptr=std::make_shared<Array<VectorD> >(particles.nRef());
            Array<VectorD> normal_buffer(particles.Size());
            
            #pragma omp parallel for
            for (int i = 0; i < particles.Size(); i++) {
                normal_buffer[i]=particles.n(i);
            }
            
            #pragma omp parallel for
            for (int i = 0; i < particles.Size(); i++) {
                VectorD normal=particles.n(i);
                VectorD pos=particles.x(i);

                //// choose fitting method
                bool use_asmls=false;
                {
                    Array<int> nbs;
                    local_geom->nbs_searcher->Find_Nbs(pos, grid.dx, nbs); ////TODO: hyper param!
                    real reverse_nbs_dist=1e8;
                    for(int j=0;j<nbs.size();j++){
                        int nb=nbs[j];
                        VectorD nb_normal=particles.n(nb);
                        VectorD nb_pos=particles.x(nb);
                        if(nb_normal.dot(normal)<0.5f)
                            reverse_nbs_dist=std::min(reverse_nbs_dist,(pos-nb_pos).norm());
                    }
                    if(reverse_nbs_dist<grid.dx){
                        use_asmls=true;
                    }
                }
                
                if(use_asmls){
                    //// fit asmls
                    LeastSquares::ASMLSPointSet<d> asmls;
                    Array<int> nbs;
                    real search_r=std::max(grid.dx*0.5,std::min(particles.r(i), local_geom->v_r));
                    int n=local_geom->Fit_Local_Geometry_ASMLS(particles.x(i), search_r, nbs, asmls);
                    if(n<0 || asmls.Sphere_Radius()>grid.dx*0.5f) use_asmls=false;
                    else{
                        // VectorD new_normal=asmls.Grad(particles.x(i)).normalized();
                        // new_normal=(new_normal.dot(normal)>0)?new_normal:-new_normal;
                        // normal_buffer[i]=new_normal;

                        // geo_debug_buffer.Add_Debug_Circle(asmls.Sphere_Center(), asmls.Sphere_Radius());
                        // geo_debug_buffer.Add_Debug_Line(asmls.Sphere_Center(), particles.x(i));

                        particles.r(i)=asmls.Sphere_Radius();
                        // particles.v(i)=1.0/particles.r(i)*new_normal;
                    }
                }
                if(!use_asmls){
                    //// fit mls
                    MatrixD local_frame=local_geom->Spawn_Local_Frame(normal);
                    LeastSquares::MLSPointSet<d-1> mls;
                    Array<int> nbs;
                    int n=local_geom->Fit_Local_Geometry_MLS(particles.x(i),local_frame, nbs, mls);
                    if(n<0) continue;

                    //// fit normal
                    VectorD local_normal=local_geom->Normal_On_Local_MLS(VectorT::Zero(), mls);
                    VectorD new_normal=local_geom->Unproject_To_World(local_normal,local_frame).normalized();
                    new_normal=(new_normal.dot(normal)>0)?new_normal:-new_normal;
                    normal_buffer[i]=new_normal;

                    //// curvature->radius
                    std::function<real(const int)> Point_Position[d];
                    VectorD curvature;
                    for(int di=0;di<d;di++){
                        Point_Position[di] = [&](const int idx)->real
                            {return particles.x(idx)[di]; };
                        curvature[di]=local_geom->Laplacian_MLS(i, local_frame, mls, Point_Position[di]);
                    }
                    if(curvature.norm()<1e-8){
                        // std::cout<<"curvature too small: curv="<<curvature.transpose()<<",|curv|="<<curvature.norm()<<",n="<<n<<std::endl;
                        particles.r(i)=(real)1e8;
                    }else
                        particles.r(i)=(real)1.0/curvature.norm();
                    // particles.v(i)=curvature;

                }
            }
            
            #pragma omp parallel for
            for (int i = 0; i < particles.Size(); i++) {
                particles.n(i)=normal_buffer[i];
            }
        }

        static void Reseed_Remove_Particles(LevelSet<d>& levelset,  EulerianParticles<d>& particles, std::shared_ptr<PointLocalGeometry<d>> local_geom, bool verbose=false){
            Timer timer;
            //// remove particles that are too close
            local_geom->nbs_searcher->Update_Points(particles.xRef());
            if(verbose) Info("ParticleLevelSetHelper::Reseed_Remove_Particles::update point, time={}", timer.Lap_Time());
            // std::priority_queue<int, Array<int>, std::greater<int>> remove_heap;
            std::set<int> remove_set;
            Array<std::pair<int, int>> remove_pairs;//// record the remove pair for redistribution (i, insert_id)

            int k=64;////64 points on the circle
            const real pi=CommonConstants::pi;
            const real sqrt_pi=sqrt(CommonConstants::pi);
            std::function<real(const real)> mean_particle_distance[4]={
                [&](const real r)->real {return 0; },//// placehold for d=0
                [&](const real r)->real {return 0; },//// placehold for d=1
                [&](const real r)->real {return (real)2.0*pi*r/k; },//// for d=2
                [&](const real r)->real {return (real)2.0*sqrt_pi*r/k; } //// for d=3
            };

            if(verbose) std::cout<<"Deletion param: v_r="<<local_geom->v_r<<
                ", delete distance threshold="<<mean_particle_distance[d](local_geom->v_r)<<std::endl;

        #pragma omp parallel for
            for(int i=0;i<particles.Size();i++){
                VectorD x_i=particles.x(i);
                real r_i=particles.r(i);
                int j=i;
                {
                    Array<int> nbs;
                    local_geom->nbs_searcher->Find_K_Nearest_Nbs(x_i, 2, nbs);
                    for(int jj=0;jj<nbs.size();jj++){
                        if(nbs[jj]!=i) j=nbs[jj];
                    }
                }
                VectorD x_j=particles.x(j);
                real r_j=particles.r(j);
                real dist=(x_i-x_j).norm();

                real dist_thres;
                if constexpr(d==2) dist_thres=mean_particle_distance[d](std::min(local_geom->v_r, r_i))*0.25f;
                if constexpr(d==3) dist_thres=mean_particle_distance[d](std::min(local_geom->v_r, r_i))*1.8f;

                if(r_i<r_j && dist<dist_thres && particles.t(j)!=1){
        #pragma omp critical
                    {
                        remove_set.insert(j);
                        remove_pairs.push_back(std::make_pair(i,j));                        
                    }
                }
                if(r_i==r_j && i<j && dist<dist_thres && particles.t(j)!=1){
        #pragma omp critical
                    {
                        remove_set.insert(j);
                        remove_pairs.push_back(std::make_pair(i,j));                        
                    }
                }
                // else if(dist<dist_thres) std::cout<<"r="<<r_i<<","<<r_j<<std::endl;
            }
            if(verbose) Info("ParticleLevelSetHelper::Reseed_Remove_Particles::update remove set, time={}", timer.Lap_Time());

            //// TODO: redistribute the attributes!
            // for (auto itr=remove_set.begin();itr!=remove_set.end();itr++){
            //     int p_i=*itr;
            //     const VectorD& x_i=particles.x(p_i);
            //     const VectorD& v_i=particles.v(p_i);
            //     real V_i=particles.V(p_i);
            //     Array<int> nbs; local_geom->nbs_searcher->Find_Nbs(x_i, local_geom->v_r, nbs);

            //     //// constraint the direction
            //     Array<int> valid_nbs;
            //     VectorD normal=(*local_geom->normal_ptr)[p_i];
            //     for (size_t i = 0; i < nbs.size(); i++) {
            //         if((*local_geom->normal_ptr)[nbs[i]].dot(normal)>0)
            //             valid_nbs.push_back(nbs[i]);
            //     }
            //     nbs=valid_nbs;
            //     real V_js=0;
            //     VectorD torque_js=VectorD::Zero();
            //     VectorD x_c=VectorD::Zero();
            //     for(int jj=0;jj<nbs.size();jj++){
            //         int p_j=nbs[jj]; 
            //         VectorD x_j=particles.x(p_j);
            //         x_c+=particles.V(p_j)*x_j;
            //         if(p_j==p_i) continue;
            //         V_js+=particles.V(p_j);
            //         torque_js+=particles.x(p_j)*particles.V(p_j);
            //     }
            //     x_c=x_c/(V_js+V_i); /// bar(x)
            //     torque_js-=V_js*x_c;/// sum{V_j(x_j-x_c)}

            //     for(int jj=0;jj<nbs.size();jj++){
            //         int p_j=nbs[jj]; if(p_j==p_i) continue;
            //         real V_j=particles.V(p_j);
            //         VectorD v_j=particles.v(p_j);
            //         particles.V(p_j)=V_j+V_i*V_j/V_js; //// TODO: not mass center conservation
            //         particles.v(p_j)=(v_j*V_js+v_i*V_i)/(V_i+V_js);
            //     }
            // }

            auto cmp = [](const std::pair<int, int>& a, const std::pair<int, int>& b) {return a.first<b.first;};
            std::sort(remove_pairs.begin(),remove_pairs.end(),cmp);

            for(int ii=0;ii<remove_pairs.size();ii++){
                auto& pair=remove_pairs[ii];
                int i=pair.first, j=pair.second;
                
                VectorD& v_i=particles.v(i); real V_i=particles.V(i); real c_i=particles.c(i);
                VectorD& v_j=particles.v(j); real V_j=particles.V(j); real c_j=particles.c(j);

                //// update volume
                particles.V(i)=V_i+V_j;
                //// update velocity
                particles.v(i)=(V_i*v_i+V_j*v_j)/(V_i+V_j);
                //// update concentration
                particles.c(i)=(c_i*V_i+c_j*V_j)/(V_i+V_j);
            }

            int pnum=particles.Size();
            Remove_Particles(particles,remove_set);
            if(verbose) Info("After particle removal, #particle={}->{}, time={}",pnum,particles.xRef().size(), timer.Lap_Time());
        }

        static void Remove_Particles(EulerianParticles<d>& particles, std::set<int> remove_set){
            
            PointUtil::Remove_Vector_Elements<VectorD>(remove_set, particles.xRef());
            PointUtil::Remove_Vector_Elements<VectorD>(remove_set, particles.vRef());
            PointUtil::Remove_Vector_Elements<VectorD>(remove_set, particles.nRef());
            PointUtil::Remove_Vector_Elements<VectorD>(remove_set, particles.vtRef());

            PointUtil::Remove_Vector_Elements<real>(remove_set, particles.rRef());
            PointUtil::Remove_Vector_Elements<real>(remove_set, particles.VRef());
            PointUtil::Remove_Vector_Elements<real>(remove_set, particles.aRef());

            PointUtil::Remove_Vector_Elements<real>(remove_set, particles.etaRef());
            PointUtil::Remove_Vector_Elements<real>(remove_set, particles.cRef());
            PointUtil::Remove_Vector_Elements<real>(remove_set, particles.mRef());
            PointUtil::Remove_Vector_Elements<int>(remove_set, particles.tRef());
            
            particles.size=particles.xRef().size();
            PointUtil::Check_Points_Size(particles);
        }

        static void Remove_Particles(LagrangianParticles<d>& particles, std::set<int> remove_set){
            
            PointUtil::Remove_Vector_Elements<VectorD>(remove_set, particles.xRef());
            PointUtil::Remove_Vector_Elements<VectorD>(remove_set, particles.vRef());
            PointUtil::Remove_Vector_Elements<VectorD>(remove_set, particles.nRef());

            PointUtil::Remove_Vector_Elements<MatrixT>(remove_set, particles.BRef());
            PointUtil::Remove_Vector_Elements<MatrixT>(remove_set, particles.DRef());

            PointUtil::Remove_Vector_Elements<real>(remove_set, particles.iaRef());
            PointUtil::Remove_Vector_Elements<real>(remove_set, particles.cRef());
            
            particles.size=particles.xRef().size();
            PointUtil::Check_Points_Size(particles);
        }



        //// suppose that levelset is build from the narrowband phi
        static int Reseed_Insert_Flip_Particles(LevelSet<d>& levelset,  EulerianParticles<d>& particles, std::shared_ptr<PointLocalGeometry<d>> local_geom, bool verbose=false){
            
            Timer timer;
            const Grid<d>& grid=levelset.phi.grid;
            
            //// count particles
            constexpr int max_particle_per_bin=constexpr_pow(2,d);
            Field<std::array<int,max_particle_per_bin>,d> particle_bins(grid);
            // std::atomic<int> atomic_zero=0;
            Field<AtomicWarpper<int>,d> particle_cnts(grid);

            levelset.phi.Exec_Nodes([&](const VectorDi cell) {
                // particle_cnts(cell)=atomic_zero;
                particle_cnts(cell).val.store(0);
            });
            
            #pragma omp parallel for
            for(int pi=0;pi<particles.Size();pi++){
                VectorD x_i=particles.x(pi);
                VectorDi cell_i=GridUtil<d>::Cell_Coord(grid, x_i);
                if(!grid.Valid(cell_i)){continue;}

                int cnt=particle_cnts(cell_i).val.fetch_add(1);
                if(cnt<max_particle_per_bin)
                    particle_bins(cell_i)[cnt]=pi;
            }
            if(verbose) Info("Reseed_Insert_Flip_Particles: Fill particle bins, time={}", timer.Lap_Time());


            //// find cell_to reseed
            Array<VectorDi> cell_to_reseed;
            levelset.phi.Exec_Nodes([&](const VectorDi cell) {
                if(abs(levelset.phi(cell))>2*grid.dx) return;
                
                if(abs(levelset.phi(cell))<0.5*grid.dx || particle_cnts(cell).val>0)
                if(particle_cnts(cell).val<max_particle_per_bin ){
                    #pragma omp critcal
                    cell_to_reseed.push_back(cell);
                }
            });
            // std::cout<<"Reseed_Insert_Flip_Particles: Get cells for reseed, #cell={}, time={}"<<std::endl;
            if(verbose) Info("Reseed_Insert_Flip_Particles: Get cells for reseed, #cell={}, time={}",cell_to_reseed.size(), timer.Lap_Time());


            //// remove particles
            std::set<int> particle_to_remove;
            for(int ci=0;ci<cell_to_reseed.size();ci++){
                const VectorDi& cell=cell_to_reseed[ci];
                for(int ii=0; ii<particle_cnts(cell).val; ii++){
                    int pi=particle_bins(cell)[ii];
                    particle_to_remove.insert(pi);
                }
            }
            {
                int pnum=particles.Size();
                Remove_Particles(particles, particle_to_remove);
                // std::cout<<"Reseed_Insert_Flip_Particles: After particle removal, #particle={}->{}, time={}"<<std::endl;
                if(verbose) Info("Reseed_Insert_Flip_Particles: After particle removal, #particle={}->{}, time={}",pnum,particles.xRef().size(), timer.Lap_Time());
            }

            //// insert particles
            std::shared_ptr<EulerianParticles<d>> tmp_particles=std::make_shared<EulerianParticles<d>>();
            tmp_particles->Resize(cell_to_reseed.size()*max_particle_per_bin);
            #pragma omp parallel for
            for(int ci=0;ci<cell_to_reseed.size();ci++){
                const VectorDi& cell=cell_to_reseed[ci];
                VectorD cell_center=grid.Position(cell);
                for(int pii=0;pii<max_particle_per_bin;pii++){
                    VectorD x_i = cell_center + VectorD::Random() * grid.dx * 0.5;
                    int pi=ci*max_particle_per_bin+pii;
                    tmp_particles->x(pi)=x_i;
                }
            }
            {
                int pnum=particles.Size();
                particles.Append(*tmp_particles);
                // std::cout<<"After particle insertion, #particle={}->{}, time={}"<<std::endl;
                if(verbose) Info("After particle insertion, #particle={}->{}, time={}", pnum, particles.Size(), timer.Lap_Time());
            }
            return cell_to_reseed.size()*max_particle_per_bin;
        }

        static int Reseed_Insert_Particles(LevelSet<d>& levelset,  EulerianParticles<d>& particles, std::shared_ptr<PointLocalGeometry<d>> local_geom, bool verbose=false){
            Timer timer;
            //// remove particles that are too close
            local_geom->nbs_searcher->Update_Points(particles.xRef());
            if(verbose) Info("ParticleLevelSetHelper::Reseed_Insert_Particles::update point, time={}", timer.Lap_Time());

            Array<VectorD> insert_positions;
            Array<VectorD> insert_normals;
            Array<VectorD> insert_velocity;
            Array<real> insert_volume;
            Array<real> insert_radius;
            Array<real> insert_concentration;
            Array<real> insert_thickness;
            Array<int> insert_type;

            Array<std::tuple<int, int, int>> insert_tuples;//// record the insert pair for redistribution (i*num+j, insert_id)

            int k=64;////64 points on the circle
            const real pi=CommonConstants::pi;
            const real sqrt_pi=sqrt(CommonConstants::pi);
            std::function<real(const real)> mean_particle_distance[4]={
                [&](const real r)->real {return 0; },//// placehold for d=0
                [&](const real r)->real {return 0; },//// placehold for d=1
                [&](const real r)->real {return (real)2.0*pi*r/k; },//// for d=2, 0.19625dx
                [&](const real r)->real {return (real)2.0*sqrt_pi*r/k; } //// for d=3, 0.11dx
            };

            if(verbose) std::cout<<"Insertion param: v_r="<<local_geom->v_r<<
                ", insert distance threshold="<<mean_particle_distance[d](local_geom->v_r)*2.0f<<" - "<<mean_particle_distance[d](local_geom->v_r)*8.0f<<std::endl;

            const real sqrt_3=sqrt((real)3.0);

            // std::atomic<int> nb_cnt=0;
            // std::atomic<int> all_nb_cnt=0;
            // std::atomic<int> nbnb_cnt=0;
            // std::atomic<int> all_nbnb_cnt=0;
            int p_num=particles.Size();
            #pragma omp parallel for
            for(int i=0;i<particles.Size();i++){
                VectorD x_i=particles.x(i);
                VectorD n_i=particles.n(i);
                real r_i=particles.r(i);
                Array<int> nbs;

                real max_dist_thres;
                real min_dist_thres;
                if constexpr(d==2){
                    max_dist_thres=mean_particle_distance[d](std::min(local_geom->v_r, r_i))*8.0;
                    min_dist_thres=mean_particle_distance[d](std::min(local_geom->v_r, r_i))*1.0;
                }
                if constexpr(d==3){
                    max_dist_thres=mean_particle_distance[d](std::min(local_geom->v_r, r_i))*16.0f;//15;//12.0;
                    min_dist_thres=mean_particle_distance[d](std::min(local_geom->v_r, r_i))*7.2f;//5.0;//4.0;
                }
                real max_dist_thres2=max_dist_thres*max_dist_thres;
                real min_dist_thres2=min_dist_thres*min_dist_thres;

                local_geom->nbs_searcher->Find_Nbs(x_i, max_dist_thres, nbs);
                
                // if(nbs.size()>200){std::cout<<"too much neighbors! i="<<i<<", #"<<nbs.size()<<" with r="<<max_dist_thres<<std::endl;}
                for(int jj=0;jj<nbs.size();jj++){
                    int j=nbs[jj];
                    VectorD x_j=particles.x(j);
                    VectorD n_j=particles.n(j);
                    real r_j=particles.r(j);
                    
                    // all_nb_cnt++;

                    //// only when the dist is 
                    if(r_i<r_j || n_i.dot(n_j)<0.5) continue;
                    real dist2=(x_j-x_i).squaredNorm();
                    if(dist2>max_dist_thres2 || dist2<min_dist_thres2) continue;
                    real dist=sqrt(dist2);
                    VectorD rn_ij=(x_j-x_i)/dist;
                    // nb_cnt++;

                    //// if there is no point in the cone, insert
                    // bool need_insert=true;
                    // for(int kk=0;kk<nbs.size();kk++){
                    //     int k=nbs[kk];
                    //     VectorD x_k=particles.x(k);
                    //     all_nbnb_cnt++;

                    //     if(k==i || k==j) continue;
                    //     VectorD ik=(x_k-x_i);
                    //     real proj_ik=ik.dot(rn_ij);
                    //     if(proj_ik<=0 || proj_ik>dist) continue;

                    //     nbnb_cnt++;

                    //     VectorD rn_ki=(x_i-x_k).normalized();
                    //     VectorD rn_kj=(x_j-x_k).normalized();
                    //     // if(rn_ij.dot(rn_kj)>0.5*sqrt_3 && -rn_ij.dot(rn_ki)>0.5*sqrt_3){
                    //     if constexpr(d==2)
                    //     if(rn_ij.dot(rn_kj)>0.5 && -rn_ij.dot(rn_ki)>0.5){
                    //         need_insert=false; break;
                    //     }
                    //     if constexpr(d==3)
                    //     if(rn_ij.dot(rn_kj)>0.5*1.41421 && -rn_ij.dot(rn_ki)>0.5*1.41421){
                    //         need_insert=false; break;
                    //     }
                    // }

                    //// if there is no point in the sphere, insert
                    bool need_insert=true;
                    VectorD x_t=(x_i+x_j)*0.5f;
                    real radius2=pow(dist*0.49,2);
                    for(int kk=0;kk<nbs.size();kk++){
                        int k=nbs[kk];
                        VectorD x_k=particles.x(k);
                        // all_nbnb_cnt++;
                        if(k==i || k==j) continue;
                        if((x_k-x_t).squaredNorm()>=radius2) continue;
                        // need_insert=false;
                        // break;

                        VectorD rn_ki=(x_i-x_k).normalized();
                        VectorD rn_kj=(x_j-x_k).normalized();
                        // if(rn_ij.dot(rn_kj)>0.5*sqrt_3 && -rn_ij.dot(rn_ki)>0.5*sqrt_3){
                        if constexpr(d==2)
                        if(rn_ij.dot(rn_kj)>0.5 && -rn_ij.dot(rn_ki)>0.5){
                            need_insert=false; break;
                        }
                        if constexpr(d==3)
                        if(rn_ij.dot(rn_kj)>0.5*1.41421 && -rn_ij.dot(rn_ki)>0.5*1.41421){
                            need_insert=false; break;
                        }
                    }

                    //// do insertion
                    if(need_insert){
                        #pragma omp critcal
                        {
                            // id=insert_positions.size();
                            int id=insert_tuples.size();
                            insert_tuples.push_back(std::make_tuple(i,j,id));
                        }
                    }
                }
            }
            if(verbose) Info("ParticleLevelSetHelper::Reseed_Insert_Particles::insert set, time={}", timer.Lap_Time());
            // std::cout<<"cnt="<<particles.Size()<<std::endl;
            // std::cout<<"nb_cnt="<<nb_cnt<<std::endl;
            // std::cout<<"all_nb_cnt="<<all_nb_cnt<<std::endl;
            // std::cout<<"nbnb_cnt="<<nbnb_cnt<<std::endl;
            // std::cout<<"all_nbnb_cnt="<<all_nbnb_cnt<<std::endl;

            //// TODO: redistribute the attributes!
            auto cmp = [](const std::tuple<int, int, int>& a, const std::tuple<int, int, int>& b) {
                if(std::get<0>(a)<std::get<0>(b))return true;
                if(std::get<0>(a)>std::get<0>(b))return false;
                if(std::get<1>(a)<std::get<1>(b))return true;
                if(std::get<1>(a)>std::get<1>(b))return false;
                if(std::get<2>(a)<std::get<2>(b))return true;
                return false;
            };
            std::sort(insert_tuples.begin(),insert_tuples.end(),cmp);
            
            std::shared_ptr<EulerianParticles<d>> tmp_particles=std::make_shared<EulerianParticles<d>>();
            tmp_particles->Resize(insert_tuples.size());
            #pragma omp parallel for
            for(int ii=0;ii<insert_tuples.size();ii++){
                auto tuple=insert_tuples[ii];
                int i=std::get<0>(tuple); int j=std::get<1>(tuple);
                int id=std::get<2>(tuple);

                // std::cout<<"inset at "<<i<<","<<j<<",ijpos=["<<particles.x(i).transpose()<<"], ["<<particles.x(j).transpose()<<"],pos=["<<insert_positions[id].transpose()<<"], r="<<particles.r(i)<<","<<particles.r(j)<<std::endl;

                tmp_particles->x(id)=(particles.x(i)+particles.x(j))*0.5f;
                tmp_particles->n(id)=(particles.n(i)+particles.n(j)).normalized();
                tmp_particles->r(id)=(particles.r(i)+particles.r(j))*0.5f;
                tmp_particles->eta(id)=(particles.eta(i)+particles.eta(j))*0.5f;
                if(particles.t(i)==1 && particles.t(j)==1) tmp_particles->t(id)=1;
                else tmp_particles->t(id)=0;

            }
            for(int ii=0;ii<insert_tuples.size();ii++){
                auto tuple=insert_tuples[ii];
                int i=std::get<0>(tuple); int j=std::get<1>(tuple);
                int id=std::get<2>(tuple);
                VectorD& v_i=particles.v(i); real V_i=particles.V(i); real c_i=particles.c(i);
                VectorD& v_j=particles.v(j); real V_j=particles.V(j); real c_j=particles.c(j);
                //// update volume
                tmp_particles->V(id)=(1.0/3.0)*(V_i+V_j);
                particles.V(i)=(2.0/3.0)*V_i;
                particles.V(j)=(2.0/3.0)*V_j;
                //// update velocity
                tmp_particles->v(id)=(V_i*v_i+V_j*v_j)/(V_i+V_j);
                //// update concentration
                tmp_particles->c(id)=(c_i*V_i+c_j*V_j)/(V_i+V_j);
            }

            int pnum=particles.Size();
            particles.Append(*tmp_particles);

            if(verbose) Info("After particle insertion, #particle={}->{}, time={}", pnum, particles.Size(), timer.Lap_Time());
            
        }

        static void Transfer_Particles(EulerianParticles<d>& dest_particles, EulerianParticles<d>& src_particles, std::set<int> indices, bool verbose=false){
            Timer timer;
            //// insert into particles
            PointUtil::Transfer_Vector_Elements<VectorD>(dest_particles.xRef(), src_particles.xRef(), indices);
            PointUtil::Transfer_Vector_Elements<VectorD>(dest_particles.vRef(), src_particles.vRef(), indices);
            PointUtil::Transfer_Vector_Elements<VectorD>(dest_particles.nRef(), src_particles.nRef(), indices);
            PointUtil::Transfer_Vector_Elements<VectorD>(dest_particles.vtRef(), src_particles.vtRef(), indices);

            PointUtil::Transfer_Vector_Elements<real>(dest_particles.rRef(), src_particles.rRef(), indices);
            PointUtil::Transfer_Vector_Elements<real>(dest_particles.VRef(), src_particles.VRef(), indices);
            PointUtil::Transfer_Vector_Elements<real>(dest_particles.aRef(), src_particles.aRef(), indices);
            PointUtil::Transfer_Vector_Elements<real>(dest_particles.etaRef(), src_particles.etaRef(), indices);
            PointUtil::Transfer_Vector_Elements<real>(dest_particles.cRef(), src_particles.cRef(), indices);
            PointUtil::Transfer_Vector_Elements<real>(dest_particles.mRef(), src_particles.mRef(), indices);

            PointUtil::Transfer_Vector_Elements<int>(dest_particles.tRef(), src_particles.tRef(), indices);

            if(verbose) Info("After particle transfer, #particle={}->{}, time={}", dest_particles.size, dest_particles.xRef().size(), timer.Lap_Time());
            dest_particles.size=dest_particles.xRef().size();
        }

        static void Append_Particles(EulerianParticles<d>& dest_particles, EulerianParticles<d>& src_particles, bool verbose=false){
            Timer timer;
            PointUtil::Append_Vector_Elements<VectorD>(dest_particles.xRef(), src_particles.xRef());
            PointUtil::Append_Vector_Elements<VectorD>(dest_particles.vRef(), src_particles.vRef());
            PointUtil::Append_Vector_Elements<VectorD>(dest_particles.nRef(), src_particles.nRef());
            PointUtil::Append_Vector_Elements<VectorD>(dest_particles.vtRef(), src_particles.vtRef());

            PointUtil::Append_Vector_Elements<real>(dest_particles.rRef(), src_particles.rRef());
            PointUtil::Append_Vector_Elements<real>(dest_particles.VRef(), src_particles.VRef());
            PointUtil::Append_Vector_Elements<real>(dest_particles.aRef(), src_particles.aRef());
            PointUtil::Append_Vector_Elements<real>(dest_particles.etaRef(), src_particles.etaRef());
            PointUtil::Append_Vector_Elements<real>(dest_particles.cRef(), src_particles.cRef());
            PointUtil::Append_Vector_Elements<real>(dest_particles.mRef(), src_particles.mRef());
            
            PointUtil::Append_Vector_Elements<int>(dest_particles.tRef(), src_particles.tRef());
            
            if(verbose) Info("After particle append, #particle={}->{}, time={}", dest_particles.size, dest_particles.xRef().size(), timer.Lap_Time());
            dest_particles.size=dest_particles.xRef().size();
            PointUtil::Check_Points_Size(dest_particles);

        }
    
    //// regenerate pls
        static void Regenerate_Particles_On_Levelset(EulerianParticles<d>& particles, const LevelSet<d>& levelset){
            Array<VectorD> temp_x;
            if constexpr(d==2)
                LevelSetUtil<d>::Sample_On_Levelset_Surface(temp_x, levelset, 40, 1, 10);
            if constexpr(d==3)
                LevelSetUtil<d>::Sample_On_Levelset_Surface(temp_x, levelset, 20, 1, 10);
            particles.Resize(temp_x.size());
            for(int i=0;i<temp_x.size();i++){
                particles.x(i)=temp_x[i];
                particles.v(i)=VectorD::Zero();
                particles.n(i)=LevelSetUtil<d>::Normal(levelset,temp_x[i]);
            }
        }

        static void Regenerate_Particles_On_Levelset(LagrangianParticles<d>& particles, const LevelSet<d>& levelset){
            Array<VectorD> temp_x;
            if constexpr(d==2)
                LevelSetUtil<d>::Sample_On_Levelset_Surface(temp_x, levelset, 120, 1, 10);
            if constexpr(d==3)
                LevelSetUtil<d>::Sample_On_Levelset_Surface(temp_x, levelset, 80, 1, 10);
            particles.Resize(temp_x.size());
            for(int i=0;i<temp_x.size();i++){
                particles.x(i)=temp_x[i];
                particles.v(i)=VectorD::Zero();
                particles.n(i)=LevelSetUtil<d>::Normal(levelset,temp_x[i]);
            }
        }
    
    //// 
        static std::tuple<int, real, VectorD, VectorD> Closest_Point_On_Particle_Surface(const VectorD& cell_x, std::shared_ptr<EulerianParticles<d>> particles, std::shared_ptr<PointLocalGeometry<d>> local_geom, int mls_closest_point_iteration, real mls_narrow_band_width){
            // Timer timer;
            const int max_iteration=1;

            real sign=0; ////useless define
            // VectorD cell_x=grid.Position(cell);
            
            int ci=local_geom->Closest_Point(cell_x);
            if(ci<0){
                std::cout<<"incorrect local_geom->Closest_Point "<<ci<<" at ("<<cell_x<<"), psize="<<particles->Size()<<std::endl;
            }
            VectorD intf_x=particles->x(ci);
            VectorD normal=particles->n(ci);
            MatrixD local_frame;

            if((intf_x-cell_x).norm()>mls_narrow_band_width) return std::tuple<int, real, VectorD, VectorD> (0, 0, VectorD::Zero(), cell_x);
            
            // std::cout<<"pre update"<<timer.Lap_Time()<<std::endl;
            int iteration=0;
            for(;iteration<max_iteration;iteration++){
                local_frame=local_geom->Spawn_Local_Frame(normal);
                LeastSquares::MLSPointSet<d-1> mls;
                Array<int> nbs;
                Array<VectorD> local_nbs_x;
                int n=local_geom->Fit_Local_Geometry_MLS_Valid(intf_x,local_frame, nbs, local_nbs_x, mls);
                // int n=local_geom->Fit_Local_Geometry_MLS(intf_x,local_frame, nbs, local_nbs_x, mls);
                
                // std::cout<<"fit "<<nbs.size()<<","<<timer.Lap_Time()<<std::endl;
                if(n<d*2) break;

                //// find closest point on MLS surface
                VectorD u=cell_x-intf_x;
                VectorD local_pos=local_geom->Local_Coords(u,local_frame);
                VectorD new_local_pos=local_geom->Closest_Point_To_Local_MLS(local_pos, mls, mls_closest_point_iteration);
                // std::cout<<"proj"<<timer.Lap_Time()<<std::endl;
                //// limit local pos on tangent plane
                // Array<VectorT> local_nbs_t_pos;
                // for(int i=0;i<nbs.size();i++){
                //     VectorD u=particles->x(nbs[i])-intf_x;
                //     local_nbs_t_pos.push_back(local_geom->Local_Coords(u,local_frame).head(d-1));
                // }
                // VectorT limit_local_pos=local_geom->Limit_Local_Tangent(new_local_pos.head(d-1), local_nbs_t_pos);

                // VectorT limit_local_pos=local_geom->Limit_Local_Tangent(new_local_pos.head(d-1), intf_x, local_frame, nbs);
                VectorT limit_local_pos=local_geom->Limit_Local_Tangent_D(new_local_pos, local_nbs_x);
                new_local_pos<<limit_local_pos, mls(limit_local_pos);
                // std::cout<<"local tan"<<timer.Lap_Time()<<std::endl;

                //// get normal vector
                VectorD local_normal=local_geom->Normal_On_Local_MLS(new_local_pos.head(d-1), mls);
                VectorD new_normal=local_geom->Unproject_To_World(local_normal,local_frame).normalized();
                normal=(new_normal.dot(normal)>0)?new_normal:-new_normal;

                intf_x=local_geom->Unproject_To_World(new_local_pos, local_frame)+intf_x;
                // std::cout<<"un proj"<<timer.Lap_Time()<<std::endl;

            }

            //// update levelset
            VectorD rel=cell_x-intf_x;
            real dist=rel.norm();

            real new_sign=rel.dot(normal)<0?-1:1;
            sign=new_sign;
            
            if(iteration>0){
                // narrowband_phi(cell).push_back(std::make_pair(rid,sign*dist));
                return std::tuple<int, real, VectorD, VectorD> (iteration, sign*dist, normal, intf_x);
            }

            return std::tuple<int, real, VectorD, VectorD> (0, 0, VectorD::Zero(), cell_x);
        }
    };


}