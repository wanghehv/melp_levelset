//////////////////////////////////////////////////////////////////////////
// Initializer of MLSTest
// Copyright (c) (2022-), Hui Wang
// This file is part of MESO, whose distribution is governed by the LICENSE file.
//////////////////////////////////////////////////////////////////////////
#pragma once
#include "MLSTest.h"
#include "ImplicitManifold.h"
#include "Json.h"
#include "GridEulerFunc.h"

namespace Meso {
    template<int d>
    class MLSTestInitializer {
    public:
        Typedef_VectorD(d);
        Typedef_MatrixD(d);

        void Apply(json& j, MLSTest<d>& test) {
            // std::string test = Json::Value(j, "test", std::string("single_vortex"));

            Init_Test(j, test);

            test.Reseed_Geo_Particles();
            test.Reseed_Track_Particles();
            // test.Reseed_Debug_Particles(test.track_particles.Size());
            
            std::shared_ptr<NeighborSearcher<d>> nbs_searcher=std::make_shared<NeighborKDTree<d> >();
            std::shared_ptr<PointLocalGeometry<d>> local_geom=std::make_shared<PointLocalGeometry<d> > (test.levelset.phi.grid.dx, 2/1.5, nbs_searcher);
            test.nbs_searcher=nbs_searcher;
            test.local_geom=local_geom;
            
            test.Init(j);
        }

        void Init_Test(json& j, MLSTest<d>& test){
            if constexpr(d==3){
                Error("No single vortex test for 3D");
            }
            int scale = Json::Value<int>(j, "scale", 32);
            real side_len = 1.0;
            real dx = side_len / scale;
            VectorDi grid_size = scale * MathFunc::Vi<d>(1, 1, 1);
            Grid<d> grid(grid_size, dx, VectorD::Zero(), CENTER);

            //// init levelset
            VectorD x=grid.Center();
            x[1]=side_len*0.75;
            Sphere<d> sphere(x, side_len*0.15);
            test.levelset.Init(grid, sphere);
            
        }


    };
}