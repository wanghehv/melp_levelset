//////////////////////////////////////////////////////////////////////////
// Point Local Geometry
// Copyright (c) (2022-), Hui Wang
// This file is part of MESO, whose distribution is governed by the LICENSE file.
//////////////////////////////////////////////////////////////////////////
#pragma once

#include "EulerianParticles.h"
#include "NeighborSearcher.h"
#include "LeastSquares.h"
#include "AuxFunc.h"
#include "Timer.h"
namespace Meso {
    template<int d>
    class PointLocalGeometry{
    ////fit local geometry based on std::shared_ptr<NeighborSearcher<d>> nbs_searcher
    public:
        Typedef_VectorD(d);
        Typedef_MatrixD(d);
        using VectorT=Vector<real,d-1>;								////tangential vector type
        using VectorTi=Vector<int,d-1>;								////tangential vector int
        using MatrixT=Matrix<real,d-1,d-1>;								////metric tensor matrix type

        real v_r; //// use as a default radius;
        real t_r;
        std::shared_ptr<NeighborSearcher<d>> nbs_searcher;

        std::shared_ptr<Array<VectorD>> normal_ptr;

        PointLocalGeometry(){
            t_r=(real)1.0f;
            v_r=t_r*(real)1.5f;
            nbs_searcher=nullptr;
        }

        PointLocalGeometry(const real _dx, const real _t_grid_s, std::shared_ptr<NeighborSearcher<d>> _nbs_searcher){
            t_r=_dx*_t_grid_s;
            v_r=t_r*1.5f;
            nbs_searcher=_nbs_searcher;
        }

        //////////////////////////////////////////////////////////////////////////
        ////Projection functions between global and local frames
        static Vector<real, d> Local_Coords(const VectorD& u, const MatrixD& e)
        {
            //[u.dot(e.col(0)), u.dot(e.col(1)), ...]
            return u.transpose() * e;
        }

        static void Local_Coords(const VectorD& u, const MatrixD& e, VectorT& t, real& z)
        {
            VectorD local_coords = Local_Coords(u, e);
            // t = AuxFunc::V<d - 1>(local_coords);
            t = MathFunc::V<d - 1>(local_coords);
            z = local_coords[d - 1];
        }

        static Vector<real, d - 1> Project_To_TPlane(const VectorD& u, const MatrixD& e)
        {
            VectorT t_coords;
            for (int i = 0; i < d - 1; i++) {
                t_coords[i] = u.dot(e.col(i));
            }
            return t_coords;
        }

        static Vector<real, d - 1> Rotate_To_TPlane(const VectorD& u, const MatrixD& e)
        {
            VectorT t_coords;
            for (int i = 0; i < d - 1; i++) {
                t_coords[i] = u.dot(e.col(i));
            }
            if (t_coords.norm() == 0.) return VectorT::Zero();
            else return u.norm() * t_coords.normalized();
        }

        static real Project_To_TPlane_H(const VectorD& u, const MatrixD& e)
        {
            return u.dot(e.col(d - 1));
        }

        static MatrixD Rotate_Matrix(const VectorD& u, const VectorD& v){
            //// return M that u*M=v
            MatrixD m=MatrixD::Identity();
            real cross_prod;
            if constexpr(d==2) cross_prod=u(0)*v(1) - v(0)*u(1);
            if constexpr(d==3) cross_prod=(u.cross(v)).norm();
            real dot_prod=u.dot(v);
            m(0,0)=dot_prod; m(0,1)=-cross_prod;
            m(1,0)=cross_prod; m(1,1)=dot_prod;
            return m;
        }

        ////Take tangential components
        static VectorD Unproject_To_World(const VectorT& t, const MatrixD& e){
            VectorD g=VectorD::Zero();
            for (int i = 0; i < d - 1; i++)
                g += e.col(i)*t[i];
            return g;
        }

        static VectorD Unproject_To_World(const VectorD& l, const MatrixD& e){
            VectorD g=VectorD::Zero();
            for (int i = 0; i < d; i++)
                g += e.col(i)*l[i];
            return g;
        }

        
        static MatrixD Spawn_Local_Frame(const VectorD& normal){
            MatrixD e=MatrixD::Zero();
            if constexpr(d==2){
                VectorD tang = -MathFunc::Orthogonal_Vector(normal);
                e.col(0) = tang.normalized();
                e.col(1) = normal.normalized();
            }
            if constexpr(d==3){
                VectorD t0 = -MathFunc::Orthogonal_Vector(normal);
                VectorD t1 = t0.cross(normal);
                e.col(0) = t0.normalized();
                e.col(1) = t1.normalized();
                e.col(2) = normal.normalized();
            }
            return e;
        }

        //// return the normal at a point on the mls surface
        static VectorD Normal_On_Local_MLS(VectorT t, const LeastSquares::MLSPointSet<d-1>& mls){
            if constexpr(d==2){
                VectorT grad=mls.Grad(t);
                VectorD tang; tang<<1,grad; tang.normalize();
                return MathFunc::Orthogonal_Vector(tang).normalized();
            }
            if constexpr(d==3){
                VectorT grad=mls.Grad(t);
                VectorD tang0; tang0<<1,0,grad(0);
                VectorD tang1; tang1<<0,1,grad(1);
                return (tang0.cross(tang1)).normalized();
            }
        }
        
        //////////////////////////////////////////////////////////////////////////
        //// return the closest point 
        inline int Closest_Point(const VectorD& pos) const { return nbs_searcher->Find_Nearest_Nb(pos); }

        //////////////////////////////////////////////////////////////////////////
        //// Kernel function
        real W_PCA(const real r) const
        {
            if(r<v_r)return (real)1-pow(r/v_r,3);
            else return (real)0;	
        }

        MatrixD Local_Frame(const VectorD& xc) const {
            Array<int> nbs; 
            nbs_searcher->Find_Nbs(xc,v_r,nbs);
            size_t size=nbs.size();

            VectorD xp = VectorD::Zero();
            real w = (real)0;
            for (size_t i = 0; i < size; i++) {
                ////here we use volumetric distance instead of tangential distance
                // real dis = (points->X(q) - points->X(p)).norm();
                real dis = ((*nbs_searcher->Points_Ptr())[nbs[i]] - xc).norm();
                real w0 = W_PCA(dis);
                xp += w0 * (*nbs_searcher->Points_Ptr())[nbs[i]];
                w += w0;
            }

            if (w != (real)0)xp /= w;
            MatrixD C = MatrixD::Zero();
            real wc = (real)0;
            for (int i = 0; i < size; i++) {
                const VectorD& xq = (*nbs_searcher->Points_Ptr())[nbs[i]];
                real dis = (xq - xp).norm();
                real w0 = W_PCA(dis);
                C += w0 * (xq - xp) * (xq - xp).transpose();
                wc += w0;
            }
            if (wc != (real)0)C /= wc;
            // VectorD normal = MathFunc::Min_Eigenvector_Temp(C);
            VectorD normal;
            if constexpr(d==2) normal=MathFunc::Min_Eigenvector((const Matrix2&)C);
            if constexpr(d==3) normal=MathFunc::Min_Eigenvector((const Matrix3&)C);
            //// correct norm
            // if (normal.dot(normc) < (real)0)normal *= (real)-1;

            MatrixD E=MatrixD::Zero();
            ////update local frame according to the PCA normal
            if constexpr (d == 2) {
                VectorD tang = -MathFunc::Orthogonal_Vector(normal);
                E.col(0) = tang.normalized();
                E.col(1) = normal.normalized();
            }
            else if constexpr (d == 3) {
                VectorD t1 = -MathFunc::Orthogonal_Vector(normal);
                VectorD t2 = t1.cross(normal);
                E.col(0) = t1.normalized();
                E.col(1) = t2.normalized();
                E.col(2) = normal.normalized();
            }

            ////Correct local frame to align with the tangent space?

            return E;
        }

        //////////////////////////////////////////////////////////////////////////
        //// neighbor operation on tangent plane
        bool Is_Tangential_Neighbor(const VectorD& pos, const MatrixD& local_frame, const int p) const
        {
            ////check angle
            VectorD dir = local_frame.col(d - 1);
            //// TODO: add filter
            // VectorD dir_p = points->E(p).col(d - 1);
            // real dot = dir.dot(dir_p);
            // if (dot < t_dot) return false;	////skip the points with large angles

            ////check distance
            // VectorD u = points->X(p) - pos;
            VectorD u = (*nbs_searcher->Points_Ptr())[p] - pos;
            VectorT t = Project_To_TPlane(u, local_frame);
            return t.norm() < t_r;
        }

        Array<int> Find_Tangential_Nbs(const VectorD& pos, const MatrixD& local_frame) const
        {
            std::function<bool(const int)> filter_func = [&](const int p) {return Is_Tangential_Neighbor(pos, local_frame, p); };
            Array<int> result;
            nbs_searcher->Find_Nbs(pos, t_r, filter_func, result);
            return result;
        }

        //// limit the position in the neighboring convex on the tangent plane
        static VectorT Limit_Local_Tangent_D(const VectorD& local_x, const Array<VectorD>& local_nbs_x){
            if(local_x.head(d-1).norm()<1e-8) return local_x.head(d-1);
            VectorT dir=local_x.head(d-1).normalized();
            real min_len=1e8;
            real max_len=-1e8;

            for(int i=0;i<local_nbs_x.size();i++){
                real len=(local_nbs_x[i]).head(d-1).dot(dir);
                min_len=std::min(min_len,len);
                max_len=std::max(max_len,len);
            }
            real len=dir.dot(local_x.head(d-1));
            len=std::max(min_len,len);
            len=std::min(max_len,len);
            return len*dir;
        }

        static VectorT Limit_Local_Tangent(const VectorT& local_x, const Array<VectorT>& local_nbs_x)
        {
            if(local_x.norm()<1e-8) return local_x;
            VectorT dir=local_x.normalized();
            real min_len=1e8;
            real max_len=-1e8;

            for(int i=0;i<local_nbs_x.size();i++){
                real len=local_nbs_x[i].dot(dir);
                min_len=std::min(min_len,len);
                max_len=std::max(max_len,len);
            }
            real len=dir.dot(local_x);
            len=std::max(min_len,len);
            len=std::min(max_len,len);
            return len*dir;
        }

        VectorT Limit_Local_Tangent(const VectorT& local_t, const VectorD intf_x/*center*/, const MatrixD& local_frame, const Array<int>& nbs){

            if(local_t.norm()<1e-8) return local_t;
            VectorT dir=local_t.normalized();
            real min_len=1e8;
            real max_len=-1e8;

            for(int i=0;i<nbs.size();i++){
                VectorD u=(*nbs_searcher->Points_Ptr())[nbs[i]]-intf_x;
                VectorT local_nbs_x=Local_Coords(u,local_frame).head(d-1);
                real len=local_nbs_x.dot(dir);
                min_len=std::min(min_len,len);
                max_len=std::max(max_len,len);
            }
            
            real len=dir.dot(local_t);
            len=std::max(min_len,len);
            len=std::min(max_len,len);
            return len*dir;
        }


        /////////////////////////////////////////////////
        //// MLS fitting
        ////helper function, fit phi on an arbitrary position using MLS
        template<typename F> VectorX Fit_Local_MLS(const VectorD& pos,const MatrixD& lf,const F& phi){
            using namespace LeastSquares;
            Array<int> tang_nbs = Find_Tangential_Nbs(pos, lf);
            MLSPointSet<d-1> mls;
            size_t n = tang_nbs.size();
            // if(n==0) std::cout<<"Warning: no neighbor around"<<pos.transpose()<<std::endl;

            // Array<int> nbs; nbs_searcher->Find_Nbs(pos, v_r, nbs);
            // size_t n = tang_nbs.size();
            // if(n==0) std::cout<<"Warning: no neighbor around"<<pos.transpose()<<std::endl;
            if(n<d) {
                std::cout<<"switch to volume nbs "<<pos.transpose()<<std::endl;
                tang_nbs.clear(); nbs_searcher->Find_Nbs(pos, v_r, tang_nbs);
                n = tang_nbs.size();
            }

            // Array<int> nbs; nbs_searcher->Find_Nbs(pos, v_r, nbs);
            // MLSPointSet<d-1> mls;
            // size_t n = nbs.size();

            if(n==0) {
                std::cout<<"Warning: no neighbor around"<<pos.transpose()<<std::endl;
                // Array<int> nbs; nbs_searcher->Find_Nbs(pos, v_r, nbs);
                // std::cout<<"global neghbor size="<<nbs.size()<<std::endl;
            }
            
            ////TOFIX? discontinuous weight function
            Array<real> data(n * (size_t)d);
            int pi=-1;
            for (size_t i = 0; i < n; i++) {
                int nb = tang_nbs[i];
                // int nb = nbs[i];
                VectorD th = Local_Coords((*nbs_searcher->Points_Ptr())[nb] - pos, lf);
                for (size_t j=0;j<d-1;j++)data[i*(size_t)d+j]=th[j];
                data[i*(size_t)d+(size_t)d-(size_t)1] = phi(nb);
            }
            mls.Fit(&data[0], (int)n, pi);//// degenerate to LS
            if(n==0) {VectorX res(mls.c.rows()); res.fill(0); return res;}
            return mls.c;
        }

        ////helper function, fit phi on particle i using MLS
        template<typename F> VectorX Fit_Local_MLS(const int p,const MatrixD& lf,const F& phi){
            using namespace LeastSquares;
            VectorD pos=(*nbs_searcher->Points_Ptr())[p];
            Array<int> tang_nbs = Find_Tangential_Nbs(pos, lf);
            MLSPointSet<d-1> mls;
            size_t n = tang_nbs.size();
            if(n==0) std::cout<<"Warning: no neighbor around"<<pos.transpose()<<std::endl;

            Array<real> data(n * (size_t)d);
            // VectorD x_p=points->X(p);
            int pi=-1;
            for (size_t i = 0; i < n; i++) {
                int nb = tang_nbs[i];
                if (nb == p) pi = (int)i;
                VectorD th = Local_Coords((*nbs_searcher->Points_Ptr())[nb] - pos, lf);
                for (size_t j=0;j<d-1;j++)data[i*(size_t)d+j]=th[j];
                data[i*(size_t)d+(size_t)d-(size_t)1] = phi(nb);
            }
            mls.Fit(&data[0], (int)n, pi);		
            return mls.c;
        }

        int Fit_Local_Geometry_MLS_Valid(const VectorD& pos, const MatrixD& lf, Array<int>& valid_nbs, Array<VectorD>& local_nbs_x, LeastSquares::MLSPointSet<d-1>& mls){
            // Timer timer;

            using namespace LeastSquares;
            Array<int> nbs; nbs_searcher->Find_Nbs(pos, v_r, nbs);
            
            // std::cout<<"fit:find nbs"<<timer.Lap_Time()<<std::endl;

            //// constraint the direction
            VectorD normal=lf.col(d-1);

            int n=(int)nbs.size();
            if(n<2*d) return n;
            // std::cout<<"fit:find valid nbs"<<timer.Lap_Time()<<std::endl;

            Array<real> data(n*d);
            int pi=-1;
            local_nbs_x.clear(); local_nbs_x.resize(nbs.size());
            valid_nbs.clear(); valid_nbs.resize(nbs.size());
            int valid_size=0;
            for (size_t i = 0; i < nbs.size(); i++) {
                int nb = nbs[i];
                
                if((*normal_ptr)[nb].dot(normal)<=0) continue;
                
                valid_nbs[valid_size]=nb;
                VectorD th = Local_Coords((*nbs_searcher->Points_Ptr())[nb] - pos, lf);
                local_nbs_x[valid_size]=th;
                // VectorD th = Local_Coords(points->X(nb) - pos, lf);
                for(int j=0;j<d;j++)data[valid_size*d+j]=th[j];
                valid_size++;
            }
            local_nbs_x.erase(local_nbs_x.begin()+valid_size, local_nbs_x.end());
            valid_nbs.erase(valid_nbs.begin()+valid_size, valid_nbs.end());
            // std::cout<<"fit:set data"<<timer.Lap_Time()<<std::endl;
            
            mls.Fit(&data[0], (int)valid_size, pi);//// degenerate to LS
            // std::cout<<"fit:fit"<<timer.Lap_Time()<<std::endl;
            return n;
        }


        int Fit_Local_Geometry_MLS(const VectorD& pos, const MatrixD& lf, Array<int>& valid_nbs, Array<VectorD>& local_nbs_x, LeastSquares::MLSPointSet<d-1>& mls){
            Timer timer;

            using namespace LeastSquares;
            Array<int> nbs; nbs_searcher->Find_Nbs(pos, v_r, nbs);
            
            std::cout<<"fit:find nbs"<<timer.Lap_Time()<<std::endl;

            //// constraint the direction
            valid_nbs.clear();
            VectorD normal=lf.col(d-1);
            for (size_t i = 0; i < nbs.size(); i++) {
                if((*normal_ptr)[nbs[i]].dot(normal)>0)
                    valid_nbs.push_back(nbs[i]);
            }
            nbs=valid_nbs;
            int n=(int)nbs.size();
            if(n<2*d) return n;
            std::cout<<"fit:find valid nbs"<<timer.Lap_Time()<<std::endl;

            Array<real> data(n*d);
            int pi=-1;
            local_nbs_x.resize(nbs.size());
            for (size_t i = 0; i < nbs.size(); i++) {
                int nb = nbs[i];
                VectorD th = Local_Coords((*nbs_searcher->Points_Ptr())[nb] - pos, lf);
                local_nbs_x[i]=th;
                // VectorD th = Local_Coords(points->X(nb) - pos, lf);
                for(int j=0;j<d;j++)data[i*d+j]=th[j];
            }
            std::cout<<"fit:set data"<<timer.Lap_Time()<<std::endl;

            mls.Fit(&data[0], (int)n, pi);//// degenerate to LS
            std::cout<<"fit:fit"<<timer.Lap_Time()<<std::endl;
            return n;
        }

        ////helper function, fit local geometry on an arbitrary position using MLS
        int Fit_Local_Geometry_MLS(const VectorD& pos, const MatrixD& lf, Array<int>& valid_nbs, LeastSquares::MLSPointSet<d-1>& mls){
            using namespace LeastSquares;
            Array<int> nbs; nbs_searcher->Find_Nbs(pos, v_r, nbs);

            //// constraint the direction
            valid_nbs.clear();
            VectorD normal=lf.col(d-1);
            for (size_t i = 0; i < nbs.size(); i++) {
                if((*normal_ptr)[nbs[i]].dot(normal)>0)
                    valid_nbs.push_back(nbs[i]);
            }
            nbs=valid_nbs;

            int n=(int)nbs.size();
            if(n<2*d) {
                // std::cout<<"Warning: Fit_Local_Geometry_MLS: no neighbor around "<<pos.transpose()<<std::endl;
                if constexpr(d==2) mls.c=VectorXd::Zero(3);
                if constexpr(d==3) mls.c=VectorXd::Zero(6);
                return -1;
            }
            
            ////TOFIX? discontinuous weight function
            Array<real> data(n*d);
            int pi=-1;
            for (size_t i = 0; i < n; i++) {
                int nb = nbs[i];
                VectorD th = Local_Coords((*nbs_searcher->Points_Ptr())[nb] - pos, lf);
                // VectorD th = Local_Coords(points->X(nb) - pos, lf);
                for(int j=0;j<d;j++)data[i*d+j]=th[j];
            }
            mls.Fit(&data[0], (int)n, pi);//// degenerate to LS
            // return mls.c;
            return n;
        }

        
        ////helper function, fit local geometry on an arbitrary position using ASMLS
        int Fit_Local_Geometry_ASMLS(const VectorD& pos, real r, Array<int>& valid_nbs, LeastSquares::ASMLSPointSet<d>& asmls){
            using namespace LeastSquares;
            if(r<=0) r=v_r;
            Array<int> nbs; nbs_searcher->Find_Nbs(pos, r, nbs);

            //// constraint the direction
            // valid_nbs.clear();
            // VectorD normal=lf.col(d-1);
            // for (size_t i = 0; i < nbs.size(); i++) {
            //     if((*normal_ptr)[nbs[i]].dot(normal)>0)
            //         valid_nbs.push_back(nbs[i]);
            // }
            // nbs=valid_nbs;
            valid_nbs=nbs;

            int n=(int)nbs.size();
            if(n<2*d) {
                // std::cout<<"Warning: Fit_Local_Geometry_asmls: no neighbor around "<<pos.transpose()<<std::endl;
                if constexpr(d==2) asmls.c=VectorXd::Zero(3);
                if constexpr(d==3) asmls.c=VectorXd::Zero(6);
                return -1;
            }
            
            ////TOFIX? discontinuous weight function
            Array<real> data(n*d);
            Array<real> normals(n*d);
            Array<real> weights(n, 1.0);
            int pi=-1;
            for (size_t i = 0; i < n; i++) {
                int nb = nbs[i];
                VectorD nb_pos=(*nbs_searcher->Points_Ptr())[nb];
                VectorD nb_normal=(*normal_ptr)[nb];
                // VectorD th = Local_Coords(points->X(nb) - pos, lf);
                for(int j=0;j<d;j++)data[i*d+j]=nb_pos[j];
                for(int j=0;j<d;j++)normals[i*d+j]=nb_normal[j];
                weights[i]=asmls.Weight(nb_pos-pos,r);
            }
            asmls.Fit(&data[0], &weights[0], (int)n);//// degenerate to LS
            // asmls.Fit_With_Normal(&data[0], &normals[0], &weights[0], (int)n);//// degenerate to LS
            // return asmls.c;
            return n;
        }

        ////////////////////////////////////
        //// MLS based operator

        inline real MLS_Newton_F(const LeastSquares::MLSPointSet<d-1>& mls, const VectorD& x, const VectorT& t){
            real h=mls(t);
            VectorD cx; cx<<t,h;//// curve point
            return (cx-x).squaredNorm();
        };

        inline VectorT MLS_Newton_Grad(const LeastSquares::MLSPointSet<d-1>& mls, const VectorD& x, const VectorT& t){
            return 2*(t-x.head(d-1))+2*(mls(t)-x(d-1))*mls.Grad(t);
        };

        inline VectorT MLS_Newton_Lplc(const LeastSquares::MLSPointSet<d-1>& mls, const VectorD& x, const VectorT& t){
            return 2*VectorT::Ones(2)+
                2*( (mls(t)-x(d-1))*mls.Lplc(t) + mls.Grad(t).cwiseProduct(mls.Grad(t)) );
        };

        //// return the closest point on MLS the using Newton method
        VectorD Closest_Point_To_Local_MLS(const VectorD& local_x, const LeastSquares::MLSPointSet<d-1>& mls, const int max_iter=5){
            // using namespace LeastSquares;
            
            // MLSPointSet<d-1> ls;
            // ls.c=c;

            //// f=(x-(t,ls(t)))^2=(x1-t1)^2+(x2-t2)^2+(x3-ls(t1,t2))^2
            // const auto f=[](const LeastSquares::MLSPointSet<d-1>& mls, const VectorD& x, const VectorT& t)->real{
            //     real h=mls(t);
            //     VectorD cx; cx<<t,h;//// curve point
            //     return (cx-x).squaredNorm();
            // };

            //// f_grad_t1=2(t1-x1)+2(ls(t1,t2)-x3)*ls'(t1)
            //// f_grad_t2=2(t2-x2)+2(ls(t1,t2)-x3)*ls'(t2)
            // const auto f_grad=[](const LeastSquares::MLSPointSet<d-1>& mls, const VectorD& x, const VectorT& t)->VectorT{
            //     return 2*(t-x.head(d-1))+2*(mls(t)-x(d-1))*mls.Grad(t);
            // };

            //// 2*(ls(t1,t2)-x3)*ls'(t1)
            //// f_lap_t1=2+2*[(ls(t1,t2)-x3)*ls''(t1) + ls'(t1)*ls'(t1)]
            // const auto f_lplc=[](const LeastSquares::MLSPointSet<d-1>& mls, const VectorD& x, const VectorT& t)->VectorT{
            //     return 2*VectorT::Ones(2)+
            //         2*( (mls(t)-x(d-1))*mls.Lplc(t) + mls.Grad(t).cwiseProduct(mls.Grad(t)) );
            // };

            VectorT t=local_x.head(d-1);
            real step=1.0;
            real decay_rate=1.0;

            for (int iter = 0; iter < max_iter; iter++) {
                VectorT grad=MLS_Newton_Grad(mls,local_x,t);
                VectorT lplc=MLS_Newton_Lplc(mls,local_x,t);
                // std::cout<<"#iter="<<iter<<
                //     ",d^2="<<f(mls,local_x,t)<<
                //     ",grad="<<grad.transpose()<<
                //     ",lplc="<<lplc.transpose()<<std::endl;
                // std::cout<<"t="<<t.transpose();
                for(int i=0;i<d;i++)
                    if(abs(lplc(i))>1e-8)
                        t(i)=t(i)-grad(i)/lplc(i);
                // std::cout<<"->"<<t.transpose()<<std::endl;
            }

            real h=mls(t);
            VectorD result_x; result_x<<t,h;
            return result_x;
        }


        //// geometric operator on MLS
        template<typename F> real Laplacian_MLS(const int& p, const MatrixD& lf, const LeastSquares::MLSPointSet<d-1>& mls, const F& phi)
        {
            ////following Codim-MLS fluid by Wang
            VectorX coeff=Fit_Local_MLS(p,lf,phi);
            MatrixT g=mls.Metric_Tensor();
            MatrixT g_inv=g.inverse();
            real g_det=g.determinant();
            MatrixT par2;
            if constexpr(d==2){
                par2(0)=2*coeff(2);}
            else if constexpr(d==3){
                par2(0,0)=2*coeff(3);
                par2(1,1)=2*coeff(4);
                par2(0,1)=par2(1,0)=coeff(5);}

            real lplc=(real)0.0;
            for(int tk=0;tk<d-1;tk++){
                for(int tl=0;tl<d-1;tl++){
                    lplc+=g_inv(tk,tl)*par2(tl,tk);}}
            return lplc;
        }

        //// geometric operator on MLS
        template<typename F> real Laplacian_MLS(const VectorD& pos, const MatrixD& lf, const LeastSquares::MLSPointSet<d-1>& mls, const F& phi)
        {
            ////following Codim-MLS fluid by Wang
            VectorX coeff=Fit_Local_MLS(pos,lf,phi);
            if(coeff.norm()==0) {Warn("Laplacian_MLS: incorrect norm at pos {}", pos.transpose()); return 0;}
            MatrixT g=mls.Metric_Tensor();
            MatrixT g_inv=g.inverse();
            real g_det=g.determinant();
            MatrixT par2;
            if constexpr(d==2){
                par2(0)=2*coeff(2);}
            else if constexpr(d==3){
                par2(0,0)=2*coeff(3);
                par2(1,1)=2*coeff(4);
                par2(0,1)=par2(1,0)=coeff(5);}

            real lplc=(real)0.0;
            for(int tk=0;tk<d-1;tk++){
                for(int tl=0;tl<d-1;tl++){
                    lplc+=g_inv(tk,tl)*par2(tl,tk);}}
            return lplc;
        }

        // //// find closest point by simply removing its bias to mls surface
        // int Nearest_Geometry(VectorD& pos, MatrixD& local_frame, const VectorD& search_pos, int minimum_eqns, bool verbose) const{
        //     ////The current implementation only conducts one iteration. The function can be called for multiple times for an iterative projection.
        //     using namespace LeastSquares;
        //     // local_frame = Local_Frame(pos);
        //     local_frame = Local_Frame(search_pos);
            
        //     // Array<int> nbs = Find_Tangential_Nbs(pos, local_frame);
        //     // Array<int> nbs; nbs_searcher->Find_Nbs(pos, v_r, nbs);
        //     Array<int> nbs; nbs_searcher->Find_Nbs(search_pos, v_r, nbs);

        //     MLS<d - 1, 2> ls;
        //     size_t n = nbs.size(); Array<real> data(n * (size_t)d);
        //     if (verbose) std::cout << "number of neighbors? " << nbs.size() << std::endl;
        //     if (n < minimum_eqns) {
        //         // int closest_p = Closest_Point(pos);
        //         int closest_p = Closest_Point(pos);
        //         // VectorD norm = points->Normal(closest_p);
        //         MatrixD E = Local_Frame((*nbs_searcher->Points_Ptr())[closest_p]);
        //         VectorD norm = E.col(d - 1);
        //         // real offset = norm.normalized().dot(pos - points->X(closest_p));
        //         real offset = norm.normalized().dot(pos - (*nbs_searcher->Points_Ptr())[closest_p]);
        //         pos = pos - offset * norm;
        //         if(verbose)	std::cout << "[Warning]PointSet<d>::Project_To_Surface: " <<pos.transpose()<<" , "<< search_pos.transpose() << " find only " << n << " neighbors against minimum " << minimum_eqns << "\n";
        //         return 1;
        //     }
        //     else {
        //         for (size_t i = 0; i < n; i++) {
        //             int p = nbs[i];
        //             // VectorD u = points->X(p) - pos;
        //             VectorD u = (*nbs_searcher->Points_Ptr())[p] - pos;
        //             VectorD th = Local_Coords(u, local_frame);
        //             for (size_t j = 0; j < d; j++)data[i * (size_t)d + j] = th[j];
        //         }
        //         ls.Fit(data.data(), (int)n, 0, 0);
        //         // pos = pos + local_frame.col(d - 1) * ls(VectorT::Zero());
        //         //// TODO: it's hard to project the pos onto the surface
        //         pos = search_pos + local_frame.col(d - 1) * ls(VectorT::Zero());

        //         return 0;
        //     }
        // }

        // int Nearest_Geometry(VectorD& pos, MatrixD& local_frame, int minimum_eqns, bool verbose) const{
        //     return Nearest_Geometry(pos,local_frame,pos,minimum_eqns,verbose);
        // }
        
    };
    
}