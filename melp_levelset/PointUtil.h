//////////////////////////////////////////////////////////////////////////
// Point Util, basic operators used for points manipulation
// Copyright (c) (2022-), Hui Wang
// This file is part of MESO, whose distribution is governed by the LICENSE file.
//////////////////////////////////////////////////////////////////////////
#pragma once

#include "EulerianParticles.h"
#include "NeighborSearcher.h"
#include "LeastSquares.h"

#include <set>

namespace Meso {
    class PointUtil{
    public:
        // template<class T>
        // int Remove_Vector_Elements(const std::set<int>& remove_set, Array<T>& arr);

        // template<class ...Args>
        // int Remove_Vectors_Elements(const std::set<int>& remove_set, Args&... args);
        
        template<class T>
        static void Swap_Attribute_Element(Array<T>& arr, int i, int j){
            auto tmp=arr[i];
            arr[i]=arr[j];
            arr[j]=tmp;
        }
            
        template<class T>
        static int Remove_Vector_Elements(const std::set<int>& remove_set, Array<T>& arr){
            int tid=arr.size();//// tail id

            //// swap the removed element to the tail
            std::set<int>::reverse_iterator rit;
            //// large->small
            for (rit=remove_set.rbegin();rit!=remove_set.rend();rit++){
                tid--;
                Swap_Attribute_Element<T>(arr,(*rit),tid);
            }

            //// remove together
            arr.erase(arr.begin()+tid,arr.end());
            return remove_set.size();
        }

        template<class T>
        static int Append_Vector_Elements(Array<T>& arr, const Array<T>& insert_arr){
            arr.insert(arr.end(), insert_arr.begin(), insert_arr.end());
            return insert_arr.size();
        }

        template<class T>
        static int Append_Vector_Elements(Array<T>& arr, int n, const T& elem){
            arr.insert(arr.end(), n, elem);
            return n;
        }

        template<class T>
        static int Transfer_Vector_Elements(Array<T>& dest_arr, Array<T>& src_arr, std::set<int> indices){
            //// TODO: accelerate
            for (auto i: indices){
                dest_arr.push_back(src_arr[i]);
            }
            return indices.size();
        }

        template<class T>
        static int Move_Vector_Elements(Array<T>& dest_arr, Array<T>& src_arr){
            dest_arr.reserve(dest_arr.size() + src_arr.size());
            std::move(src_arr.begin(), src_arr.end(), std::inserter(dest_arr, dest_arr.end()));
            src_arr.clear();
        }

        static void Check_Points_Size(Points& points){
           for (auto iter = points.att_map.begin(); iter != points.att_map.end(); iter++) {
                if(iter->second->Size()!=points.Size())
                    Error("incorrect attribute size #{}={}!={}", iter->first, iter->second->Size(), points.Size());
            }
        }


        // int _Remove_Vectors_Elements(const std::set<int>& remove_set){
        //     return 0;
        // }

        // template<class T, class ...Args>
        // int _Remove_Vectors_Elements(const std::set<int>& remove_set, Array<T>& arr, Args&... args){
        //     _Remove_Vector_Elements(remove_set, arr);
        //     return _Remove_Vectors_Elements(remove_set, args...);
        // }

        // template<class T>
        // int Remove_Vector_Elements(const std::set<int>& remove_set, Array<T>& arr){
        //     return _Remove_Vector_Elements<T>(remove_set, arr);
        // }

        // template<class ...Args>
        // int Remove_Vectors_Elements(const std::set<int>& remove_set, Args&... args){
        //     return _Remove_Vectors_Elements(remove_set, args...);
        // }


    };
}