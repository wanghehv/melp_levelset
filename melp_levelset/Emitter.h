#pragma once

#include "FaceField.h"
#include "EulerianParticles.h"
#include "LagrangianParticles.h"

namespace Meso{
    template<int d>
    class Emitter{
    public:
        virtual void Force_Velocity_Field(FaceField<real,d>& velocity)=0;
        virtual void Force_Eulerian_Particles(EulerianParticles<d>& particles)=0;
        virtual void Force_Lagrangian_Particles(LagrangianParticles<d>& particles)=0;
    };

    template<int d>
    class BoxEmitter : public Emitter<d>{
        Typedef_VectorD(d);
        Typedef_MatrixD(d);
    public:
        VectorD min_corner, max_corner;
        VectorD v;
        BoxEmitter(const VectorD& _min_corner, const VectorD& _max_corner, const VectorD& _v) : min_corner(_min_corner),max_corner(_max_corner),v(_v){;}
        virtual void Force_Velocity_Field(FaceField<real,d>& velocity){
            velocity.Exec_Faces([&](const int axis, const VectorDi face) {
                VectorD fx=velocity.grid.Face_Center(axis,face);
                bool is_inside=true;
                for(int dd=0;dd<d;dd++){
                    if(fx(dd)<min_corner(dd) || fx(dd)>=max_corner(dd)){
                        is_inside=false;
                        break;
                    }
                }
                if(is_inside) {velocity(axis,face)=v(axis);}
            });
        };
        virtual void Force_Eulerian_Particles(EulerianParticles<d>& particles){
            //// mark all inner particles as fixed
            #pragma omp parallel for
            for(int i=0;i<particles.Size();i++){
                VectorD& x_i=particles.x(i);
                bool is_fixed=true;
                for(int dd=0;dd<d;dd++){
                    if(x_i(dd)<min_corner(dd) || x_i(dd)>=max_corner(dd)){
                        is_fixed=false;
                        break;
                    }
                }
                if(is_fixed) particles.t(i)=1;
            }
        };

        virtual void Force_Lagrangian_Particles(LagrangianParticles<d>& particles){}
        
    };

    template<int d>
    class BoxForcer : public Emitter<d>{
        Typedef_VectorD(d);
        Typedef_MatrixD(d);
    public:
        VectorD min_corner, max_corner;
        VectorD v;
        BoxForcer(const VectorD& _min_corner, const VectorD& _max_corner, const VectorD& _v) : min_corner(_min_corner),max_corner(_max_corner),v(_v){;}
        virtual void Force_Velocity_Field(FaceField<real,d>& velocity){};
        
        virtual void Force_Eulerian_Particles(EulerianParticles<d>& particles){
            #pragma omp parallel for
            for(int i=0;i<particles.Size();i++){
                VectorD& x_i=particles.x(i);
                bool is_inside=true;
                for(int dd=0;dd<d;dd++){
                    if(x_i(dd)<min_corner(dd) || x_i(dd)>=max_corner(dd)){
                        is_inside=false;
                        break;
                    }
                }
                if(is_inside){
                    VectorD& n_i=particles.n(i);
                    //// use cross product as velocity
                    if constexpr(d==3) particles.vt(i)=n_i.cross(v);
                    if constexpr(d==2) ;//// only used in 3d

                }
            }
        };
        virtual void Force_Lagrangian_Particles(LagrangianParticles<d>& particles){
            #pragma omp parallel for
            for(int i=0;i<particles.Size();i++){
                VectorD& x_i=particles.x(i);
                bool is_inside=true;
                for(int dd=0;dd<d;dd++){
                    if(x_i(dd)<min_corner(dd) || x_i(dd)>=max_corner(dd)){
                        is_inside=false;
                        break;
                    }
                }
                if(is_inside){
                    VectorD& n_i=particles.n(i);
                    //// use cross product as velocity
                    if constexpr(d==3) particles.v(i)=n_i.cross(v);
                    if constexpr(d==2) ;//// only used in 3d

                }
            }
        }
    };


}