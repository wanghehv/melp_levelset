//////////////////////////////////////////////////////////////////////////
// Initializer of FluidPLS
// Copyright (c) (2022-), Hui Wang
// This file is part of MESO, whose distribution is governed by the LICENSE file.
//////////////////////////////////////////////////////////////////////////
#pragma once
#include "FluidPLS.h"
#include "ImplicitManifold.h"
#include "Json.h"
#include "GridEulerFunc.h"

namespace Meso {
    template<int d>
    class FluidPLSInitializer {
    public:
        Typedef_VectorD(d);
        Typedef_MatrixD(d);

        void Apply(json& j, FluidPLS<d>& fluid) {
            std::string test = Json::Value(j, "test", std::string("zalesaks_sphere"));
            if (test == "zalesaks_sphere") Init_Zalesaks_Sphere(j, fluid);
            else if (test == "single_vortex") Init_Single_Vortex(j, fluid);
            else if (test == "cube_oscillation") Init_Cube_Oscillation(j, fluid);
            else Assert(false, "test {} not exist", test);

            fluid.Regenerate_PLS();
            
            //// init local_geom
            std::shared_ptr<NeighborSearcher<d>> nbs_searcher=std::make_shared<NeighborKDTree<d> >();
            std::shared_ptr<PointLocalGeometry<d>> local_geom=std::make_shared<PointLocalGeometry<d> > (fluid.pls.levelset.phi.grid.dx, 2/1.5, nbs_searcher);
            fluid.local_geom=local_geom;

            //// init particle levelset particles normal
            nbs_searcher->Update_Points(fluid.pls.particles.xRef());
            #pragma omp parallel for
            for(int i=0;i<fluid.pls.particles.Size();i++){
                VectorD pos=fluid.pls.particles.x(i);
                MatrixD local_frame=local_geom->Local_Frame(pos);
                VectorD normal=local_frame.col(d-1);
                if(normal.dot(LevelSetUtil<d>::Normal(fluid.pls.levelset,pos))<0)
                    normal=-normal;
                fluid.pls.particles.n(i)=normal;
            }

            //// init cell type
            fluid.cell_type.Init(fluid.pls.levelset.phi.grid, CellType::FLUID);
            Eigen::Matrix<int, 3, 2> bc_width;
            bc_width << 1, 1, 1, 1, 1, 1;
            GridEulerFunc::Set_Boundary_Cells(fluid.cell_type, bc_width, CellType::SOLID);
            
            //// init other params
            fluid.Init(j);

            Info("FluidPLSInitializer::initialization finished");
        }

        void Init_Single_Vortex(json& j, FluidPLS<d>& fluid) {
            //// ref to Enright, "A hybrid particle level set method for improved interface capturing..." Sec 3.2
            //// sphere at (50,75,50) with radiu r=15, in a 100x100x100 domain
            //// slot width 5, deep 12.5
            if constexpr(d==3){
                Error("No single vortex test for 3D");
            }
            int scale = Json::Value<int>(j, "scale", 32);
            real side_len = 1.0;
            real dx = side_len / scale;
            VectorDi grid_size = scale * MathFunc::Vi<d>(1, 1, 1);
            Grid<d> grid(grid_size, dx, VectorD::Zero(), CENTER);

            //// init levelset
            VectorD x=grid.Center();
            x[1]=side_len*0.75;
            Sphere<d> sphere(x, side_len*0.15);
            fluid.pls.levelset.Init(grid, sphere);
            
            //// init velocity field
            //// stream function \Psi=1/pi * sin^2(pi*x) * sin^2(pi*y)
            //// u_x=-2*cos(pi*y)*sin(pi*y)*sin^2(pi*x)
            //// u_y=2*cos(pi*x)*sin(pi*x)*sin^2(pi*y)
            fluid.velocity.Init(grid,0);
            fluid.velocity.Calc_Faces(
                [&](const int axis, const VectorDi face) {
                	VectorD pos = grid.Face_Center(axis, face);
                    //// u in x-axis
                    if(axis==0)
                        return  2*cos(CommonConstants::pi*pos(1))*sin(CommonConstants::pi*pos(1)) * pow(sin(CommonConstants::pi*pos(0)),2);
                    //// v in y-axis
                    if(axis==1)
                        return  -2*cos(CommonConstants::pi*pos(0))*sin(CommonConstants::pi*pos(0)) * pow(sin(CommonConstants::pi*pos(1)),2);
                    return 0.0;
                }
            );

        }

        void Init_Zalesaks_Sphere(json& j, FluidPLS<d>& fluid) {
            //// ref to Enright, "A hybrid particle level set method for improved interface capturing..." Sec 3.1
            //// sphere at (50,75,50) with radiu r=15, in a 100x100x100 domain
            //// slot width 5, deep 12.5
            int scale = Json::Value<int>(j, "scale", 32);
            real side_len = 1.0;
            real dx = side_len / scale;
            VectorDi grid_size = scale * MathFunc::Vi<d>(1, 1, 1);
            Grid<d> grid(grid_size, dx, VectorD::Zero(), CENTER);

            VectorD x=grid.Center();
            x[1]=side_len*0.75;
            Sphere<d> sphere(x, side_len*0.15);
            
            BoxShape<d> slot_shape;
            if constexpr(d==2){
                slot_shape.min_corner=VectorD(0.5-0.025,0.75+0.15-0.125)*side_len;
                slot_shape.max_corner=VectorD(0.5+0.025,0.75+0.15+0.05)*side_len;
            }
            if constexpr(d==3){
                slot_shape.min_corner=VectorD(0.5-0.025,0.75+0.15-0.125,0)*side_len;
                slot_shape.max_corner=VectorD(0.5+0.025,0.75+0.15+0.05,1)*side_len;
            }            

            //// init levelset
            fluid.pls.levelset.Init(grid);
            fluid.pls.levelset.phi.Calc_Nodes(
                [&](const VectorDi cell) {
                    real slot_phi=slot_shape.Phi(grid.Position(cell));
                    real sphere_phi=sphere.Phi(grid.Position(cell));
                    // return min(slot_phi,sphere_phi);
                    return max(-slot_phi,sphere_phi);
                    // return sphere_phi;
                }
			);

            //// init velocity field
            fluid.velocity.Init(grid,0);
            fluid.velocity.Calc_Faces(
                [&](const int axis, const VectorDi face) {
                	VectorD pos = grid.Face_Center(axis, face);
                    //// u in x-axis
                    if(axis==0)
                        return  (CommonConstants::pi/314)*(0.5*side_len-pos(1));
                    //// v in y-axis
                    if(axis==1)
                        return (CommonConstants::pi/314)*(pos(0)-0.5*side_len);
                    return 0.0;
                }
            );
        }
    
        void Init_Cube_Oscillation(json& j, FluidPLS<d>& fluid) {
            int scale = Json::Value<int>(j, "scale", 32);
            real length = Json::Value<real>(j, "length", 0.75); //// x
            real height = Json::Value<real>(j, "height", 0.25); //// yz
            real side_len = 1.0;
            real dx = side_len / scale;
            VectorDi grid_size = scale * MathFunc::Vi<d>(1, 1, 1);
            Grid<d> grid(grid_size, dx, VectorD::Zero(), CENTER);

            //// init levelset
            VectorD min_corner=VectorD::Ones()*(side_len*(1-height)*0.5);
            min_corner.x()=side_len*(1-length)*0.5;
            VectorD max_corner=VectorD::Ones()*(side_len*(1+height)*0.5);
            max_corner.x()=side_len*(1+length)*0.5;

            Box<d> box(min_corner, max_corner);
            fluid.pls.levelset.Init(grid, box);
            
            //// init velocity field
            fluid.velocity.Init(grid,0);
        }
    };
}