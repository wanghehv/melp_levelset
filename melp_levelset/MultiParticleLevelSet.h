//////////////////////////////////////////////////////////////////////////
// Particle levelset
// Copyright (c) (2022-), Hui Wang
// This file is part of MELPLEVELSET, whose distribution is governed by the LICENSE file.
//////////////////////////////////////////////////////////////////////////
#pragma once

#include "EulerianParticles.h"
#include "LagrangianParticles.h"
#include "LevelSet.h"
#include "Region.h"
#include "ParticleLevelSetHelper.h"
#include "MultiParticleLevelSetHelper.h"
#include "PointUtil.h"
#include "Random.h"

namespace Meso {
    template<int d>
    class MultiParticleLevelSet {
        Typedef_VectorD(d);
        Typedef_MatrixD(d);
        using VectorT=Vector<real,d-1>;  ////tangential vector type
        using VectorTi=Vector<int,d-1>;  ////tangential vector int
    public:

    
        LevelSet<d> levelset;
        Field<int,d> indicator;
        std::map<int, std::shared_ptr<Region<d>>> region_map;
        Field<Array<std::pair<int, real>>,d> narrowband_phi;
        std::shared_ptr<LagrangianParticles<d>> l_particles;
        int region_offset;
        
        MultiParticleLevelSet() {}
        MultiParticleLevelSet(const Grid<d> _grid) {
            Init(_grid);
        }

        void Init(const Grid<d> _grid){
            Assert(_grid.Is_Unpadded(), "MultiParticleLevelSet::Init _grid={}, padding not allowed", _grid);
            levelset.Init(_grid);
            indicator.Init(_grid, -1);
            narrowband_phi.Init(_grid);
            l_particles=std::make_shared<LagrangianParticles<d>>();
            region_offset=0;
        }

    public:
        ///////////////////////////////////////
        //// helper function for add a new region
        void Init_Region_Levelset(int rid, const LevelSet<d>& _levelset){
            Grid<d>& grid=levelset.phi.grid;
            int narrow_band_cell_num=5;
            real narrow_band_width=grid.dx*narrow_band_cell_num;

            levelset.phi.Exec_Nodes([&](const VectorDi cell) {
                if(_levelset.phi(cell)<0){
                    if(levelset.phi(cell)<0 || indicator(cell)>=0){
                        // Warn("{} already in region {}",cell.transpose(), indicator(cell));
                    }
                    levelset.phi(cell)=_levelset.phi(cell);
                    indicator(cell)=rid;
                }else if(_levelset.phi(cell)<narrow_band_width){
                    levelset.phi(cell)=max(-_levelset.phi(cell),levelset.phi(cell));
                }
            });
        }

        void Init_Region_Particles(int rid, std::shared_ptr<EulerianParticles<d>>& particles){
            //// particles
            auto iter = region_map.find(rid);
            if(iter != region_map.end()) iter->second->particles=particles;
            else Warn("Init_Region_Particles: find no region {}", rid);
            // else region.insert(std::make_pair(rid,particles));
        }

        void Add_Lagrangian_Particles(const LevelSet<d>& _levelset){
            std::shared_ptr<LagrangianParticles<d>> tmp_particles=std::make_shared<LagrangianParticles<d>>();

            ParticleLevelSetHelper<d>::Regenerate_Particles_On_Levelset(*tmp_particles, _levelset);

            l_particles->Append(*tmp_particles);
        }

        void Add_Lagrangian_Particles(const LevelSet<d>& _levelset, const VectorD& pos, int size){
            std::shared_ptr<LagrangianParticles<d>> tmp_particles=std::make_shared<LagrangianParticles<d>>();
            tmp_particles->Resize(size);
            for(int i=0;i<size;i++){
                VectorD x=pos+Random::Uniform_In_Box<real,d>(-VectorD::Ones(),VectorD::Ones())*_levelset.phi.grid.dx*0.1f;
                // std::cout<<"inser x="<<x.transpose()<<std::endl;
                x=LevelSetUtil<d>::Closest_Point_With_Iterations(_levelset,x,5);
                // std::cout<<"insert x="<<x.transpose()<<std::endl;
                tmp_particles->x(i)=x;
                tmp_particles->v(i)=VectorD::Zero();
                tmp_particles->n(i)=LevelSetUtil<d>::Normal(_levelset,pos);
            }
            l_particles->Append(*tmp_particles);
        }

        ///////////////////////////////////////
        //// helper func
        bool Is_Interface(const VectorDi& cell0, const VectorDi& cell1) const {
            return (indicator(cell0)!=-1 && indicator(cell1)!=-1) && (indicator(cell0)!=indicator(cell1));
        }

        static real Theta(real phi0, real phi1){
            //// in mpls, the levelset are all negative
            phi0 = std::abs(phi0);
            phi1 = -std::abs(phi1);
            return phi0 / (phi0 - phi1);
        }
        
        ///////////////////////////////////////
        //// add region func
        int Add_Empty_Region(typename Region<d>::RegionType _type){
            int i=region_offset;
            region_map.insert(std::make_pair(i,std::make_shared<Region<d>>(_type)));
            region_offset++;
            return i;
        }

        // int Add_Region(const LevelSet<d>& _levelset, std::shared_ptr<EulerianParticles<d>>& _particles){
        //     int rid=Add_Empty_Region();
        //     Init_Region_Levelset(rid, _levelset);
        //     Init_Region_Particles(rid, _particles);
        //     return rid;
        // }

        int Add_Region(const LevelSet<d>& _levelset, typename Region<d>::RegionType _type=Region<d>::RegionType::AIR){
            int rid=Add_Empty_Region(_type);
            Init_Region_Levelset(rid, _levelset);
            std::shared_ptr<EulerianParticles<d>> _particles=std::make_shared<EulerianParticles<d>>();
            ParticleLevelSetHelper<d>::Regenerate_Particles_On_Levelset(*_particles, _levelset);
            Init_Region_Particles(rid, _particles);
            return rid;
        }
        
        //// remove region from region map
        int Remove_Region(int i){
            auto iter = region_map.find(i);
            if(iter != region_map.end()){
                region_map.erase(i);
            }else{
                Warn("MultiParticleLevelSet::Remove_Region_Particles warning: cannot find the levelset {}", i);
            }
            return i;
        }

        int Duplicate_Empty_Region(int i){
            auto iter_i = region_map.find(i);
            int j=Add_Empty_Region(iter_i->second->type);
            auto iter_j = region_map.find(j);
            iter_i->second->Copy_Param(*iter_j->second);
            //// TODO: fix it
            iter_j->second->rho=DefaultDynamicsParameter::air_density*50;
            return j;
        }


        int Merge_Regions(int i, int j, std::shared_ptr<PointLocalGeometry<d>> local_geom){
            Grid<d>& grid=levelset.phi.grid;
            //// ensure i<j, merge j into i
            if(i>j) {int tmp=i; i=j; j=tmp;}

            if(i==j){
                Warn("MultiParticleLevelSet::Merge_Regions warning: same region id {}=={}", i, j);
                return -1;
            }
            auto iter_i = region_map.find(i);
            if(iter_i == region_map.end()){
                Warn("MultiParticleLevelSet::Merge_Regions warning: find no region {}",i);
                return -1;
            }
            auto iter_j = region_map.find(j);
            if(iter_j == region_map.end()){
                Warn("MultiParticleLevelSet::Merge_Regions warning: find no region {}",j);
                return -1;
            }
            if(iter_i->second->type!=iter_j->second->type){
                Warn("MultiParticleLevelSet::Merge_Regions warning: different region type (Region {}){}<>(Region {}){}",i,iter_i->second->type, j, iter_j->second->type);
                return -1;
            }

            //// update indicator
            indicator.Exec_Nodes([&](const VectorDi cell) {
                if(indicator(cell)==j)
                    indicator(cell)=i;
            });
            // std::cout<<"update indicator"<<std::endl;

            //// merge particles
            local_geom->nbs_searcher->Update_Points(iter_j->second->particles->xRef());
            local_geom->normal_ptr=std::make_shared<Array<VectorD> >(iter_j->second->particles->nRef());
            std::set<int> remove_set_i, remove_set_j;
            #pragma omp parallel for
            for(int pi=0;pi<iter_i->second->particles->Size();pi++){
                VectorD x_i=iter_i->second->particles->x(pi);
                Array<int> nbs; local_geom->nbs_searcher->Find_Nbs(x_i, grid.dx*0.75, nbs);
                if(nbs.size()>0) remove_set_i.insert(pi);
                for(int jj=0;jj<nbs.size();jj++){
                    int pj=nbs[jj]; 
                    #pragma omp crtical
                    remove_set_j.insert(pj);
                }
            }
            // std::cout<<"remove set"<<std::endl;

            ParticleLevelSetHelper<d>::Remove_Particles(*iter_i->second->particles, remove_set_i);
            ParticleLevelSetHelper<d>::Remove_Particles(*iter_j->second->particles, remove_set_j);

            ParticleLevelSetHelper<d>::Append_Particles(*iter_i->second->particles, *iter_j->second->particles);

            //// volume
            iter_i->second->init_volume=iter_i->second->init_volume+iter_j->second->init_volume;
            iter_i->second->use_volume_control=iter_i->second->use_volume_control && iter_j->second->use_volume_control;

            this->Remove_Region(j);
        }

        ///////////////////////////////////////
        //// fix geometry
        void Correction(std::shared_ptr<PointLocalGeometry<d>> local_geom){
            levelset.phi.Exec_Nodes([&](const VectorDi cell) {
                narrowband_phi(cell).clear();
            });
            
            Grid<d> grid=levelset.phi.grid;
            int max_iteration=1;

            VectorDi ref_cell; ref_cell<<23,15;

            Timer timer;

            //// build narrowband_phi by particle mls
            for (auto itr = region_map.begin(); itr != region_map.end(); ++itr) {
                int rid=itr->first;
                std::shared_ptr<EulerianParticles<d>> particles=itr->second->particles;

                local_geom->nbs_searcher->Update_Points(particles->xRef());
                local_geom->normal_ptr=std::make_shared<Array<VectorD> >(particles->nRef());

                Info("MultiParticleLevelSet::Correction::update points, time={}", timer.Lap_Time());

                levelset.phi.Exec_Nodes([&](const VectorDi cell) {
                    
                    // if(cell==ref_cell) {
                    //     std::cout<<"debug cell: ("<<cell.transpose()<<"), phi="<<levelset.phi(cell)<<", region="<<rid<<std::endl;
                    // }

                    // real phi=levelset.phi(cell);
                    // if(abs(phi)>2*levelset.phi.grid.dx) return;

                    // real sign=(phi>=0?1:-1);
                    real sign=0; ////useless define
                    VectorD cell_x=grid.Position(cell);
                    
                    int ci=local_geom->Closest_Point(cell_x);
                    VectorD intf_x=particles->x(ci);
                    VectorD normal=particles->n(ci);
                    MatrixD local_frame;

                    // if(cell==ref_cell) {
                    //     std::cout<<"debug cell: ("<<cell.transpose()<<"),"<<"intf x=("<<intf_x.transpose()<<"), intf phi="<<levelset.Phi(intf_x)<<"<>"<<2*levelset.phi.grid.dx<<", region="<<rid<<std::endl;
                    // }
                    // if(abs(levelset.Phi(intf_x))>2*levelset.phi.grid.dx) return;
                    // if((intf_x-cell_x).norm()>3*levelset.phi.grid.dx) return;
                    
                    int iteration=0;
                    for(;iteration<max_iteration;iteration++){
                        local_frame=local_geom->Spawn_Local_Frame(normal);
                        LeastSquares::MLSPointSet<d-1> mls;
                        Array<int> nbs;
                        int n=local_geom->Fit_Local_Geometry_MLS(intf_x,local_frame, nbs, mls);
                    
                        // if(cell==ref_cell) {
                        //     std::cout<<"debug cell: ("<<cell.transpose()<<"), intf phi="<<levelset.Phi(intf_x)<<", region="<<rid<<", n="<<n<<std::endl;
                        // }
                    
                        if(n<d*2) break;

                        //// find closest point on MLS surface
                        VectorD u=cell_x-intf_x;
                        VectorD local_pos=local_geom->Local_Coords(u,local_frame);
                        VectorD new_local_pos=local_geom->Closest_Point_To_Local_MLS(local_pos, mls, 5);

                        //// limit local pos on tangent plane
                        Array<VectorT> local_nbs_t_pos;
                        for(int i=0;i<nbs.size();i++){
                            VectorD u=particles->x(nbs[i])-intf_x;
                            local_nbs_t_pos.push_back(local_geom->Local_Coords(u,local_frame).head(d-1));
                        }
                        VectorT limit_local_pos=local_geom->Limit_Local_Tangent(new_local_pos.head(d-1), local_nbs_t_pos);
                        new_local_pos<<limit_local_pos, mls(limit_local_pos);

                        //// get normal vector
                        VectorD local_normal=local_geom->Normal_On_Local_MLS(new_local_pos.head(d-1), mls);
                        VectorD new_normal=local_geom->Unproject_To_World(local_normal,local_frame).normalized();
                        normal=(new_normal.dot(normal)>0)?new_normal:-new_normal;

                        // if(cell==ref_cell) {
                        //     // debug_pos.push_back(intf_x);
                        //     // debug_normal.push_back(normal);

                        //     //// print curve here to visualize the surface!!
                        //     {
                        //         VectorT st; st<<-grid.dx; VectorD sx; sx<<st, mls(st); VectorD wx=local_geom->Unproject_To_World(sx, local_frame)+intf_x;
                        //         std::cout<<"debug cell: "<<wx(0)<<","<<wx(1)<<",0,"<<std::endl;
                        //     }
                        //     {
                        //         VectorT st; st<<-grid.dx/2; VectorD sx; sx<<st, mls(st); VectorD wx=local_geom->Unproject_To_World(sx, local_frame)+intf_x;
                        //         std::cout<<"debug cell: "<<wx(0)<<","<<wx(1)<<",0,"<<std::endl;
                        //     }
                        //     {
                        //         VectorT st; st<<0; VectorD sx; sx<<st, mls(st); VectorD wx=local_geom->Unproject_To_World(sx, local_frame)+intf_x;
                        //         std::cout<<"debug cell: "<<wx(0)<<","<<wx(1)<<",0,"<<std::endl;
                        //     }
                        //     {
                        //         VectorT st; st<<grid.dx/2; VectorD sx; sx<<st, mls(st); VectorD wx=local_geom->Unproject_To_World(sx, local_frame)+intf_x;
                        //         std::cout<<"debug cell: "<<wx(0)<<","<<wx(1)<<",0,"<<std::endl;
                        //     }
                        //     {
                        //         VectorT st; st<<grid.dx; VectorD sx; sx<<st, mls(st); VectorD wx=local_geom->Unproject_To_World(sx, local_frame)+intf_x;
                        //         std::cout<<"debug cell: "<<wx(0)<<","<<wx(1)<<",0,"<<std::endl;
                        //     }
                        // }
                        intf_x=local_geom->Unproject_To_World(new_local_pos, local_frame)+intf_x;
                        
                        // if(cell==ref_cell) {
                        //     std::cout<<"debug cell: "<<"iteration="<<iteration<<std::endl;
                        //     std::cout<<"debug cell: "<<"new_local_pos="<<new_local_pos.transpose()<<std::endl;
                        //     std::cout<<"debug cell: "<<"new_pos="<<intf_x.transpose()<<std::endl;
                        //     std::cout<<"debug cell: "<<"normal="<<normal.transpose()<<std::endl;
                        //     std::cout<<"debug cell: "<<"n="<<n<<", <mls="<<mls.c.transpose()<<std::endl;
                        // }

                    }

                    //// update levelset
                    VectorD rel=cell_x-intf_x;
                    real dist=rel.norm();

                    real new_sign=rel.dot(normal)<0?-1:1;
                    sign=new_sign;
                    
                    // if(cell==ref_cell) {
                    //     std::cout<<"after iteration"<<std::endl;
                    //     std::cout<<"cell="<<cell.transpose()<<", phi="<<levelset.phi(cell)<<"->"<<sign*rel.norm()<<std::endl;
                    //     std::cout<<"n="<<normal.transpose()<<", rel=" << rel.transpose()<<",nr="<<rel.dot(normal)<<std::endl;
                    //     std::cout<<"intf_x="<<intf_x.transpose()<<std::endl;
                    // }
                    // if(iteration>0 && abs(levelset.phi(cell)-sign*dist)<bias_threshold)
                    //     levelset.phi(cell)=sign*dist;
                    if(iteration>0)
                        narrowband_phi(cell).push_back(std::make_pair(rid,sign*dist));
                });
                Info("MultiParticleLevelSet::Correction::update narrowband, time={}", timer.Lap_Time());
            }

            #define pii std::pair<real,int>
            //// correct on narrowband_phi, following "2006-Multiple Interacting Liquids"
            //// setup levelset
            levelset.phi.Exec_Nodes([&](const VectorDi cell) {
                std::priority_queue<pii, Array<pii>, std::greater<pii>> min_heap;//// min at top
                // std::cout<<"narrowband phi at ("<<cell.transpose()<<"), #"<<narrowband_phi(cell).size()<<std::endl;

                //// find min phi
                for(int l=0;l<narrowband_phi(cell).size();l++){
                    std::pair<int, real> pair=narrowband_phi(cell)[l];
                    min_heap.push({pair.second, pair.first});
                }

                if(min_heap.size()==0) {levelset.phi(cell)=1e8; indicator(cell)=-1;}
                else if(min_heap.size()==1) {
                    auto [phi, rid] = min_heap.top();
                    levelset.phi(cell)=phi; indicator(cell)=rid;
                    // if(phi<=0) {levelset.phi(cell)=phi; indicator(cell)=rid;}
                    // else {levelset.phi(cell)=1e8; indicator(cell)=-1;}
                }else if(min_heap.size()>=2) {
                    auto [phi0, rid0] = min_heap.top();
                    min_heap.pop();
                    auto [phi1, rid1] = min_heap.top();
                    min_heap.pop();

                    real mean_phi=0.5*(phi0+phi1);
                    phi0-=mean_phi;
                    phi1-=mean_phi;

                    for(int l=0;l<narrowband_phi(cell).size();l++){
                        narrowband_phi(cell)[l].second-=mean_phi;
                        // std::cout<<"debug: phi-="<<mean_phi<<"at ("<<cell.transpose()<<")"<<std::endl;
                    }

                    // levelset.phi(cell)=phi0<phi1?phi0:phi1;
                    indicator(cell)=phi0<phi1?rid0:rid1;
                }
                levelset.phi(cell)=1e8;
                
            });
            Info("MultiParticleLevelSet::Correction::correct narrowband, time={}", timer.Lap_Time());

            #undef pii

            //// correct particles by the narrowband_phi, push particles onto phi=0, update phi
            for (auto itr = region_map.begin(); itr != region_map.end(); ++itr) {
                int rid=itr->first;
                std::shared_ptr<EulerianParticles<d>> particles=itr->second->particles;

                LevelSet<d> tmp_levelset(levelset.phi.grid);
                levelset.phi.Exec_Nodes([&](const VectorDi cell) {
                    for(int l=0;l<narrowband_phi(cell).size();l++){
                        std::pair<int, real> pair=narrowband_phi(cell)[l];
                        if(pair.first==rid){
                            tmp_levelset.phi(cell)=pair.second;
                            // if(cell==ref_cell) {
                            //     std::cout<<"debug cell: set tmp_levelset: ("<<cell.transpose()<<"), phi="<<pair.second<<", region="<<rid<<std::endl;
                            // }
                        }
                    }
                });

                tmp_levelset.Fast_Marching(-1);

                levelset.phi.Exec_Nodes([&](const VectorDi cell) {
                    // if(cell==ref_cell) {
                    //     std::cout<<"debug cell: set levelset: ("<<cell.transpose()<<"), phi="<<levelset.phi(cell)<<", region="<<indicator(cell)<<"->"<<tmp_levelset.phi(cell)<<", region="<<rid<<"??"<<std::endl;
                    // }

                    if(tmp_levelset.phi(cell)<levelset.phi(cell)){
                        // std::cout<<"set indicator at ("<<cell.transpose()<<"), phi="<<levelset.phi(cell)<<"->"<<tmp_levelset.phi(cell)<<", region "<<indicator(cell)<<"->"<<rid<<std::endl;
                        levelset.phi(cell)=tmp_levelset.phi(cell);
                        indicator(cell)=rid;
                    }
                });

                // std::cout<<"debug cell: final levelset: ("<<ref_cell.transpose()<<"), phi="<<levelset.phi(ref_cell)<<", region="<<indicator(ref_cell)<<std::endl;
                
                #pragma omp parallel for
                for(int i=0;i<particles->Size();i++){
                    VectorD x=particles->x(i);
                    VectorD intf_x=LevelSetUtil<d>::Closest_Point_With_Iterations(tmp_levelset,x,5);
                    // int cnt=0;
                    // while(abs(tmp_levelset.Phi(intf_x))>tmp_levelset.phi.grid.dx*1e-3 && cnt<3){
                    //     intf_x=LevelSetUtil<d>::Closest_Point_With_Iterations(tmp_levelset,intf_x,5);
                    //     cnt++;
                    // }
                    // if(cnt==2){
                    //     std::cout<<i<<" rerun interface projection "<<cnt<<" times at ["<<intf_x.transpose()<<"], phi="<<tmp_levelset.Phi(intf_x)<<std::endl;
                    // }
                    VectorD n=LevelSetUtil<d>::Normal(tmp_levelset,intf_x);
                    particles->x(i)=intf_x;
                    particles->n(i)=n;
                }
            }
            Info("MultiParticleLevelSet::Correction::correct particles by the narrowband_phi, time={}", timer.Lap_Time());

        }

        ///////////////////////////////////////
        //// fix geometry
        void Fast_Correction(std::shared_ptr<PointLocalGeometry<d>> local_geom, bool verbose=false){
            levelset.phi.Exec_Nodes([&](const VectorDi cell) {
                narrowband_phi(cell).clear();
            });
            
            Grid<d> grid=levelset.phi.grid;
            int max_iteration=1;

            //// narrowband for fast marching
            int narrow_band_cell_num=5;
            real narrow_band_width=grid.dx*narrow_band_cell_num;

            //// narrowband for initialize the levelset with mls
            int mls_narrow_band_cell_num=3;
            real mls_narrow_band_width=grid.dx*mls_narrow_band_cell_num;
            
            //// the threshold to start the marching, all the value smaller than the threshold will be treated as initialized before marching.
            real fast_marching_start_threshold=sqrt((real)d)*grid.dx;


            int mls_closest_point_iteration=2;
            int levelset_closest_point_iteration=5;


            Timer timer;

            //// build narrowband_phi by particle mls
            for (auto itr = region_map.begin(); itr != region_map.end(); ++itr) {
                int rid=itr->first;
                std::shared_ptr<EulerianParticles<d>> particles=itr->second->particles;

                local_geom->nbs_searcher->Update_Points(particles->xRef());
                local_geom->normal_ptr=std::make_shared<Array<VectorD> >(particles->nRef());

                if(verbose) std::cout<<"MultiParticleLevelSet::Correction::update points, region="<<rid<<std::endl;
                if(verbose) Info("MultiParticleLevelSet::Correction::update points, time={}", timer.Lap_Time());
                std::atomic<int> cell_cnt=0;
                std::atomic<int> empty_cell_cnt=0;

                levelset.phi.Exec_Nodes([&](const VectorDi cell) {
                    VectorD x=grid.Position(cell);

                    //// filter
                    if(levelset.phi(cell)<-mls_narrow_band_width-grid.dx)
                        return;

                    //// filter based on neighboring indicator
                    {
                        bool skip_cell=true;
                        for(int ni=0;ni<GridUtil<d>::Cell_Neighbor_Cell_Num_2_Layer();ni++){
                            VectorDi nb_cell=cell+GridUtil<d>::Cell_Neighbor_Cell_Offset_2_Layer(ni);
                            if(grid.Valid(nb_cell) && indicator(nb_cell)==rid){
                                skip_cell=false;
                                break;
                            }
                                
                        }
                        if(skip_cell) return;
                    }
                    auto [iteration, new_phi, normal, intf_x]=ParticleLevelSetHelper<d>::Closest_Point_On_Particle_Surface(x, particles, local_geom, mls_closest_point_iteration, mls_narrow_band_width);
                    cell_cnt++;
                    if(iteration>0)
                        narrowband_phi(cell).push_back(std::make_pair(rid,(real)new_phi));
                });
                if(verbose) std::cout<<"MultiParticleLevelSet::Correction::update narrowband, cell cnt"<<cell_cnt<<", region="<<rid<<std::endl;
                if(verbose) Info("MultiParticleLevelSet::Correction::update narrowband, time={}", timer.Lap_Time());
            }


            // auto cmp = [](VectorDi a, VectorDi b) { 
            //     for(int i=0;i<d;i++) 
            //         if(a(i)<b(i)) return true;
            //     return false; 
            // };
            // std::set<VectorDi, decltype(cmp)> ref_cell_set(cmp);
            // { VectorDi ref_cell; ref_cell<<30,29; 
            //     ref_cell_set.insert(ref_cell);}
            // { VectorDi ref_cell; ref_cell<<31,29; 
            //     ref_cell_set.insert(ref_cell);}
            // { VectorDi ref_cell; ref_cell<<32,29; 
            //     ref_cell_set.insert(ref_cell);}
            // { VectorDi ref_cell; ref_cell<<33,29; 
            //     ref_cell_set.insert(ref_cell);}
            // { VectorDi ref_cell; ref_cell<<34,29; 
            //     ref_cell_set.insert(ref_cell);}

            MultiParticleLevelSetHelper<d>::Correct_Multiple_Levelset(levelset, indicator, narrowband_phi);
            if(verbose) Info("MultiParticleLevelSet::Correction::correct multiple levelset, time={}", timer.Lap_Time());


            std::atomic<int> remove_cnt=0;
            levelset.phi.Exec_Nodes([&](const VectorDi cell) {
                Array<int> to_remove;
                for(int l=0;l<narrowband_phi(cell).size();l++){
                    std::pair<int, real> pair=narrowband_phi(cell)[l];
                    
                    // if(ref_cell_set.find(cell)!=ref_cell_set.end()){
                    //     std::cout<<"debug cell: ("<<cell.transpose()<<"), rid="<<pair.first<<", phi="<<pair.second<<std::endl;
                    // }

                    if(pair.second>0 || pair.second<-0.5*grid.dx) continue;
                    bool skip_cell=false;
                    for(int ni=0;ni<Grid<d>::Neighbor_Ring_Number();ni++){
                        VectorDi nb_cell=Grid<d>::Neighbor_Ring_Node(cell,ni);
                        if(!grid.Valid(nb_cell)) continue;

                        for(int nb_l=0;nb_l<narrowband_phi(nb_cell).size();nb_l++){
                            std::pair<int, real> nb_pair=narrowband_phi(nb_cell)[nb_l];
                            // if(pair.first==1 && nb_pair.first==1 && ref_cell_set.find(cell)!=ref_cell_set.end()){
                            //     std::cout<<"debug cell: ("<<cell.transpose()<<"), nbcell=("<<nb_cell.transpose()<<"), phi="<<nb_pair.second<<std::endl;
                            // }
                            if(nb_pair.first==pair.first && nb_pair.second<-0.5*grid.dx) {skip_cell=true; break;}
                        }
                        if(skip_cell) break;
                    }
                    if(!skip_cell){
                        remove_cnt++;
                        // narrowband_phi(cell)[l].second=1e8;
                        to_remove.push_back(l);
                        if(verbose) std::cout<<"remove cell "<<cell.transpose()<<", region "<<pair.first<<std::endl;
                    }
                }
                for(int ii=to_remove.size()-1;ii>=0;ii--){
                    int l=to_remove[ii];
                    // std::cout<<"remove cell "<<cell.transpose()<<", layer "<<l<<", region "<<narrowband_phi(cell)[l].first<<std::endl;
                    narrowband_phi(cell).erase(narrowband_phi(cell).begin()+l);
                }
            });
            if(verbose) Info("MultiParticleLevelSet::Correction::remove cell, time={}", timer.Lap_Time());


            MultiParticleLevelSetHelper<d>::Correct_Multiple_Levelset(levelset, indicator, narrowband_phi);
            if(verbose) Info("MultiParticleLevelSet::Correction::correct multiple levelset, time={}", timer.Lap_Time());
            
            LevelSetUtil<d>::Fast_Marching_With_Threshold(levelset, -1, fast_marching_start_threshold);
            if(verbose) Info("MultiParticleLevelSet::Correction::marching, time={}", timer.Lap_Time());

            //// correct particles by the narrowband_phi, push particles onto phi=0, update phi
            for (auto itr = region_map.begin(); itr != region_map.end(); ++itr) {
                int rid=itr->first;
                std::shared_ptr<EulerianParticles<d>> particles=itr->second->particles;

                LevelSet<d> tmp_levelset(levelset.phi.grid);
                // levelset.phi.Exec_Nodes([&](const VectorDi cell) {
                //     if(indicator(cell)==rid) tmp_levelset.phi(cell)=-1e8;
                //     else tmp_levelset.phi(cell)=1e8;

                //     for(int l=0;l<narrowband_phi(cell).size();l++){
                //         std::pair<int, real> pair=narrowband_phi(cell)[l];
                //         if(pair.first==rid){
                //             tmp_levelset.phi(cell)=pair.second;
                //         }
                //     }
                // });


                // tmp_levelset.Fast_Marching(narrow_band_width);
                if(remove_cnt>0)
                    MultiParticleLevelSetHelper<d>::Restore_Levelset_From_Narrowband(tmp_levelset,indicator,narrowband_phi,rid,narrow_band_width);
                else
                    MultiParticleLevelSetHelper<d>::Restore_Levelset_From_Narrowband(tmp_levelset,indicator,narrowband_phi,rid,0);
                if(verbose) Info("MultiParticleLevelSet::Correction::restore levelset for region{}, time={}", rid, timer.Lap_Time());

                #pragma omp parallel for
                for(int i=0;i<particles->Size();i++){
                    if(particles->t(i)==1) continue;
                    
                    VectorD x=particles->x(i);
                    VectorD intf_x=LevelSetUtil<d>::Closest_Point_With_Iterations(tmp_levelset,x,levelset_closest_point_iteration);
                    VectorD n=LevelSetUtil<d>::Normal(tmp_levelset,intf_x);
                    particles->x(i)=intf_x;
                    particles->n(i)=n;
                    real curv=LevelSetUtil<d>::Curvature(tmp_levelset,intf_x);
                    if(abs(curv)<1e-8)
                        particles->r(i)=1e8;
                    else
                        particles->r(i)=1.0/abs(curv);


                    // VectorD detect_x=particles->x(i)-particles->n(i)*grid.dx*sqrt((real)d);
                    // bool use_neighbor_interface=false;
                    // if(region_levelset->Phi(detect_x)>0) use_neighbor_interface=true;

                    // if(use_neighbor_interface){
                    //     // Info("use neighbor interface at particle ({}), in region {}", x.transpose(), rid);
                    //     real min_phi=1e8;
                    //     int min_rid=-1;
                    //     std::shared_ptr<LevelSet<d>> min_region_levelset=nullptr;
                    //     VectorDi cell; VectorD frac;
                    //     grid.Get_Fraction(x,cell,frac);
                    //     for(int ci=0;ci<GridUtil<d>::Particle_Neighbor_Cell_Num();ci++){
                    //         VectorDi nb_cell=cell+GridUtil<d>::Particle_Neighbor_Cell_Offset(ci);
                    //         if(!grid.Valid(nb_cell)) continue;
                    //         int nb_rid=indicator(nb_cell);
                    //         if(nb_rid==rid || nb_rid==min_rid || nb_rid<0) continue;

                    //         std::shared_ptr<LevelSet<d>> nb_region_levelset=debug__levelset__map.find(nb_rid)->second;
                    //         real nb_phi=nb_region_levelset->Phi(x);
                    //         if(nb_phi<min_phi){
                    //             min_phi=nb_phi;
                    //             min_rid=nb_rid;
                    //             min_region_levelset=nb_region_levelset;
                    //             // std::cout<<"update min phi-"<<nb_phi<<std::endl;
                    //         }
                    //     }
                    //     if(min_rid>=0){
                    //         VectorD intf_x=LevelSetUtil<d>::Closest_Point_With_Iterations(*min_region_levelset,x,levelset_closest_point_iteration);
                    //         VectorD n=-min_region_levelset->Normal(intf_x);
                    //         particles->x(i)=intf_x;
                    //         particles->n(i)=n;    
                    //     }else{
                    //         std::cout<<"warn! cannot find neigbor region!"<<std::endl;
                    //     }
                    // }else{
                    //     VectorD intf_x=LevelSetUtil<d>::Closest_Point_With_Iterations(*region_levelset,x,levelset_closest_point_iteration);
                    //     VectorD n=region_levelset->Normal(intf_x);
                    //     particles->x(i)=intf_x;
                    //     particles->n(i)=n;
                    // }

                    // //// check if levelset is too thin
                    // {
                    //     VectorDi cell; VectorD frac;
                    //     grid.Get_Fraction(x,cell,frac);
                    //     bool thin_neck=true;
                    //     for(int ci=0;ci<GridUtil<d>::Particle_Neighbor_Cell_Num();ci++){
                    //         VectorDi nb_cell=cell+GridUtil<d>::Particle_Neighbor_Cell_Offset(ci);
                    //         if(!grid.Valid(nb_cell)) continue;
                    //         int nb_rid=indicator(nb_cell);
                    //         if(nb_rid==rid) {thin_neck=false; break;}
                    //     }
                    //     if(thin_neck){
                    //         VectorD  intf_x=LevelSetUtil<d>::Closest_Point_With_Iterations(*region_levelset,x,levelset_closest_point_iteration);
                    //         VectorD n=region_levelset->Normal(intf_x);
                    //         particles->x(i)=intf_x;
                    //         particles->n(i)=n;
                    //     }
                    // }
                }
                if(verbose) Info("MultiParticleLevelSet::Correction::correct particles for region{}, time={}", rid, timer.Lap_Time());

            }
            if(verbose) std::cout<<"MultiParticleLevelSet::Correction::correct particles by the narrowband_phi, time={}"<<std::endl;
            if(verbose) Info("MultiParticleLevelSet::Correction::correct particles by the narrowband_phi, time={}", timer.Lap_Time());
            
        }

        
        void Update_Volume(bool init, bool verbose){
            int precision=1000;
            //// clear volume
            for (auto itr = region_map.begin(); itr != region_map.end(); ++itr) {
                itr->second->integer_volume=0;
                itr->second->volume_correct_cell_cnt=0;
            }
            
            // std::atomic<int> inner_cell_num=0;

            indicator.Exec_Nodes([&](const VectorDi cell) {
                int rid=indicator(cell);
                auto itr=region_map.find(rid);
                if(itr==region_map.end()) Error("Update_Volume: find no region {}", rid);

                real cell_vol=levelset.Cell_Fraction(cell);
                
                itr->second->integer_volume+=(int)(cell_vol*precision); //// atomic
                // if(cell_vol==1.0) inner_cell_num++;
                if(cell_vol==1.0) itr->second->volume_correct_cell_cnt+=1;

                //// the volume of positive phi
                for(int l=0;l<narrowband_phi(cell).size();l++){
                    real phi=narrowband_phi(cell)[l].second;
                    if(phi>0 && phi<levelset.phi.grid.dx*0.5f){
                        real cell_vol=LevelSetUtil<d>::Cell_Fraction(levelset, phi);
                        itr->second->integer_volume+=(int)(cell_vol*precision); //// atomic
                    }
                }
            });
            for (auto itr = region_map.begin(); itr != region_map.end(); ++itr) {
                real volume=(itr->second->integer_volume)/precision+(itr->second->integer_volume%precision)/(real)precision;
                itr->second->volume=volume;
                if(init) itr->second->init_volume=volume;
                // if(inner_cell_num>0) itr->second->delta_volume_per_cell=(itr->second->init_volume-itr->second->volume)/inner_cell_num;
                if(itr->second->volume_correct_cell_cnt>0) itr->second->delta_volume_per_cell=(itr->second->init_volume-itr->second->volume)/itr->second->volume_correct_cell_cnt;
                
                if(verbose){
                    std::cout<<"Region "<<itr->first<<", volume="<<itr->second->volume<<", init_volume="<<itr->second->init_volume<<std::endl;
                    std::cout<<"Region "<<itr->first<<", delta_volume_per_cell="<<itr->second->delta_volume_per_cell<<", volume_correct_cell_cnt="<<itr->second->volume_correct_cell_cnt<<std::endl;
                    std::cout<<"Region "<<itr->first<<", volume control="<<itr->second->use_volume_control<<std::endl;
                    Info("Region {} volume={}, init volume={}, delta_volume_per_cell={}, volume_correct_cell_cnt={}, volme_control={}",itr->first,itr->second->volume,itr->second->init_volume,itr->second->delta_volume_per_cell,itr->second->volume_correct_cell_cnt, itr->second->use_volume_control);
                }
            }
        }


        void Reseed_Particles(std::shared_ptr<PointLocalGeometry<d>> local_geom){
            Timer timer;
            for (auto itr = region_map.begin(); itr != region_map.end(); ++itr) {
                int rid=itr->first;
                std::shared_ptr<EulerianParticles<d>> particles=itr->second->particles;

                local_geom->nbs_searcher->Update_Points(particles->xRef());
                local_geom->normal_ptr=std::make_shared<Array<VectorD> >(particles->nRef());
                Info("MultiParticleLevelSet::Reseed_Particles::update points for region {} time={}", rid, timer.Lap_Time());

                
                // ParticleLevelSetHelper<d>::Update_Normal_And_Radius(levelset, *particles, local_geom);
                // Info("MultiParticleLevelSet::Reseed_Particles::update normals for region {} time={}", rid, timer.Lap_Time());

                Grid<d>& grid=levelset.phi.grid;

                int insert_num;
                LevelSet<d> tmp_levelset(levelset.phi.grid);
                MultiParticleLevelSetHelper<d>::Restore_Levelset_From_Narrowband(tmp_levelset,indicator,narrowband_phi,rid,0);
                insert_num=ParticleLevelSetHelper<d>::Reseed_Insert_Flip_Particles(tmp_levelset, *particles, local_geom, true);

                Info("MultiParticleLevelSet::Reseed_Particles::insert particles for region {} time={}", rid, timer.Lap_Time());

                // LevelSet<d> tmp_levelset(levelset.phi.grid);
                int narrow_band_cell_num=5;
                real narrow_band_width=grid.dx*narrow_band_cell_num;
                int levelset_closest_point_iteration=5;

                // MultiParticleLevelSetHelper<d>::Restore_Levelset_From_Narrowband(tmp_levelset,indicator,narrowband_phi,rid,narrow_band_width);
                // MultiParticleLevelSetHelper<d>::Restore_Levelset_From_Narrowband(tmp_levelset,indicator,narrowband_phi,rid,0);
            #pragma omp parallel for
                for(int i=particles->Size()-insert_num;i<particles->Size();i++){
                    if(particles->t(i)==1) continue;
                    
                    VectorD x=particles->x(i);
                    VectorD intf_x=LevelSetUtil<d>::Closest_Point_With_Iterations(tmp_levelset,x,levelset_closest_point_iteration);
                    VectorD n=LevelSetUtil<d>::Normal(tmp_levelset,intf_x);
                    particles->x(i)=intf_x;
                    particles->n(i)=n;
                    particles->r(i)=LevelSetUtil<d>::Curvature(tmp_levelset,intf_x);
                }



                local_geom->nbs_searcher->Update_Points(particles->xRef());
                local_geom->normal_ptr=std::make_shared<Array<VectorD> >(particles->nRef());
                Info("MultiParticleLevelSet::Reseed_Particles::update points for region {} time={}", rid, timer.Lap_Time());

                ParticleLevelSetHelper<d>::Reseed_Remove_Particles(levelset, *particles, local_geom, true);
                Info("MultiParticleLevelSet::Reseed_Particles::remove particles for region {} time={}", rid, timer.Lap_Time());
            }
        };

        void Build_Inner_Cells(){

            for (auto itr = region_map.begin(); itr != region_map.end(); ++itr) {
                itr->second->inner_cells.clear();
            }

            indicator.Exec_Nodes([&](const VectorDi cell) {
                int rid=indicator(cell);
                if(rid<0) return;
                
                auto itr=region_map.find(rid);
                if(itr==region_map.end()) Warn("find no region {}", rid);

                #pragma omp critical
                {
                    itr->second->inner_cells.push_back(cell);
                }

            });
        }

        int Detect_Merge_Region(std::shared_ptr<PointLocalGeometry<d>> local_geom){
            Grid<d>& grid=indicator.grid;

            auto cmp = [](const std::pair<int, int>& a, const std::pair<int, int>& b) {return a.first>b.first;}; //// large->small
            std::set<std::pair<int,int>, decltype(cmp)> merge_pairs(cmp);

            grid.Exec_Faces([&](const int axis, const VectorDi face) {
                auto [cell0, cell1] = Face_Neighbor_Cells<d>(grid, axis, face);
                if(!grid.Valid(cell0) || !grid.Valid(cell1)) return;
                if(!Is_Interface(cell0,cell1)) return;
                
                int rid0=indicator(cell0); auto itr0=region_map.find(rid0);
                int rid1=indicator(cell1); auto itr1=region_map.find(rid1);

                if(itr0->second->type!=Region<d>::RegionType::WATER || itr1->second->type!=Region<d>::RegionType::WATER) return;

            #pragma omp critical
                merge_pairs.insert(rid0>rid1?std::make_pair(rid0, rid1) : std::make_pair(rid1, rid0) ); //// large, small
            });

            for (auto rit=merge_pairs.begin();rit!=merge_pairs.end();rit++){
                int rid0=rit->first;
                int rid1=rit->second;
                Info("merge {} into {}",rid0,rid1);
                Merge_Regions(rid1, rid0, local_geom);
            }
            
        }

        int Detect_Split_Region(std::shared_ptr<PointLocalGeometry<d>> local_geom){
            Grid<d> grid=levelset.phi.grid;
            Field<VectorDi,d> parent(levelset.phi.grid, VectorDi::Ones()*(-1));

            // auto Find_Parent=[&](const VectorDi& cell, int& _depth, VectorDi& _parent_cell){
            //     VectorDi cur_cell=cell;
            //     // VectorDi cur_cell=parent(cell);
            //     int depth=0;
            //     while(grid.Valid(cur_cell)){
            //         depth++;
            //         VectorDi parent_cell=parent(cur_cell);
            //         if(parent_cell==cur_cell) {
            //             _depth=depth; //// how many parents does cell have
            //             _parent_cell=cur_cell;
            //             return;
            //         }
            //         cur_cell=parent_cell;
            //     }

            //     if(depth>1) Warn("incorrect chain at {}",cell.transpose());
            //     _depth=0;
            //     _parent_cell=cur_cell;
                
            //     return;
            // };

            // auto Correct_Parents=[&](const VectorDi& cell, VectorDi target_parent){
            //     VectorDi cur_cell=parent(cell);
            //     parent(cell)=target_parent;//// correct parent after fetch parent
            //     parent(pre_cell)=target_parent;
            //     int depth=0;
            //     while(grid.Valid(cur_cell)){
            //         depth++;
            //         VectorDi parent_cell=parent(cur_cell);
            //         parent(cur_cell)=target_parent;//// correct parent after fetch parent
            //         if(parent_cell==cur_cell) break;
            //         cur_cell=parent_cell;
            //     }
            //     return;
            // };

            for (auto itr = region_map.begin(); itr != region_map.end(); ++itr) {
                itr->second->subregion_parent_cells.clear();
            }

            //// flood fill
            grid.Iterate_Nodes([&](const VectorDi cell) {
                if(grid.Valid(parent(cell))) return;
                //// TODO: skip solid
                int rid=indicator(cell);
                std::queue<VectorDi> cell_queue;
                cell_queue.push(cell); 
                region_map.find(rid)->second->subregion_parent_cells.push_back(cell);

                while(!cell_queue.empty()){
                    VectorDi cur_cell=cell_queue.front();
                    cell_queue.pop();
                    if(grid.Valid(parent(cur_cell))) continue;


                    parent(cur_cell)=cell;

                    // for(int ni=0;ni<Grid<d>::Neighbor_Node_Number();ni++){
                    //     VectorDi nb_cell=Grid<d>::Neighbor_Node(cur_cell,ni);
                    for(int ni=0;ni<Grid<d>::Neighbor_Ring_Number();ni++){
                        VectorDi nb_cell=Grid<d>::Neighbor_Ring_Node(cur_cell,ni);
                        if(!grid.Valid(nb_cell)) continue; //// skip invalid nb cell
                        if(grid.Valid(parent(nb_cell))) continue; //// skip done nb cell
                        if(indicator(nb_cell)==rid) cell_queue.push(nb_cell);
                    }
                }
            });

            //// find the regions to be split
            Array<int> split_region_indices; //// region indices to be split
            for (auto itr = region_map.begin(); itr != region_map.end(); ++itr) {
                int region_num=itr->second->subregion_parent_cells.size();
                if(region_num==0) Warn("incorrect region {} with no volume", itr->first);
                if(region_num>1) {
                    Info("split region {} into {} subregions", itr->first, region_num);
                    split_region_indices.push_back(itr->first);
                }
            }

            auto cmp = [](VectorDi a, VectorDi b) { 
                for(int i=0;i<d;i++){ 
                    if(a(i)<b(i)) return true;
                    if(a(i)>b(i)) return false;
                }
                return false; 
            };
            std::map<VectorDi, int, decltype(cmp)> parent_to_new_region_map(cmp); //// map parent cell to new region index
            Array<Array<int>> split_subregion_indices;
            //// split the regions
            for(int ii=0;ii<split_region_indices.size();ii++){
                int rid_i=split_region_indices[ii];
                auto itr_i=region_map.find(rid_i);

                //// indices for sub regions
                {
                    VectorDi parent_cell=itr_i->second->subregion_parent_cells[0];
                    parent_to_new_region_map.insert(std::make_pair(parent_cell, rid_i));
                }
                

                Array<int> subregion_indices;
                subregion_indices.push_back(rid_i);

                std::shared_ptr<EulerianParticles<d>> particles=itr_i->second->particles;
                
                //// generate duplicate regions
                int region_num=itr_i->second->subregion_parent_cells.size();
                for(int jj=1;jj<region_num;jj++){
                    VectorDi parent_cell=itr_i->second->subregion_parent_cells[jj];
                    int rid_j=Duplicate_Empty_Region(rid_i);
                    auto itr_j=region_map.find(rid_j);
                    
                    parent_to_new_region_map.insert(std::make_pair(parent_cell, rid_j));
                    subregion_indices.push_back(rid_j);
                }
                split_subregion_indices.push_back(subregion_indices);

                //// for particle split
                Array<std::set<int>> subregion_particles(region_num);
                

            #pragma omp parallel for
                for(int pi=0;pi<particles->Size();pi++){
                    VectorD pos=particles->x(pi);
                    VectorDi cell=GridUtil<d>::Cell_Coord(grid,pos);

                    //// find the closest cell
                    real closest_dist=1e8;
                    VectorDi closest_cell=VectorDi::Zero();
                    for(int ni=0;ni<Grid<d>::Neighbor_Ring_Number();ni++){
                        VectorDi nb_cell=Grid<d>::Neighbor_Ring_Node(cell,ni);

                        if(!grid.Valid(nb_cell)) continue;
                        
                        real dist=(grid.Position(nb_cell)-pos).norm();
                            
                        // std::cout<<"particle="<<pi<<", nb_cell=("<<nb_cell.transpose()<<"), indicator="<<indicator(nb_cell)<<"<>"<<rid_i<<", dist="<<dist<<std::endl;

                        if(indicator(nb_cell)!=rid_i) continue;
                        
                        if(dist<closest_dist) {
                            closest_dist=dist;
                            closest_cell=nb_cell;
                            // std::cout<<"update closest cell=("<<nb_cell.transpose()<<"), dist="<<dist<<", closest dist="<<closest_dist<<std::endl;
                        }
                    }
                    if(closest_dist==1e8) Warn("find no closest cell for particle! {}", closest_dist);
                    
                    //// add particle into the corresponding subregion
                    VectorDi parent_cell=parent(closest_cell);
                    for(int srid=0;srid<region_num;srid++){
                        // std::cout<<"particle "<<pi<<"at("<<pos.transpose()<<")"<<"find subregion "<<itr_i->second->subregion_parent_cells[i].transpose()<<std::endl;
                        if(parent_cell==itr_i->second->subregion_parent_cells[srid]){
                            #pragma omp critical
                            {subregion_particles[srid].insert(pi);}
                                break;
                        }
                    }
                }

                //// transfer particle to subregion 1~n
                for(int jj=1;jj<region_num;jj++){
                    real rid_j=subregion_indices[jj];
                    auto itr_j=region_map.find(rid_j);
                    itr_j->second->particles=std::make_shared<EulerianParticles<d>>();
                    ParticleLevelSetHelper<d>::Transfer_Particles(*itr_j->second->particles, *particles, subregion_particles[jj]);
                    Info("from region {} to {}, result size={}", rid_i, rid_j, region_map.find(rid_j)->second->particles->Size());
                }
                
                //// transfer particle to subregion 0
                {
                    std::shared_ptr<EulerianParticles<d>> new_particles=std::make_shared<EulerianParticles<d>>();
                    ParticleLevelSetHelper<d>::Transfer_Particles(*new_particles, *particles, subregion_particles[0]);
                    region_map.find(rid_i)->second->particles=new_particles;
                    Info("from region {} to {}, result size={}", rid_i, rid_i, region_map.find(rid_i)->second->particles->Size());
                }

            }

            for(int ii=0;ii<split_region_indices.size();ii++){
                //// clean integer_volume
                for(int jj=0;jj<split_subregion_indices[ii].size();jj++){
                    int rid_j=split_subregion_indices[ii][jj];
                    auto itr_j=region_map.find(rid_j);
                    itr_j->second->integer_volume=0;
                }
            }


            //// update indicator & volume
            int precision=1000;
            
            grid.Exec_Nodes([&](const VectorDi cell) {
                int rid=indicator(cell);
                bool reassign=false;
                for(int i=0;i<split_region_indices.size();i++)
                    if(rid==split_region_indices[i]){
                        reassign=true;
                        break;
                    }

                if(reassign){
                    VectorDi parent_cell=parent(cell);
                    
                    auto itr=parent_to_new_region_map.find(parent_cell);
                    if(itr==parent_to_new_region_map.end()) {
                        Warn("find no map in parent_to_new_region_map, cell=({}), parent cell=({})",cell.transpose(), parent_cell.transpose());
                        Warn("find no map in parent_to_new_region_map,indicator={},{}", indicator(cell), indicator(parent_cell));
                        }
                    else{
                        indicator(cell)=itr->second;
                        real cell_vol=levelset.Cell_Fraction(cell);

            #pragma omp critical
                        region_map.find(itr->second)->second->integer_volume+=(int)(cell_vol*precision); //// atomic
                        // Info("region {} add cell ({}) v={}",itr->second,cell.transpose(),cell_vol*precision);
                    }
                }
            });

            //// distribute the initial volume onto different subregions
            for(int ii=0;ii<split_region_indices.size();ii++){
                int rid_i=split_subregion_indices[ii][0];
                auto itr_i=region_map.find(rid_i);
                real init_volume=itr_i->second->init_volume;
                Info("region {}, original init_volume={}",rid_i, init_volume);
                real total_volume=0;
                
                //// sum up volume
                for(int jj=0;jj<split_subregion_indices[ii].size();jj++){
                    int rid_j=split_subregion_indices[ii][jj];
                    auto itr_j=region_map.find(rid_j);
                    real volume=(itr_j->second->integer_volume)/precision+(itr_j->second->integer_volume%precision)/(real)precision;
                    total_volume+=volume;
                    Info("region {}, int_volume={}, volume={}",rid_j, itr_j->second->integer_volume, volume);
                }

                Info("total volume={}", total_volume);

                for(int jj=0;jj<split_subregion_indices[ii].size();jj++){
                    int rid_j=split_subregion_indices[ii][jj];
                    auto itr_j=region_map.find(rid_j);
                    real volume=(itr_j->second->integer_volume)/precision+(itr_j->second->integer_volume%precision)/(real)precision;
                    itr_j->second->init_volume=volume/total_volume*init_volume;
                    Info("region {}, init_volume={}",rid_j, itr_j->second->init_volume);
                }
            }

            int subregion_cnt=0;
            for(int ii=0;ii<split_region_indices.size();ii++){
                subregion_cnt+=split_subregion_indices[ii].size()-1;
            }
            return subregion_cnt;
        }

        //// remove the region with 0 volume, must be called after Update_Volume()
        void Remove_Empty_Region(){
            std::set<int> remove_set;
            std::set<int> zero_particle_set;
            for (auto itr = region_map.begin(); itr != region_map.end(); ++itr) {
                if(itr->second->init_volume==0 || itr->second->volume==0){
                    remove_set.insert(itr->first);
                }else if(itr->second->particles->Size()==0){
                    zero_particle_set.insert(itr->first);
                }
            }

            for (auto rit=remove_set.begin();rit!=remove_set.end();rit++){
                int rid=*rit;
                std::cout<<"remove the region "<<rid<<" with empty volume"<<std::endl;
                region_map.erase (rid);
            }

            for (auto rit=zero_particle_set.begin();rit!=zero_particle_set.end();rit++){
                int rid=*rit;
                std::cout<<"remove the region "<<rid<<" with zero particle"<<std::endl;
                region_map.erase (rid);
            }

            Grid<d>& grid=levelset.phi.grid;
            grid.Exec_Nodes([&](const VectorDi cell) {
                int rid=indicator(cell);
                if(remove_set.find(rid)!=remove_set.end()){
                    std::cout<<"warning: cell "<<cell.transpose()<<" in region "<<rid<<", which is already removed"<<std::endl;
                }
                if(zero_particle_set.find(rid)!=zero_particle_set.end()){
                    real min_phi=1e8;
                    VectorDi min_cell=-VectorDi::Ones();
                    for(int nbi=0;nbi<grid.Neighbor_Node_Number();nbi++){
                        const VectorDi nb_cell=grid.Neighbor_Node(cell,nbi);
                        if(levelset.phi(nb_cell)<min_phi && indicator(cell)!=indicator(nb_cell)){
                            min_phi=levelset.phi(nb_cell);
                            min_cell=nb_cell;
                        }
                    }
                    #pragma omp critical
                    indicator(cell)=indicator(min_cell);
                }
            });
        }
    };
}