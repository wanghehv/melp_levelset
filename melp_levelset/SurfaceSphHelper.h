//////////////////////////////////////////////////////////////////////////
// SurfaceSphHelper, surface sph functions
// Copyright (c) (2022-), Hui Wang
// This file is part of MELPLEVELSET, whose distribution is governed by the LICENSE file.
//////////////////////////////////////////////////////////////////////////

#pragma once
// #include "SPH_Utils.h"
#include "DefaultDynamicsParameter.h"
#include "CustomizedSPHUtils.h"

#include "EulerianParticles.h"
#include "LagrangianParticles.h"
#include "PointLocalGeometry.h"

#include "Timer.h"

namespace Meso{
    template<int d>
    class SurfaceSphHelper{
    public:
        Typedef_VectorD(d);
        Typedef_MatrixD(d);
        using VectorT=Vector<real,d-1>;
        using VectorTi=Vector<int,d-1>;
        using MatrixT=Matrix<real,d-1,d-1>;
        // SPH_Utils<d> sph;
        // SPH_Utils<d-1> sph_t;
        CustomizedSPHUtils<d> sph;
        CustomizedSPHUtils<d-1> sph_t;
        Array<Array<int>> le_nbs;
        Array<Array<int>> ee_nbs;
        
        //// transfer velocity, thickness, concentration from lagrangian particles to eulerian particles
        void Lagrangian_To_Eulerian(std::shared_ptr<PointLocalGeometry<d>> l_local_geom, std::shared_ptr<LagrangianParticles<d>> l_particles, std::shared_ptr<PointLocalGeometry<d>> e_local_geom, std::shared_ptr<EulerianParticles<d>> e_particles){
            Timer timer;
            
            // e_local_geom->nbs_searcher->Update_Points(e_particles->xRef());
            // e_local_geom->normal_ptr=std::make_shared<Array<VectorD> >(e_particles->nRef());

            Array<MatrixT> l_BDinv(l_particles->Size(), MatrixT::Zero());
            //// update a_L for lagrangian particles
            #pragma omp parallel for
            for (int pi = 0; pi < l_particles->Size(); pi++) {
                VectorD x_i=l_particles->x(pi);
                // Array<int> e_nbs; e_local_geom->nbs_searcher->Find_Nbs(x_i, e_local_geom->v_r, e_nbs);
                Array<int>& e_nbs=le_nbs[pi];

                real a_L = sph.Sum([&](int idx)->real {return 1.0; },
                    [&](int idx)->VectorD {return x_i-e_particles->x(idx); },
                    e_nbs, e_local_geom->v_r);
                if(a_L>0) l_particles->ia(pi)=1.0/a_L; //// inverse of a_L
                if(a_L==0 || std::isnan(a_L))
                    std::cout<<"l: a_L is nan "<<a_L<<std::endl;
                if(std::isnan(l_particles->v(pi).maxCoeff())) std::cout<<"l: lagrangian velocity is nan "<<l_particles->v(pi).transpose()<<std::endl;
                l_BDinv[pi]=l_particles->B(pi)*l_particles->D(pi).inverse();
            }

            Info("Lagrangian_To_Eulerian::update a_L, time={}", timer.Lap_Time());
            
            real m_per_lag=1.0f;
            real c_per_lag=1.0f;
            real V_per_lag=2e-7;

            l_local_geom->nbs_searcher->Update_Points(l_particles->xRef());
            l_local_geom->normal_ptr=std::make_shared<Array<VectorD> >(l_particles->nRef());

            //// transfer from lagrangian particles to eulerian particles
            #pragma omp parallel for
            for (int pi = 0; pi < e_particles->Size(); pi++) {
                VectorD x_i=e_particles->x(pi);
                Array<int> l_nbs; l_local_geom->nbs_searcher->Find_Nbs(x_i, l_local_geom->v_r, l_nbs);

                real rho_i = sph.Sum([&](int idx)->real {return 1.0*l_particles->ia(idx); },
                    [&](int idx)->VectorD {
                        return x_i-l_particles->x(idx); 
                    },
                    l_nbs, l_local_geom->v_r);
                if(rho_i==0) std::cout<<"eulerian rho_i==0 #nbs="<<l_nbs.size()<<std::endl;
                e_particles->m(pi)=rho_i*m_per_lag;
                e_particles->c(pi)=rho_i*c_per_lag;
                e_particles->V(pi)=rho_i*V_per_lag;
            // }

            // Info("Lagrangian_To_Eulerian::transfer real , time={}", timer.Lap_Time());


            // //// transfer momentum
            // #pragma omp parallel for
            // for (int pi = 0; pi < e_particles->Size(); pi++) {
            //     VectorD x_i=e_particles->x(pi);
            //     Array<int> l_nbs; l_local_geom->nbs_searcher->Find_Nbs(x_i, l_local_geom->v_r, l_nbs);
                MatrixD E_i=e_local_geom->Spawn_Local_Frame(e_particles->n(pi));


                VectorD mom_i = m_per_lag*sph.Sum([&](int idx)->VectorD {return l_particles->v(idx)*l_particles->ia(idx); },
                    [&](int idx)->VectorD {return x_i-l_particles->x(idx); },
                    l_nbs, e_local_geom->v_r);
                if(e_particles->m(pi)==0) std::cout<<"eulerian mass is zero"<<std::endl;

                VectorD affine_mom_i = m_per_lag*sph.Sum([&](int idx)->VectorD {
                    VectorT BDx=l_BDinv[idx]*e_local_geom->Project_To_TPlane(x_i-l_particles->x(idx), E_i);
                    return e_local_geom->Unproject_To_World(BDx, E_i)*l_particles->ia(idx); },
                    [&](int idx)->VectorD {return x_i-l_particles->x(idx); },
                    l_nbs, e_local_geom->v_r);
                if(e_particles->m(pi)!=0)
                    e_particles->vt(pi)=(mom_i+affine_mom_i)/e_particles->m(pi);
                else
                    e_particles->vt(pi)=VectorD::Zero();
                
                if(std::isnan(e_particles->vt(pi).maxCoeff())) std::cout<<"l2e: eulerian velocity is nan "<<e_particles->m(pi)<<","<<mom_i.transpose()<<","<<affine_mom_i.transpose()<<std::endl;
            }
            Info("Lagrangian_To_Eulerian::transfer momentum , time={}", timer.Lap_Time());
        }

        //// transfer velocity from eulerian particles to lagrangian particles
        void Eulerian_To_Lagrangian(std::shared_ptr<PointLocalGeometry<d>> l_local_geom, std::shared_ptr<LagrangianParticles<d>> l_particles, std::shared_ptr<PointLocalGeometry<d>> e_local_geom, std::shared_ptr<EulerianParticles<d>> e_particles){
            
            
            // e_local_geom->nbs_searcher->Update_Points(e_particles->xRef());
            // e_local_geom->normal_ptr=std::make_shared<Array<VectorD> >(e_particles->nRef());

            // #pragma omp parallel for
            // for (int pi = 0; pi < l_particles->Size(); pi++) {
            //     VectorD x_i=l_particles->x(pi);
            //     // Array<int> e_nbs; e_local_geom->nbs_searcher->Find_Nbs(x_i, e_local_geom->v_r, e_nbs);
            //     Array<int>& e_nbs=le_nbs[pi];
            //     MatrixD E_i=l_local_geom->Spawn_Local_Frame(l_particles->n(pi));

            //     real ia_i=l_particles->ia(pi);
            //     l_particles->v(pi) = ia_i*sph.Sum([&](int idx)->VectorD {
            //             // if(std::isnan(e_particles->vt(idx).maxCoeff())) 
            //                 // std::cout<<"eparticle v is nan"<<std::endl;
            //             return e_particles->vt(idx); },
            //         [&](int idx)->VectorD { return x_i-e_particles->x(idx); },
            //         e_nbs, e_local_geom->v_r);
            //     l_particles->v(pi) -= l_particles->v(pi).dot(l_particles->n(pi))*l_particles->n(pi);
            //     if(std::isnan(l_particles->v(pi).maxCoeff())) std::cout<<"lagrangian velocity is nan "<<ia_i<<","<<e_nbs.size()<<std::endl;
                
            //     l_particles->B(pi) = ia_i*sph.Sum([&](int idx)->MatrixT {
            //             MatrixT mat=e_local_geom->Project_To_TPlane(e_particles->vt(idx), E_i)*e_local_geom->Project_To_TPlane(e_particles->x(idx)-x_i, E_i).transpose();
            //             return mat; },
            //         [&](int idx)->VectorD { return x_i-e_particles->x(idx); },
            //         e_nbs, e_local_geom->v_r);

            //     l_particles->D(pi) = ia_i*sph.Sum([&](int idx)->MatrixT {
            //             return e_local_geom->Project_To_TPlane(e_particles->x(idx)-x_i, E_i)*e_local_geom->Project_To_TPlane(e_particles->x(idx)-x_i, E_i).transpose(); },
            //         [&](int idx)->VectorD { return x_i-e_particles->x(idx); },
            //         e_nbs, e_local_geom->v_r);
                
            // }
            
            
            #pragma omp parallel for
            for (int pi = 0; pi < l_particles->Size(); pi++) {
                VectorD x_i=l_particles->x(pi);
                // Array<int> e_nbs; e_local_geom->nbs_searcher->Find_Nbs(x_i, e_local_geom->v_r, e_nbs);
                Array<int>& e_nbs=le_nbs[pi];
                MatrixD E_i=l_local_geom->Spawn_Local_Frame(l_particles->n(pi));

                real ia_i=l_particles->ia(pi);
                VectorD sum_v=VectorD::Zero();
                MatrixT sum_B=MatrixT::Zero();
                MatrixT sum_D=MatrixT::Zero();

                for (int k = 0; k < e_nbs.size(); k++) {
                    int pj = e_nbs[k];
                    VectorD wr = x_i-e_particles->x(pj);
                    real w = sph.Kernel_Weight(wr, e_local_geom->v_r);
                    
                    VectorT local_vel=e_local_geom->Project_To_TPlane(e_particles->vt(pj), E_i);
                    VectorT local_rel=e_local_geom->Project_To_TPlane(e_particles->x(pj)-x_i, E_i);
                    sum_v += e_particles->vt(pj) * w;
                    sum_B += local_vel*local_rel.transpose()*w;
                    sum_D += local_rel*local_rel.transpose()*w;
                }
                sum_v*=ia_i;
                l_particles->v(pi) = sum_v;
                l_particles->v(pi) -= l_particles->v(pi).dot(l_particles->n(pi))*l_particles->n(pi);

                sum_B*=ia_i;
                l_particles->B(pi) = sum_B;
                sum_D*=ia_i;
                l_particles->D(pi) = sum_D;

            }
        }

        
        void Update_Eulerian_Geometry(std::shared_ptr<PointLocalGeometry<d>> e_local_geom, std::shared_ptr<EulerianParticles<d>> e_particles){
            
            e_local_geom->nbs_searcher->Update_Points(e_particles->xRef());
            e_local_geom->normal_ptr=std::make_shared<Array<VectorD> >(e_particles->nRef());

            #pragma omp parallel for
            for (int pi = 0; pi < e_particles->Size(); pi++) {
                VectorD x_i=e_particles->x(pi);
                MatrixD E_i=e_local_geom->Spawn_Local_Frame(e_particles->n(pi));
                // Array<int> nbs; e_local_geom->nbs_searcher->Find_Nbs(x_i, e_local_geom->v_r, nbs);
                Array<int>& nbs=ee_nbs[pi];

				real nden = sph_t.Sum([&](int idx)->real {return 1.0f; },
                    [&](int idx)->VectorT {return e_local_geom->Project_To_TPlane(x_i-e_particles->x(idx), E_i); },
                    nbs, e_local_geom->v_r);
                if(nden>0){
                    e_particles->a(pi) = 1.0/nden;
                    
                }else{
                    Warn("zero nden at x={}", x_i.transpose()); //// use V as nden now
                    e_particles->a(pi) = 0.0;
                    e_particles->eta(pi) = 0.0f;
                }
                if(e_particles->a(pi)>0){
                    e_particles->eta(pi) = e_particles->V(pi)/e_particles->a(pi);
                    if(isnan(e_particles->eta(pi)) || e_particles->eta(pi)==0)
                        std::cout<<"pi="<<pi<<", eta="<<e_particles->eta(pi)<<std::endl;
                }else{
                    e_particles->eta(pi) = 0;
                }
            }
			Info("Total area: {}", e_particles->Size() * ArrayFunc::Mean<real>(e_particles->aRef()));
        }

        void Solve_Eulerian_System(std::shared_ptr<PointLocalGeometry<d>> e_local_geom, std::shared_ptr<EulerianParticles<d>> e_particles, real dt){

            //// define a surface gradient
            auto Surface_Gradient=[&](const int i, const MatrixD local_frame, const Array<int>& nbs, std::function<real(const int)>& val_func, std::function<real(const int)>& vol_func){
                real val_i=val_func(i);

                return sph_t.Grad(
                    [&](int idx)->real {return val_func(idx)-val_i; },
                    [&](int idx)->VectorT {return e_local_geom->Project_To_TPlane(e_particles->x(i)-e_particles->x(idx),local_frame); },
                    [&](int idx)->real {return vol_func(idx); },
                    nbs, e_local_geom->v_r
                );
            };

            //// compute the acc using density
            std::function<real(const int)> V_func = [&](const int idx)->real {
                return e_particles->V(idx);
            };
            std::function<real(const int)> a_func = [&](const int idx)->real {
                return e_particles->a(idx);
            };

            e_local_geom->nbs_searcher->Update_Points(e_particles->xRef());
            e_local_geom->normal_ptr=std::make_shared<Array<VectorD> >(e_particles->nRef());

            #pragma omp parallel for
            for (int pi = 0; pi < e_particles->Size(); pi++) {
                VectorD x_i=e_particles->x(pi);
                MatrixD E_i=e_local_geom->Spawn_Local_Frame(e_particles->n(pi));
                // Array<int> nbs; e_local_geom->nbs_searcher->Find_Nbs(x_i, e_local_geom->v_r, nbs);
                Array<int>& nbs=ee_nbs[pi];

                if(std::isnan(e_particles->vt(pi).maxCoeff())) std::cout<<"eulerian tangent velocity is nan"<<std::endl;

                VectorT tang_acc=Surface_Gradient(pi, E_i, nbs, V_func, a_func);
                if(std::isnan(tang_acc.maxCoeff())) std::cout<<"eulerian tangent velocity is nan"<<std::endl;

                VectorD acc = e_local_geom->Unproject_To_World(tang_acc, E_i);
                if(std::isnan(acc.maxCoeff())) std::cout<<"eulerian velocity is nan"<<std::endl;
                e_particles->vt(pi) += 0.1f * dt * acc;

            }
        }

        void Solve_MELP_Eulerian_System(std::shared_ptr<PointLocalGeometry<d>> e_local_geom, std::shared_ptr<EulerianParticles<d>> e_particles, real dt){
            e_local_geom->nbs_searcher->Update_Points(e_particles->xRef());
            e_local_geom->normal_ptr=std::make_shared<Array<VectorD> >(e_particles->nRef());

            //// define a surface operators
            auto Surface_Gradient=[&](const int i, const MatrixD local_frame, const Array<int>& nbs, std::function<real(const int)>& val_func, std::function<real(const int)>& vol_func){
                real val_i=val_func(i);

                return sph_t.Grad(
                    [&](int idx)->real {return val_func(idx)-val_i; },
                    [&](int idx)->VectorT {return e_local_geom->Project_To_TPlane(e_particles->x(i)-e_particles->x(idx),local_frame); },
                    [&](int idx)->real {return vol_func(idx); },
                    nbs, e_local_geom->v_r
                );
            };

            auto Surface_Divergence=[&](const int i, const MatrixD local_frame, const Array<int>& nbs, std::function<VectorD(const int)>& val_func, std::function<real(const int)>& vol_func){
                VectorD val_i=val_func(i);

                return sph_t.Div(
                    [&](int idx)->VectorT {return e_local_geom->Project_To_TPlane(val_func(idx)-val_i,local_frame); },
                    [&](int idx)->VectorT {return e_local_geom->Project_To_TPlane(e_particles->x(i)-e_particles->x(idx),local_frame); },
                    [&](int idx)->real {return vol_func(idx); },
                    nbs, e_local_geom->v_r
                );
            };

            auto Surface_Laplacian=[&](const int i, const MatrixD local_frame, const Array<int>& nbs, std::function<real(const int)>& val_func, std::function<real(const int)>& vol_func){
                real val_i=val_func(i);

                return sph_t.Lap_Brookshaw(
                    [&](int idx)->real {return val_func(idx)-val_i; },
                    [&](int idx)->VectorT {return e_local_geom->Project_To_TPlane(e_particles->x(i)-e_particles->x(idx),local_frame); },
                    [&](int idx)->real {return vol_func(idx); },
                    nbs, e_local_geom->v_r
                );
            };

            //// define surface operator coeffcients
            auto Surface_Gradient_Coeff=[&](const int i, const MatrixD local_frame, const Array<int>& nbs, std::function<real(const int)>& vol_func){
                // real val_i=val_func(i);
                Array<VectorT> coeffs=sph_t.Grad_Coeff(
                    [&](int idx)->VectorT {return e_local_geom->Project_To_TPlane(e_particles->x(i)-e_particles->x(idx),local_frame); },
                    [&](int idx)->real {return vol_func(idx); },
                    nbs, e_local_geom->v_r
                );
                
                // int ki=0; VectorT diag_coeff=VectorT::Zero();
                // for(int k=0;k<nbs.size();k++){
                //     int j=nbs[k];
                //     if(i==j) ki=k;
                //     diag_coeff-=coeffs[k];
                // }
                // coeffs[ki]+=diag_coeff;
                return Array<VectorT>(coeffs);
            };

            auto Surface_Laplacian_Coeff=[&](const int i, const MatrixD local_frame, const Array<int>& nbs, std::function<real(const int)>& vol_func){
                // real val_i=val_func(i);
                Array<real> coeffs=sph_t.Lap_Brookshaw_Coeff(
                    [&](int idx)->VectorT {return e_local_geom->Project_To_TPlane(e_particles->x(i)-e_particles->x(idx),local_frame); },
                    [&](int idx)->real {return vol_func(idx); },
                    nbs, e_local_geom->v_r
                );
                
                // int ki=0; real diag_coeff=0;
                // for(int k=0;k<nbs.size();k++){
                //     int j=nbs[k];
                //     if(i==j) ki=k;
                //     diag_coeff-=coeffs[k];
                // }
                // coeffs[ki]+=diag_coeff;
                return Array<real>(coeffs);
            };

            // auto Surface_Laplaican_Coeff_Melp=[&](const int i, const MatrixD local_frame, const Array<int>& nbs, std::function<real(const int)>& vol_func){
            //     //// TODO: ref to MELP Appendix
            //     int ki=0; real diag_coeff=0;
            //     for(int k=0;k<nbs.size();k++){
            //         int j=nbs[k];
            //         if(i==j) ki=k;
            //         diag_coeff-=coeffs[k];
            //     }
            //     // coeffs[ki]+=diag_coeff;
            //     return Array<real>(coeffs);
            // };


            //// define sample functions
            std::function<real(const int)> a_func = [&](const int idx)->real {
                return e_particles->a(idx);
            };
            std::function<VectorD(const int)> vt_func = [&](const int idx)->VectorD {
                return e_particles->vt(idx);
            };
            std::function<real(const int)> c_func = [&](const int idx)->real {
                return e_particles->c(idx);
            };
            std::function<real(const int)> inv_eta_func = [&](const int idx)->real {
                return 1.0/e_particles->eta(idx);
            };

            //// define buffers
            // Array<real> a_diag(e_particles->Size(),0);
            // Array<VectorT> a_grad(e_particles->Size(),0);
            // Array<real> a_lapl(e_particles->Size(),0);
            Array<real> b(e_particles->Size(),0);

            Array<Array<int>> particle_nbs(e_particles->Size());
            Array<Array<real>> particle_coeff(e_particles->Size());
            
            real rho=DefaultDynamicsParameter::liquid_density;
            real R=DefaultDynamicsParameter::ideal_gas_const;
            real T=DefaultDynamicsParameter::temperature;
            std::cout<<"build functions..."<<std::endl;
            //// build functions
            #pragma omp parallel for
            for (int pi = 0; pi < e_particles->Size(); pi++) {
                VectorD x_i=e_particles->x(pi);
                MatrixD E_i=e_local_geom->Spawn_Local_Frame(e_particles->n(pi));
                // Array<int> nbs; e_local_geom->nbs_searcher->Find_Nbs(x_i, e_local_geom->v_r, nbs);
                Array<int>& nbs=ee_nbs[pi];
                // particle_nbs[pi]=nbs;


                VectorT grad_inv_eta=Surface_Gradient(pi, E_i, nbs, inv_eta_func, a_func);

                real multiplier_diag=-1.0/(dt*e_particles->c(pi));
                VectorT multiplier_grad=dt*R*T/rho*grad_inv_eta;
                real multiplier_lapl=dt*R*T/(rho*e_particles->eta(pi));

                auto coef_grad=Surface_Gradient_Coeff(pi, E_i, nbs, a_func);
                auto coef_lapl=Surface_Laplacian_Coeff(pi, E_i, nbs, a_func);

                particle_coeff[pi].resize(nbs.size(),0.0f);

                int ki=0; real diag_coeff=0;
                for(int k=0;k<nbs.size();k++){
                    int pj=nbs[k];
                    particle_coeff[pi][k]+=multiplier_grad.dot(coef_grad[k])+multiplier_lapl*coef_lapl[k];
                    
                    if(std::isnan(particle_coeff[pi][k])) std::cout<<"matrix: particle coeff is nan "<<multiplier_grad.dot(coef_grad[k])<<"; "<<multiplier_lapl*coef_lapl[k]<<std::endl;
                    // if(pi==pj)
                    //    particle_coeff[pi][k]+=multiplier_diag;
                    if(pi==pj) ki=k;
                    diag_coeff-=particle_coeff[pi][k];
                }
                particle_coeff[pi][ki]=diag_coeff+multiplier_diag;

                real div_v=Surface_Divergence(pi, E_i, nbs, vt_func, a_func);
                b[pi]=div_v-1.0/dt;
            }


            std::cout<<"begin iterate..."<<std::endl;
            //// iterates
            Array<real> old_c=e_particles->cRef();
            Array<real> tmp_c=e_particles->cRef();
            for(int itr=0;itr<20;itr++){
                real w=0.2f;//// 1=jacobi
                std::cout<<"begin iterate..."<<itr<<std::endl;
                #pragma omp parallel for
                for (int pi = 0; pi < e_particles->Size(); pi++) {
                    
                    Array<int>& nbs=ee_nbs[pi];
                    real Ac=0.0f;
                    real A_ii=0.0f;
                    for(int k=0;k<nbs.size();k++){
                        int pj=nbs[k];
                        Ac+=particle_coeff[pi][k]*e_particles->c(pj);
                        if(pi==pj)
                            A_ii=particle_coeff[pi][k];
                    }

                    tmp_c[pi]=(1-w)*e_particles->c(pi)+w*(b[pi]-Ac)/A_ii;
                }
                e_particles->cRef().swap(tmp_c);
            }

            std::cout<<"begin correction..."<<std::endl;
            #pragma omp parallel for
            for (int pi = 0; pi < e_particles->Size(); pi++) {
                // if(old_c[pi]>0){
                //     e_particles->eta(pi)=e_particles->eta(pi)*e_particles->c(pi)/old_c[pi];
                // }

                MatrixD E_i=e_local_geom->Spawn_Local_Frame(e_particles->n(pi));
                Array<int>& nbs=ee_nbs[pi];
                VectorT c_tgrad=Surface_Gradient(pi, E_i, nbs, c_func, a_func);
                VectorD c_grad=e_local_geom->Unproject_To_World(c_tgrad, E_i);
                e_particles->vt(pi)-=dt*(2*R*T/rho/e_particles->eta(pi)*c_grad);

                if(std::isnan(e_particles->vt(pi).maxCoeff())) std::cout<<"solve: eulerian velocity is nan "<<pi<<std::endl;
                if(std::isnan(e_particles->c(pi))) std::cout<<"solve: eulerian c is nan "<<pi<<std::endl;
                if(std::isnan(e_particles->eta(pi))) std::cout<<"solve: eulerian eta is nan "<<pi<<std::endl;
            }
        }

        void Correct(std::shared_ptr<PointLocalGeometry<d>> local_geom, std::shared_ptr<LagrangianParticles<d>> particles){

        }

        void Advect_Lagrangian(std::shared_ptr<PointLocalGeometry<d>> l_local_geom, std::shared_ptr<LagrangianParticles<d>> l_particles, std::shared_ptr<PointLocalGeometry<d>> e_local_geom, std::shared_ptr<EulerianParticles<d>> e_particles, real dt){

            int mls_closest_point_iteration=5;
            real mls_narrow_band_width=e_local_geom->v_r*2;
            
            e_local_geom->nbs_searcher->Update_Points(e_particles->xRef());
            e_local_geom->normal_ptr=std::make_shared<Array<VectorD> >(e_particles->nRef());

            #pragma omp parallel for
            for (int pi = 0; pi < l_particles->Size(); pi++) {
                VectorD normal=l_particles->n(pi);
                VectorD x_i=l_particles->x(pi)+l_particles->v(pi)*dt;

                Array<int> e_nbs; e_local_geom->nbs_searcher->Find_Nbs(x_i, e_local_geom->v_r, e_nbs);

                auto [iteration, new_phi, new_normal, new_intf_x]=ParticleLevelSetHelper<d>::Closest_Point_On_Particle_Surface(x_i, e_particles, e_local_geom, mls_closest_point_iteration, mls_narrow_band_width);
                if(iteration>0){
                    l_particles->x(pi)=new_intf_x;
                    //// rotate velocity
                    MatrixD rot_mat=e_local_geom->Rotate_Matrix(normal, new_normal);
                    l_particles->v(pi)=rot_mat*l_particles->v(pi);
                    l_particles->n(pi)=new_normal;
                }
            }
        }

        void Compute_Tangential_Dynamics(std::shared_ptr<PointLocalGeometry<d>> l_local_geom, std::shared_ptr<LagrangianParticles<d>> l_particles, std::shared_ptr<PointLocalGeometry<d>> e_local_geom, std::shared_ptr<EulerianParticles<d>> e_particles, real dt){

            Timer timer;
            // //// 0. set neighbor seacher
            // l_local_geom->nbs_searcher->Update_Points(l_particles->xRef());
            // l_local_geom->normal_ptr=std::make_shared<Array<VectorD> >(l_particles->nRef());
            // e_local_geom->nbs_searcher->Update_Points(e_particles->xRef());
            // e_local_geom->normal_ptr=std::make_shared<Array<VectorD> >(e_particles->nRef());
            // std::cout<<"set nbs searcher"<<std::endl;
            // Info("set nbs searcher, time={}", timer.Lap_Time());

            //// 0. build neighbors
            e_local_geom->nbs_searcher->Update_Points(e_particles->xRef());
            e_local_geom->normal_ptr=std::make_shared<Array<VectorD> >(e_particles->nRef());
            le_nbs.resize(l_particles->Size());
            #pragma omp parallel for
            for(int pi=0;pi<l_particles->Size();pi++){
                VectorD x_i=l_particles->x(pi);
                e_local_geom->nbs_searcher->Find_Nbs(x_i, e_local_geom->v_r, le_nbs[pi]);
            }
            ee_nbs.resize(e_particles->Size());
            #pragma omp parallel for
            for(int pi=0;pi<e_particles->Size();pi++){
                VectorD x_i=e_particles->x(pi);
                e_local_geom->nbs_searcher->Find_Nbs(x_i, e_local_geom->v_r, ee_nbs[pi]);
            }
            Info("Build neighbor, time={}", timer.Lap_Time());

            //// 1. lagrangian to eulerian
            Lagrangian_To_Eulerian(l_local_geom, l_particles, e_local_geom, e_particles);
            std::cout<<"Lagrangian To Eulerian"<<std::endl;
            Info("Lagrangian To Eulerian, time={}", timer.Lap_Time());

            //// 2. compute area and thickness based on volume and area
            Update_Eulerian_Geometry(e_local_geom, e_particles);
            std::cout<<"Update Eulerian Geometry"<<std::endl;
            Info("Update Eulerian Geometry, time={}", timer.Lap_Time());

            //// 3. solve a simple dynamics
            // Solve_Eulerian_System(e_local_geom, e_particles, dt);
            Solve_MELP_Eulerian_System(e_local_geom, e_particles, dt);
            std::cout<<"Solve System"<<std::endl;
            Info("Solve System, time={}", timer.Lap_Time());
            
            //// 4. eulerian to lagrangian
            Eulerian_To_Lagrangian(l_local_geom, l_particles, e_local_geom, e_particles);
            std::cout<<"Eulerian To Lagrangian"<<std::endl;
            Info("Eulerian To Lagrangian, time={}", timer.Lap_Time());

            //// 5. advect lagrangian particles and project onto eulerian geometry
            // Advect_Lagrangian(l_local_geom, l_particles, e_local_geom, e_particles, dt);
        }

    };
}
