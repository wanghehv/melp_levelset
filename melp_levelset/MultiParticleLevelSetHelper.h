#pragma once

#include "LevelSet.h"
#include "Region.h"

namespace Meso {
    template<int d> 
    class MultiParticleLevelSetHelper{
        Typedef_VectorD(d);
        Typedef_MatrixD(d);
        using VectorT=Vector<real,d-1>;    ////tangential vector type
        using VectorTi=Vector<int,d-1>;    ////tangential vector int

public:
        static void Correct_Multiple_Levelset(LevelSet<d>& levelset, Field<int,d>& indicator,Field<Array<std::pair<int, real>>,d>& narrowband_phi){
        
        #define pii std::pair<real,int>

            levelset.phi.Exec_Nodes([&](const VectorDi cell) {
                    
                levelset.phi(cell)=-1e8;

                std::priority_queue<pii, Array<pii>, std::greater<pii>> min_heap;//// min at top

                //// find min phi
                for(int l=0;l<narrowband_phi(cell).size();l++){
                    std::pair<int, real> pair=narrowband_phi(cell)[l];
                    min_heap.push({pair.second, pair.first});
                }

                if(min_heap.size()==0) {levelset.phi(cell)=-1e8;}
                else if(min_heap.size()==1) {
                    auto [phi, rid] = min_heap.top();
                    // levelset.phi(cell)=phi; indicator(cell)=rid;
                    levelset.phi(cell)=-1e8;
                    // if(ref_cell_set.find(cell)!=ref_cell_set.end()){
                    //     std::cout<<"debug cell: ("<<cell.transpose()<<"), heap size=1, rid="<<rid<<", phi="<<phi<<std::endl;
                    // }
                    // if(phi<=0) {levelset.phi(cell)=phi; indicator(cell)=rid;}
                    // else {levelset.phi(cell)=1e8; indicator(cell)=-1;}
                }else if(min_heap.size()>=2) {
                    auto [phi0, rid0] = min_heap.top();
                    min_heap.pop();
                    auto [phi1, rid1] = min_heap.top();
                    min_heap.pop();

                    // if(ref_cell_set.find(cell)!=ref_cell_set.end()){
                    //     std::cout<<"debug cell: ("<<cell.transpose()<<"), rid="<<rid0<<","<<rid1<<", phi="<<phi0<<","<<phi1<<std::endl;
                    // }

                    real mean_phi=0.5*(phi0+phi1);
                    phi0-=mean_phi;
                    phi1-=mean_phi;

                    for(int l=0;l<narrowband_phi(cell).size();l++){
                        narrowband_phi(cell)[l].second-=mean_phi;
                        // if(ref_cell_set.find(cell)!=ref_cell_set.end()){
                        //     std::cout<<"debug cell: narrowband phi is updated: rid="<<narrowband_phi(cell)[l].first<<",phi="<<narrowband_phi(cell)[l].second<<std::endl;
                        // }
                    }

                    // levelset.phi(cell)=phi0<phi1?phi0:phi1;
                    indicator(cell)=phi0<phi1?rid0:rid1;
                    levelset.phi(cell)=phi0<phi1?phi0:phi1;
                    // if(ref_cell_set.find(cell)!=ref_cell_set.end()){
                    //     std::cout<<"debug cell: result:("<<cell.transpose()<<"), indicator="<<indicator(cell)<<",phi="<<levelset.phi(cell)<<std::endl;
                    // }
                }
                
            });

        #undef pii
        }
    
        static void Restore_Levelset_From_Narrowband(LevelSet<d>& levelset, Field<int,d>& indicator, Field<Array<std::pair<int, real>>,d>& narrowband_phi, int rid, real narrow_band_width){
            levelset.Init(narrowband_phi.grid);
            levelset.phi.Exec_Nodes([&](const VectorDi cell) {
                if(indicator(cell)==rid) levelset.phi(cell)=-1e8;
                else levelset.phi(cell)=1e8;

                for(int l=0;l<narrowband_phi(cell).size();l++){
                    std::pair<int, real> pair=narrowband_phi(cell)[l];
                    if(pair.first==rid){
                        levelset.phi(cell)=pair.second;
                    }
                }
            });
            if(narrow_band_width!=0)
            levelset.Fast_Marching(narrow_band_width);

        }
    };
}