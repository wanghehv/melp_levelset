//////////////////////////////////////////////////////////////////////////
// PLS particle representation
// Copyright (c) (2022-), Hui Wang
// This file is part of MELPLEVELSET, whose distribution is governed by the LICENSE file.
//////////////////////////////////////////////////////////////////////////
#pragma once

#include "Points.h"
#include "PointUtil.h"

namespace Meso{
    //// particle holder
    template<int d>
    class LagrangianParticles : public Points{
        Typedef_VectorD(d);
        Typedef_MatrixD(d);
        using VectorT=Vector<real,d-1>;
        using MatrixT=Matrix<real,d-1,d-1>;
    public:
        
        LagrangianParticles() : Points() {}
        Setup_Attribute(x, VectorD, VectorD::Zero()); //// position
        Setup_Attribute(v, VectorD, VectorD::Zero()); //// velocity
        Setup_Attribute(n, VectorD, VectorD::Zero()); //// normal

        Setup_Attribute(B, MatrixT, MatrixT::Identity()); //// B
        Setup_Attribute(D, MatrixT, MatrixT::Identity()); //// D

        //// area(number density?)
        Setup_Attribute(ia, real, 1.0); //// inverse of the area, used for partition of the unity weight

        //// physics attribute
        Setup_Attribute(c, real, 1.0); //// concentration

        void Append(const LagrangianParticles& other){
            PointUtil::Append_Vector_Elements<VectorD>(this->xRef(), other.xRef());
            PointUtil::Append_Vector_Elements<VectorD>(this->vRef(), other.vRef());
            PointUtil::Append_Vector_Elements<VectorD>(this->nRef(), other.nRef());

            PointUtil::Append_Vector_Elements<MatrixT>(this->BRef(), other.BRef());
            PointUtil::Append_Vector_Elements<MatrixT>(this->DRef(), other.DRef());


            PointUtil::Append_Vector_Elements<real>(this->iaRef(), other.iaRef());

            PointUtil::Append_Vector_Elements<real>(this->cRef(), other.cRef());

            this->size=this->xRef().size();
            PointUtil::Check_Points_Size(*this);
        }
        
    };
}