#pragma once

#include "Constants.h"
#include "EulerianParticles.h"

namespace Meso{
    template<int d>
    class GeometryDebugBuffer{
        Typedef_VectorD(d);
        Typedef_MatrixD(d);
        using VectorT=Vector<real,d-1>;								////tangential vector type
        using VectorTi=Vector<int,d-1>;								////tangential vector int

    public:
        Array<VectorD> vertices; 
        Array<VectorDi> elements;
        
        EulerianParticles<d> particles;
        
        void Clear(){
            Clear_Mesh();
            Clear_Particles();
        }

        void Clear_Mesh(){vertices.clear();elements.clear();}
        void Clear_Particles(){/*particles.Clear();*/}

        void Add_Particles(const Array<VectorD>&x, const Array<VectorD>&v){
            int offset=particles.Size();
            particles.Resize(offset+x.size());
            #pragma omp parallel for
            for(int i=0;i<x.size();i++){
                particles.x(i)=x[i];
                particles.v(i+offset)=v[i];
                particles.n(i+offset)=v[i];
            }
        }

        void Add_Particles(const Array<VectorD>&x, const Array<VectorD>&v, const Array<VectorD>&n){
            int offset=particles.Size();
            if(offset>0) Warn("overwrite particle data!");
            // particles.Resize(offset+x.size());
            // #pragma omp parallel for
            // for(int i=0;i<x.size();i++){
            //     particles.x(i+offset)=x[i];
            //     particles.v(i+offset)=v[i];
            //     particles.n(i+offset)=n[i];
            // }
            particles.Resize(x.size());
            #pragma omp parallel for
            for(int i=0;i<x.size();i++){
                particles.x(i)=x[i];
                particles.v(i)=v[i];
                particles.n(i)=n[i];
            }
        }

        //// 2d only
        void Add_Debug_Circle(VectorD c, real r){
            const real split_num=24;
            int v_off=vertices.size();
            int e_off=elements.size();
            for(int i=0;i<split_num;i++){
                vertices.push_back(c+
                    VectorD::Unit(0)*r*cos(CommonConstants::pi*(real)(2*i)/split_num)+
                    VectorD::Unit(1)*r*sin(CommonConstants::pi*(real)(2*i)/split_num));
                elements.push_back(
                    VectorDi::Unit(0)*(v_off+i)+
                    VectorDi::Unit(1)*(v_off+(i+1)%(int)split_num));
            }
        }

        void Add_Debug_Line(VectorD x0, VectorD x1){
            int v_off=vertices.size();
            vertices.push_back(x0);
            vertices.push_back(x1);
            elements.push_back(VectorDi::Ones()*v_off+VectorDi::Unit(1));
        }

        void Convert_Mesh(VertexMatrix<real, d>& vertex_matrix, ElementMatrix<d>& element_matrix){
            if constexpr(d==2){
                vertex_matrix = Eigen::Map<VertexMatrix<real, d>>(vertices[0].data(), vertices.size(), d);
                element_matrix = Eigen::Map<ElementMatrix<d>>(elements[0].data(), elements.size(), d);
                return;
            }
            if constexpr(d==3){;}
            Error("No implementation");

        }
        
    };
}