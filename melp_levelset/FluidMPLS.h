//////////////////////////////////////////////////////////////////////////
// Fluid Euler with Particle Level Set
// Copyright (c) (2022-), Hui Wang
// This file is part of MESO, whose distribution is governed by the LICENSE file.
//////////////////////////////////////////////////////////////////////////
#pragma once
#include "Json.h"
#include "Simulator.h"
#include "MultiParticleLevelSet.h"
#include "Advection.h"
#include "MarchingCubes.h"
#include "GridEulerFunc.h"
#include "Interpolator.h"
#include "Random.h"

// emitter
#include "Emitter.h"
#include "ShapeGenerateUtil.h"

// geometry
#include "GridUtil.h"
#include "FieldUtil.h"
#include "PointUtil.h"
#include "LevelSetUtil.h"
#include "PointLocalGeometry.h"
#include "GeometryDebugBuffer.h"


// dynamics
#include "DefaultDynamicsParameter.h"
#include "Multigrid.h"
#include "ConjugateGradient.h"
#include "SPH_Utils.h"
#include "SurfaceTension.h"

// tangential dynamics
#include "SurfaceSphHelper.h"


#include "IOFunc.h"
#include "ParticlesIOUtil.h"
#include <vtkTriangle.h>
#include <vtkLine.h>
#include <vtkCellData.h>
#include <vtkCellArray.h>

#include "FluidPLS.h" //// for enum CellType

namespace Meso {
    
    template<int d>
    int Neighbor_Cell_Sign(const Vector<int, d>& cell, const Vector<int, d>& nb_cell){
        for(int di=0;di<d;di++){
            if(cell(di)>nb_cell(di)) return -1;
            else if(cell(di)<nb_cell(di)) return 1;
        }
    }

    template<int d>
    class FluidMPLS : public Simulator {
        Typedef_VectorD(d);
        Typedef_MatrixD(d);
        using VectorT=Vector<real,d-1>;								////tangential vector type
        using VectorTi=Vector<int,d-1>;								////tangential vector int
    public:
        
        //// geometry, velocity initialized outside
        MultiParticleLevelSet<d> mpls;
        std::shared_ptr<PointLocalGeometry<d>> e_local_geom;
        std::shared_ptr<PointLocalGeometry<d>> l_local_geom;
        FaceField<real, d> velocity;

        //// levelset for comparison
        LevelSet<d> ls;

        //// emitter
        Array<std::shared_ptr<Emitter<d>>> emitters;

        //// options
        bool compare_with_levelset=false;
        bool use_normal_dynamics=false;
        bool use_tangent_dynamics=false;
        bool use_particle_correction=true;
        bool use_gravity=false;
        bool auto_generate_bubble=false;
        bool use_film_density=false;
        bool write_binary_particles=false;
        
        //// for normal dynamics
        // Needs to be initialized
        // physical quantities
        // real air_density;
        // real liquid_density;
        VectorD gravity_acc;
        real sigma;//// only used for region initialization
                
        //// from FluidFreeSurface
        // define the system behavior
        Field<CellType,d> cell_type;
        BoundaryConditionDirect<FaceFieldDv<real, d>> velocity_bc;

        FaceField<real,d> film_volume_host;

        //utilities
        MaskedPoissonMapping<real, d> poisson;
        VCycleMultigridIntp<real, d> MG_precond;
        ConjugateGradient<real> MGPCG;

        //Temporary variables, don't need to be initialized
        FaceFieldDv<real, d> velocity_dev;
        FaceFieldDv<real, d> temp_velocity_dev;
        FieldDv<real, d> temp_phi_dev;
        FieldDv<real, d> temp_field_dev;
        FieldDv<real, d> pressure_dev;
        Field<bool, d> fixed_host;
        FaceField<real, d> vol_host;
        Field<real, d> div_host;
        FaceField<real, d> pressure_jump;

        //// for debug
        GeometryDebugBuffer<d> geo_debug_buffer;
        
        void Init(json& j){
            //// init local_geom
            std::shared_ptr<NeighborSearcher<d>> nbs_searcher=std::make_shared<NeighborKDTree<d> >();
            e_local_geom=std::make_shared<PointLocalGeometry<d> > (mpls.levelset.phi.grid.dx, 2/1.5, nbs_searcher);
            l_local_geom=std::make_shared<PointLocalGeometry<d> > (mpls.levelset.phi.grid.dx, 2/1.5, nbs_searcher);

            
            //// init levelset
            if(compare_with_levelset){
                ls.Init(mpls.levelset.phi.grid);
                ls.phi.Deep_Copy(mpls.levelset.phi);
            }

            //// set options, in the default setting, pls correction with levelset comparison
            use_normal_dynamics=Json::Value<int>(j, "use_normal_dynamics", 0);
            use_tangent_dynamics=Json::Value<int>(j, "use_tangent_dynamics", 0);
            use_particle_correction=Json::Value<int>(j, "use_particle_correction", 1);
            compare_with_levelset=Json::Value<int>(j, "compare_with_levelset", 0);
            use_gravity=Json::Value<int>(j, "use_gravity", 0);
            auto_generate_bubble=Json::Value<int>(j, "auto_generate_bubble", 0);
            write_binary_particles=Json::Value<int>(j, "write_binary_particles", 0);

            //// 
            if(use_normal_dynamics)
            {
                // air_density = Json::Value<real>(j, "air_density", DefaultDynamicsParameter::air_density);
                // liquid_density = Json::Value<real>(j, "liquid_density", DefaultDynamicsParameter::liquid_density);
                gravity_acc = MathFunc::V<d>(Json::Value<Vector3>(j, "gravity_acc", Vector3::Unit(1) * DefaultDynamicsParameter::gravity));
                sigma = Json::Value<real>(j, "sigma", DefaultDynamicsParameter::sigma);

                for (auto itr = mpls.region_map.begin(); itr != mpls.region_map.end(); ++itr) {
                    if(itr->second->type==Region<d>::RegionType::AIR)
                        itr->second->sigma=sigma;
                }

                FaceField<bool, d> face_fixed(cell_type.grid);
                face_fixed.Calc_Faces(
                    [&](const int axis, const VectorDi face) {
                        auto [cell0, cell1, val0, val1] = Face_Neighbor_Cells_And_Values(cell_type, axis, face, INVALID);
                        if (val0 == CellType::SOLID || val1 == CellType::SOLID) return true;
                        return false;
                    }
                );
                velocity_bc.Init(face_fixed, velocity);

                poisson.Init(velocity.grid);
                MG_precond.Allocate_Poisson(velocity.grid);
                MGPCG.Init(&poisson, &MG_precond, false, -1, 1e-6);
            }

            Correct_MPLS();
            Update_Volume_MPLS(true);

            Enforce_Emitter_Eulerian_Particles();
        }

        void Output_Mesh_As_VTU(const VertexMatrix<real, d> &verts, const ElementMatrix<d> &elements, const std::string file_name) {
            Typedef_VectorD(d);

            // setup VTK
            vtkNew<vtkXMLUnstructuredGridWriter> writer;
            vtkNew<vtkUnstructuredGrid> unstructured_grid;

            vtkNew<vtkPoints> nodes;
            nodes->Allocate(verts.rows());
            vtkNew<vtkCellArray> cellArray;

            for (int i = 0; i < verts.rows(); i++) {
                Vector<real, d> pos = verts.row(i).template cast<real>();
                Vector3 pos3 = MathFunc::V<3>(pos);
                nodes->InsertNextPoint(pos3[0], pos3[1], pos3[2]);
            }
            unstructured_grid->SetPoints(nodes);

            if constexpr (d == 2) {
                for (int i = 0; i < elements.rows(); i++) {
                    vtkNew<vtkLine> line;
                    line->GetPointIds()->SetId(0, elements(i, 0));
                    line->GetPointIds()->SetId(1, elements(i, 1));
                    cellArray->InsertNextCell(line);
                }
                unstructured_grid->SetCells(VTK_LINE, cellArray);
            }
            else if constexpr (d == 3) {
                for (int i = 0; i < elements.rows(); i++) {
                    vtkNew<vtkTriangle> triangle;
                    triangle->GetPointIds()->SetId(0, elements(i, 0));
                    triangle->GetPointIds()->SetId(1, elements(i, 1));
                    triangle->GetPointIds()->SetId(2, elements(i, 2));
                    cellArray->InsertNextCell(triangle);
                }
                unstructured_grid->SetCells(VTK_TRIANGLE, cellArray);
            }

            writer->SetFileName(file_name.c_str());
            writer->SetInputData(unstructured_grid);
            writer->Write();
        }

        virtual void Output(DriverMetaData& metadata){
            Timer timer;
            //// velocity
            if constexpr(d==2){
                // Info("write velocity...");
                std::string vts_name = fmt::format("vts{:04d}.vts", metadata.current_frame);
                FileFunc::Create_Directory(metadata.base_path / bf::path("velocity"));
                bf::path vtk_path = metadata.base_path / bf::path("velocity") / bf::path(vts_name);
                VTKFunc::Write_VTS(velocity, vtk_path.string());
                Info("write velocity, time={}", timer.Lap_Time());
            }

            if constexpr(d==2)
            if(compare_with_levelset){
                // Info("write surface levelset only...");
                VertexMatrix<real, d> verts; ElementMatrix<d> elements;
                Marching_Cubes<real, d, HOST>(verts, elements, ls.phi);
                std::string surface_name = fmt::format("surface_ls{:04d}.vtu", metadata.current_frame);
                bf::path surface_path = metadata.base_path / bf::path(surface_name);
                Output_Mesh_As_VTU(verts, elements, surface_path.string());
                Info("write surface levelset only, time={}", timer.Lap_Time());
            }
            
            //// indicator
            if constexpr(d==2)
            {
                Info("write indicator...");
                FileFunc::Create_Directory(metadata.base_path / bf::path("indicator"));
                std::string vts_name = fmt::format("indicator_{:04d}.vts", metadata.current_frame);
                bf::path vtk_path = metadata.base_path / bf::path("indicator") / bf::path(vts_name);
                VTKFunc::Write_VTS(mpls.indicator, vtk_path.string());
            }

            //// type
            // if(metadata.current_frame==0)
            // {
            //     Info("write type...");
            //     std::string vts_name = fmt::format("type_{:04d}.vts", metadata.current_frame);
            //     bf::path vtk_path = metadata.base_path / bf::path(vts_name);
            //     Field<int,d> type_field(mpls.indicator.grid, -1);
            //     type_field.Calc_Nodes([&](const VectorDi cell) {
            //         if(mpls.indicator(cell)<0){
            //             Warn("invalid indicator at cell {}", cell.transpose());
            //             return -1;}
            //         return (int)mpls.region_map.find(mpls.indicator(cell))->second->type;
            //     });
            //     VTKFunc::Write_VTS(type_field, vtk_path.string());
            // }

            //// levelset
            if constexpr(d==2)
            {
                Info("write levelset...");
                FileFunc::Create_Directory(metadata.base_path / bf::path("levelset"));
                std::string vts_name = fmt::format("levelset_{:04d}.vts", metadata.current_frame);
                bf::path vtk_path = metadata.base_path / bf::path("levelset") / bf::path(vts_name);
                VTKFunc::Write_VTS(mpls.levelset.phi, vtk_path.string());
                Info("write levelset, time={}", timer.Lap_Time());
            }

            // Info("write Eulerian particles ...");
            // if constexpr(d==2)
            for (auto itr = mpls.region_map.begin(); itr != mpls.region_map.end(); ++itr) {
                // Info("write particles {}...", itr->first);
                // Info("type={}, rho={}, sigma={}", itr->second->type, itr->second->rho, itr->second->sigma);
                FileFunc::Create_Directory(metadata.base_path / bf::path("particles"));
                std::string particles_name = fmt::format("particles{}_{:04d}.vtp", itr->first, metadata.current_frame);
                bf::path particles_path = metadata.base_path / bf::path("particles") / bf::path(particles_name);
                // VTKFunc::Write_VTU_Particles<d>(itr->second->particles->xRef(), itr->second->particles->nRef(), particles_path.string());
                ParticlesIOUtil<d>::Write_Full_Eulerian_Particles(*itr->second->particles,particles_path.string());
            }
            Info("write Eulerian particles, time={}", timer.Lap_Time());


            if(write_binary_particles){
                for (auto itr = mpls.region_map.begin(); itr != mpls.region_map.end(); ++itr) {
                    FileFunc::Create_Directory(metadata.base_path / bf::path("bin_particles"));
                    std::string particles_name = fmt::format("particles{}_{:04d}.bin", itr->first, metadata.current_frame);
                    bf::path particles_path = metadata.base_path / bf::path("bin_particles") / bf::path(particles_name);
                    // VTKFunc::Write_VTU_Particles<d>(itr->second->particles->xRef(), itr->second->particles->nRef(), particles_path.string());
                    ParticlesIOUtil<d>::Write_Binary_Eulerian_Particles(*itr->second->particles,particles_path.string());
                }
                Info("write Eulerian particles for Houdini, time={}", timer.Lap_Time());
            }

            {
                FileFunc::Create_Directory(metadata.base_path / bf::path("lag_particles"));
                std::string particles_name = fmt::format("lag_particles_{:04d}.vtp", metadata.current_frame);
                bf::path particles_path = metadata.base_path / bf::path("lag_particles") / bf::path(particles_name);
                // VTKFunc::Write_VTU_Particles<d>(mpls.l_particles->xRef(), mpls.l_particles->vRef(), particles_path.string());
                ParticlesIOUtil<d>::Write_Full_Lagrangian_Particles(*mpls.l_particles,particles_path.string());
            }
            Info("write Lagrangian particles, time={}", timer.Lap_Time());

            // for (auto itr = mpls.region_map.begin(); itr != mpls.region_map.end(); ++itr) {
            //     Info("write particles volume {}...", itr->first);
            //     Info("type={}, rho={}, sigma={}", itr->second->type, itr->second->rho, itr->second->sigma);
            //     Array<VectorD> info(itr->second->particles->Size(), VectorD::Zero());
            //     for(int i=0;i<itr->second->particles->Size();i++){
            //         info[i](0)=itr->second->particles->V(i);
            //         info[i](1)=itr->second->particles->a(i);
            //         info[i](2)=itr->second->particles->c(i);
            //     }

            //     std::string particles_name = fmt::format("pinfo{}_{:04d}.vtu", itr->first, metadata.current_frame);
            //     bf::path particles_path = metadata.base_path / bf::path(particles_name);
            //     VTKFunc::Write_VTU_Particles<d>(itr->second->particles->xRef(), info, particles_path.string());
            // }

            FileFunc::Create_Directory(metadata.base_path / bf::path("surface"));
            for (auto itr = mpls.region_map.begin(); itr != mpls.region_map.end(); ++itr) {
                // std::string vts_name = fmt::format("levelset{}_{:04d}.vts", itr->first, metadata.current_frame);
                // bf::path vtk_path = metadata.base_path / bf::path(vts_name);
                // VTKFunc::Write_VTS(itr->second->phi, vtk_path.string());
                int narrow_band_cell_num=5;
                real narrow_band_width=mpls.levelset.phi.grid.dx*narrow_band_cell_num;

                int rid=itr->first;
                LevelSet<d> levelset;
                MultiParticleLevelSetHelper<d>::Restore_Levelset_From_Narrowband(levelset,mpls.indicator,mpls.narrowband_phi,rid,0);

                VertexMatrix<real, d> verts; ElementMatrix<d> elements;
                if constexpr(d==2)
                    Marching_Cubes<real, d, HOST>(verts, elements, levelset.phi);
                if constexpr(d==3){
                    Field<real,d,DEVICE> phi_dev=levelset.phi;
                    Marching_Cubes<real, d, DEVICE>(verts, elements, phi_dev);
                }

                std::string surface_name = fmt::format("surface{}_{:04d}.vtu", rid, metadata.current_frame);
                bf::path surface_path = metadata.base_path/ bf::path("surface")  / bf::path(surface_name);
                Output_Mesh_As_VTU(verts, elements, surface_path.string());
            }
            Info("write surface, time={}", timer.Lap_Time());


            if(metadata.current_frame%50==0){
                Info("write region info...");
                json j;
                for (auto itr = mpls.region_map.begin(); itr != mpls.region_map.end(); ++itr) {
                    // Info("write region info {}...", itr->first);
                    j[std::to_string(itr->first)]=itr->second->To_Json();
                }
                std::string region_name = fmt::format("region_{:04d}.json", metadata.current_frame);
                bf::path region_path = metadata.base_path / bf::path(region_name);
                
                std::ofstream region_output(region_path.string());
                region_output <<std::setw(4)<< j;
                region_output.close();
            }
        };

        //can return inf
        virtual real CFL_Time(const real cfl){
            real dx = velocity.grid.dx;
            real max_vel = sqrt(2.0)*GridEulerFunc::Linf_Norm(velocity);
            return dx * cfl / max_vel;
        };

        virtual void Advance(DriverMetaData& metadata){
            real dt = metadata.dt;
            Timer timer;

            Advect_Eulerian(dt);
            Info("Advect Eulerian, time={}", timer.Lap_Time());
            
            static int cnt=0;
            if(use_particle_correction){
                std::cout<<"correct mpls"<<std::endl;
                Correct_MPLS();
                Info("Correct MPLS, time={}", timer.Lap_Time());
            }
            {
                if(cnt%2==0){
                    mpls.Reseed_Particles(e_local_geom);
                    Info("Reseed MPLS, time={}", timer.Lap_Time());
                    // Correct_MPLS();
                    // Info("Correct MPLS after reseed, time={}", timer.Lap_Time());
                }
                cnt++;
                Update_Volume_MPLS(false);
                Info("Update_Volume, time={}", timer.Lap_Time());
            }

            Advect_Lagrangian(dt);
            Info("Advect Lagrangian, time={}", timer.Lap_Time());

            Advect_Velocity(dt);
            Info("Advect Velocity, time={}", timer.Lap_Time());
            
            // for (auto itr = mpls.region_map.begin(); itr != mpls.region_map.end(); ++itr) {
            //     int rid=itr->first;
            //     std::shared_ptr<EulerianParticles<d>> particles=itr->second->particles;
            //     std::cout<<"check particle num, region "<<rid<<", particle num="<<particles->Size()<<std::endl;
            // }
            Update_Region_MPLS();
            Info("Update Region MPLS, time={}", timer.Lap_Time());

            Enforce_Emitter_Velocity();
            // if(metadata.current_frame<3)
            //     Enforce_Emitter_Lagrangian_Particles();
            Info("Force Emitter Velocity, time={}", timer.Lap_Time());
            
            std::cout<<"build inner cells"<<std::endl;
            mpls.Build_Inner_Cells();
            Info("Build Inner Cells, time={}", timer.Lap_Time());

            //// transfer particle mass to grid
            if(use_film_density) {Transfer_Particle_To_Grid(); Info("Transfer particles to grid, time={}", timer.Lap_Time());}

            std::cout<<"compute normal dynamics"<<std::endl;
            if(use_normal_dynamics) {Compute_Normal_Dynamics(dt); Info("Normal dynamics, time={}", timer.Lap_Time());}
            if(use_tangent_dynamics) {Compute_Tangent_Dynamics(dt); Info("Tangent dynamics, time={}", timer.Lap_Time());}


            if(auto_generate_bubble && metadata.current_frame%200==2) {
                // for(int i=0;i<30;i++){
                //     Generate_New_Bubble(); Info("Generate new bubble, time={}", timer.Lap_Time());
                // }

                Grid<d>& grid=mpls.levelset.phi.grid;
                VectorD length=grid.Node_Max()-grid.Node_Min();

                Array<SphereShape<d>> sphere_list;
                Array<VectorD> sphere_centers;
                if constexpr(d==2){
                    {
                        Array<real> x_positions(3);
                        x_positions[0]=0.3; x_positions[1]=0.5; x_positions[2]=0.7;
                        for(int ii=0;ii<x_positions.size();ii++){
                            sphere_centers.push_back(VectorD(x_positions[ii],0.3)*length(0));
                        }
                    }
                    {
                        Array<real> x_positions(4);
                        x_positions[0]=0.2; x_positions[1]=0.4; x_positions[2]=0.6; x_positions[3]=0.8;
                        for(int ii=0;ii<x_positions.size();ii++){
                            sphere_centers.push_back(VectorD(x_positions[ii],0.1)*length(0));
                        }
                    }
                }
                if constexpr(d==3){
                    {
                        Array<real> x_positions(3);
                        x_positions[0]=0.3; x_positions[1]=0.5; x_positions[2]=0.7;
                        for(int ii=0;ii<x_positions.size();ii++)
                        for(int jj=0;jj<x_positions.size();jj++){
                            sphere_centers.push_back(VectorD(x_positions[ii],0.3,x_positions[jj])*length(0));
                        }
                    }

                    {
                        Array<real> x_positions(4);
                        x_positions[0]=0.2; x_positions[1]=0.4; x_positions[2]=0.6; x_positions[3]=0.8;
                        for(int ii=0;ii<x_positions.size();ii++)
                        for(int jj=0;jj<x_positions.size();jj++){
                            sphere_centers.push_back(VectorD(x_positions[ii],0.1,x_positions[jj])*length(0));
                        }
                    }
                }
                for(int i=0;i<sphere_centers.size();i++){
                    Generate_Sphere_Bubble(SphereShape<d>(sphere_centers[i], length[0]*0.08f));
                }
            }
        };

        void Advect_Velocity(real dt){
            ///// advect velocity
            if(use_normal_dynamics){
                Info("Advect Velocity");
                
                Advection<IntpLinearPadding0>::Advect(dt, temp_velocity_dev, velocity_dev, velocity_dev);
                velocity = temp_velocity_dev;
                velocity_dev = temp_velocity_dev;
            }
        }

        void Advect_Eulerian(real dt){
            
            //// advect levelset
            {
                velocity_dev=velocity;

                temp_phi_dev = mpls.levelset.phi;
                Advection<IntpLinearClamp>::Advect(dt, temp_field_dev, temp_phi_dev, velocity_dev);
                mpls.levelset.phi = temp_field_dev;
            }

            //// advect particle & normal
            for (auto itr = mpls.region_map.begin(); itr != mpls.region_map.end(); ++itr) {
                #pragma omp parallel for
                for (int i = 0; i < itr->second->particles->Size(); i++) {
                    if(itr->second->particles->t(i)==1) continue; //// skip fixed particles
                    VectorD x=itr->second->particles->x(i);
                    // VectorD v=IntpLinear::Face_Vector(velocity,x);
                    VectorD v=IntpLinearClamp::Face_Vector(velocity,x);
                    x+=dt * v;
                    itr->second->particles->x(i) = x;
                    itr->second->particles->v(i) = v;
                    VectorD n=itr->second->particles->n(i);
                    
                    VectorD acc_n=VectorD::Zero();
                    MatrixD grad_v=FieldSampler<IntpLinearClamp>::Gradient(velocity,x);

                    for(int di=0;di<d;di++)
                    for(int dj=0;dj<d;dj++)
                        acc_n(di)-=grad_v(dj,di)*n(dj);

                    for(int dj=0;dj<d;dj++)
                    for(int dk=0;dk<d;dk++)
                        acc_n+=n(dj)*n(dk)*grad_v(dj,dk)*n;
                    itr->second->particles->n(i)=(n+acc_n*dt).normalized();
                }
            }


        }

        void Advect_Lagrangian(real dt){
            Timer timer;
            for (auto itr = mpls.region_map.begin(); itr != mpls.region_map.end(); ++itr) {
                int rid=itr->first;
                if(rid!=0) continue;
                Info("Advect Lagrangian...");
                
                std::shared_ptr<Region<d>> region=itr->second;
                std::shared_ptr<EulerianParticles<d>> e_particles=region->particles;

                //// advect normal velocity
                #pragma omp parallel for
                for (int pi = 0; pi < mpls.l_particles->Size(); pi++) {
                    VectorD x=mpls.l_particles->x(pi);
                    VectorD v=IntpLinearClamp::Face_Vector(velocity,x);
                    mpls.l_particles->x(pi)=x+dt*v;
                }

                Info("Advect Lagrangian::advect normal vel, time={}", timer.Lap_Time());
                
                //// advect tangent velocity, correct position
                SurfaceSphHelper<d> sph_helper;
                sph_helper.Advect_Lagrangian(l_local_geom, mpls.l_particles, e_local_geom, e_particles, dt);
                Info("Advect Lagrangian::advect tangent vel, time={}", timer.Lap_Time());
            }
        }

        void Correct_MPLS(){
            // mpls.Correction(local_geom);
            mpls.Fast_Correction(e_local_geom);
        }

        void Update_Volume_MPLS(bool init=false){
            mpls.Update_Volume(init, false);
        }

        //// TODO:
        void Update_Region_MPLS(){
            Info("Detect Split Region...");
            //// flood fill to find split
            int subregion_cnt=mpls.Detect_Split_Region(e_local_geom);
            //// update volume
            if(subregion_cnt>0) mpls.Update_Volume(false, false);
            mpls.Remove_Empty_Region();

            Info("Detect Merge Region...");
            mpls.Detect_Merge_Region(e_local_geom);
        }

        void Enforce_Emitter_Eulerian_Particles(){
            for(auto itr=mpls.region_map.begin();itr!=mpls.region_map.end();++itr) {
                std::shared_ptr<EulerianParticles<d>> particles=itr->second->particles;
                for(auto emitter:emitters){
                    emitter->Force_Eulerian_Particles(*particles);
                }
                Info("enforce emitter eulerian particles for region {}",itr->first);
            }
        }

        void Enforce_Emitter_Lagrangian_Particles(){            
            for(auto emitter:emitters){
                emitter->Force_Lagrangian_Particles(*mpls.l_particles);
            }
            Info("enforce emitter lagrangian particles for region");
        }

        void Enforce_Emitter_Velocity(){
            for(auto emitter:emitters){
                emitter->Force_Velocity_Field(velocity);
            }
        }


        void Compute_Normal_Dynamics(real dt){
            Timer timer;

            if(use_gravity){
                velocity=velocity_dev;
                velocity.Calc_Faces([&](const int axis, const VectorDi face)->real {
                    return velocity(axis,face)+gravity_acc(axis)*dt;
                });
                velocity_dev=velocity;
                velocity_bc.Apply(velocity_dev);
                Info("Compute_Normal_Dynamics::apply gravity g={}, time={}", gravity_acc.transpose(), timer.Lap_Time());
            }

            temp_field_dev=mpls.levelset.phi;
            ExteriorDerivativePadding0::Apply(temp_field_dev, velocity_dev);

            div_host = temp_field_dev;
            Update_Poisson_System(fixed_host, vol_host, div_host);
            Info("Compute_Normal_Dynamics::update poisson system, time={}", timer.Lap_Time());
            
            pressure_jump=FaceField<real, d>(mpls.levelset.phi.grid,0);
            Update_Jump_Condition(mpls, vol_host, dt);
            Apply_Jump_Condition_To_b(div_host, cell_type, mpls, vol_host, dt);
            Info("Compute_Normal_Dynamics::jump condition, time={}", timer.Lap_Time());

            Apply_Volume_Correction_To_b(div_host, cell_type, mpls, dt);
            Info("Compute_Normal_Dynamics::volume correction, time={}", timer.Lap_Time());

            
            poisson.Init(fixed_host, vol_host);
            Info("Compute_Normal_Dynamics::init poisson system, time={}", timer.Lap_Time());
            MG_precond.Update_Poisson(poisson, 2, 2);
            temp_field_dev = div_host;
            Info("Compute_Normal_Dynamics::update preconditioner, time={}", timer.Lap_Time());
            
            pressure_dev.Init(temp_field_dev.grid,0);
            auto [iter, res] = MGPCG.Solve(pressure_dev.Data(), temp_field_dev.Data());
            Info("Compute_Normal_Dynamics::Solve poisson with {} iters and residual {}, time={}", iter, res, timer.Lap_Time());

            //Info("Compute_Normal_Dynamics::solved pressure: \n{}", pressure_dev);

            //velocity+=grad(p)
            ExteriorDerivativePadding0::Apply(temp_velocity_dev, pressure_dev);
            temp_velocity_dev *= poisson.vol;
            Info("Compute_Normal_Dynamics::correct, time={}", timer.Lap_Time());

            velocity_dev += temp_velocity_dev;

            velocity=velocity_dev;
            Apply_Jump_Condition_To_Velocity(velocity, cell_type, mpls, vol_host, dt);
            velocity_dev=velocity;

            velocity_bc.Apply(velocity_dev);
            Info("Compute_Normal_Dynamics::velocity correct, time={}", timer.Lap_Time());

            Info("Compute_Normal_Dynamics::After projection max velocity {}", GridEulerFunc::Linf_Norm(velocity_dev));

            // Extrapolation(velocity_dev);
            // velocity_bc.Apply(velocity_dev);

            velocity=velocity_dev;
        }

        void Compute_Tangent_Dynamics(real dt){
            for (auto itr = mpls.region_map.begin(); itr != mpls.region_map.end(); ++itr) {
                int rid=itr->first;
                if(rid!=0) continue;

                
                std::shared_ptr<Region<d>> region=itr->second;
                std::shared_ptr<EulerianParticles<d>> particles=region->particles;
                SurfaceSphHelper<d> sph_helper;
                sph_helper.Compute_Tangential_Dynamics(l_local_geom, mpls.l_particles, e_local_geom, particles, dt);

            }
        }

        //// normal dynamics
    public:
        //// only used if(use_film_density)
        void Transfer_Particle_To_Grid(){
            const Grid<d>& grid=mpls.levelset.phi.grid;

            //// update num density
            for (auto itr = mpls.region_map.begin(); itr != mpls.region_map.end(); ++itr) {
                int rid=itr->first;
                std::shared_ptr<Region<d>> region=itr->second;
                std::shared_ptr<EulerianParticles<d>> particles=region->particles;

                e_local_geom->nbs_searcher->Update_Points(particles->xRef());
                e_local_geom->normal_ptr=std::make_shared<Array<VectorD> >(particles->nRef());//// update local geometry

            }


            film_volume_host.Init(grid,0);
            FaceField<real,d> weight; weight.Init(grid,0);

            static constexpr int dx[8] = { 0,1,0,1,0,1,0,1 };
            static constexpr int dy[8] = { 0,0,1,1,0,0,1,1 };
            static constexpr int dz[8] = { 0,0,0,0,1,1,1,1 };
            
            for (auto itr = mpls.region_map.begin(); itr != mpls.region_map.end(); ++itr) {
                int rid=itr->first;
                std::shared_ptr<Region<d>> region=itr->second;
                std::shared_ptr<EulerianParticles<d>> particles=region->particles;
                
                for (int pi = 0; pi < particles->Size(); pi++) 
                for (int axis = 0; axis < d; axis++) {
                    Grid<d> face_grid = grid.Face_Grid(axis);
                    VectorDi node; VectorD frac;
                    face_grid.Get_Fraction(particles->x(pi), node, frac);
                    VectorDi face_counts = face_grid.Counts();
                    //clamp
                    for (int axis = 0; axis < d; axis++) {
                        if (node[axis] < 0) node[axis] = 0, frac[axis] = 0;
                        if (node[axis] > face_counts[axis] - 2) node[axis] = face_counts[axis] - 2, frac[axis] = 1;
                    }
                    if constexpr (d == 2) {
                        real w[2][2] = { {1.0 - frac[0],frac[0]},{1.0 - frac[1],frac[1]} };
                        for (int s = 0; s < 4; s++){
                            int d0 = dx[s], d1 = dy[s];
                            int idx = face_grid.Index(node[0] + d0, node[1] + d1);
                            (*film_volume_host.face_data[axis])[idx] += w[0][d0] * w[1][d1] * particles->V(pi);
                            (*weight.face_data[axis])[idx] += w[0][d0] * w[1][d1];
                        }
                    }
                    if constexpr (d == 3) {
                        real w[3][2] = { {1.0 - frac[0],frac[0]},{1.0 - frac[1],frac[1]} ,{1.0 - frac[2],frac[2]} };
                        for (int s = 0; s < 8; s++){
                            int d0 = dx[s], d1 = dy[s], d2 = dz[s];
                            int idx = face_grid.Index(node[0] + d0, node[1] + d1, node[2] + d2);
                            (*film_volume_host.face_data[axis])[idx] += w[0][d0] * w[1][d1] * w[2][d2] * particles->V(pi);
                            (*weight.face_data[axis])[idx] += w[0][d0] * w[1][d1] * w[2][d2];
                        }
                    }
                }

                // film_volume_host.Calc_Faces([&](const int axis, const VectorDi& face) ->real{
                //     int idx = weight.grid.Face_Grid(axis).Index(face);
                //     if (( * weight.face_data[axis])[idx] > 0)
                //         return (* film_volume_host.face_data[axis])[idx] / (*weight.face_data[axis])[idx];
                //     else
                //         return 0;
                // });
            }
        }

        void Update_Poisson_System(Field<bool, d>& fixed, FaceField<real, d>& vol, Field<real, d>& div) {
            const Grid<d>& grid=mpls.levelset.phi.grid;
            //Step 1: decide cell types
            cell_type.Calc_Nodes(
                [&](const VectorDi cell) {
                    
                    if (cell_type(cell) == SOLID) {
                        return SOLID;
                    }
                    // else if (mpls.levelset.phi(cell) >= 0) return EMPTY; 
                    // else return FLUID;
                    int rid = mpls.indicator(cell);
                    typename Region<d>::RegionType type = mpls.region_map.find(rid)->second->type;

                    if (type==Region<d>::RegionType::WATER)
                        return FLUID;
                    else if (type==Region<d>::RegionType::AIR)
                        return FLUID;
                    else if (type==Region<d>::RegionType::OIL)
                        return FLUID;
                    else
                        Warn("incorrect type! at ({}) {}",cell.transpose(),(int)type);
                }
            );

            //Step 2: decide fixed from cell types
            fixed.Init(cell_type.grid);
            fixed.Calc_Nodes(
                [&](const VectorDi cell) {
                    if (cell_type(cell) == FLUID) return false;
                    else return true;
                }
            );

            //Step 3: set div to 0 for air and solid cells
            div.Init(cell_type.grid);
            div.Exec_Nodes(
                [&](const VectorDi cell) {
                    if (cell_type(cell) != FLUID) div(cell) = 0;
                }
            );

            //Step 4: set vol, and modify div additionally on the interface
            vol.Init(cell_type.grid);
            vol.Calc_Faces(
                [&](const int axis, const VectorDi face)->real {
                    auto [cell0, cell1, type0, type1] = Face_Neighbor_Cells_And_Values(cell_type, axis, face, INVALID);
                    
                    //order: invalid, fluid,air,solid
                    if (type0 > type1) {
                        std::swap(cell0, cell1);
                        std::swap(type0, type1);
                    }
                    
                    if (type0 == INVALID && type1 == INVALID) {
                        Warn("INVALID:INVALID ({}),({})",cell0.transpose(), cell1.transpose());
                        return 0;
                    }
                    // if (type0 == INVALID && type1 == FLUID) return 1.0 / liquid_density;
                    if (type0 == INVALID && type1 == EMPTY) {
                        Warn("should be no EMPTY?");
                        // return 1.0 / air_density;
                        return 0;
                    }
                    if (type0 == INVALID && type1 == SOLID) return 0;
                    // if (type0 == FLUID && type1 == FLUID) return 1.0 / liquid_density;
                    if (type0 == FLUID && type1 == EMPTY) {//interface!
                        Warn("should be no EMPTY?");
                        // return 1.0 / liquid_density;
                        return 0;
                    }
                    if ((type0 == INVALID && type1 == FLUID) || (type0 == FLUID && type1 == FLUID)){
                        int rid0=(type0 == INVALID)?-1:mpls.indicator(cell0);
                        int rid1=mpls.indicator(cell1);
                        if(rid0!=-1 && rid1!=-1 && rid0!=rid1){
                            real phi0 = mpls.levelset.phi(cell0), phi1 = mpls.levelset.phi(cell1);
                            real theta = mpls.Theta(phi0, phi1);
                            real den0 = mpls.region_map.find(rid0)->second->rho;
                            real den1 = mpls.region_map.find(rid1)->second->rho;
                            // real den0 = liquid_density, den1 = air_density;
                            real density = theta * den0 + (1.0 - theta) * den1;
                            if(!std::isnormal(density) || density==0) 
                                Warn("weird density ({}),({})",cell0.transpose(), cell1.transpose());
                            // Warn("XXX:FLUID ({}),({}), den={},{}, theta={}, phi={},{}",cell0.transpose(), cell1.transpose(),den0, den1, theta, phi0, phi1);
                            
                            if(use_film_density)
                            if(mpls.region_map.find(rid0)->second->type==Region<d>::RegionType::AIR && mpls.region_map.find(rid1)->second->type==Region<d>::RegionType::AIR){
                                real liquid_density=DefaultDynamicsParameter::liquid_density;
                                //// TODO: to update
                                real particle_volume=std::pow(0.1/128*0.1,d)*film_volume_host(axis,face);

                                real film_density=((std::pow(grid.dx,d)-particle_volume)*density + particle_volume*liquid_density) / std::pow(grid.dx,d);
                                return 1.0 / film_density;
                            }

                            // if(mpls.region_map.find(rid0)->second->type==Region<d>::RegionType::AIR && mpls.region_map.find(rid1)->second->type==Region<d>::RegionType::AIR){
                            //     real liquid_density=DefaultDynamicsParameter::liquid_density;
                            //     real thickness=0.1/128*0.1;
                            //     real film_density=((grid.dx-thickness)*density+thickness*liquid_density)/grid.dx;
                            //     return 1.0 / film_density;
                            // }
                            return 1.0 / density;
                        }else{
                            int rid=rid0<0? rid1:rid0;
                            real density = mpls.region_map.find(rid)->second->rho;
                            return 1.0 / density;
                        }
                        
                    }
                    if (type0 == FLUID && type1 == SOLID) return 0;
                    //type0,type1 in {EMPTY,SOLID}, the result is 0
                    return 0;
                }
            );
        }

        //// pressure jump on mls particle
        inline real Pressure_Jump(const std::shared_ptr<EulerianParticles<d>>& particles, std::shared_ptr<PointLocalGeometry<d>> e_local_geom, const VectorD& pos, real sigma, real dt){

            int ci=e_local_geom->Closest_Point(pos);
            if(ci<0){
                std::cout<<"pressur jump at "<<pos.transpose()<<std::endl;
                return 1e8;
            }
            VectorD intf_x=particles->x(ci);
            VectorD normal=particles->n(ci);
            MatrixD local_frame;
            LeastSquares::MLSPointSet<d-1> mls;
            
            {
                local_frame=e_local_geom->Spawn_Local_Frame(normal);
                // LeastSquares::MLSPointSet<d-1> mls;
                Array<int> nbs;
                int n=e_local_geom->Fit_Local_Geometry_MLS(intf_x,local_frame, nbs, mls);

                //// find closest point on MLS surface
                VectorD u=pos-intf_x;
                VectorD local_pos=e_local_geom->Local_Coords(u,local_frame);
                VectorD new_local_pos=e_local_geom->Closest_Point_To_Local_MLS(local_pos, mls, 5);   

                //// get normal vector
                VectorD local_normal=e_local_geom->Normal_On_Local_MLS(new_local_pos.head(d-1), mls);
                VectorD new_normal=e_local_geom->Unproject_To_World(local_normal,local_frame).normalized();
                normal=(new_normal.dot(normal)>0)?new_normal:-new_normal;

                //// limit local pos on tangent plane
                Array<VectorT> local_nbs_t_pos;
                for(int i=0;i<nbs.size();i++){
                    VectorD u=particles->x(nbs[i])-intf_x;
                    local_nbs_t_pos.push_back(e_local_geom->Local_Coords(u,local_frame).head(d-1));
                }
                VectorT limit_local_pos=e_local_geom->Limit_Local_Tangent(new_local_pos.head(d-1), local_nbs_t_pos);
                intf_x=e_local_geom->Unproject_To_World(new_local_pos, local_frame)+intf_x;
            }

            std::function<real(const int)> Point_Position[d];
            VectorD curvature;
            for(int di=0;di<d;di++){
                Point_Position[di] = [&](const int idx)->real
                    {return particles->x(idx)[di]; };
                curvature[di]=e_local_geom->Laplacian_MLS(intf_x, local_frame, mls, Point_Position[di]);
                // if(curvature[di]==0) Warn("Laplacian_MLS might be incorrect at {}, axis=={}", intf_x.transpose(), di);
            }

            // VectorD ls_normal=levelset.Normal(pos);
            if(curvature.dot(normal)>0) return -curvature.norm()*sigma;
            else return curvature.norm()*sigma;
        }

        void Update_Jump_Condition(const MultiParticleLevelSet<d>& multiparticlelevelset, const FaceField<real, d>& vol, real dt){
            real one_over_dx = (real)1 / vol.grid.dx;
            const Grid<d>& grid=vol.grid;

            pressure_jump.Fill(0);
            FaceField<int,d> jump_cnt(grid, 0);

            for (auto itr = multiparticlelevelset.region_map.begin(); itr != multiparticlelevelset.region_map.end(); ++itr) {

                std::shared_ptr<EulerianParticles<d>> particles = itr->second->particles;
                e_local_geom->nbs_searcher->Update_Points(particles->xRef());
                e_local_geom->normal_ptr=std::make_shared<Array<VectorD> >(particles->nRef());//// update local geometry
                
                #pragma omp parallel for 
                for(int i=0;i<itr->second->inner_cells.size();i++){
                    VectorDi cell=itr->second->inner_cells[i];
                    if (cell_type(cell) != FLUID) { /*Warn("cell {} is not FLUID but {}", cell.transpose(), cell_type(cell)); */ continue;}
                    if(abs(multiparticlelevelset.levelset.phi(cell))>grid.dx*1.5f) continue;
                    for(int nbi=0;nbi<grid.Neighbor_Node_Number();nbi++){
                        const VectorDi nb_cell=grid.Neighbor_Node(cell,nbi);
                        // if(levelset.Is_Interface(cell, nb_cell)){
                        if(!grid.Valid(nb_cell) || cell_type(nb_cell)!=FLUID) continue;
                        if(grid.Valid(nb_cell) && multiparticlelevelset.Is_Interface(cell, nb_cell)){
                            // real theta=levelset.Theta(levelset.phi(cell), levelset.phi(nb_cell));
                            real theta = multiparticlelevelset.Theta(multiparticlelevelset.levelset.phi(cell), multiparticlelevelset.levelset.phi(nb_cell));
                            auto [axis,face]=GridUtil<d>::Neighbor_Face(grid,cell,nbi);
                            
                            VectorD intf_pos = ((real)1 - theta) * grid.Position(cell) + theta * grid.Position(nb_cell);
                            

                            //// inner, outer?
                            real p_jump=Pressure_Jump(particles, e_local_geom, intf_pos, itr->second->sigma, dt);
                            if(p_jump>=1e8){
                                std::cout<<"incorrect pjump pressur jump at "<<intf_pos.transpose()<<std::endl;
                                std::cout<<"region="<<itr->first<<", cell=("<<cell.transpose()<<"),("<<nb_cell.transpose()<<"),cell pose="<<grid.Position(cell).transpose()<<","<<grid.Position(nb_cell).transpose()<<std::endl;
                                std::cout<<"indicator="<<multiparticlelevelset.indicator(cell)<<","<<multiparticlelevelset.indicator(nb_cell)<<std::endl;
                                std::cout<<"phi="<<multiparticlelevelset.levelset.phi(cell)<<","<<multiparticlelevelset.levelset.phi(nb_cell)<<",theta="<<theta<<std::endl;
                            }
                            
                            // real coef=1.0;
                            // real intf_coef=vol(axis,face);
                            jump_cnt(axis, face)++;
                            
                            //// p_{i+1}+pressure_jump-p_{i}=...
                            pressure_jump(axis, face)+=p_jump*Neighbor_Cell_Sign<d>(cell, nb_cell);
                        }
                    }
                }

                // Info("update jump condition for region {}, sigma={}",itr->first,itr->second->sigma);
            }

            pressure_jump.Exec_Faces([&](const int axis, const VectorDi face) {
                auto [cell0, cell1] = Face_Neighbor_Cells<d>(grid, axis, face);
                if(!pressure_jump.grid.Valid(cell0) || !pressure_jump.grid.Valid(cell1)) return;
                if(jump_cnt(axis,face)<=0) return;
                if(jump_cnt(axis,face)!=2){
                    std::cout<<"cell region "<<multiparticlelevelset.indicator(cell0)<<", "<<multiparticlelevelset.indicator(cell1)<<std::endl;
                    std::cout<<"cell type "<<cell_type(cell0)<<","<<cell_type(cell1)<<std::endl;
                    Warn("incorrect jump cnt! {}({}) #{}", axis, face.transpose(), jump_cnt(axis,face));
                }
                real rid0=multiparticlelevelset.indicator(cell0);
                real rid1=multiparticlelevelset.indicator(cell1);
                typename Region<d>::RegionType type0=multiparticlelevelset.region_map.find(rid0)->second->type;
                typename Region<d>::RegionType type1=multiparticlelevelset.region_map.find(rid1)->second->type;
                if(type0==Region<d>::RegionType::WATER && type1==Region<d>::RegionType::WATER)
                    pressure_jump(axis, face)*=0.5;
                else if(type0==Region<d>::RegionType::AIR && type1==Region<d>::RegionType::WATER)
                    pressure_jump(axis, face)*=0.5;
                else if(type0==Region<d>::RegionType::WATER && type1==Region<d>::RegionType::AIR)
                    pressure_jump(axis, face)*=0.5;
                else if(type0==Region<d>::RegionType::AIR && type1==Region<d>::RegionType::AIR)
                    pressure_jump(axis, face)*=1.0;
                else Warn("incorrect type! {}({}), {}, {}", axis, face.transpose(), type0, type1);
            });

        }
        

        void Apply_Jump_Condition_To_b(Field<real, d>& div, const Field<CellType, d>& cell_type, const MultiParticleLevelSet<d>& multiparticlelevelset, const FaceField<real, d>& vol, real dt) {
            real one_over_dx = (real)1 / div.grid.dx;
            Grid<d>& grid=div.grid;

            div.Exec_Nodes([&](const VectorDi cell) {
                if (cell_type(cell) != FLUID) return;
                for(int nbi=0;nbi<grid.Neighbor_Node_Number();nbi++){
                    const VectorDi nb_cell=grid.Neighbor_Node(cell,nbi);
                    // if(levelset.Is_Interface(cell, nb_cell)){
                    if(grid.Valid(nb_cell))
                    if(multiparticlelevelset.Is_Interface(cell, nb_cell)){
                        // real theta=levelset.Theta(levelset.phi(cell), levelset.phi(nb_cell));
                        real theta = multiparticlelevelset.Theta(multiparticlelevelset.levelset.phi(cell), multiparticlelevelset.levelset.phi(nb_cell));
                        auto [axis,face]=GridUtil<d>::Neighbor_Face(grid,cell,nbi);
                        
                        VectorD intf_pos = ((real)1 - theta) * grid.Position(cell) + theta * grid.Position(nb_cell);
                        
                        real rid=multiparticlelevelset.indicator(cell), nb_rid=multiparticlelevelset.indicator(nb_cell);
                        std::shared_ptr<EulerianParticles<d>> particles=multiparticlelevelset.region_map.find(rid)->second->particles;

                        real p_jump=pressure_jump(axis, face);
                        real coef=1.0;
                        real intf_coef=vol(axis,face);
                        div(cell) -= coef*intf_coef*p_jump*Neighbor_Cell_Sign<d>(cell, nb_cell)*dt/grid.dx;
                    }
                }
            });
        }

        void Apply_Jump_Condition_To_Velocity(FaceField<real,d>& velocity, const Field<CellType, d>& cell_type, const MultiParticleLevelSet<d>& multiparticlelevelset, const FaceField<real,d>&vol, real dt){
            
            Grid<d>& grid=velocity.grid;
            velocity.Exec_Faces([&](const int axis, const VectorDi face) {
                auto [cell0, cell1] = Face_Neighbor_Cells<d>(grid, axis, face);
                if(!grid.Valid(cell0) || !grid.Valid(cell1)) return;
                if(!multiparticlelevelset.Is_Interface(cell0, cell1)) return;
                
                real p_jump=pressure_jump(axis, face);
                velocity(axis, face)-=p_jump*vol(axis,face)*dt/grid.dx;
            });
        }

        
        void Apply_Volume_Correction_To_b(Field<real, d>& div, const Field<CellType, d>& cell_type, const MultiParticleLevelSet<d>& multiparticlelevelset, real dt) {
            real one_over_dx = (real)1 / div.grid.dx;
            Grid<d>& grid=div.grid;
            real face_area=pow(grid.dx, d-1);
            real cell_vol=pow(grid.dx, d);

            div.Exec_Nodes([&](const VectorDi cell) {
                if (cell_type(cell) != FLUID) return;
                auto itr=multiparticlelevelset.region_map.find(multiparticlelevelset.indicator(cell));
                if(itr==multiparticlelevelset.region_map.end()) Error("find no region at cell ({}), region id={}", cell.transpose(), itr->first);
                if(multiparticlelevelset.levelset.Cell_Fraction(cell)!=1) return;
                if(!itr->second->use_volume_control) return;

                real delta_volume=itr->second->delta_volume_per_cell*cell_vol;
                // if(itr->second->type==Region<d>::RegionType::WATER)
                //     // div(cell)-=0.2*delta_volume/face_area/dt;
                //     ;
                // else
                div(cell)-=0.5*delta_volume/face_area/dt;
            });
        }



    //// particle geometry
    public:
        void Update_Particle_Area(EulerianParticles<d>& particles){}

    //// regenerate regions
    public:
        void Generate_New_Bubble(){
            Grid<d> grid=mpls.levelset.phi.grid;
            SphereShape<d> sphere=Generate_Sphere();
            LevelSet<d> new_levelset;

            std::atomic<int> is_valid=false;
            int cnt=0;
            while(!is_valid){
                sphere=Generate_Sphere();
                new_levelset.Init(grid,sphere);

                is_valid=true;    
                grid.Exec_Nodes([&](const VectorDi cell) {
                    if(new_levelset.phi(cell)<0 && mpls.levelset.phi(cell)>-2*grid.dx){
                        Info("invalid at ({}), {}>{}", cell.transpose(), mpls.levelset.phi(cell), -2*grid.dx);
                        is_valid=false;
                    }
                });

                Info("check sphere at ({}) with radius {}", sphere.center, sphere.radius);
                cnt++;
                if(cnt>10) return;
            }

            //// check bg_region if in the liquid
            VectorDi bg_cell=GridUtil<d>::Cell_Coord(grid,sphere.center);
            int bg_rid=mpls.indicator(bg_cell);
            if(bg_rid<0) Warn("inccorect id {}", bg_rid);
            std::shared_ptr<Region<d>> bg_region=mpls.region_map.find(bg_rid)->second;
            if(bg_region->type!=Region<d>::RegionType::WATER){
                Info("discard the bubble not in the water");
                return;
            }

            Info("add sphere at ({}) with radius {}", sphere.center, sphere.radius);

            real new_volume=0;
            if(d==2) new_volume=CommonConstants::pi*pow(sphere.radius/grid.dx,2);
            if(d==3) new_volume=CommonConstants::pi*4.0/3.0*pow(sphere.radius/grid.dx,3);

            ////find fluid region
            bg_region->init_volume=bg_region->init_volume-new_volume;
            
            //// add particles to bg_region
            std::shared_ptr<EulerianParticles<d>> new_bg_particles=std::make_shared<EulerianParticles<d>>();
            ParticleLevelSetHelper<d>::Regenerate_Particles_On_Levelset(*new_bg_particles, new_levelset);
        #pragma omp parallel for
            for(int i=0;i<new_bg_particles->Size();i++){
                new_bg_particles->n(i)=-new_bg_particles->n(i);
            }

            std::cout<<"bg region particles size="<<bg_region->particles->Size()<<std::endl;
            bg_region->particles->Append(*new_bg_particles);
            std::cout<<"bg region particles size="<<bg_region->particles->Size()<<std::endl;

            //// add region
            int new_rid=mpls.Add_Region(new_levelset, Region<d>::RegionType::AIR);
            std::shared_ptr<Region<d>> new_region=mpls.region_map.find(new_rid)->second;
            new_region->rho*=50;
            new_region->sigma=sigma;
            new_region->init_volume=new_volume;
        }

        SphereShape<d> Generate_Sphere(){
            Grid<d> grid=mpls.levelset.phi.grid;

            real min_radius=grid.dx*8;
            real max_radius=grid.dx*16;
            
            // real min_radius=grid.dx*10;
            // real max_radius=grid.dx*20;
            real radius=Random::Uniform(min_radius,max_radius);


            VectorD min_corner=grid.Node_Min(); //// min node
            VectorD max_corner=grid.Node_Max()-(grid.Node_Max()-grid.Node_Min())(1)*0.5*VectorD::Unit(1); //// max node with 0.5 height

            VectorD min_pos=min_corner+VectorD::Ones()*(2*grid.dx+radius);
            VectorD max_pos=max_corner-VectorD::Ones()*(2*grid.dx+radius);
            
            VectorD center=Random::Uniform_In_Box(min_pos, max_pos);
            return SphereShape<d>(center,radius);
        }

        void Generate_Sphere_Bubble(SphereShape<d>& sphere){
            Info("add sphere at ({}) with radius {} ...", sphere.center, sphere.radius);
            Grid<d> grid=mpls.levelset.phi.grid;
            LevelSet<d> new_levelset;

            {
                std::atomic<int> is_valid=false;
                new_levelset.Init(grid,sphere);
                is_valid=true;
                grid.Exec_Nodes([&](const VectorDi cell) {
                    if(new_levelset.phi(cell)<0 && mpls.levelset.phi(cell)>-2*grid.dx){
                        Info("invalid at ({}), {}>{}", cell.transpose(), mpls.levelset.phi(cell), -2*grid.dx);
                        is_valid=false;
                    }
                });
                if(!is_valid){
                    Info("skip");
                    return;
                }
            }

            //// check bg_region if in the liquid
            VectorDi bg_cell=GridUtil<d>::Cell_Coord(grid,sphere.center);
            int bg_rid=mpls.indicator(bg_cell);
            if(bg_rid<0) Warn("incorrect id {}", bg_rid);
            std::shared_ptr<Region<d>> bg_region=mpls.region_map.find(bg_rid)->second;
            if(bg_region->type!=Region<d>::RegionType::WATER){
                Info("discard the bubble not in the water");
                return;
            }

            Info("add sphere at ({}) with radius {}", sphere.center, sphere.radius);

            real new_volume=0;
            if(d==2) new_volume=CommonConstants::pi*pow(sphere.radius/grid.dx,2);
            if(d==3) new_volume=CommonConstants::pi*4.0/3.0*pow(sphere.radius/grid.dx,3);

            ////find fluid region
            bg_region->init_volume=bg_region->init_volume-new_volume;
            
            //// add particles to bg_region
            std::shared_ptr<EulerianParticles<d>> new_bg_particles=std::make_shared<EulerianParticles<d>>();
            ParticleLevelSetHelper<d>::Regenerate_Particles_On_Levelset(*new_bg_particles, new_levelset);
        #pragma omp parallel for
            for(int i=0;i<new_bg_particles->Size();i++){
                new_bg_particles->n(i)=-new_bg_particles->n(i);
            }

            std::cout<<"bg region particles size="<<bg_region->particles->Size()<<std::endl;
            bg_region->particles->Append(*new_bg_particles);
            std::cout<<"bg region particles size="<<bg_region->particles->Size()<<std::endl;

            //// add region
            int new_rid=mpls.Add_Region(new_levelset, Region<d>::RegionType::AIR);
            std::shared_ptr<Region<d>> new_region=mpls.region_map.find(new_rid)->second;
            new_region->rho*=10;
            new_region->sigma=sigma;
            new_region->init_volume=new_volume;
        }
    };
}