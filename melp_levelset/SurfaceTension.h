//////////////////////////////////////////////////////////////////////////
// Surface Tension
// Copyright (c) (2022-), Hui Wang
// This file is part of MESO, whose distribution is governed by the LICENSE file.
//////////////////////////////////////////////////////////////////////////
#pragma once

#include "LevelSet.h"
#include "Constants.h"

namespace Meso {

    template<class T>
    T Levelset_Dirac(const T phi, const T dirac_band_width)
	{
		if (phi < -dirac_band_width) return 0;
		else if (phi > dirac_band_width) return 0;
		else return 0.5 * (1.0 + cos(CommonConstants::pi * phi / dirac_band_width)) / dirac_band_width;
	}

    class SurfaceTension {
    public:
        template<class T, int d>
        static void Explicit_Surface_Tension(const T dt, FaceField<T,d>& velocity, LevelSet<d>& levelset, const T sigma, const T narrow_band_width, const T dirac_band_width){
            Typedef_VectorD(d);

            Grid<d>& grid=velocity.grid;
            grid.Exec_Faces(
                [&](const int axis, const VectorDi face) {
                    VectorD pos=grid.Face_Center(axis,face);
                    auto kappa = LevelSetUtil<d>::Curvature(levelset,pos);
                    VectorD normal = LevelSetUtil<d>::Normal(levelset,pos);
                    velocity(axis, face) = velocity(axis, face) - sigma * Levelset_Dirac(levelset.Phi(pos), dirac_band_width) * dt * kappa * normal[axis];
                }
            );

        }

        template<class T, int d>
        static void Explicit_Mean_Curvature(const T dt, FaceField<T,d>& velocity, LevelSet<d>& levelset, const T sigma, const T narrow_band_width, const T dirac_band_width){
            Typedef_VectorD(d);
            
            //// compute mean curvature
            Grid<d>& grid=velocity.grid;
            real sum_dirac=0;
            real sum_curv=0;
            grid.Iterate_Faces(
                [&](const int axis, const VectorDi face) {
                    VectorD pos=grid.Face_Center(axis,face);
                    auto kappa = LevelSetUtil<d>::Curvature(levelset,pos);
                    auto dirac = Levelset_Dirac(levelset.Phi(pos), dirac_band_width);
                    if(dirac>0) {
                        sum_dirac+=dirac;
                        sum_curv+=dirac*kappa;
                    }
                }
            );
            real mean_curv=(sum_dirac!=0)?sum_curv/sum_dirac:0;

            //// apply surface tension with the mean curvature
            grid.Exec_Faces(
                [&](const int axis, const VectorDi face) {
                    VectorD pos=grid.Face_Center(axis,face);
                    auto kappa = LevelSetUtil<d>::Curvature(levelset,pos);
                    VectorD normal = LevelSetUtil<d>::Normal(levelset,pos);
                    velocity(axis, face) = velocity(axis, face) - sigma * (kappa-mean_curv) * Levelset_Dirac(levelset.Phi(pos), dirac_band_width) * dt * normal[axis];
                }
            );

        }
    };
}