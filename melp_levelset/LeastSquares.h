//////////////////////////////////////////////////////////////////////////
// Least squares
// Copyright (c) (2018-), Bo Zhu, Xiangxin Kong, Mengdi Wang
// This file is part of SimpleX, whose distribution is governed by the LICENSE file.
//////////////////////////////////////////////////////////////////////////
#pragma once
#include "Common.h"
#include "AuxFunc.h"
#include <iostream>

namespace Meso{
namespace LeastSquares
{
	template<int d,int p> class LS {};
	template<int d,int p> class WLS{};
	template<int d,int p> class MLS{};
	template<int d> class MLSPointSet{};
	template<int d> class ASMLSPointSet{};

	//////////////////////////////////////////////////////////////////////////
	////LS

	////f(x)=c0+c1*x
	template<> class LS<1,1>
	{public:
		VectorX c;
	
		////[x0,f0,x1,f1,x2,f2,...]
		void Fit(const real* data,const int m)
		{
			MatrixX B(m,2);
			VectorX f(m);
			for(int i=0;i<m;i++){
				B.coeffRef(i,0)=(real)1;
				B.coeffRef(i,1)=data[i*2];
				f[i]=data[i*2+1];}
			c=B.colPivHouseholderQr().solve(f);
		}

		inline real operator() (const real& x) const {return c[0]+c[1]*x;}
		inline real operator() (const Vector1& x) const {return c[0]+c[1]*x[0];}
		inline Vector1 Grad(const Vector1& x) const {return Vector1(c[1]);}
	};

	////f(x)=c0+c1*x+c2*x^2
	template<> class LS<1,2>
	{public:
		VectorX c;
	
		////[x0,f0, x1,f1, x2,f2, ...]
		void Fit(const real* data,const int m)
		{
			MatrixX B(m,3);
			VectorX f(m);
			for(int i=0;i<m;i++){
				B.coeffRef(i,0)=(real)1;
				B.coeffRef(i,1)=data[i*2];
				B.coeffRef(i,2)=pow(data[i*2],2);
				f[i]=data[i*2+1];}
			c=B.colPivHouseholderQr().solve(f);
		}

		inline real operator() (const real& x) const {return c[0]+c[1]*x+c[2]*pow(x,2);}
		inline real operator() (const Vector1& x) const {return c[0]+c[1]*x[0]+c[2]*pow(x[0],2);}
		inline Vector1 Grad(const Vector1& x) const {return Vector1(c[1]+(real)2*c[2]*x[0]);}
	};

	////f(x)=c0+c1*x+c2*x^2+c3*x^3
	template<> class LS<1,3>
	{public:
		VectorX c;
	
		////[x0,f0, x1,f1, x2,f2, ...]
		void Fit(const real* data,const int m)
		{
			MatrixX B(m,4);
			VectorX f(m);
			for(int i=0;i<m;i++){
				B.coeffRef(i,0)=(real)1;
				B.coeffRef(i,1)=data[i*2];
				B.coeffRef(i,2)=pow(data[i*2],2);
				B.coeffRef(i,3)=pow(data[i*2],3);
				f[i]=data[i*2+1];}
			c=B.colPivHouseholderQr().solve(f);
		}

		inline real operator() (const real& x) const {return c[0]+c[1]*x+c[2]*pow(x,2)+c[3]*pow(x,3);}
		inline real operator() (const Vector1& x) const {return c[0]+c[1]*x[0]+c[2]*pow(x[0],2)+c[3]*pow(x[0],3);}
		inline Vector1 Grad(const Vector1& x) const {return Vector1(c[1]+(real)2*c[2]*x[0]+(real)3*c[3]*pow(x[0],2));}
	};


	////f(x,y)=c0+c1*x+c2*y
	template<> class LS<2,1>
	{public:
		VectorX c;
	
		////[x0,y0,f0, x1,y1,f1, x2,y2,f2, ...]
		void Fit(const real* data,const int m)
		{
			MatrixX B(m,3);
			VectorX f(m);
			for(int i=0;i<m;i++){
				B.coeffRef(i,0)=(real)1;
				B.coeffRef(i,1)=data[i*3];
				B.coeffRef(i,2)=data[i*3+1];
				f[i]=data[i*3+2];}
			c=B.colPivHouseholderQr().solve(f);
		}

		inline real operator() (const real& x,const real& y) const {return c[0]+c[1]*x+c[2]*y;}
		inline real operator() (const Vector2& x) const {return c[0]+c[1]*x[0]+c[2]*x[1];}
		inline Vector2 Grad(const Vector2& x) const {return Vector2(c[1],c[2]);}
	};

	////f(x,y)=c0+c1*x+c2*y+c3*x^2+c4*y^2+c5*xy
	template<> class LS<2, 2>
	{
	public:
		Vector<double, 6> c;

		////[x0,y0,f0, x1,y1,f1, x2,y2,f2, ...]
		void Fit(const real* data, const int m)
		{
			if (m == 0) { Error("LS::Fit passed m=0"); }
			MatrixX B(m, 6);
			VectorX f(m);
			for (int i = 0; i < m; i++) {
				B.coeffRef(i, 0) = (real)1;
				B.coeffRef(i, 1) = data[i * 3];
				B.coeffRef(i, 2) = data[i * 3 + 1];
				B.coeffRef(i, 3) = pow(data[i * 3], 2);
				B.coeffRef(i, 4) = pow(data[i * 3 + 1], 2);
				B.coeffRef(i, 5) = data[i * 3] * data[i * 3 + 1];
				f[i] = data[i * 3 + 2];
			}
			c = B.colPivHouseholderQr().solve(f);
		}

		inline real operator() (const real& x, const real& y) const
		{
			return c[0] + c[1] * x + c[2] * y + c[3] * pow(x, 2) + c[4] * pow(y, 2) + c[5] * x * y;
		}

		inline real operator() (const Vector2& x) const
		{
			return c[0] + c[1] * x[0] + c[2] * x[1] + c[3] * pow(x[0], 2) + c[4] * pow(x[1], 2) + c[5] * x[0] * x[1];
		}

		inline Vector2 Grad(const Vector2& x) const
		{
			return Vector2(c[1] + (real)2 * c[3] * x[0] + c[5] * x[1], c[2] + (real)2 * c[4] * x[1] + c[5] * x[0]);
		}
	};

	//////////////////////////////////////////////////////////////////////////
	////WLS

	////f(x,y)=c0+c1*x+c2*y
	template<> class WLS<2,1>
	{public:
		VectorX c;
	
		////[x0,y0,f0,t0, x1,y1,f1,t1, x2,y2,f2,t2, ...]
		void Fit(const real* data,const int m)
		{
			MatrixX B(m,3);
			VectorX f(m);
			for(int i=0;i<m;i++){
				real w=sqrt(data[i*4+3]);
				B.coeffRef(i,0)=w*(real)1;
				B.coeffRef(i,1)=w*data[i*4];
				B.coeffRef(i,2)=w*data[i*4+1];
				f[i]=w*data[i*4+2];}
			c=B.colPivHouseholderQr().solve(f);
		}

		inline real operator() (const real& x,const real& y) const {return c[0]+c[1]*x+c[2]*y;}
		inline real operator() (const Vector2& x) const {return c[0]+c[1]*x[0]+c[2]*x[1];}
		inline Vector2 Grad(const Vector2& x) const {return Vector2(c[1],c[2]);}
	};

	////f(x,y)=c0+c1*x+c2*y+c3*x^2+c4*y^2+c5*xy
	template<> class WLS<2,2>
	{public:
		VectorX c;
	
		////[x0,y0,f0,t0, x1,y1,f1,t1, x2,y2,f2,t2, ...]
		void Fit(const real* data,const int m)
		{
			MatrixX B(m,6);
			VectorX f(m);
			for(int i=0;i<m;i++){
				real w=sqrt(data[i*4+3]);
				B.coeffRef(i,0)=w*(real)1;
				B.coeffRef(i,1)=w*data[i*4];
				B.coeffRef(i,2)=w*data[i*4+1];
				B.coeffRef(i,3)=w*data[i*4]*data[i*4];
				B.coeffRef(i,4)=w*data[i*4+1]*data[i*4+1];
				B.coeffRef(i,5)=w*data[i*4]*data[i*4+1];
				f[i]=w*data[i*4+2];}
			c=B.colPivHouseholderQr().solve(f);
		}

		inline real operator() (const real& x,const real& y) const {return c[0]+c[1]*x+c[2]*y+c[3]*x*x+c[4]*y*y+c[5]*x*y;}
		inline real operator() (const Vector2& x) const {return c[0]+c[1]*x[0]+c[2]*x[1]+c[3]*x[0]*x[0]+c[4]*x[1]*x[1]+c[5]*x[0]*x[1];}
		inline Vector2 Grad(const Vector2& x) const {return Vector2(c[1],c[2]);}
	};

	////f(x,y,z)=c0+c1*x+c2*y+c3*z+c4*x^2+c5*y^2+c6*z^2+c7*x*y+c8*y*z+c9*z*x
	template<> class WLS<3,2>
	{public:
		VectorX c;
	
		////[x0,y0,z0,f0,t0, x1,y1,z1,f1,t1, x2,y2,z2,f2,t2, ...]
		void Fit(const real* data,const int m)
		{
			MatrixX B(m,10);
			VectorX f(m);
			for(int i=0;i<m;i++){
				real w=sqrt(data[i*5+4]);
				B.coeffRef(i,0)=w*(real)1;
				B.coeffRef(i,1)=w*data[i*5+0];////x
				B.coeffRef(i,2)=w*data[i*5+1];////y
				B.coeffRef(i,3)=w*data[i*5+2];////z
				B.coeffRef(i,4)=w*data[i*5+0]*data[i*5+0];////x2
				B.coeffRef(i,5)=w*data[i*5+1]*data[i*5+1];////y2
				B.coeffRef(i,6)=w*data[i*5+2]*data[i*5+2];////z2
				B.coeffRef(i,7)=w*data[i*5+0]*data[i*5+1];////xy
				B.coeffRef(i,8)=w*data[i*5+1]*data[i*5+2];////yz
				B.coeffRef(i,9)=w*data[i*5+2]*data[i*5+0];////zx
				f[i]=w*data[i*5+3];}
			c=B.colPivHouseholderQr().solve(f);
		}

		inline real operator() (const real& x,const real& y,const real& z) const {return c[0]+c[1]*x+c[2]*y+c[3]*z+c[4]*x*x+c[5]*y*y+c[6]*z*z+c[7]*x*y+c[8]*y*z+c[9]*z*x;}
		inline real operator() (const Vector3& x) const {return c[0]+c[1]*x[0]+c[2]*x[1]+c[3]*x[2]+c[4]*x[0]*x[0]+c[5]*x[1]*x[1]+c[6]*x[2]*x[2]+c[7]*x[0]*x[1]+c[8]*x[1]*x[2]+c[9]*x[2]*x[0];}
		inline Vector3 Grad(const Vector3& x) const {return Vector3(c[1],c[2],c[3]);}
	};

	//////////////////////////////////////////////////////////////////////////
	////MLS
	
	////f(x)=c0+c1*x+c2*x^2
	template<> class MLS<1, 2>
	{
	public:
		VectorX c;
		void Fit(const real* data, const int m, const real& x, const real& y) {
			MatrixX B(m, 3);
			VectorX f(m);
			for (int i = 0; i < m; i++) {
				real w = sqrt(Weight(data[i * 2], x));
				B.coeffRef(i, 0) = w * (real)1;
				B.coeffRef(i, 1) = w * data[i * 2];
				B.coeffRef(i, 2) = w * pow(data[i * 2], 2);
				f[i] = w * data[i * 2 + 1];
			}
			c = B.colPivHouseholderQr().solve(f);
		}
		inline real operator() (const Vector1& x) const {
			return c[0] + c[1] * x[0] + c[2] * pow(x[0], 2);
		}
	protected:
		inline real Weight(const real& x0, const real& x1) const
		{
			real dis2 = pow(x0 - x1, 2);
			return exp(-dis2);
		}
	};

	////f(x,y)=c0+c1*x+c2*y+c3*x^2+c4*y^2+c5*xy
	template<> class MLS<2, 2>
	{
	public:
		VectorX c;

		////[x0,y0,f0, x1,y1,f1, x2,y2,f2, ...]
		void Fit(const real* data, const int m, const real& x, const real& y)
		{
			MatrixX B(m, 6);
			VectorX f(m);
			for (int i = 0; i < m; i++) {
				real w = sqrt(Weight(data[i * 3], data[i * 3 + 1], x, y));
				B.coeffRef(i, 0) = w * (real)1;
				B.coeffRef(i, 1) = w * data[i * 3];
				B.coeffRef(i, 2) = w * data[i * 3 + 1];
				B.coeffRef(i, 3) = w * MathFunc::Power2(data[i * 3]);
				B.coeffRef(i, 4) = w * MathFunc::Power2(data[i * 3 + 1]);
				B.coeffRef(i, 5) = w * data[i * 3] * data[i * 3 + 1];
				f[i] = w * data[i * 3 + 2];
			}
			if (m <= 6) {
				std::cout << "MLS::Fit error: only " << m << " equations for 6 parameters. Fitting that may cause severe artifact\n";
				std::cout << "(x,y)=" << x << " , " << y << "\n";
				std::cout << "data: "; for (int i = 0; i < m * 3; i++) { std::cout << data[i] << " "; }std::cout << "\n";
				std::cout << "B:\n" << B << "\n";
				std::cout << "f: " << f.transpose() << "\n";
				exit(1);
			}
			c = B.colPivHouseholderQr().solve(f);
		}

		inline real operator() (const real& x, const real& y) const
		{
			return c[0] + c[1] * x + c[2] * y + c[3] * MathFunc::Power2(x) + c[4] * MathFunc::Power2(y) + c[5] * x * y;
		}

		inline real operator() (const Vector2& x) const
		{
			return c[0] + c[1] * x[0] + c[2] * x[1] + c[3] * MathFunc::Power2(x[0]) + c[4] * MathFunc::Power2(x[1]) + c[5] * x[0] * x[1];
		}

		inline Vector2 Grad(const Vector2& x) const
		{
			return Vector2(c[1] + (real)2 * c[3] * x[0] + c[5] * x[1], c[2] + (real)2 * c[4] * x[1] + c[5] * x[0]);
		}

	protected:
		inline real Weight(const real& x0, const real& y0, const real& x1, const real& y1) const
		{
			real dis2 = MathFunc::Power2(x0 - x1) + MathFunc::Power2(y0 - y1);
			return exp(-dis2);
		}
	};




	////f(x)=c0+c1*x+c2*x^2
	template<> class MLSPointSet<1>
	{public:
		VectorX c;
		int point_n=0;
		int p_idx=-1;

		////1D: [x0,f0, x1,f1, x2,f2, ...]
		////p is one particle among the particles with their positions specified in data
		void Fit(const real* data,const int m,const int p)
		{
			point_n=m;
			p_idx=p;

			MatrixX B(m,3);
			VectorX f(m);
			for(int i=0;i<m;i++){
				real w=sqrt(Weight(i));
				B.coeffRef(i,0)=w*(real)1;
				B.coeffRef(i,1)=w*data[i*2];
				B.coeffRef(i,2)=w*pow(data[i*2],2);
				f[i]=w*data[i*2+1];}
			c=B.colPivHouseholderQr().solve(f);
		}

		void Fit(const real* data,const real* ws,const int m)
		{
			point_n=m;

			MatrixX B(m,3);
			VectorX f(m);
			for(int i=0;i<m;i++){
				real w=sqrt(ws[i]);
				B.coeffRef(i,0)=w*(real)1;
				B.coeffRef(i,1)=w*data[i*2];
				B.coeffRef(i,2)=w*pow(data[i*2],2);
				f[i]=w*data[i*2+1];}
			c=B.colPivHouseholderQr().solve(f);
		}

		inline real operator() (const real& x) const 
		{return c[0]+c[1]*x+c[2]*pow(x,2);}	
		inline real operator() (const Vector1& x) const 
		{return c[0]+c[1]*x[0]+c[2]*pow(x[0],2);}	
		inline Vector1 Grad(const Vector1& x) const {return Vector1(c[1]+(real)2*c[2]*x[0]);}
		inline Vector1 Lplc(const Vector1& x) const {return Vector1((real)2*c[2]);}
		inline Vector1 Diff2(const Vector1& x)const { return Vector1(2 * c[2]); }
		inline Matrix1 Metric_Tensor() const {Matrix1 mt; mt<<(1+c[1]*c[1]); return mt;};
		real Curvature(const Vector1& xx) const
		{
			real x=xx(0);
			Vector2 grad_F(c[1]+2*c[2]*x, -1); 
			Matrix2 H; H<<2*c[2],0, /**/ 0,-1;
			return ((grad_F.transpose()*H*grad_F)(0) - grad_F.squaredNorm()*H.trace()) / (2*grad_F.squaredNorm());
		}

	protected:
		inline real Weight(const int p) const
		{
			if(p==p_idx)return (real)1;
			else return (real)1/(real)point_n;
		}
	};

	////f(x,y)=c0+c1*x+c2*y+c3*x^2+c4*y^2+c5*xy
	template<> class MLSPointSet<2>
	{public:
		VectorX c;
		int point_n=0;
		int p_idx=-1;

		////2D: [x0,y0,f0, x1,y1,f1, x2,y2,f2, ...]
		void Fit(const real* data,const int m,const int p)
		{
			point_n=m;
			p_idx=p;

			MatrixX B(m,6);
			VectorX f(m);
			for(int i=0;i<m;i++){
				real w=sqrt(Weight(i));
				B.coeffRef(i,0)=w*(real)1;
				B.coeffRef(i,1)=w*data[i*3];
				B.coeffRef(i,2)=w*data[i*3+1];
				B.coeffRef(i,3)=w*pow(data[i*3],2);
				B.coeffRef(i,4)=w*pow(data[i*3+1],2);
				B.coeffRef(i,5)=w*data[i*3]*data[i*3+1];
				f[i]=w*data[i*3+2];}
			c=B.colPivHouseholderQr().solve(f);
		}

		////2D: [x0,y0,f0, x1,y1,f1, x2,y2,f2, ...]
		//// Fit with weights
		void Fit(const real* data,const real* ws,const int m)
		{
			point_n=m;

			MatrixX B(m,6);
			VectorX f(m);
			for(int i=0;i<m;i++){
				real w=sqrt(ws[i]);
				B.coeffRef(i,0)=w*(real)1;
				B.coeffRef(i,1)=w*data[i*3];
				B.coeffRef(i,2)=w*data[i*3+1];
				B.coeffRef(i,3)=w*pow(data[i*3],2);
				B.coeffRef(i,4)=w*pow(data[i*3+1],2);
				B.coeffRef(i,5)=w*data[i*3]*data[i*3+1];
				f[i]=w*data[i*3+2];}
			c=B.colPivHouseholderQr().solve(f);
		}

		inline real operator() (const real& x,const real& y) const 
		{return c[0]+c[1]*x+c[2]*y+c[3]*pow(x,2)+c[4]*pow(y,2)+c[5]*x*y;}	
		
		inline real operator() (const Vector2& x) const 
		{return c[0]+c[1]*x[0]+c[2]*x[1]+c[3]*pow(x[0],2)+c[4]*pow(x[1],2)+c[5]*x[0]*x[1];}	

		inline Vector2 Grad(const Vector2& x) const
		{return Vector2(c[1]+(real)2*c[3]*x[0]+c[5]*x[1],c[2]+(real)2*c[4]*x[1]+c[5]*x[0]);}

		inline Vector2 Lplc(const Vector2& x) const 
		{return Vector2((real)2*c[3], (real)2*c[4]);}

		inline Matrix2 Metric_Tensor() const 
		{Matrix2 mt; mt<<(1+c[1]*c[1]), (c[1]*c[2]), (c[1]*c[2]), (1+c[2]*c[2]); return mt;};

		real Curvature(const Vector2& xx) const
		{
			real x=xx(0), y=xx(1);
			Vector3 grad_F(c[1]+2*c[3]*x+c[5]*y, c[2]+2*c[4]*y+c[5]*x,-1); 
			Matrix3 H; H<<2*c[3],c[5],0, /**/ c[5],2*c[4],0, /**/ 0,0,-1;
			return ((grad_F.transpose()*H*grad_F)(0) - grad_F.squaredNorm()*H.trace()) / (2*grad_F.squaredNorm());
		}

	protected:
		inline real Weight(const int p) const
		{
			if(p==p_idx)return (real)1;
			else return (real)1/(real)point_n;
		}
	};
	
	///////////////////////////////////////////////////////////
	//// algebraic sphere MLS
	//// ref to 2007-Algebraic Point Set Surfaces
	////f(x,y,z)=c0+c1*x+c2*y+c3*(x^2+y^2)
	template<> class ASMLSPointSet<2>
	{public:
		VectorX c;
		int point_n=0;
		int p_idx=-1;
		int d=2;

		////2D: [x0,y0, x1,y1, x2,y2, ...]
		////f(x,y,z)=c0+c1*x+c2*y=-(x^2+y^2), c3=1
		//// Fit with weights
		void Fit(const real* data,const real* ws,const int m)
		{
			point_n=m;

			MatrixX B(m,d+1);
			VectorX f(m);
			for(int i=0;i<m;i++){
				real w=sqrt(ws[i]);
				B.coeffRef(i,0)=w*(real)1;
				B.coeffRef(i,1)=w*data[i*d];
				B.coeffRef(i,2)=w*data[i*d+1];
				// B.coeffRef(i,3)=w*(pow(data[i*2],2)+pow(data[i*2+1],2));
				f[i]=-w*(pow(data[i*d],2)+pow(data[i*d+1],2));}
			VectorX c_d=B.colPivHouseholderQr().solve(f);
			c=VectorX(d+2); c<<c_d,1;
		}

		void Fit_With_Normal(const real* data, const real* normals, const real* ws,const int m)
		{
			point_n=m;
			real n_weight=0.25f;

			MatrixX B(m*(d+1),d+2);
			VectorX f(m*(d+1));
			for(int i=0;i<m;i++){
				real w=sqrt(ws[i]);
				real wn=w*sqrt(n_weight);
				B.coeffRef(i*(d+1),0)=w*(real)1;
				B.coeffRef(i*(d+1),1)=w*data[i*d];
				B.coeffRef(i*(d+1),2)=w*data[i*d+1];
				B.coeffRef(i*(d+1),3)=w*(pow(data[i*d],2)+pow(data[i*d+1],2));
				
				B.coeffRef(i*(d+1)+1,0)=(real)0;
				B.coeffRef(i*(d+1)+1,1)=wn*(real)1;
				B.coeffRef(i*(d+1)+1,2)=(real)0;
				B.coeffRef(i*(d+1)+1,3)=2*wn*data[i*d];
				
				B.coeffRef(i*(d+1)+2,0)=(real)0;
				B.coeffRef(i*(d+1)+2,1)=(real)0;
				B.coeffRef(i*(d+1)+2,2)=wn*(real)1;
				B.coeffRef(i*(d+1)+2,3)=2*wn*data[i*d+1];
				
				f[i*(d+1)]=0;
				f[i*(d+1)+1]=wn*normals[i*d];
				f[i*(d+1)+2]=wn*normals[i*d+1];}
			c=B.colPivHouseholderQr().solve(f);
		}
		
		inline real operator() (const Vector2& x) const 
		{return c[0]+c[1]*x[0]+c[2]*x[1]+c[3]*(pow(x[0],2)+pow(x[1],2));}

		inline Vector2 Grad(const Vector2& x) const
		{return Vector2(c[1]+(real)2*c[3]*x[0], c[2]+(real)2*c[3]*x[1]);}

		inline Vector2 Sphere_Center() const
		{return -(real)0.5f/c[3]*Vector2(c[1],c[2]);}

		inline real Sphere_Radius() const
		{return sqrt(Sphere_Center().squaredNorm()-c[0]/c[3]);}

	// protected:
		inline real Weight(Vector2 u, real h) const
		{
			return pow(1-u.squaredNorm()/(h*h),4);
		}
	};

	////f(x,y,z)=c0+c1*x+c2*y+c3*z+c4*(x1^2+x2^2+x3^2)
	template<> class ASMLSPointSet<3>
	{public:
		VectorX c;
		int point_n=0;
		int p_idx=-1;
		int d=3;

		////2D: [x0,y0,z0, x1,y1,z1, x2,y2,z2, ...]
		////f(x,y,z)=c0+c1*x+c2*y+c3*z=-(x1^2+x2^2+x3^2), c4=1
		//// Fit with weights
		void Fit(const real* data,const real* ws,const int m)
		{
			point_n=m;

			MatrixX B(m,4);
			VectorX f(m);
			for(int i=0;i<m;i++){
				real w=sqrt(ws[i]);
				B.coeffRef(i,0)=w*(real)1;
				B.coeffRef(i,1)=w*data[i*3];
				B.coeffRef(i,2)=w*data[i*3+1];
				B.coeffRef(i,3)=w*data[i*3+2];
				// B.coeffRef(i,4)=w*(pow(data[i*3],2)+pow(data[i*3+1],2)+pow(data[i*3+2],2));
				f[i]=-w*(pow(data[i*3],2)+pow(data[i*3+1],2)+pow(data[i*3+2],2));}
			VectorX c_d=B.colPivHouseholderQr().solve(f);
			c=VectorX(5); c<<c_d,1;
			c<<c_d,1;
		}

		void Fit_With_Normal(const real* data, const real* normals, const real* ws,const int m)
		{
			point_n=m;
			real n_weight=0.25f;

			MatrixX B(m*(d+1),d+2);
			VectorX f(m*(d+1));
			for(int i=0;i<m;i++){
				real w=sqrt(ws[i]);
				real wn=w*sqrt(n_weight);
				B.coeffRef(i*(d+1),0)=w*(real)1;
				B.coeffRef(i*(d+1),1)=w*data[i*d];
				B.coeffRef(i*(d+1),2)=w*data[i*d+1];
				B.coeffRef(i*(d+1),3)=w*data[i*d+2];
				B.coeffRef(i*(d+1),4)=w*(pow(data[i*d],2)+pow(data[i*d+1],2)+pow(data[i*d+2],2));
				
				B.coeffRef(i*(d+1)+1,0)=(real)0;
				B.coeffRef(i*(d+1)+1,1)=wn*(real)1;
				B.coeffRef(i*(d+1)+1,2)=(real)0;
				B.coeffRef(i*(d+1)+1,3)=(real)0;
				B.coeffRef(i*(d+1)+1,4)=2*wn*data[i*d];
				
				B.coeffRef(i*(d+1)+2,0)=(real)0;
				B.coeffRef(i*(d+1)+2,1)=(real)0;
				B.coeffRef(i*(d+1)+2,2)=wn*(real)1;
				B.coeffRef(i*(d+1)+2,3)=(real)0;
				B.coeffRef(i*(d+1)+2,4)=2*wn*data[i*d+1];
				
				B.coeffRef(i*(d+1)+3,0)=(real)0;
				B.coeffRef(i*(d+1)+3,1)=(real)0;
				B.coeffRef(i*(d+1)+3,2)=(real)0;
				B.coeffRef(i*(d+1)+3,3)=wn*(real)1;
				B.coeffRef(i*(d+1)+3,4)=2*wn*data[i*d+2];
				
				f[i*(d+1)]=0;
				f[i*(d+1)+1]=wn*normals[i*d];
				f[i*(d+1)+2]=wn*normals[i*d+1];
				f[i*(d+1)+2]=wn*normals[i*d+2];}
			c=B.colPivHouseholderQr().solve(f);
		}
		
		inline real operator() (const Vector3& x) const 
		{return c[0]+c[1]*x[0]+c[2]*x[1]+c[3]*x[2]+c[4]*(pow(x[0],2)+pow(x[1],2)+pow(x[2],2));}

		inline Vector3 Grad(const Vector3& x) const
		{return Vector3(c[1]+(real)2*c[4]*x[0], c[2]+(real)2*c[4]*x[1], c[3]+2*c[4]*x[0]);}

		inline Vector3 Sphere_Center() const
		{return -(real)0.5f/c[4]*Vector3(c[1],c[2],c[3]);}

		inline real Sphere_Radius() const
		{return sqrt(Sphere_Center().squaredNorm()-c[0]/c[4]);}

	// protected:
		inline real Weight(Vector3 u, real h) const
		{
			return pow(1-u.squaredNorm()/(h*h),4);
		}
	};
};
}
