#include "PointLocalGeometry.h"

namespace Meso {

// template<int d>
// Vector<real, d> PointLocalGeometry<d>::Local_Coords(const VectorD& u, const MatrixD& e)
// {
// 	//[u.dot(e.col(0)), u.dot(e.col(1)), ...]
// 	return u.transpose() * e;
// }

// template<int d>
// void PointLocalGeometry<d>::Local_Coords(const VectorD& u, const MatrixD& e, VectorT& t, real& z)
// {
// 	VectorD local_coords = Local_Coords(u, e);
// 	// t = AuxFunc::V<d - 1>(local_coords);
// 	t = MathFunc::V<d - 1>(local_coords);
// 	z = local_coords[d - 1];
// }

// template<int d>
// Vector<real, d - 1> PointLocalGeometry<d>::Project_To_TPlane(const VectorD& u, const MatrixD& e)
// {
// 	VectorT t_coords;
// 	for (int i = 0; i < d - 1; i++) {
// 		t_coords[i] = u.dot(e.col(i));
// 	}
// 	return t_coords;
// }

// template<int d>
// Vector<real, d - 1> PointLocalGeometry<d>::Rotate_To_TPlane(const VectorD& u, const MatrixD& e)
// {
// 	VectorT t_coords;
// 	for (int i = 0; i < d - 1; i++) {
// 		t_coords[i] = u.dot(e.col(i));
// 	}
// 	if (t_coords.norm() == 0.) return VectorT::Zero();
// 	else return u.norm() * t_coords.normalized();
// }

// template<int d>
// real PointLocalGeometry<d>::Project_To_TPlane_H(const VectorD& u, const MatrixD& e)
// {
// 	return u.dot(e.col(d - 1));
// }

// template<int d>
// int PointLocalGeometry<d>::Nearest_Geometry(VectorD& pos, MatrixD& local_frame, VectorD& search_pos, int minimum_eqns, bool verbose) const
// {
//     ////The current implementation only conducts one iteration. The function can be called for multiple times for an iterative projection.
//     using namespace LeastSquares;
//     // local_frame = Local_Frame(pos);
//     local_frame = Local_Frame(search_pos);
    
//     // Array<int> nbs = Find_Tangential_Nbs(pos, local_frame);
//     // Array<int> nbs = nbs_searcher->Find_Neighbors(pos, v_r);
//     Array<int> nbs = nbs_searcher->Find_Neighbors(search_pos, v_r);

//     MLS<d - 1, 2> ls;
//     size_t n = nbs.size(); Array<real> data(n * (size_t)d);
//     if (verbose) std::cout << "number of neighbors? " << nbs.size() << std::endl;
//     if (n < minimum_eqns) {
//         // int closest_p = Closest_Point(pos);
//         int closest_p = Closest_Point(pos);
//         // VectorD norm = points->Normal(closest_p);
//         MatrixD E = Local_Frame((*nbs_searcher->Point_Ptr)[closest_p]);
//         VectorD norm = E.col(d - 1);
//         // real offset = norm.normalized().dot(pos - points->X(closest_p));
//         real offset = norm.normalized().dot(pos - (*nbs_searcher->Point_Ptr)[closest_p]);
//         pos = pos - offset * norm;
//         if(verbose)	std::cout << "[Warning]PointSet<d>::Project_To_Surface: " << search_pos.transpose() << " find only " << n << " neighbors against minimum " << minimum_eqns << "\n";
//         return 1;
//     }
//     else {
//         for (size_t i = 0; i < n; i++) {
//             int p = nbs[i];
//             // VectorD u = points->X(p) - pos;
//             VectorD u = (*nbs_searcher->Point_Ptr)[p] - pos;
//             VectorD th = Local_Coords(u, local_frame);
//             for (size_t j = 0; j < d; j++)data[i * (size_t)d + j] = th[j];
//         }
//         ls.Fit(data.data(), (int)n, 0, 0);
//         // pos = pos + local_frame.col(d - 1) * ls(VectorT::Zero());
//         //// TODO: it's hard to project the pos onto the surface
//         pos = search_pos + local_frame.col(d - 1) * ls(VectorT::Zero());
//         return 0;
//     }
// }

// template<int d>
// int PointLocalGeometry<d>::Nearest_Geometry(VectorD& pos, MatrixD& local_frame, int minimum_eqns, bool verbose) const
// {
//     return Nearest_Geometry(pos,local_frame,pos,minimum_eqns,verbose);
// }


};