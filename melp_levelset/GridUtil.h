//////////////////////////////////////////////////////////////////////////
// Grid Utils, for cell operations
// Copyright (c) (2022-), Hui Wang
// This file is part of MESO, whose distribution is governed by the LICENSE file.
//////////////////////////////////////////////////////////////////////////
#pragma once

#include "Grid.h"

namespace Meso {
    template<int d>
    class GridUtil{
        Typedef_VectorD(d);
    public:
        static VectorDi Cell_Coord(const Grid<d>& grid, const VectorD& pos){
            // Vector<real,d> coord_with_frac=(pos-grid.domain_min)/grid.dx;
            // VectorD coord_with_frac=(pos-grid.Node_Min()-VectorD::Ones() * 0.5 * grid.dx)/grid.dx;
            VectorD coord_with_frac=(pos-grid.Domain_Min(GridType::CENTER))/grid.dx;
            return coord_with_frac.template cast<int>();
        }
        
        static bool Valid(const Grid<d>& grid, const VectorD& pos){
            VectorD grid_min=grid.Domain_Min(GridType::CENTER);
            VectorD grid_max=grid.Domain_Max(GridType::CENTER);
            
            if constexpr(d==1) return (pos(0)>=grid_min(0) && pos(0)<=grid_max(0));
            if constexpr(d==2) return (pos(0)>=grid_min(0) && pos(0)<=grid_max(0)) && (pos(1)>=grid_min(1) && pos(1)<=grid_max(1));
            if constexpr(d==3) return (pos(0)>=grid_min(0) && pos(0)<=grid_max(0)) && (pos(1)>=grid_min(1) && pos(1)<=grid_max(1))  && (pos(2)>=grid_min(2) && pos(2)<=grid_max(2));
            
            return true;
        }

        static std::tuple<int, VectorDi>  Neighbor_Face(const Grid<d>& grid, VectorDi coord, int i){
            int axis=grid.Neighbor_Node_Axis(i);
            VectorDi face=coord;
            if constexpr (d == 2) {
                const int side[4] = { 0,0,1,1 };
                face = coord + VectorDi::Unit(axis)*side[i];
            }
            else if constexpr (d == 3) {
                const int side[6] = { 0,0,0,1,1,1 };
                face = coord + VectorDi::Unit(axis)*side[i];
            }
            return std::make_tuple(axis, face);
        }

        static VectorD Clamp_To_Grid(const Grid<d>& grid, VectorD pos){
            VectorD domain_min=grid.Domain_Min(GridType::CENTER);
            VectorD domain_max=grid.Domain_Max(GridType::CENTER);
            for(int i=0;i<d;i++)
                pos(i)=std::max(std::min(pos(i), domain_max(i)), domain_min(i));
            return pos;
        }

        static int Particle_Neighbor_Cell_Num(){
            return pow(2,d);
        }

        static VectorDi Particle_Neighbor_Cell_Offset(const int i){
            if constexpr(d==2) {
                const Vector2i neighbor_offset[4] = { Vector2i(0,0),Vector2i(0,1),Vector2i(1,0),Vector2i(1,1) };
                return neighbor_offset[i];
            }
            if constexpr(d==3) {
                const Vector3i neighbor_offset[8] = { Vector3i(0,0,0),Vector3i(0,0,1),Vector3i(0,1,0),Vector3i(0,1,1),Vector3i(1,0,0),Vector3i(1,0,1),Vector3i(1,1,0),Vector3i(1,1,1) };
                return neighbor_offset[i];
            }
        }

        static int Cell_Neighbor_Cell_Num_2_Layer(){
            return pow(5,d);
        }

        static VectorDi Cell_Neighbor_Cell_Offset_2_Layer(const int i){
            if constexpr(d==2) {
                return VectorDi(i/5-2, i%5-2);
            }
            if constexpr(d==3) {
                return VectorDi(i/25-2, (i%25)/5-2, i%5-2);
            }
        }
    };
}