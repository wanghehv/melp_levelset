//////////////////////////////////////////////////////////////////////////
// Particle levelset
// Copyright (c) (2022-), Hui Wang
// This file is part of MELPLEVELSET, whose distribution is governed by the LICENSE file.
//////////////////////////////////////////////////////////////////////////
#pragma once

#include "EulerianParticles.h"
#include "LevelSet.h"
#include "ParticleLevelSetHelper.h"


namespace Meso {
    template<int d>
    class ParticleLevelSet {
        Typedef_VectorD(d);
    public:
        ParticleLevelSet() {}
        ParticleLevelSet(const Grid<d> _grid) {
            levelset.Init(_grid);
        }

        LevelSet<d> levelset;
        EulerianParticles<d> particles;

    public:
        void Correct_Levelset_With_Normal_MLS_Projection(std::shared_ptr<PointLocalGeometry<d>> local_geom, int max_iter=2){
            return ParticleLevelSetHelper<d>::Correct_Levelset_With_Normal_MLS_Projection(levelset, particles, local_geom, max_iter);
        }
         
        void Fix_Particle_Normal(std::shared_ptr<PointLocalGeometry<d>> local_geom){
            return ParticleLevelSetHelper<d>::Fix_Particle_Normal(levelset, particles, local_geom);
        }

        void Reseed_Insert_Particles(std::shared_ptr<PointLocalGeometry<d>> local_geom){
            ParticleLevelSetHelper<d>::Reseed_Insert_Particles(levelset, particles, local_geom);
            return;
        };
        void Reseed_Remove_Particles(std::shared_ptr<PointLocalGeometry<d>> local_geom){
            return ParticleLevelSetHelper<d>::Reseed_Remove_Particles(levelset, particles, local_geom);
        };

    public://// deprecated func
        void Correct_Levelset_With_Particles_Naive(std::shared_ptr<PointLocalGeometry<d>> local_geom){
            return ParticleLevelSetHelper<d>::Correct_Levelset_With_Particles_Naive(levelset, particles, local_geom);
        }
        
        void Correct_Levelset_With_Closest_Particles(std::shared_ptr<PointLocalGeometry<d>> local_geom, bool use_default=false){
            return ParticleLevelSetHelper<d>::Correct_Levelset_With_Closest_Particles(levelset, particles, local_geom, use_default);
        }
        
        void Correct_Levelset_With_MLS_Projection(std::shared_ptr<PointLocalGeometry<d>> local_geom, int max_iter=2){
            return ParticleLevelSetHelper<d>::Correct_Levelset_With_MLS_Projection(levelset, particles, local_geom, max_iter);
        }
    };
}