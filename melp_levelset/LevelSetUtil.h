#pragma once
#include "LevelSet.h"
#include "Grid.h"
#include "GridUtil.h"
#include <stack>

namespace Meso {
    template<int d>
    class LevelSetUtil{
        Typedef_VectorD(d);
    public:
        static void Flood_Fill(Field<real,d>& color, const LevelSet<d>& levelset, const Field<int,d>& psi_P, const int interior, const int interface, const int exterior){
            Typedef_VectorD(d);
            
            std::stack<int> cell_index_stack;
            const Grid<d>& grid=levelset.phi.grid;
            //// seed negative and not particle cell
            
            // levelset.phi.grid.Iterate_Nodes([&](const VectorDi& cell) {
            //     if(levelset.phi(cell)<-grid.dx && psi_P(cell)<0){
            //         cell_index_stack.push(grid.Index(cell));
            //         break;
            //     }
            // });
            int cell_num=grid.Valid_Size();
            // #pragma omp parallel for
            for(int i=0;i<cell_num;i++){
                VectorDi cell=grid.Coord(i);
                if(levelset.phi(cell)<-grid.dx && psi_P(cell)<0){
                    cell_index_stack.push(grid.Index(cell));
                    break;
                }
            }
                
            //// fill
            color.Fill(exterior);
            //// fill interfaces
            #pragma omp parallel for
            for(int i=0;i<cell_num;i++){
                VectorDi cell=grid.Coord(i);
                if(psi_P(cell)>0){
                    color(cell)=interface;
                }
            }

            while(!cell_index_stack.empty()){
                int cell_index=cell_index_stack.top();cell_index_stack.pop();
                VectorDi cell=grid.Coord(cell_index);
                color(cell)=interior;
                for(int i=0;i<grid.Neighbor_Node_Number();i++){
                    VectorDi nb_cell=grid.Neighbor_Node(cell,i);
                    if(grid.Valid(nb_cell)&&color(nb_cell)!=interior&&psi_P(nb_cell)<0){
                        cell_index_stack.push(grid.Index(nb_cell));
                    }
                }
            }
        }

        //// find closest point based on levelset gradient, levelset function
        static Vector<real,d> Closest_Point_With_Iterations(const LevelSet<d>& levelset, Vector<real,d>& pos, const int max_iter/*=5*/)
        {
            Typedef_VectorD(d);
            VectorD intf_pos = pos;
            for (int i = 0; i < max_iter; i++) {
                intf_pos = levelset.Closest_Point(intf_pos);
                intf_pos=GridUtil<d>::Clamp_To_Grid(levelset.phi.grid, intf_pos);
                if (levelset.Phi(intf_pos) < (real)0)return intf_pos;
            }
            return intf_pos;
        }
            
        //// sample particles on levelset surface
        static void Sample_On_Levelset_Surface(Array<Vector<real,d>>& x, const LevelSet<d> & levelset, const int particles_per_cell, const real narrow_band_cell_num=1, const int max_iter=5){
            Typedef_VectorD(d);
            // x.Clear();
            const Grid<d>& grid=levelset.phi.grid;
            grid.Iterate_Nodes([&](const VectorDi& cell) {
                
                const VectorD cell_center = grid.Position(cell);
                // real frac=levelset.Cell_Fraction(cell);
                // if(frac<1 && frac>0){
                // if(abs(levelset.Phi(cell_center))<grid.dx){
                if(abs(levelset.Phi(cell_center))<grid.dx*narrow_band_cell_num){
                    for (int i = 0; i < particles_per_cell; i++)
                    {
                        VectorD particle_pos = cell_center + VectorD::Random() * grid.dx * 0.5;
                        if(levelset.Phi(particle_pos)>0){
                            particle_pos=Closest_Point_With_Iterations(levelset,particle_pos,max_iter);
                            x.push_back(particle_pos);
                        }
                    }
                }
            });
            Info("sample particles on surface #{}",x.size());
        }

        //// sample particle in the narrowband of a interface
        static void Sample_On_Levelset_Narrowband(Array<Vector<real,d>>& x, const LevelSet<d> & levelset, const int particles_per_cell, const real narrow_band_cell_num=1.0){
            Typedef_VectorD(d);
            // x.Clear();
            const Grid<d>& grid=levelset.phi.grid;
            grid.Iterate_Nodes([&](const VectorDi& cell) {
                const VectorD cell_center = grid.Position(cell);
                // real frac=levelset.Cell_Fraction(cell);
                // if(frac<1 && frac>0){
                if(abs(levelset.Phi(cell_center))<grid.dx*narrow_band_cell_num){
                    if(particles_per_cell<=0){
                        // VectorD particle_pos = cell_center + VectorD::Ones() * grid.dx * 0.5;
                        VectorD particle_pos = cell_center;
                        x.push_back(particle_pos);
                    }
                    else{
                        for (int i = 0; i < particles_per_cell; i++){
                            VectorD particle_pos = cell_center + VectorD::Random() * grid.dx * 0.5;
                            x.push_back(particle_pos);
                        }
                    }
                }});
            Info("sample particles in narrowband #{}",x.size());
        }

        static real Cell_Fraction(const LevelSet<d>& levelset, const real phi)
        {
            real dx = levelset.phi.grid.dx;
		    return (real).5 - MathFunc::Clamp(phi, -(real).5 * dx, (real).5 * dx) / dx;
        }

        static VectorD Normal(const LevelSet<d>& levelset, const VectorD& pos){
            return levelset.Gradient(pos).normalized();
        }

        static real Curvature(const LevelSet<d>& levelset, const VectorD& pos){
            real one_over_dx = (real)1 / levelset.phi.grid.dx; real one_over_two_dx = (real).5 * one_over_dx; real curvature = (real)0;
            for (int i = 0; i < d; i++) {
                VectorD normal_left = Normal(levelset,pos - VectorD::Unit(i) * levelset.phi.grid.dx);
                VectorD normal_right = Normal(levelset,pos + VectorD::Unit(i) * levelset.phi.grid.dx);
                curvature += (normal_right[i] - normal_left[i]) * one_over_two_dx;
            }
            return abs(curvature) < one_over_dx ? curvature : (curvature <= (real)0 ? (real)-1 : (real)1) * one_over_dx;

        }

        	//// the 
        using PRI = std::pair<real, int>;
	    static void Fast_Marching_With_Threshold(LevelSet<d>& levelset, const real band_width, const real threshold)
        {
            Grid<d> grid = levelset.phi.grid;
            //Timer timer;
            //timer.Reset();
            
            Field<real, d> tent(grid, band_width < 0 ? std::numeric_limits<real>::max() : band_width);
            Array<ushort> done(grid.Memory_Size(), 0);
            
            std::priority_queue<PRI, Array<PRI>, std::greater<PRI> > heaps[2];
            const int cell_num = grid.Counts().prod();
            //real far_from_intf_phi_val=grid.dx*(real)5;

            //// Step 1: find interface cells
    #pragma omp parallel for
            for (int i = 0; i < cell_num; i++) {
                const VectorDi cell = grid.Coord(i);
                //if(abs(phi(cell))>far_from_intf_phi_val)continue;		////ATTENTION: this might cause problem if the levelset is badly initialized
                if (abs(levelset.phi(cell))<threshold) {
                    done[i] = true; continue;
                }
                for (int j = 0; j < Grid<d>::Neighbor_Node_Number(); j++) {
                    VectorDi nb = grid.Neighbor_Node(cell, j);
                    if (!grid.Valid(nb))continue;
                    if (levelset.Is_Interface(cell, nb)) {
                        done[i] = true; break;
                    }

                }
            }
            //if (verbose)timer.Elapse_And_Output_And_Reset("FMM Precond: find interface");

            //// Step 2: calculate initial phi values for interface cells
    #pragma omp parallel for
            for (int c = 0; c < cell_num; c++) {
                if (!done[c])continue;		////select interface cells
                const VectorDi cell = grid.Coord(c);

                // VectorD correct_phi = VectorD::Ones() * std::numeric_limits<real>::max();
                // VectorDi correct_axis = VectorDi::Zero();
                // for (int i = 0; i < Grid<d>::Neighbor_Node_Number(); i++) {
                // 	VectorDi nb = grid.Neighbor_Node(cell, i);
                // 	if (!grid.Valid(nb)) continue;
                // 	const int nb_idx = grid.Index(nb);
                // 	if (done[nb_idx] && Is_Interface(cell, nb)) {
                // 		real c_phi = Theta(phi(cell), phi(nb)) * grid.dx; // always non-negative
                // 		int axis = grid.Neighbor_Node_Axis(i);
                // 		correct_axis[axis] = 1;
                // 		correct_phi[axis] = std::min(correct_phi[axis], c_phi);
                // 	}
                // }
                // if (correct_axis != VectorDi::Zero()) {
                // 	real hmnc_mean = (real)0;
                // 	for (int i = 0; i < d; i++) {
                // 		if (correct_axis[i] == 0)continue;
                // 		hmnc_mean += (real)1 / (correct_phi[i] * correct_phi[i]);
                // 	}
                // 	hmnc_mean = sqrt((real)1 / hmnc_mean);
                // 	tent(cell) = hmnc_mean;
                // }
                // else {
                // 	// Error("[Levelset] bad preconditioning");
                // }
                tent(cell)=abs(levelset.phi(cell));//// TODO: add
    #pragma omp critical
                {heaps[MathFunc::Sign(levelset.phi(cell)) > 0 ? 0 : 1].push(PRI(tent(cell), c)); }
            }
            std::cout<<"heap size: "<<heaps[0].size()<<","<<heaps[1].size()<<std::endl;

            //// Step 3: perform relaxation on interface cells to fix their values
            //// NOTE: interface cells will solve Eikonal equation with cells on the other side of the interface,
            //// with their ABS values instead of actual phi values

    #pragma omp parallel for
            for (int h = 0; h < 2; h++) {
                levelset.Relax_Heap(heaps[h], tent, done, levelset.phi, true);
            }

    #pragma omp parallel for
            for (int i = 0; i < cell_num; i++) {
                const VectorDi cell = grid.Coord(i);
                if (done[i]) {
    #pragma omp critical
                    {heaps[MathFunc::Sign(levelset.phi(cell)) > 0 ? 0 : 1].push(PRI(tent(cell), i)); }
                }
            }
            
            //// Step 4: relax the other part of field
    #pragma omp parallel for
            for (int h = 0; h < 2; h++) {
                levelset.Relax_Heap(heaps[h], tent, done, levelset.phi, false);
            }

            ArrayFunc::Binary_Transform(
                levelset.phi.Data(),
                tent.Data(),
                [=](const real phi_i, const real tent_i) {return MathFunc::Sign(phi_i) * tent_i; },
                levelset.phi.Data()
            );
        }



// 	The code should be insert into levelset.h

	// template<int d> real LevelSet<d>::Curvature(const VectorD& pos) const
	// {
	// 	real one_over_dx = (real)1 / phi.grid.dx; real one_over_two_dx = (real).5 * one_over_dx; real curvature = (real)0;
	// 	for (int i = 0; i < d; i++) {
	// 		VectorD normal_left = Normal(pos - VectorD::Unit(i) * phi.grid.dx);
	// 		VectorD normal_right = Normal(pos + VectorD::Unit(i) * phi.grid.dx);
	// 		curvature += (normal_right[i] - normal_left[i]) * one_over_two_dx;
	// 	}
	// 	return abs(curvature) < one_over_dx ? curvature : (curvature <= (real)0 ? (real)-1 : (real)1) * one_over_dx;

	// }

    // VectorD Normal(const VectorD& pos) const
    // {
    // 	return Gradient(pos).normalized();
    // }

    // bool Is_Interface(const VectorDi cell0, const VectorDi cell1) const { return MathFunc::Sign(phi(cell0)) != MathFunc::Sign(phi(cell1)); }  

// 	The code should be insert into levelset.cu
// 	template<int d> void LevelSet<d>::Fast_Marching_With_Threshold(const real band_width, const real threshold)
// 	{
// 		Grid<d> grid = phi.grid;
// 		//Timer timer;
// 		//timer.Reset();
		
// 		Field<real, d> tent(grid, band_width < 0 ? std::numeric_limits<real>::max() : band_width);
// 		Array<ushort> done(grid.Memory_Size(), 0);
		
// 		std::priority_queue<PRI, Array<PRI>, std::greater<PRI> > heaps[2];
// 		const int cell_num = grid.Counts().prod();
// 		//real far_from_intf_phi_val=grid.dx*(real)5;

// 		//// Step 1: find interface cells
// #pragma omp parallel for
// 		for (int i = 0; i < cell_num; i++) {
// 			const VectorDi cell = grid.Coord(i);
// 			//if(abs(phi(cell))>far_from_intf_phi_val)continue;		////ATTENTION: this might cause problem if the levelset is badly initialized
// 			if (abs(phi(cell))<threshold) {
// 				done[i] = true; continue;
// 			}
// 			for (int j = 0; j < Grid<d>::Neighbor_Node_Number(); j++) {
// 				VectorDi nb = grid.Neighbor_Node(cell, j);
// 				if (!grid.Valid(nb))continue;
// 				if (Is_Interface(cell, nb)) {
// 					done[i] = true; break;
// 				}

// 			}
// 		}
// 		//if (verbose)timer.Elapse_And_Output_And_Reset("FMM Precond: find interface");

// 		//// Step 2: calculate initial phi values for interface cells
// #pragma omp parallel for
// 		for (int c = 0; c < cell_num; c++) {
// 			if (!done[c])continue;		////select interface cells
// 			const VectorDi cell = grid.Coord(c);

// 			// VectorD correct_phi = VectorD::Ones() * std::numeric_limits<real>::max();
// 			// VectorDi correct_axis = VectorDi::Zero();
// 			// for (int i = 0; i < Grid<d>::Neighbor_Node_Number(); i++) {
// 			// 	VectorDi nb = grid.Neighbor_Node(cell, i);
// 			// 	if (!grid.Valid(nb)) continue;
// 			// 	const int nb_idx = grid.Index(nb);
// 			// 	if (done[nb_idx] && Is_Interface(cell, nb)) {
// 			// 		real c_phi = Theta(phi(cell), phi(nb)) * grid.dx; // always non-negative
// 			// 		int axis = grid.Neighbor_Node_Axis(i);
// 			// 		correct_axis[axis] = 1;
// 			// 		correct_phi[axis] = std::min(correct_phi[axis], c_phi);
// 			// 	}
// 			// }
// 			// if (correct_axis != VectorDi::Zero()) {
// 			// 	real hmnc_mean = (real)0;
// 			// 	for (int i = 0; i < d; i++) {
// 			// 		if (correct_axis[i] == 0)continue;
// 			// 		hmnc_mean += (real)1 / (correct_phi[i] * correct_phi[i]);
// 			// 	}
// 			// 	hmnc_mean = sqrt((real)1 / hmnc_mean);
// 			// 	tent(cell) = hmnc_mean;
// 			// }
// 			// else {
// 			// 	// Error("[Levelset] bad preconditioning");
// 			// }
// 			tent(cell)=abs(phi(cell));//// TODO: add
// #pragma omp critical
// 			{heaps[MathFunc::Sign(phi(cell)) > 0 ? 0 : 1].push(PRI(tent(cell), c)); }
// 		}
// 		std::cout<<"heap size: "<<heaps[0].size()<<","<<heaps[1].size()<<std::endl;

// 		//// Step 3: perform relaxation on interface cells to fix their values
// 		//// NOTE: interface cells will solve Eikonal equation with cells on the other side of the interface,
// 		//// with their ABS values instead of actual phi values

// #pragma omp parallel for
// 		for (int h = 0; h < 2; h++) {
// 			Relax_Heap(heaps[h], tent, done, phi, true);
// 		}

// #pragma omp parallel for
// 		for (int i = 0; i < cell_num; i++) {
// 			const VectorDi cell = grid.Coord(i);
// 			if (done[i]) {
// #pragma omp critical
// 				{heaps[MathFunc::Sign(phi(cell)) > 0 ? 0 : 1].push(PRI(tent(cell), i)); }
// 			}
// 		}
		
// 		//// Step 4: relax the other part of field
// #pragma omp parallel for
// 		for (int h = 0; h < 2; h++) {
// 			Relax_Heap(heaps[h], tent, done, phi, false);
// 		}

// 		ArrayFunc::Binary_Transform(
// 			phi.Data(),
// 			tent.Data(),
// 			[=](const real phi_i, const real tent_i) {return MathFunc::Sign(phi_i) * tent_i; },
// 			phi.Data()
// 		);
// 	}


    };
    



}