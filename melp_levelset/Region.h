//////////////////////////////////////////////////////////////////////////
// Region
// Copyright (c) (2022-), Hui Wang
// This file is part of MELPLEVELSET, whose distribution is governed by the LICENSE file.
//////////////////////////////////////////////////////////////////////////
#pragma once

#include "DefaultDynamicsParameter.h"
#include "EulerianParticles.h"
#include "LevelSet.h"

#include "Json.h"
#include <atomic>

namespace Meso {


    template<int d>
    class Region {
        Typedef_VectorD(d);
        Typedef_MatrixD(d);
        using VectorT=Vector<real,d-1>;  ////tangential vector type
        using VectorTi=Vector<int,d-1>;  ////tangential vector int
    public:
        enum RegionType : int {
            INVALID = -1,
            WATER,
            AIR,
            OIL
        };
        
    public:
        RegionType type=RegionType::INVALID;

        real sigma=0.0; //// surface tension
        real rho=1.0; //// density

        //// volume control
        bool use_volume_control=true;
        real init_volume=0.0; //// initial volume
        real volume=0.0; //// current volume
        std::atomic<long long> integer_volume=0;
        std::atomic<long long> volume_correct_cell_cnt=0;
        real delta_volume_per_cell=0.0; //// (target-current)/inner_cell_num


        std::shared_ptr<EulerianParticles<d>> particles=nullptr; //// shared with mpls
        Array<VectorDi> inner_cells;
        Array<VectorDi> subregion_parent_cells; //// only used in Detect_Split_Region
        

        Region(){ Init(RegionType::AIR, nullptr); }
        Region(RegionType _type){ Init(_type, nullptr); }
        
        void Copy_Param(Region& region){ 
            //// duplicate the physics parameters
            region.type=this->type;
            region.sigma=this->sigma;
            region.rho=this->rho;
            region.use_volume_control=this->use_volume_control;

            //// skip the volume params
            // region.
        }

        void Init(RegionType _type, std::shared_ptr<EulerianParticles<d>> _particles){
            type=_type;
            particles=_particles;
            switch(_type){
                case RegionType::INVALID:
                case RegionType::AIR:
                    sigma=1.0;
                    rho=DefaultDynamicsParameter::air_density;
                    break;
                case RegionType::WATER:
                case RegionType::OIL:
                    sigma=1.0;
                    rho=DefaultDynamicsParameter::liquid_density;
                    break;
            }
        }

        json To_Json(){
            json j;
            j["type"]=int(this->type);
            j["sigma"]=this->sigma;
            j["rho"]=this->rho;
            j["use_volume_control"]=this->use_volume_control;
            j["init_volume"]=this->init_volume;
            j["volume"]=this->volume;
            return j;
        }

        bool From_Json(json& j){
            this->type=j["type"];
            this->sigma=j["sigma"];
            this->rho=j["rho"];
            this->use_volume_control=j["use_volume_control"];
            this->init_volume=j["init_volume"];
            this->volume=j["volume"];
        }
    };
}
