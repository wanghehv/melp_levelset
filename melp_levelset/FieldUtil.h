//////////////////////////////////////////////////////////////////////////
// Field Utils, for differential operation on fields
// Copyright (c) (2022-), Hui Wang
// This file is part of MESO, whose distribution is governed by the LICENSE file.
//////////////////////////////////////////////////////////////////////////
#pragma once

#include "Field.h"
#include "FaceField.h"
#include "Interpolation.h"

namespace Meso {

    template<class Intp = IntpLinearClamp>
    class FieldSampler{
    public:
        
        template<class T, int d>
        static Vector<T, d> Gradient(const Field<T, d>& field, const Vector<T, d> pos)
        {
            Typedef_VectorD(d);
            Typedef_MatrixD(d);
            real dx = field.grid.dx;
            VectorD grad=VectorD::Zero();
            for (int i = 0; i < d; i++){
                VectorD x0=pos - VectorD::Unit(i) * dx;
                VectorD x1=pos + VectorD::Unit(i) * dx;
                real v0=Intp::Value(field, x0);
                real v1=Intp::Value(field, x1);
                grad[i] = (v1-v0) / (dx * 2);
            };
            return grad;
        }

        //// grad(i,j)=dv(i)/dx(j)
        template<class T, int d>
        static Matrix<real,d,d> Gradient(const FaceField<T, d>& field, const Vector<T, d> pos)
        {
            Typedef_VectorD(d);
            Typedef_MatrixD(d);
            real dx = field.grid.dx;
            MatrixD grad=MatrixD::Zero();
            for (int i = 0; i < d; i++){
                VectorD x0=pos - VectorD::Unit(i) * dx;
                VectorD x1=pos + VectorD::Unit(i) * dx;
                Vector<T, d> v0=Intp::Face_Vector(field, x0);
                Vector<T, d> v1=Intp::Face_Vector(field, x1);
                grad.col(i) = (v1-v0) / (dx * 2);
            };
            return grad;
        }
    };
}