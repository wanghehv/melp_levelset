//////////////////////////////////////////////////////////////////////////
// PLS particle representation
// Copyright (c) (2022-), Hui Wang
// This file is part of MELPLEVELSET, whose distribution is governed by the LICENSE file.
//////////////////////////////////////////////////////////////////////////
#pragma once

#include "Points.h"
#include "PointUtil.h"

namespace Meso{
    //// particle holder
    template<int d>
    class EulerianParticles : public Points{
        Typedef_VectorD(d);
    public:
        
        EulerianParticles() : Points() {}
        Setup_Attribute(x, VectorD, VectorD::Zero()); //// position
        Setup_Attribute(v, VectorD, VectorD::Zero()); //// velocity
        Setup_Attribute(n, VectorD, VectorD::Zero()); //// normal
        Setup_Attribute(vt, VectorD, VectorD::Zero()); //// tangential velocity
        
        //// geometry
        Setup_Attribute(r, real, 0.0); //// radius, inverse of curvature

        //// area(number density?)
        Setup_Attribute(V, real, 1.0); //// volume
        Setup_Attribute(a, real, 1.0); //// control area

        //// physics attribute
        Setup_Attribute(eta, real, 1.0); //// thickness
        Setup_Attribute(c, real, 1.0); //// concentration
        Setup_Attribute(m, real, 1.0); //// mass

        Setup_Attribute(t, int, 0); //// type: 0 for active, 1 for fixed

        void Append(const EulerianParticles& other){
            PointUtil::Append_Vector_Elements<VectorD>(this->xRef(), other.xRef());
            PointUtil::Append_Vector_Elements<VectorD>(this->vRef(), other.vRef());
            PointUtil::Append_Vector_Elements<VectorD>(this->nRef(), other.nRef());
            PointUtil::Append_Vector_Elements<VectorD>(this->vtRef(), other.vtRef());

            PointUtil::Append_Vector_Elements<real>(this->rRef(), other.rRef());

            PointUtil::Append_Vector_Elements<real>(this->VRef(), other.VRef());
            PointUtil::Append_Vector_Elements<real>(this->aRef(), other.aRef());

            PointUtil::Append_Vector_Elements<real>(this->etaRef(), other.etaRef());
            PointUtil::Append_Vector_Elements<real>(this->cRef(), other.cRef());
            PointUtil::Append_Vector_Elements<real>(this->mRef(), other.mRef());

            PointUtil::Append_Vector_Elements<int>(this->tRef(), other.tRef());

            this->size=this->xRef().size();
            PointUtil::Check_Points_Size(*this);
        }
        
    };
}