//////////////////////////////////////////////////////////////////////////
// Initializer of Test MLS
// Copyright (c) (2022-), Hui Wang
// This file is part of MESO, whose distribution is governed by the LICENSE file.
//////////////////////////////////////////////////////////////////////////


#pragma once

#include "Json.h"
#include "Simulator.h"
#include "ImplicitManifold.h"
#include "GridEulerFunc.h"
#include "NeighborSearcher.h"
#include "PointLocalGeometry.h"
#include "LevelSetUtil.h"

#include "IOFunc.h"
#include <vtkTriangle.h>
#include <vtkLine.h>
#include <vtkCellData.h>
#include <vtkCellArray.h>

namespace Meso {
    template<int d>
    class MLSTest : public Simulator {
    public:
        Typedef_VectorD(d);
        Typedef_MatrixD(d);
        using VectorT=Vector<real,d-1>;								////tangential vector type
        using VectorTi=Vector<int,d-1>;								////tangential vector int

        std::shared_ptr<NeighborSearcher<d>> nbs_searcher;
        std::shared_ptr<PointLocalGeometry<d>> local_geom;
        LevelSet<d> levelset;

        EulerianParticles<d> geo_particles;
        EulerianParticles<d> track_particles;
        // EulerianParticles<d> debug_particles;

        void Init(json& j){
            // nbs_searcher=std::make_shared<NeighborKDTree<d> >();
            // local_geom=std::make_shared<PointLocalGeometry<d> > (grid.dx, 2/1.5, nbs_searcher);
            
            // int scale = Json::Value<int>(j, "scale", 32);
            // real side_len = 1.0;
            // real dx = side_len / scale;
            // VectorDi grid_size = scale * MathFunc::Vi<d>(1, 1, 1);
            // grid=Grid<d>(grid_size, dx, VectorD::Zero(), CENTER);
        }
        
        void Output_Mesh_As_VTU(const VertexMatrix<real, d> &verts, const ElementMatrix<d> &elements, const std::string file_name) {
            Typedef_VectorD(d);

            // setup VTK
            vtkNew<vtkXMLUnstructuredGridWriter> writer;
            vtkNew<vtkUnstructuredGrid> unstructured_grid;

            vtkNew<vtkPoints> nodes;
            nodes->Allocate(verts.rows());
            vtkNew<vtkCellArray> cellArray;

            for (int i = 0; i < verts.rows(); i++) {
                Vector<real, d> pos = verts.row(i).template cast<real>();
                Vector3 pos3 = MathFunc::V<3>(pos);
                nodes->InsertNextPoint(pos3[0], pos3[1], pos3[2]);
            }
            unstructured_grid->SetPoints(nodes);

            if constexpr (d == 2) {
                for (int i = 0; i < elements.rows(); i++) {
                    vtkNew<vtkLine> line;
                    line->GetPointIds()->SetId(0, elements(i, 0));
                    line->GetPointIds()->SetId(1, elements(i, 1));
                    cellArray->InsertNextCell(line);
                }
                unstructured_grid->SetCells(VTK_LINE, cellArray);
            }
            else if constexpr (d == 3) {
                for (int i = 0; i < elements.rows(); i++) {
                    vtkNew<vtkTriangle> triangle;
                    triangle->GetPointIds()->SetId(0, elements(i, 0));
                    triangle->GetPointIds()->SetId(1, elements(i, 1));
                    triangle->GetPointIds()->SetId(2, elements(i, 2));
                    cellArray->InsertNextCell(triangle);
                }
                unstructured_grid->SetCells(VTK_TRIANGLE, cellArray);
            }

            writer->SetFileName(file_name.c_str());
            writer->SetInputData(unstructured_grid);
            writer->Write();
        }

        virtual void Output(DriverMetaData& metadata){
                        
            ///// particles
            {
                Info("write geo_particles...");
                std::string particles_name = fmt::format("geo_particles{:04d}.vtu", metadata.current_frame);
                bf::path particles_path = metadata.base_path / bf::path(particles_name);
                VTKFunc::Write_VTU_Particles<d>(geo_particles.xRef(), geo_particles.vRef(), particles_path.string());
            }
            {
                Info("write track_particles...");
                std::string particles_name = fmt::format("track_particles{:04d}.vtu", metadata.current_frame);
                bf::path particles_path = metadata.base_path / bf::path(particles_name);
                VTKFunc::Write_VTU_Particles<d>(track_particles.xRef(), track_particles.vRef(), particles_path.string());
            }
        };

        //can return inf
        virtual real CFL_Time(const real cfl){
            // real dx = velocity.grid.dx;
            // real max_vel = GridEulerFunc::Linf_Norm(velocity);
            return 1e9;
        };

        virtual void Advance(DriverMetaData& metadata){
            Info("Advance at frame #{}, dt={}", metadata.current_frame, metadata.dt);
            real dt = metadata.dt;
            // Advect_Track_Particles();

            Info("Before advect track particle");
            Debug_Levelset_Closest_Point_Distance_To_Single_Sphere();
            Advect_Track_Particles_By_MLS_Projection();
            Debug_Track_Particle_Distance_To_Single_Sphere();
        };

        void Advect_Track_Particles_By_MLS_Projection(){
            nbs_searcher->Update_Points(geo_particles.xRef());
            int max_iter=5;

            
            for(int i=0;i<track_particles.Size();i++){
                
                VectorD pos=track_particles.x(i);
                track_particles.v(i)=pos;//// TOFIX: store old position in v for debug!!!
                VectorD search_pos=LevelSetUtil<d>::Closest_Point_With_Iterations(levelset, pos, 5);

                for(int iter=0;iter<max_iter;iter++){
                    MatrixD local_frame=local_geom->Local_Frame(search_pos);
                    LeastSquares::MLSPointSet<d-1> mls;
                    Array<int> nbs;
                    int n=local_geom->Fit_Local_Geometry_MLS(search_pos,local_frame, nbs, mls);
                    if(n<d*2) break;
                    // VectorX mls_c=local_geom->Fit_Local_Geometry_MLS(search_pos,local_frame);

                    VectorD u=pos-search_pos;
                    VectorD local_pos=local_geom->Local_Coords(u,local_frame);
                    VectorD new_local_pos=local_geom->Closest_Point_To_Local_MLS(local_pos, mls, 5);
                    search_pos=local_geom->Unproject_To_World(new_local_pos, local_frame)+search_pos;

                    // std::cout<<"iter="<<iter<<
                    //         ", d(p,s)="<<(search_pos-pos).norm()<<
                    //         ", d(p,s)-d(p,i)="<<(search_pos-pos).norm()-d_pi<<std::endl;
                }

                if((search_pos-pos).norm()>0.2){
                    track_particles.x(i)=search_pos;
                }else{
                    track_particles.x(i)=search_pos;
                }
            }
        };

        void Debug_Levelset_Closest_Point_Distance_To_Single_Sphere(){
            //// for debug
            VectorD xc=levelset.phi.grid.Center();////c
            xc[1]=1*0.75f;
            real r=1*0.15f;

            for(int i=0;i<track_particles.Size();i++){
                VectorD pos=track_particles.x(i);
                VectorD search_pos=LevelSetUtil<d>::Closest_Point_With_Iterations(levelset, pos, 5);
                
                real d_pi=(pos-xc).norm()-r;
                d_pi=abs(d_pi);
                std::cout<<"#"<<i<<
                    ", d(p,i)="<<d_pi<<
                    ", d(p,s)="<<(search_pos-pos).norm()<<
                    ", d(p,s)-d(p,i)="<<(search_pos-pos).norm()-d_pi<<std::endl;
            }
        }

        void Debug_Track_Particle_Distance_To_Single_Sphere(){
            //// for debug
            VectorD xc=levelset.phi.grid.Center();////c
            xc[1]=1*0.75f;
            real r=1*0.15f;

            for(int i=0;i<track_particles.Size();i++){
                VectorD pos=track_particles.v(i);
                VectorD search_pos=track_particles.x(i);

                real d_pi=(pos-xc).norm()-r;
                d_pi=abs(d_pi);
                std::cout<<"#"<<i<<
                    ", d(p,i)="<<d_pi<<
                    ", d(p,s)="<<(search_pos-pos).norm()<<
                    ", d(p,s)-d(p,i)="<<(search_pos-pos).norm()-d_pi<<std::endl;
            }
        }

        void Advect_Track_Particles(){
            nbs_searcher->Update_Points(geo_particles.xRef());
            int max_iter=5;

            //// for debug
            VectorD xc=levelset.phi.grid.Center();////c
            xc[1]=1*0.75f;
            real r=1*0.15f;
            
            ////i for interface, s for search point

            for(int i=0;i<track_particles.Size();i++){
                VectorD pos=track_particles.x(i);////i
                VectorD search_pos=LevelSetUtil<d>::Closest_Point_With_Iterations(levelset, pos, 5);
                
                real d_pi=(pos-xc).norm()-r;
                std::cout<<"ref d(p,i)="<<d_pi<<std::endl;
                d_pi=abs(d_pi);
                std::cout<<"iter="<<-1<<", d(p,s)="<<(search_pos-pos).norm()<<", d(p,s)-d(p,i)="<<(search_pos-pos).norm()-d_pi<<std::endl;

                for(int iter=0;iter<max_iter;iter++){
                    MatrixD local_frame=local_geom->Local_Frame(search_pos);
                    LeastSquares::MLSPointSet<d-1> mls;
                    int n=local_geom->Fit_Local_Geometry_MLS(search_pos,local_frame, mls);
                    if(n<d*2) break;
                    // VectorX mls_c=local_geom->Fit_Local_Geometry_MLS(search_pos,local_frame);

                    VectorD u=pos-search_pos;
                    VectorD local_pos=local_geom->Local_Coords(u,local_frame);
                    VectorD new_local_pos=local_pos; new_local_pos[d-1]=0;
                    VectorD new_pos=local_geom->Unproject_To_World(new_local_pos, local_frame)+search_pos;
                    search_pos=new_pos;
                    std::cout<<"iter="<<iter<<
                        ", d(p,s)="<<(search_pos-pos).norm()<<
                        ", d(p,s)-d(p,i)="<<(search_pos-pos).norm()-d_pi<<std::endl;

                    std::cout<<"local d(p,s)"<<(new_local_pos-local_pos).norm()<<
                        ", d(p,s)-d(p,i)="<<(new_local_pos-local_pos).norm()-d_pi<<std::endl;
                }

                track_particles.x(i)=search_pos;
            }
        }


        void Reseed_Geo_Particles(){
            Array<VectorD> temp_x;
            LevelSetUtil<d>::Sample_On_Levelset_Surface(temp_x, levelset, 40, 1, 10);
            //// for debug
            VectorD xc=levelset.phi.grid.Center();////c
            xc[1]=1*0.75f;
            real r=1*0.15f;

            geo_particles.Resize(temp_x.size());
            for(int i=0;i<temp_x.size();i++){
                geo_particles.x(i)=(temp_x[i]-xc).normalized()*r+xc;
                geo_particles.v(i)=VectorD::Zero();
            }
        }

        void Reseed_Track_Particles(){
            Array<VectorD> temp_x;
            LevelSetUtil<d>::Sample_On_Levelset_Narrowband(temp_x, levelset, 0, 1);
            track_particles.Resize(temp_x.size());
            for(int i=0;i<temp_x.size();i++){
                track_particles.x(i)=temp_x[i];
                track_particles.v(i)=VectorD::Zero();
            }
        }

        // void Reseed_Debug_Particles(unsigned int size){
        //     debug_particles.Resize(size);
        //     for(int i=0;i<size;i++){
        //         debug_particles.x(i)=VectorD::Zero();
        //         debug_particles.v(i)=VectorD::Zero();
        //     }
        // }

    };
}