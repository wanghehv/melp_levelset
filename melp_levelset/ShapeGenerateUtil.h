#pragma once

#include "Random.h"
#include "ImplicitManifold.h"

namespace Meso{
    template<int d>
    class ShapeGenerateUtil{
        Typedef_VectorD(d);
    public:
        static SphereShape<d> Random_Sphere(real min_radius, real max_radius, VectorD min_corner, VectorD max_corner){
            real radius=Random::Uniform(min_radius,max_radius);

            VectorD min_pos=min_corner+VectorD::Ones()*radius;
            VectorD max_pos=max_corner-VectorD::Ones()*radius;
            VectorD center=Random::Uniform_In_Box(min_pos, max_pos);

            return SphereShape<d>(center,radius);
        }


    };
}