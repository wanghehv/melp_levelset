import math
import sys

import matplotlib.pyplot as plt
from matplotlib import *
import numpy as np


file_name='mls_test.data'





dx_arr=[[],[]]
# dx_levelset_closest_points=[]
# dx_mls_closest_points=[]
dx_i=-1

try:
    data = open(file_name, "r")
    lines=data.readlines()
    for li, line in enumerate(lines):
        words=line.split()
        if len(words)<1:
            continue
        print(words)
        pi=int(words[0][1:-1])
        dx_str=(words[-1].split('='))[-1]
        dx=float(dx_str)

        if pi==0:
            dx_i=dx_i+1
        dx_arr[dx_i].append(abs(dx))
finally:
    data.close()

fig=plt.figure()
plt.semilogy(np.arange(len(dx_arr[0])),dx_arr[0], label="use levelset closest points")
plt.semilogy(np.arange(len(dx_arr[1])),dx_arr[1], label="use mls closest points")

plt.legend()
plt.xlabel("sample id")
plt.ylabel("|d-d0|")
plt.show()