# This code is called when instances of this SOP cook.
node = hou.pwd()
geo = node.geometry()
# Get path and current frame
path = hou.pwd().evalParm("path")
frame = int(hou.frame())
# Add attributes
eta_attr = geo.addAttrib(hou.attribType.Point, "eta", (0.0)) # float
normal_attr = geo.addAttrib(hou.attribType.Point, "n", (0.0, 0.0, 0.0), True, True)

# type size
size_int=4
size_float=4
size_double=8

# Import modules
import struct
import os
# Read ps${frame}_num.dat to num
## ps${frame}_num.dat is a binary file containing one integer,
## which denotes the number of points
file = open(path + '/ps' + str(frame) + '_num.dat', mode = 'rb')
data = file.read()
file.close()

psize = struct.unpack('i', data[0:size_int])
psize = psize[0]
data=data[size_int:]

ftype = str(psize*3) + 'f'
pos_arr = struct.unpack(ftype, data[:psize*3*size_float])
data=data[psize*3*size_float:]

ftype = str(psize*3) + 'f'
normal_arr = struct.unpack(ftype, data[:psize*3*size_float])
data=data[psize*3*size_float:]

ftype = str(psize) + 'f'
eta_arr = struct.unpack(ftype, data[:psize*size_float])
data=data[psize*size_float:]

# Add points to Houdini geometry
for i in range(0,psize):
    point = geo.createPoint()
    ## Set position
    point.setPosition((pos_arr[i*3],pos_arr[i*3+1],pos_arr[i*3+2]))
    ## Set normal
    point.setAttribValue(normal_attr, (normal_arr[i*3],normal_arr[i*3+1],normal_arr[i*3+2]))
    ## Set eta
    point.setAttribValue(eta_attr, (float(eta_arr[i])))
