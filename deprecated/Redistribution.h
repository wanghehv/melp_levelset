//////////////////////////////////////////////////////////////////////////
// Surface Tension
// Copyright (c) (2022-), Hui Wang
// This file is part of MESO, whose distribution is governed by the LICENSE file.
//////////////////////////////////////////////////////////////////////////
#pragma once

/*
#include "Constants.h"
#include "LevelSet.h"
#include "EulerianParticles.h"
#include "Neighbors.h"
#include "SPH_Utils.h"

namespace Meso {
    template<int d>
    class Redistribution {
    public:
        Typedef_VectorD(d); Typedef_MatrixD(d);

        SPH_Utils<d> sph;
        std::shared_ptr<PointLocalGeometry<d>> local_geom;
        std::shared_ptr<NeighborSearcher<d>> nbs_searcher;
        Array<Neighbors> nbs_info;

        Redistribution(std::shared_ptr<PointLocalGeometry<d>> _local_geom){
            local_geom=_local_geom;
            nbs_searcher=local_geom->nbs_searcher;
        }

        void Redistribute_Particles(real dt, EulerianParticles<d>& particles){
            
            Register_Nbs(particles);
            Compute_Density(particles);
            Pressure_Force(dt,particles);
        }

        // void Register_Nbs(bool init = false) {
        void Register_Nbs(EulerianParticles<d>& particles) {
            // if (init) b_particles.Update_Searcher();
            // particles.Update_Searcher();
            nbs_searcher->Update_Points(particles.xRef());
            if (nbs_info.size() != particles.Size()) nbs_info.resize(particles.Size());
#pragma omp parallel for
            for (int i = 0; i < particles.Size(); i++) {
                // real r = Radius(i);
                real r = local_geom->v_r;
                Array<int> nbs; nbs_searcher->Find_Nbs(particles.x(i), r, nbs);
                Array<int> b_nbs; // b_particles.nbs_searcher->Find_Nbs(particles.x(i), r, b_nbs);
                nbs_info[i].set(nbs, b_nbs, r);
            }
        }

        void Compute_Density(EulerianParticles<d>& particles) {
#pragma omp parallel for
            for (int i = 0; i < particles.Size(); i++) {
                particles.rho(i) = sph.Sum([&](int idx)->real {return particles.m(idx); },
                    [&](int idx)->VectorD {return particles.x(i)-particles.x(idx); },
                    nbs_info[i].nbs, nbs_info[i].r);
                //// no boundary particle, no volume
                // particles.rho(i) += sph.Sum([&](int idx)->real {return particles.m(i); },
                //     [&](int idx)->VectorD {return particles.x(i) - b_particles.x(idx); },
                //     nbs_info[i].b_nbs, nbs_info[i].r);
                particles.V(i) = particles.m(i) / particles.rho(i);
            }
            std::cout<<"rho "<<particles.rho(0)<<std::endl;
            std::cout<<"V "<<particles.V(0)<<std::endl;
        }

        void Compute_Pressure_WCSPH(EulerianParticles<d>& particles) {
            Typedef_VectorD(d); Typedef_MatrixD(d);
            real rho_0 = 0.;
            for (int i = 0; i < particles.Size(); i++) {
                rho_0+=particles.rho(i);
            }
            if(particles.Size()>0) rho_0=rho_0/particles.Size();
            // real rho_0 = 1.;
#pragma omp parallel for
            for (int i = 0; i < particles.Size(); i++) {
                particles.p(i) = 1. * (MathFunc::Power3(particles.rho(i) / rho_0) - 1.);
                particles.p(i) = std::max<real>(0., particles.p(i));
            }
            std::cout<<"p "<<particles.p(0)<<std::endl;
        }

        void Compute_Pressure(const real dt, EulerianParticles<d>& particles) {
			// Compute_Pressure_IISPH(dt);
			Compute_Pressure_WCSPH(particles);
		}

		void Pressure_Force(const real dt, EulerianParticles<d>& particles) {
			Compute_Pressure(dt,particles);
            std::cout<<"x "<<particles.x(0).transpose()<<std::endl;
#pragma omp parallel for
			for (int i = 0; i < particles.Size(); i++) {
				VectorD grad_p = sph.Grad([&](int idx)->real {return particles.p(idx) - particles.p(i); },
					[&](int idx)->VectorD {return particles.x(i) - particles.x(idx); }, 
					[&](int idx)->real {return particles.V(idx); }, 
					nbs_info[i].nbs, nbs_info[i].r);

				// particles.acc(i) += -1./particles.rho(i) * grad_p;
				// particles.v(i) += -1./particles.rho(i) * grad_p * dt;
				particles.x(i) += -1./particles.rho(i) * grad_p * dt;
			}
            // std::cout<<"x "<<particles.x(0).transpose()<<std::endl;
		}
    };
}
*/