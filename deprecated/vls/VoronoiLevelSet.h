//////////////////////////////////////////////////////////////////////////
// Voronoi LevelSet
// Copyright (c) (2022-), Hui Wang
// This file is part of MELPLEVELSET, whose distribution is governed by the LICENSE file.
//////////////////////////////////////////////////////////////////////////

#include "LevelSet.h"
#include <map>

namespace Meso{
    //// Voronoi LevelSet following Saye "The Voronoi Implicit Interface Method with Applications to Multiphase Fluid Flow and Multiscale Modelling of Foam Dynamics"
    template<int d>
    class VoronoiLevelSet
    {
        Typedef_VectorD(d);
    public:
        LevelSet<d> v_levelset; 
        //// negative in bubble, positive outsides
        //// voronoi like levelset, positive in bubble & air, negative in fluid,
        std::map<int, ParticleLevelSet<d>> e_levelset_map; //// defined as phi(0)=phi_v(epsilon)
        Field<real,d> buffer;
        Field<int,d> indicator;
        int e_off; //// offset for epsilon levelset idx
        
        real epsilon;
        
        VoronoiLevelSet() {}
        VoronoiLevelSet(const Grid<d> _grid, real _epsilon) { Init(_grid, _epsilon); }

        void Init(const Grid<d> _grid, real _epsilon){
            Assert(_grid.Is_Unpadded(), "Levelset::Init _grid={}, padding not allowed", _grid);
            v_levelset=LevelSet<d>(_grid);
            indicator.Init(_grid, -1);
            buffer.Init(_grid, std::numeric_limits<real>::max());
            epsilon=_epsilon;
            e_off=0;
        }

        //// add a new epsilon levelset
        int AddPhase(){
            int i=e_off;
            e_levelset_map.insert(std::make_pair(i,ParticleLevelSet<d>(v_levelset.phi.grid)));
            e_off++;
            // e_levelsets.push_back(LevelSet<d>(v_levelset.phi.grid));
            return i;
        }

        //// bubble phase, positive inside, elevelset.phi=0 is the epsilon interface
        int AddBubblePhase(const ImplicitManifold<d> &geom){
            int i=AddPhase();
            auto iter = e_levelset_map.find(i);  
            if(iter != e_levelset_map.end()){
                iter->second.levelset.phi.Exec_Nodes(
                    [&](const VectorDi cell) {
                        //// geom.Phi is negative inside
                        iter->second.phi(cell)=geom.Phi(iter->second.levelset.phi.grid.Position(cell));
                        // iter->second.levelset.phi(cell)=-geom.Phi(v_levelset.phi.grid.Position(cell))-epsilon;
                    }
                );
            }else{
                std::cerr<<"Voronoi LevelSet::AddPhase error: cannot find the levelset after insertion"<<std::endl;
            }
            // e_levelsets[levelset_id].phi.Calc_Nodes(
            //     [&](const VectorDi cell) {
            //         return geom.Phi(_grid.Position(cell));
            //     }
            // );
            // e_levelsets[levelset_id].phi.Exec_Nodes(
            //     [&](const VectorDi cell) {
            //         e_levelsets[levelset_id].phi(cell)=geom.Phi(_grid.Position(cell))-epsilon;
            //     }
            // );
            return i;
        }

        int RemovePhase(int i){
            //// To implement
            // std::cerr << "Voronoi LevelSet::RemovePhase error: unimplemented"<<std::endl;
            auto iter = e_levelset_map.find(i);  
            if(iter != e_levelset_map.end()){
                e_levelset_map.erase(i);
            }else{
                std::cout<<"Voronoi LevelSet::RemovePhase warning: cannot find the levelset "<<i<<std::endl;
            }
        }


        
        //// reconstruct voronoi levelset and indicator based on epsilon levelsets
        void Reconstruct(){
            indicator.Fill(-1);
            buffer.Fill(-1e8);
            //// update indicator where Chi(x)=argmax_i(phi_i(x))
            for (auto itr = e_levelset_map.begin(); itr != e_levelset_map.end(); ++itr) {
                itr->second.levelset.phi.Exec_Nodes(
                    [&](const VectorDi cell) {
                        if(itr->second.phi(cell)<buffer(cell)){
                            buffer(cell)=itr->second.phi(cell);
                            indicator(cell)=itr->first;
                        }
                    }
                );
            }

            //// TODO: update voronoi levelset
            //// update voronoi levelset
            std::cerr << "Voronoi LevelSet::Reconstruct error: unimplemented"<<std::endl;

        }

    };
}
