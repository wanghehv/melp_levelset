//////////////////////////////////////////////////////////////////////////
// Fluid Euler with Voronoi Level Set
// Copyright (c) (2022-), Hui Wang
// This file is part of MESO, whose distribution is governed by the LICENSE file.
//////////////////////////////////////////////////////////////////////////
#pragma once
#include "Json.h"
#include "Simulator.h"
#include "ParticleLevelSet.h"
#include "VoronoiLevelSet.h"
#include "Advection.h"
#include "SurfaceTension.h"
#include "MarchingCubes.h"
#include "PointLocalGeometry.h"
#include "GridEulerFunc.h"
#include "Interpolator.h"
#include "GridUtil.h"
#include "IOFunc.h"

#include "LevelSetUtil.h"
#include "Kernel.h"

#include <vtkTriangle.h>
#include <vtkLine.h>
#include <vtkCellData.h>
#include <vtkCellArray.h>



namespace Meso {
    template<int d>
    class FluidVLS : public Simulator {
        Typedef_VectorD(d);
        Typedef_MatrixD(d);
    public:

        FaceField<real, d> velocity;
        VoronoiLevelSet<d> vls;
        Array<FaceField<real, d>> velocity_fields;
        Array<Field<real, d>> color_fields;
        std::shared_ptr<NeighborSearcher<d>> nbs_searcher;
        std::shared_ptr<PointLocalGeometry<d>> local_geom;

        bool use_constant_velocity;

        //// LevelSet
        void Init(json& j){
            int scale = Json::Value<int>(j, "scale", 32);
            real side_len = 1.0;
            real dx = side_len / scale;
            use_constant_velocity = Json::Value<int>(j, "use_constant_velocity", 0);
            // curvature_threshold=velocity.grid.dx*2;
            // nbs_searcher=std::make_shared<NeighborKDTree<d> >();
            // local_geom=std::make_shared<PointLocalGeometry<d> > (dx, 2, nbs_searcher);
        }

        
        void Output_Mesh_As_VTU(const VertexMatrix<real, d> &verts, const ElementMatrix<d> &elements, const std::string file_name) {
            Typedef_VectorD(d);

            // setup VTK
            vtkNew<vtkXMLUnstructuredGridWriter> writer;
            vtkNew<vtkUnstructuredGrid> unstructured_grid;

            vtkNew<vtkPoints> nodes;
            nodes->Allocate(verts.rows());
            vtkNew<vtkCellArray> cellArray;

            for (int i = 0; i < verts.rows(); i++) {
                Vector<real, d> pos = verts.row(i).template cast<real>();
                Vector3 pos3 = MathFunc::V<3>(pos);
                nodes->InsertNextPoint(pos3[0], pos3[1], pos3[2]);
            }
            unstructured_grid->SetPoints(nodes);

            if constexpr (d == 2) {
                for (int i = 0; i < elements.rows(); i++) {
                    vtkNew<vtkLine> line;
                    line->GetPointIds()->SetId(0, elements(i, 0));
                    line->GetPointIds()->SetId(1, elements(i, 1));
                    cellArray->InsertNextCell(line);
                }
                unstructured_grid->SetCells(VTK_LINE, cellArray);
            }
            else if constexpr (d == 3) {
                for (int i = 0; i < elements.rows(); i++) {
                    vtkNew<vtkTriangle> triangle;
                    triangle->GetPointIds()->SetId(0, elements(i, 0));
                    triangle->GetPointIds()->SetId(1, elements(i, 1));
                    triangle->GetPointIds()->SetId(2, elements(i, 2));
                    cellArray->InsertNextCell(triangle);
                }
                unstructured_grid->SetCells(VTK_TRIANGLE, cellArray);
            }

            writer->SetFileName(file_name.c_str());
            writer->SetInputData(unstructured_grid);
            writer->Write();
        }

        virtual void Output(DriverMetaData& metadata){
            //// velocity
            Info("write velocity...");
            std::string vts_name = fmt::format("vts{:04d}.vts", metadata.current_frame);
            bf::path vtk_path = metadata.base_path / bf::path(vts_name);
            VTKFunc::Write_VTS(velocity, vtk_path.string());
            
            
            for (auto itr = vls.e_levelset_map.begin(); itr != vls.e_levelset_map.end(); ++itr) {

                Info("write surface...");
                VertexMatrix<real, d> verts; ElementMatrix<d> elements;
                Marching_Cubes<real, d, HOST>(verts, elements, itr->second.levelset.phi);
                std::string surface_name = fmt::format("surface{}_{:04d}.vtu", itr->first, metadata.current_frame);
                bf::path surface_path = metadata.base_path / bf::path(surface_name);
                Output_Mesh_As_VTU(verts, elements, surface_path.string());

                Info("write levelset...");
                std::string vts_name = fmt::format("levelset{}_{:04d}.vts", itr->first, metadata.current_frame);
                bf::path vtk_path = metadata.base_path / bf::path(vts_name);
                VTKFunc::Write_VTS(itr->second.levelset.phi, vtk_path.string());

                {
                    Info("write particles...");
                    std::string particles_name = fmt::format("particles{}_{:04d}.vtu", itr->first, metadata.current_frame);
                    bf::path particles_path = metadata.base_path / bf::path(particles_name);
                    VTKFunc::Write_VTU_Particles<d>(itr->second.particles.xRef(), itr->second.particles.vRef(), particles_path.string());
                }

                {
                    std::string vts_name = fmt::format("velocity_{:04d}.vts", metadata.current_frame);
                    bf::path vtk_path = metadata.base_path / bf::path(vts_name);
                    VTKFunc::Write_VTS(velocity, vtk_path.string());
                }

                // {
                //     Info("write particles normal...");
                //     std::string particles_name = fmt::format("p_n{}_{:04d}.vtu", itr->first, metadata.current_frame);
                //     bf::path particles_path = metadata.base_path / bf::path(particles_name);
                //     VTKFunc::Write_VTU_Particles<d>(itr->second.particles.xRef(), itr->second.particles.nRef(), particles_path.string());
                // }

                // {
                //     Info("write levelset normal...");
                //     std::string particles_name = fmt::format("levelset_n{}_{:04d}.vtu", itr->first, metadata.current_frame);
                //     bf::path particles_path = metadata.base_path / bf::path(particles_name);
                    
                //     Array<VectorD> position; Array<VectorD> normal;
                //     velocity.grid.Iterate_Nodes(
                //         [&](const VectorDi cell) {
                //             VectorD pos=velocity.grid.Position(cell);
                //             position.push_back(pos);
                //             normal.push_back(itr->second.levelset.Normal(pos));
                //         }
                //     );
                //     // VTKFunc::Write_VTS(normal, vtk_path.string());
                //     VTKFunc::Write_VTU_Particles<d>(position, normal, particles_path.string());
                // }
            }

            for(int i=0;i<color_fields.size();i++){
                Info("write color field...");
                std::string vts_name = fmt::format("color{}_{:04d}.vts", i,metadata.current_frame);
                bf::path vtk_path = metadata.base_path / bf::path(vts_name);
                VTKFunc::Write_VTS(color_fields[i], vtk_path.string());
            }

            for(int i=0;i<velocity_fields.size();i++){
                Info("write velocity field...");
                std::string vts_name = fmt::format("velocity{}_{:04d}.vts", i,metadata.current_frame);
                bf::path vtk_path = metadata.base_path / bf::path(vts_name);
                VTKFunc::Write_VTS(velocity_fields[i], vtk_path.string());
            }
        };

        //can return inf
        virtual real CFL_Time(const real cfl){
            real dx = velocity.grid.dx;
            real max_vel = GridEulerFunc::Linf_Norm(velocity);
            return dx * cfl / max_vel;
        };

        virtual void Advance(DriverMetaData& metadata){
            real dt = metadata.dt;
            AdvectVLS(dt);
            CorrectVLS();
            if(metadata.current_frame%5==4)
                ReseedVLS();
        }
    
    //// 
    public:

        void AdvectVLS(real dt){
            if(use_constant_velocity){
                Info("advect with constant velocity field");
                for (auto itr = vls.e_levelset_map.begin(); itr != vls.e_levelset_map.end(); ++itr) {
                    //// advect particle
                    #pragma omp parallel for
                    for (int i = 0; i < itr->second.particles.Size(); i++) {
                        VectorD pos=itr->second.particles.x(i);
                        VectorD v=IntpLinear::Face_Vector(velocity,pos);
                        itr->second.particles.x(i) += dt * v;
                        itr->second.particles.v(i) = v;
                    }

                    //// advect e_levelsets
                    Field<real, d> temp_phi = itr->second.levelset.phi;
                    Advection<IntpLinearClamp>::Advect(dt, temp_phi, itr->second.levelset.phi, velocity);
                    itr->second.levelset.phi=temp_phi;
                    itr->second.levelset.Fast_Marching(-1);
                }
            }
            else{
                Info("advect with surface tension");
                velocity.Fill(0);
                for (auto itr = vls.e_levelset_map.begin(); itr != vls.e_levelset_map.end(); ++itr) {
                    Grid<d>& grid=itr->second.levelset.phi.grid;
                    
                    grid.Exec_Faces(
                        [&](const int axis, const VectorDi face) {
                            velocity_fields[itr->first](axis,face)=0.0f;
                        }
                    );
                    Info("Enforce_Explicit_Surface_Tension");
                    // Enforce_Explicit_Surface_Tension(dt, velocity_fields[itr->first], itr->second.levelset);
                    Enforce_Explicit_Surface_Tension(dt, velocity, itr->second.levelset);
                }

                for (auto itr = vls.e_levelset_map.begin(); itr != vls.e_levelset_map.end(); ++itr) {
                    Grid<d>& grid=itr->second.levelset.phi.grid;
                    //// advect particle
                    Info("advect particles");
                    #pragma omp parallel for
                    for (int i = 0; i < itr->second.particles.Size(); i++) {
                        VectorD x=itr->second.particles.x(i);
                        // VectorD v=IntpLinear::Face_Vector(velocity_fields[itr->first],x);
                        VectorD v=IntpLinear::Face_Vector(velocity,x);
                        VectorD nx=x+v*dt;
                        if(GridUtil<d>::Valid(grid,nx)){
                            itr->second.particles.x(i) = nx;
                            itr->second.particles.v(i) = v;
                        }
                    }

                    Info("advect levelset");
                    //// advect e_levelsets
                    Field<real, d> temp_phi = itr->second.levelset.phi;
                    // Advection<IntpLinearClamp>::Advect(dt, temp_phi, itr->second.levelset.phi, velocity_fields[itr->first]);
                    
                    Advection<IntpLinearClamp>::Advect(dt, temp_phi, itr->second.levelset.phi, velocity);
                    itr->second.levelset.phi=temp_phi;
                    itr->second.levelset.Fast_Marching(-1);
                }
            }
        }

        void CorrectVLS(){
            // for (auto itr = vls.e_levelset_map.begin(); itr != vls.e_levelset_map.end(); ++itr){
            //     Correct_Particles_Normal(itr->second.particles, itr->second.levelset);
            // }

            for (auto itr = vls.e_levelset_map.begin(); itr != vls.e_levelset_map.end(); ++itr){
                ////  particle correction
                std::cout<<"correct vls "<<itr->first<<std::endl;
                Correct_Levelset_With_Particles(itr->second.levelset, itr->second.particles);
                itr->second.levelset.Fast_Marching(-1);
                //// correct twice for better accuracy at interface
                Correct_Levelset_With_Particles(itr->second.levelset, itr->second.particles, true);
            }

            //// push particles onto levelset ridge
            Move_Particles_To_Voronoi_Levelset();////remove temporarily?

            //// redistribute
            // for (auto itr = vls.e_levelset_map.begin(); itr != vls.e_levelset_map.end(); ++itr){
            //     Enforce_Redistribution(0.75, itr->second.particles);
            // }
            // Move_Particles_To_Voronoi_Levelset();

            color_fields.clear();
            //// reset e_levelset based on particles TODO: levelset errors
            // for (auto itr = vls.e_levelset_map.begin(); itr != vls.e_levelset_map.end(); ++itr) {
            //     std::cout<<"reconstruct leveset "<<itr->first<<std::endl;
            //     Reconstruct_Levelset_With_Voronoi_Particles(itr->second.levelset, itr->second.particles, vls.epsilon);

            //     Reconstruct_Levelset_With_Voronoi_Particles_New(itr->second.levelset, itr->second.particles, vls.epsilon);
            // }
        }

    void ReseedVLS(){
        for (auto itr = vls.e_levelset_map.begin(); itr != vls.e_levelset_map.end(); ++itr){
            Reseed_Particles(itr->second.particles, itr->second.levelset);
            Info("reseed particles for {}", itr->first);
        }
        Move_Particles_To_Voronoi_Levelset();
        Info("move particles to voronoi levelset");
    }

    //// correct vls
    public:
        // void Correct_Particles_Normal(EulerianParticles<d>& particles, LevelSet<d>& levelset){
        //     nbs_searcher->Update_Points(particles.xRef());
        //     #pragma omp parallel for
        //     for(int i=0;i<particles.Size();i++){
        //         VectorD pos=particles.x(i);
        //         MatrixD local_frame=local_geom->Local_Frame(pos);
        //         VectorD normal=local_frame.col(d-1);
        //         // if(normal.dot(levelset.Normal(pos)<0))
        //         if(normal.dot(particles.n(i))<-0.01)
        //             normal=-normal;
        //         particles.n(i)=normal;
        //     }
        // }

        void Correct_Levelset_With_Particles(LevelSet<d>& levelset,  EulerianParticles<d>& particles, bool use_default=false){
            Grid<d> grid=levelset.phi.grid;
            nbs_searcher->Update_Points(particles.xRef());
            real one_over_dx = (real)1 / grid.dx; real one_over_two_dx = (real).5 * one_over_dx;
            real nbs_radius=2*grid.dx;

            real curvature_threshold=one_over_two_dx*0.5f; //// one_over_two_dx is better than one_over_dx
            real bias_threshold=grid.dx;
            
            // real bias_threshold=2*grid.dx;

            levelset.phi.Exec_Nodes(
                [&](const VectorDi cell) {
                    int min_sign=levelset.phi(cell)>=0?1:-1;
                    real phi=levelset.phi(cell);
                    real min_dist=1e8;

                    if(use_default)
                        min_dist=abs(levelset.phi(cell));
                    
                    VectorD cx=grid.Position(cell);
                    real curvature=levelset.Curvature(cx);
                    bool change_sign=true;
                    if(abs(curvature)>curvature_threshold)
                        change_sign=false;

                    Array<int> nbs; 
                    nbs_searcher->Find_Nbs(cx,nbs_radius,nbs);
                    for (int i = 0; i < nbs.size(); i++) {
                        VectorD px=particles.x(nbs[i]);
                        VectorD rel=cx-px;

                        real d=rel.norm();
                        real sign=(phi>=0?1:-1);
                        // if(change_sign) sign=rel.dot(particles.n(nbs[i]))<0?-1:1;
                        if(change_sign) sign=rel.dot(levelset.Normal(px))<0?-1:1;
                        if(d<min_dist && abs(phi-sign*d)<bias_threshold){
                        // if(d<min_dist){
                            min_dist=d;
                            min_sign=sign;
                        }
                    }
                    if(min_dist!=1e8)
                        levelset.phi(cell)=min_sign*min_dist;
                }
            );
        }

        void Correct_Levelset_With_Particles_Keep_Sign(LevelSet<d>& levelset,  EulerianParticles<d>& particles){
            Grid<d> grid=levelset.phi.grid;
            nbs_searcher->Update_Points(particles.xRef());
            levelset.phi.Exec_Nodes(
                [&](const VectorDi cell) {
                    int sign=levelset.phi(cell)>=0?1:-1;
                    real min_dist=abs(levelset.phi(cell));
                    // real min_dist=1e8;

                    VectorD cx=grid.Position(cell);
                    Array<int> nbs; 
                    nbs_searcher->Find_Nbs(cx,2*grid.dx,nbs);
                    for (int i = 0; i < nbs.size(); i++) {
                        VectorD px=particles.x(nbs[i]);
                        VectorD rel=cx-px;
                        if(rel.norm()<min_dist){
                            min_dist=rel.norm();
                            // sign=rel.dot(particles.n(nbs[i]))<0?-1:1;
                        }
                        // min_dist=min(min_dist,(cx-px).norm());
                    }
                    levelset.phi(cell)=sign*min_dist;
                }
            );
        }

        void Move_Particles_To_Voronoi_Levelset(){
            //// for all region
            for (auto itr = vls.e_levelset_map.begin(); itr != vls.e_levelset_map.end(); ++itr) {
                //// for all particles
                for(int p_i=0;p_i<itr->second.particles.Size();p_i++){
                    VectorD x_i=itr->second.particles.x(p_i);
                    real phi_i=itr->second.levelset.Phi(x_i);

                    //// find the levelset that is closest to the current levelset
                    real closest_pls_dist=1e8;
                    int closest_pls_id=-1;
                    auto closest_itr=vls.e_levelset_map.begin();
                    for (auto jtr = vls.e_levelset_map.begin(); jtr != vls.e_levelset_map.end(); ++jtr) {
                        if(jtr->first==itr->first) continue;
                        real phi_j=jtr->second.levelset.Phi(x_i);
                        if(abs(phi_i-phi_j)<closest_pls_dist){
                            closest_pls_dist=abs(phi_i-phi_j);
                            closest_pls_id=jtr->first;
                            closest_itr=jtr;
                        }
                    }

                    //// push the particle to the ridge point of levelset_i and levelset_j
                    auto _Closest_Ridge_Point=[&](VectorD pos, const LevelSet<d>& levelset_i, const LevelSet<d>& levelset_j){
                        VectorD grad_i=levelset_i.Gradient(pos);
                        VectorD grad_j=levelset_j.Gradient(pos);
                        VectorD normal=(grad_i-grad_j).normalized();

                        real phi_i=levelset_i.Phi(pos);
                        real phi_j=levelset_j.Phi(pos);
                        return pos-normal*(phi_i-phi_j)*0.5f;
                    };

                    auto _Closest_Ridge_Point_With_Iterations=[&](VectorD pos, const LevelSet<d>& levelset_i, const LevelSet<d>& levelset_j, const int max_iter=5){
                        VectorD intf_pos = pos;
                        for (int i = 0; i < max_iter; i++) {
                            intf_pos = _Closest_Ridge_Point(intf_pos,levelset_i,levelset_j);
                            if (levelset_i.Phi(intf_pos)-levelset_j.Phi(intf_pos) < (real)0)return intf_pos;
                        }
                        return intf_pos;
                    };

                    // VectorD nx_i=_Closest_Ridge_Point(x_i, itr->second.levelset,closest_itr->second.levelset);
                    VectorD nx_i=_Closest_Ridge_Point_With_Iterations(x_i, itr->second.levelset,closest_itr->second.levelset);
                    itr->second.particles.x(p_i)=nx_i;
                }//// end of for(int p_i...)
            }//// end of for (auto itr...)
        }



        void Reconstruct_Levelset_With_Voronoi_Particles(LevelSet<d>& levelset, const EulerianParticles<d>& particles, const real epsilon){

            //// flood fill with particle boundary to find inside cells
            Field<int,d> psi_P(levelset.phi.grid, -1); //// cell with particles are marked
            #pragma omp parallel for 
            for(int i=0;i<particles.Size();i++){
                VectorD x=particles.x(i);
                VectorDi cell=GridUtil<d>::Cell_Coord(levelset.phi.grid,x);
                #pragma omp critical
                {psi_P(cell)=1;}
            }

            //// 1. flood fill by particles
            Field<real,d> color(levelset.phi.grid,-1);
            LevelSetUtil<d>::Flood_Fill(color,levelset,psi_P,1,0,-1);
            color_fields.push_back(color);

            //// 2. correct levelset and rebuild levelset at -epsilon
            Grid<d> grid=levelset.phi.grid;
            levelset.phi.Exec_Nodes(
                [&](const VectorDi cell) {
                    // if(color(cell)==1){
                    if(levelset.phi(cell)<-0.5*epsilon){
                        
                        real min_dist=abs(levelset.phi(cell));
                        VectorD cx=grid.Position(cell);
                        for (int i = 0; i < particles.Size(); i++) {
                            VectorD px=particles.x(i);
                            min_dist=min(min_dist,(cx-px).norm());
                        }
                        levelset.phi(cell)=-min_dist+abs(epsilon);
                    }else{
                        levelset.phi(cell)=levelset.phi(cell)+abs(epsilon);
                    }
                }
            );
            levelset.Fast_Marching(-1);
            levelset.phi.Exec_Nodes(
                [&](const VectorDi cell) {
                    levelset.phi(cell)-=abs(epsilon);
                }
            );
        }

        void Enforce_Redistribution(real dt, EulerianParticles<d>& particles){
            // Redistribution<d> redistribution(local_geom);
            // TangentRedistribution<d> redistribution(local_geom);
            // redistribution.Redistribute_Particles(dt, particles);
        }

        //// advect vls
        void Enforce_Explicit_Surface_Tension(real dt, FaceField<real,d>& velocity, LevelSet<d>& levelset){
            // SurfaceTension::Explicit_Surface_Tension(dt,velocity,levelset,1.0, levelset.phi.grid.dx*5, levelset.phi.grid.dx*3);
            SurfaceTension::Explicit_Mean_Curvature(dt,velocity,levelset,2.0, levelset.phi.grid.dx*5, levelset.phi.grid.dx*3);
        }


        //// reseed vls
        void Reseed_Particles(EulerianParticles<d>& particles, LevelSet<d>& levelset){
            
            Array<VectorD> temp_x;
            LevelSetUtil<d>::Sample_On_Levelset_Surface(temp_x, levelset, 40);
            particles.Resize(temp_x.size());
            for(int i=0;i<temp_x.size();i++){
                particles.x(i)=temp_x[i];
                particles.v(i)=VectorD::Zero();
            }
            
        }

    };
}