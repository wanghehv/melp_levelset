//////////////////////////////////////////////////////////////////////////
// Initializer of FluidPLS
// Copyright (c) (2022-), Hui Wang
// This file is part of MESO, whose distribution is governed by the LICENSE file.
//////////////////////////////////////////////////////////////////////////
#pragma once
#include "FluidVLS.h"
#include "ImplicitManifold.h"
#include "Json.h"
#include "GridEulerFunc.h"

namespace Meso {
    template<int d>
    class FluidVLSInitializer {
    public:
        Typedef_VectorD(d);
        Typedef_MatrixD(d);

        void Apply(json& j, FluidVLS<d>& fluid) {
            std::string test = Json::Value(j, "test", std::string("single_vortex"));
            //// init levelset and
            if (test == "single_vortex") Init_Single_Vortex(j, fluid);
            else if (test == "three_joint") Init_Three_Joint(j, fluid);
            else if (test == "two_squares") Init_Two_Squares(j, fluid);
            else if (test == "three_squares") Init_Three_Squares(j, fluid);
            else Assert(false, "test {} not exist", test);

            //// sample particles
            fluid.ReseedVLS();



            //// neighbor searcher
            std::shared_ptr<NeighborSearcher<d>> nbs_searcher=std::make_shared<NeighborKDTree<d> >();
            std::shared_ptr<PointLocalGeometry<d>> local_geom=std::make_shared<PointLocalGeometry<d> > (fluid.velocity.grid.dx, 2/1.5, nbs_searcher);
            fluid.nbs_searcher=nbs_searcher;
            fluid.local_geom=local_geom;

            //// update particle normal
            for (auto itr = fluid.vls.e_levelset_map.begin(); itr != fluid.vls.e_levelset_map.end(); ++itr) {
                nbs_searcher->Update_Points(itr->second.particles.xRef());
                #pragma omp parallel for
                for(int i=0;i<itr->second.particles.Size();i++){
                    VectorD pos=itr->second.particles.x(i);
                    MatrixD local_frame=local_geom->Local_Frame(pos);
                    VectorD normal=local_frame.col(d-1);
                    if(normal.dot(itr->second.levelset.Normal(pos))<0)
                        normal=-normal;
                    itr->second.particles.n(i)=normal;
                }
            }
            
            //// other params
            fluid.Init(j);
        }


        void Init_Single_Vortex(json& j, FluidVLS<d>& fluid) {
            //// ref to Enright, "A hybrid particle level set method for improved interface capturing..." Sec 3.2
            //// sphere at (50,75,50) with radiu r=15, in a 100x100x100 domain
            //// slot width 5, deep 12.5
            if constexpr(d==3){
                Error("No single vortex test for 3D");
            }
            int scale = Json::Value<int>(j, "scale", 32);
            real side_len = 1.0;
            real dx = side_len / scale;
            VectorDi grid_size = scale * MathFunc::Vi<d>(1, 1, 1);
            Grid<d> grid(grid_size, dx, VectorD::Zero(), CENTER);

            //// init levelset
            fluid.vls.Init(grid,grid.dx*2);
            
            //// init epsilon levelsets
            VectorD x=grid.Center();
            {
                x[1]=side_len*0.75;
                Sphere<d> sphere(x, side_len*0.15);
                int id=fluid.vls.AddPhase();
                auto itr=fluid.vls.e_levelset_map.find(id);
                itr->second.levelset.Init(grid, sphere);
                Info("init levelset for {}", id);
            }
            {
                x[1]=side_len*0.25;
                Sphere<d> sphere(x, side_len*0.15);
                int id=fluid.vls.AddPhase();
                auto itr=fluid.vls.e_levelset_map.find(id);
                itr->second.levelset.Init(grid, sphere);
                Info("init levelset for {}", id);
            }
            //// TODO: correct levelset and generate particles
            //// require a external region, otherwise the Move_Particles_To_Voronoi_Levelset would report error
            {
                int id=fluid.vls.AddPhase();
                auto itr=fluid.vls.e_levelset_map.find(id);
                itr->second.levelset.Init(grid);
                itr->second.levelset.phi.Exec_Nodes(
                    [&](const VectorDi cell) {
                        real phi=1e8;
                        for (auto jtr = fluid.vls.e_levelset_map.begin(); jtr != fluid.vls.e_levelset_map.end(); ++jtr) {
                            if(jtr->first!=itr->first)
                                phi=min(jtr->second.levelset.phi(cell),phi);
                        }
                        itr->second.levelset.phi(cell)=-phi;
                    }
                );
                
                Info("init levelset for {}", id);
            }

            
            //// init velocity field
            //// stream function \Psi=1/pi * sin^2(pi*x) * sin^2(pi*y)
            //// u_x=-2*cos(pi*y)*sin(pi*y)*sin^2(pi*x)
            //// u_y=2*cos(pi*x)*sin(pi*x)*sin^2(pi*y)
            fluid.velocity.Init(grid,0);
            fluid.velocity.Calc_Faces(
                [&](const int axis, const VectorDi face) {
                    VectorD pos = grid.Face_Center(axis, face);
                    //// u in x-axis
                    if(axis==0)
                        return  2*cos(CommonConstants::pi*pos(1))*sin(CommonConstants::pi*pos(1)) * pow(sin(CommonConstants::pi*pos(0)),2);
                    //// v in y-axis
                    if(axis==1)
                        return  -2*cos(CommonConstants::pi*pos(0))*sin(CommonConstants::pi*pos(0)) * pow(sin(CommonConstants::pi*pos(1)),2);
                    return 0.0;
                }
            );
            Info("init velocity");
        }

        void Init_Three_Joint(json& j, FluidVLS<d>& fluid) {
            if constexpr(d==3){
                Error("No single vortex test for 3D");
            }
            int scale = Json::Value<int>(j, "scale", 32);
            real side_len = 1.0;
            real dx = side_len / scale;
            VectorDi grid_size = scale * MathFunc::Vi<d>(1, 1, 1);
            Grid<d> grid(grid_size, dx, VectorD::Zero(), CENTER);

            //// init levelset
            fluid.vls.Init(grid,grid.dx*2);
            
            //// init epsilon levelsets
            VectorD x=grid.Center();
            {
                Plane<d> plane_left(grid.Center(), -VectorD::Unit(0));
                Plane<d> plane_lower(grid.Center(), (-VectorD::Unit(0)+VectorD::Unit(1)).normalized());
                int id=fluid.vls.AddPhase();
                auto itr=fluid.vls.e_levelset_map.find(id);
                itr->second.levelset.Init(grid);
                itr->second.levelset.phi.Exec_Nodes(
                    [&](const VectorDi cell) {
                        VectorD pos=grid.Position(cell);
                        itr->second.levelset.phi(cell)=-min(plane_left.Phi(pos),plane_lower.Phi(pos));
                    }
                );
                Info("init levelset for {}", id);
            }
            {
                Plane<d> plane_right(grid.Center(), VectorD::Unit(0));
                Plane<d> plane_lower(grid.Center(), (VectorD::Unit(0)+VectorD::Unit(1)).normalized());
                int id=fluid.vls.AddPhase();
                auto itr=fluid.vls.e_levelset_map.find(id);
                itr->second.levelset.Init(grid);
                itr->second.levelset.phi.Exec_Nodes(
                    [&](const VectorDi cell) {
                        VectorD pos=grid.Position(cell);
                        itr->second.levelset.phi(cell)=-min(plane_right.Phi(pos),plane_lower.Phi(pos));
                    }
                );
                Info("init levelset for {}", id);
            }
            //// require a external region, otherwise the Move_Particles_To_Voronoi_Levelset would report error
            {
                int id=fluid.vls.AddPhase();
                auto itr=fluid.vls.e_levelset_map.find(id);
                itr->second.levelset.Init(grid);
                itr->second.levelset.phi.Exec_Nodes(
                    [&](const VectorDi cell) {
                        real phi=1e8;
                        for (auto jtr = fluid.vls.e_levelset_map.begin(); jtr != fluid.vls.e_levelset_map.end(); ++jtr) {
                            if(jtr->first!=itr->first)
                                phi=min(jtr->second.levelset.phi(cell),phi);
                        }
                        itr->second.levelset.phi(cell)=-phi;
                    }
                );
                itr->second.levelset.Fast_Marching(-1);
                
                Info("init levelset for {}", id);
            }

            fluid.velocity.Init(grid,0);
            fluid.velocity_fields.clear();
            for (auto itr = fluid.vls.e_levelset_map.begin(); itr != fluid.vls.e_levelset_map.end(); ++itr) {
                fluid.velocity_fields.push_back(FaceField<real,d>(grid));
            }
            Info("init velocity");

        }

        void Init_Two_Squares(json& j, FluidVLS<d>& fluid) {
            
            int scale = Json::Value<int>(j, "scale", 32);
            real side_len = 1.0;
            real dx = side_len / scale;
            VectorDi grid_size = scale * MathFunc::Vi<d>(1, 1, 1);
            Grid<d> grid(grid_size, dx, VectorD::Zero(), CENTER);

            //// init levelset
            fluid.vls.Init(grid,grid.dx*2);
            
            //// init two squares
            {
                VectorD min_corner, max_corner;
                if constexpr(d==2){
                    min_corner=VectorD(0.5-0.15,0.5-0.3)*side_len;
                    max_corner=VectorD(0.5+0.15,0.5)*side_len;
                }
                if constexpr(d==3){
                    min_corner=VectorD(0.5-0.15,0.5-0.3,0.5-0.15)*side_len;
                    max_corner=VectorD(0.5+0.15,0.5,0.5+0.15)*side_len;
                }
                Box<d> box(min_corner,max_corner);

                int id=fluid.vls.AddPhase();
                auto itr=fluid.vls.e_levelset_map.find(id);
                itr->second.levelset.Init(grid,box);
                Info("init levelset for {}", id);
            }
            {
                VectorD min_corner, max_corner;
                if constexpr(d==2){
                    min_corner=VectorD(0.5-0.15,0.5)*side_len;
                    max_corner=VectorD(0.5+0.15,0.5+0.3)*side_len;
                }
                if constexpr(d==3){
                    min_corner=VectorD(0.5-0.15,0.5,0.5-0.15)*side_len;
                    max_corner=VectorD(0.5+0.15,0.5+0.3,0.5+0.15)*side_len;
                }
                Box<d> box(min_corner,max_corner);

                int id=fluid.vls.AddPhase();
                auto itr=fluid.vls.e_levelset_map.find(id);
                itr->second.levelset.Init(grid,box);
                Info("init levelset for {}", id);
            }
            //// require a external region, otherwise the Move_Particles_To_Voronoi_Levelset would report error
            {
                int id=fluid.vls.AddPhase();
                auto itr=fluid.vls.e_levelset_map.find(id);
                itr->second.levelset.Init(grid);
                itr->second.levelset.phi.Exec_Nodes(
                    [&](const VectorDi cell) {
                        real phi=1e8;
                        for (auto jtr = fluid.vls.e_levelset_map.begin(); jtr != fluid.vls.e_levelset_map.end(); ++jtr) {
                            if(jtr->first!=itr->first)
                                phi=min(jtr->second.levelset.phi(cell),phi);
                        }
                        itr->second.levelset.phi(cell)=-phi;
                    }
                );
                itr->second.levelset.Fast_Marching(-1);
                
                Info("init levelset for {}", id);
            }

            fluid.velocity.Init(grid,0);
            fluid.velocity_fields.clear();
            for (auto itr = fluid.vls.e_levelset_map.begin(); itr != fluid.vls.e_levelset_map.end(); ++itr) {
                fluid.velocity_fields.push_back(FaceField<real,d>(grid));
            }
            Info("init velocity");

        }

        void Init_Three_Squares(json& j, FluidVLS<d>& fluid) {
            
            int scale = Json::Value<int>(j, "scale", 32);
            real side_len = 1.0;
            real dx = side_len / scale;
            VectorDi grid_size = scale * MathFunc::Vi<d>(1, 1, 1);
            Grid<d> grid(grid_size, dx, VectorD::Zero(), CENTER);

            //// init levelset
            fluid.vls.Init(grid,grid.dx*2);
            
            //// init two squares
            Array<VectorD> min_corners,max_corners;
            min_corners.resize(3);
            max_corners.resize(3);
            if constexpr(d==2){
                min_corners[0]=VectorD(0.5-0.3 ,0.5)*side_len;////left
                max_corners[0]=VectorD(0.5     ,0.5+0.3)*side_len;////left
                min_corners[1]=VectorD(0.5     ,0.5)*side_len;////right
                max_corners[1]=VectorD(0.5+0.3 ,0.5+0.3)*side_len;////right
                min_corners[2]=VectorD(0.5-0.15,0.5-0.3)*side_len;
                max_corners[2]=VectorD(0.5+0.15,0.5)*side_len;
            }
            if constexpr(d==3){
                min_corners[0]=VectorD(0.5-0.3 ,0.5     ,0.5)*side_len;////left
                max_corners[0]=VectorD(0.5     ,0.5+0.3 ,0.5+0.3)*side_len;////left
                min_corners[1]=VectorD(0.5     ,0.5     ,0.5)*side_len;////right
                max_corners[1]=VectorD(0.5+0.3 ,0.5+0.3 ,0.5+0.3)*side_len;////right
                min_corners[2]=VectorD(0.5-0.15,0.5-0.3 ,0.5)*side_len;
                max_corners[2]=VectorD(0.5+0.15,0.5     ,0.5+0.3)*side_len;
            }

            for(int i=0;i<min_corners.size();i++){
                Box<d> box(min_corners[i],max_corners[i]);

                int id=fluid.vls.AddPhase();
                auto itr=fluid.vls.e_levelset_map.find(id);
                itr->second.levelset.Init(grid,box);
                Info("init levelset for {}", id);
            }
            //// require a external region, otherwise the Move_Particles_To_Voronoi_Levelset would report error
            {
                int id=fluid.vls.AddPhase();
                auto itr=fluid.vls.e_levelset_map.find(id);
                itr->second.levelset.Init(grid);
                itr->second.levelset.phi.Exec_Nodes(
                    [&](const VectorDi cell) {
                        real phi=1e8;
                        for (auto jtr = fluid.vls.e_levelset_map.begin(); jtr != fluid.vls.e_levelset_map.end(); ++jtr) {
                            if(jtr->first!=itr->first)
                                phi=min(jtr->second.levelset.phi(cell),phi);
                        }
                        itr->second.levelset.phi(cell)=-phi;
                    }
                );
                itr->second.levelset.Fast_Marching(-1);
                
                Info("init levelset for {}", id);
            }

            fluid.velocity.Init(grid,0);
            fluid.velocity_fields.clear();
            for (auto itr = fluid.vls.e_levelset_map.begin(); itr != fluid.vls.e_levelset_map.end(); ++itr) {
                fluid.velocity_fields.push_back(FaceField<real,d>(grid));
            }
            Info("init velocity");

        }

    };
}